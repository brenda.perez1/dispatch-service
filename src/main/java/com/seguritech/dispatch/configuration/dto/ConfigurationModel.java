package com.seguritech.dispatch.configuration.dto;

import java.time.LocalDateTime;

import com.seguritech.dispatch.share.BaseModel;

import lombok.Data;


@Data
public class ConfigurationModel extends BaseModel {

//    private Long id;
	private String key;
    private String description;
    private String value;
	private Boolean active;
	private String createdBy;
	private LocalDateTime createdDate;
	private String lastModifiedBy;
	
}
