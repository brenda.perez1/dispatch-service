package com.seguritech.dispatch.configuration.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.domain.model.configuration.entity.Configuration;


@Repository
public interface ConfigurationRepository extends JpaRepository<Configuration, Long> {

//	List<Configuration> findByActive(Boolean active);
	
	@Query("FROM Configuration g WHERE g.description LIKE %:word% OR g.key LIKE %:word%")
    Page<Configuration> findByFilter(@Param("word") String word, Pageable paging);

}
