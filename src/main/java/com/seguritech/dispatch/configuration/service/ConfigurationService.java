package com.seguritech.dispatch.configuration.service;

import java.util.List;

import com.domain.model.PaginatedResultResponse;
import com.domain.model.configuration.entity.Configuration;
import com.seguritech.dispatch.configuration.dto.ConfigurationModel;



public interface ConfigurationService {

   
    Configuration getById(Long id);

    /**
     * @return List<GeofenceResponse>
     */
    List<Configuration> retrieve();

    /**
     * @param page
     * @param size
     * @param word
     * @param sort
     * @param column
     * @return PaginatedResultResponse<?>
     */
    PaginatedResultResponse<List<ConfigurationModel>> retrieve(Integer page, Integer size, String word, String sort, String column);
    

}
