package com.seguritech.dispatch.configuration.service;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seguritech.dispatch.configuration.dto.ConfigurationModel;
import com.seguritech.dispatch.configuration.service.impl.ConfigurationServiceImpl;
import com.seguritech.dispatch.share.BaseModel;
import com.seguritech.dispatch.share.BaseObjectManager;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConfigurationManager extends BaseObjectManager<ConfigurationModel> {

//	private final GeofenceServiceImpl geofenceService;
//	private List<ConfigurationModel> configuration;
	private Map<String, ConfigurationModel> items;
	private Collection<ConfigurationModel> databaseItems;
	
	@Autowired
	public ConfigurationManager(ConfigurationServiceImpl configurationService) {
        super(configurationService,ConfigurationModel.class);
//		this.geofenceService = geofenceService;
	}

	@PostConstruct
	private void postConstruct() {
		
		this.refreshItems();

//		for (Map.Entry<String,ConfigurationModel> entry : items.entrySet())
//			log.info("Key = " + entry.getKey() +
//                             ", Value = " + ReflectionToStringBuilder.toString(entry.getValue(), ToStringStyle.MULTI_LINE_STYLE));

	}
	
	
	 public void refreshItems() {
	    	
	    	if (getDataManager() != null) {
	            try {
	                writeLock();
	                databaseItems = getDataManager().getObjects();
//	                resolveMethod();
	                if (items == null) {
	                    items = new ConcurrentHashMap<>(databaseItems.size());
	                }
	                Set<String> databaseItemIds = new HashSet<>();
	                for (ConfigurationModel item : databaseItems) {
	                    databaseItemIds.add(item.getKey());
	                    if (items.containsKey(item.getKey())) {
	                        updateCachedItem(item);
	                    } else {
	                        addNewItem(item);
	                    }
	                }
	                for (String cachedItemId : items.keySet()) {
	                    if (!databaseItemIds.contains(cachedItemId)) {
	                        removeCachedItem(cachedItemId);
	                    }
	                }
	            } catch (Exception error) {
	                log.warn("Error refreshing items", error);
	            } finally {
	                writeUnlock();
	            }
	        }
	    	
	    }

	    protected void addNewItem(ConfigurationModel item) {
	        try {
	            writeLock();
	            items.put(item.getKey(), item);
	        } finally {
	            writeUnlock();
	        }
	    }

	    public void addItem(ConfigurationModel item) throws SQLException {
//	        dataManager.addObject(item);
	        addNewItem(item);
	    }

	    protected void updateCachedItem(ConfigurationModel item) {
	        try {
	            writeLock();
	            items.put(item.getKey(), item);
	        } finally {
	            writeUnlock();
	        }
	    }

	    public void updateItem(ConfigurationModel item) throws SQLException {
//	        dataManager.updateObject(item);
	        updateCachedItem(item);
	    }

	    protected void removeCachedItem(String itemId) {
	        try {
	            writeLock();
	            items.remove(itemId);
	        } finally {
	            writeUnlock();
	        }
	    }

	    public void removeItem(long itemId) throws SQLException {
	        BaseModel item = getById(itemId);
	        if (item != null) {
//	            dataManager.removeObject(baseClass, itemId);
	            removeCachedItem(itemId);
	        }
	    }

}
