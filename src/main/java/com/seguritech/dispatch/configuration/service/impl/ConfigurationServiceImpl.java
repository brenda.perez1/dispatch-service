package com.seguritech.dispatch.configuration.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.PaginatedResultResponse;
import com.domain.model.configuration.entity.Configuration;
import com.seguritech.dispatch.configuration.dto.ConfigurationModel;
import com.seguritech.dispatch.configuration.repository.ConfigurationRepository;
import com.seguritech.dispatch.configuration.service.ConfigurationService;
import com.seguritech.dispatch.exception.NoDataFoundException;
import com.seguritech.dispatch.share.DataManager;
import com.seguritech.dispatch.share.MessageResolver;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class ConfigurationServiceImpl implements ConfigurationService, DataManager<ConfigurationModel> {
	
	@Autowired
	private final ConfigurationRepository repository;

	@Override
	public List<ConfigurationModel> getObjects() {
		log.info("Get all objects");
		return this.repository.findAll().stream().map(this::toDTO).collect(Collectors.toList());
	}

	@Override
	public Configuration getById(Long id) {
		log.info("Get record by Id");
		return this.repository.findById(id).<NoDataFoundException>orElseThrow(() -> {
			throw new NoDataFoundException(MessageResolver.RECORD_NOT_FOUND_EXCEPTION);
		});
	}

	@Override
	public List<Configuration> retrieve() {
		log.info("Get all records");
		return this.repository.findAll().stream().collect(Collectors.toList());
	}

	@Override
	public PaginatedResultResponse<List<ConfigurationModel>> retrieve(Integer page, Integer size, String word, String sort,
			String column) {
		log.info("Get record query and search by filter");
		Pageable paging = PageRequest.of(page - 1, size, Sort.by(sort.toUpperCase().equals("DESC") ? Sort.Direction.DESC : Sort.Direction.ASC, column));
		Page<Configuration> response = this.repository.findByFilter(word.toUpperCase(), paging);
		List<ConfigurationModel> result = response.getContent().stream().map(this::toDTO).collect(Collectors.toList());
		return new PaginatedResultResponse<>(result, response.getTotalElements());
	}
	
	ConfigurationModel toDTO(Configuration entity) {
		return new ModelMapper().map(entity, ConfigurationModel.class);
	}

}
