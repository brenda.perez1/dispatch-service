package com.seguritech.dispatch.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.reactive.ServerHttpRequest;

import com.seguritech.dispatch.auth.AppUserPrincipal;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {


    public static final String AUTHORIZATION_HEADER = "Authorization";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).ignoredParameterTypes(ServerHttpRequest.class,
            AppUserPrincipal.class)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.seguritech"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(apiInfo())
            .securityContexts(Collections.singletonList(securityContext()))
            .securitySchemes(Collections.singletonList(apiKey()))
            .pathMapping(ServletPathCheck());
    }

    @Value("${spring.profiles.active}")
    private String enviroment;

    @Value("${git.branch}")
    private String branchName;

    @Value("${git.commit.id}")
    private String commit;

    @Value("${git.commit.message.short}")
    private String messageLastCommit;

    @Value("${git.tags}")
    private String tagVersion;

    @Value("${git.commit.user.email}")
    private String lastCommitEmail;

    @Value("${git.commit.user.name}")
    private String lastCommitUserName;

    @Value("${server.path:#{null}}")
    private String servletPath;

    @Bean
    public ApiInfo apiInfo() {
        String tagVersionOrCommit = tagVersion==null || tagVersion.trim().isEmpty() ? commit : tagVersion;
        String lastCommit = String.format("%s - %s", lastCommitEmail, lastCommitUserName);
        return new ApiInfoBuilder().title("Dispatch API")
            .description(String.format("Dispatch API core\n" +
                    "Branch: %s\n" +
                    "Commit: %s\n" +
                    "Message: %s\n" +
                    "Author: %s\n", branchName, commit, messageLastCommit, lastCommit))
            .contact(new Contact("Seguritech", "seguritech@seguritech.com", "seguritech@seguritech.com"))
            .version(String.format("%s - %s", enviroment, tagVersionOrCommit))
            .build();
    }


    private ApiKey apiKey() {
        return new ApiKey("Bearer JWT", AUTHORIZATION_HEADER , "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
            .securityReferences(defaultAuth())
            .forPaths(PathSelectors.any())
            .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("Bearer JWT", authorizationScopes));
    }

    String ServletPathCheck() {
        if(servletPath != null && !servletPath.isEmpty())
            return servletPath;
        
        return "";
    }

}
