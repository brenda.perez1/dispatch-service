package com.seguritech.dispatch.config;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.seguritech.platform.domain.DomainEventPublisher;
import com.seguritech.platform.domain.RootAggregate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TransactionAfterCommitEventListener {
	
	private final DomainEventPublisher publisher;

	public TransactionAfterCommitEventListener(DomainEventPublisher publisher) {
		this.publisher = publisher;
	}

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void handleTransactionAfterCommit(List<Object> events) {
    	
    	events.forEach(ev -> {
    		try {
        		this.publisher.publish(((RootAggregate) ev).pullEvents());	
			} catch (Exception e) {
				log.error("Error publishing event: "+ ev + " - " + e.getMessage());
			}
    	});

    }

}
