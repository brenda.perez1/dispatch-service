package com.seguritech.dispatch.share;

import java.util.List;

public interface DataManager<T> {
	
	
	public List<T> getObjects();

}
