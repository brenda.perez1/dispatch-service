package com.seguritech.dispatch.share;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseObjectManager<T extends BaseModel> {
	

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseObjectManager.class);

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private final DataManager dataManager;

    private final Class<T> baseClass;
    private Map<Long, T> items;
    Collection<T> databaseItems;

    protected BaseObjectManager(
    		DataManager dataManager,
    		Class<T> baseClass) {
        this.dataManager = dataManager;
        this.baseClass = baseClass;
    }

    protected final void readLock() {
        lock.readLock().lock();
    }

    protected final void readUnlock() {
        lock.readLock().unlock();
    }

    protected final void writeLock() {
        lock.writeLock().lock();
    }

    protected final void writeUnlock() {
        lock.writeLock().unlock();
    }

//    protected final DataManager getDataManager() {
//        return dataManager;
//    }
//
//    protected final Class<T> getBaseClass() {
//        return baseClass;
//    }

    public T getById(long itemId) {
        try {
            readLock();
            return items.get(itemId);
        } finally {
            readUnlock();
        }
    }

//    public void loadItems() {
//        if (dataManager != null) {
//            try {
//                databaseItems = dataManager.getObjects();
//                resolveMethod();
//            } catch (Exception error) {
//                LOGGER.warn("Error refreshing items", error);
//            } finally {
//            }
//        }
//    }
    
    public void refreshItems() {
    	
    	if (dataManager != null) {
            try {
                writeLock();
                databaseItems = dataManager.getObjects();
                resolveMethod();
                if (items == null) {
                    items = new ConcurrentHashMap<>(databaseItems.size());
                }
                Set<Long> databaseItemIds = new HashSet<>();
                for (T item : databaseItems) {
                    databaseItemIds.add(item.getId());
                    if (items.containsKey(item.getId())) {
                        updateCachedItem(item);
                    } else {
                        addNewItem(item);
                    }
                }
                for (Long cachedItemId : items.keySet()) {
                    if (!databaseItemIds.contains(cachedItemId)) {
                        removeCachedItem(cachedItemId);
                    }
                }
            } catch (Exception error) {
                LOGGER.warn("Error refreshing items", error);
            } finally {
                writeUnlock();
            }
        }
    	
    }

    protected void addNewItem(T item) {
        try {
            writeLock();
            items.put(item.getId(), item);
        } finally {
            writeUnlock();
        }
    }

    public void addItem(T item) throws SQLException {
//        dataManager.addObject(item);
        addNewItem(item);
    }

    protected void updateCachedItem(T item) {
        try {
            writeLock();
            items.put(item.getId(), item);
        } finally {
            writeUnlock();
        }
    }

    public void updateItem(T item) throws SQLException {
//        dataManager.updateObject(item);
        updateCachedItem(item);
    }

    protected void removeCachedItem(long itemId) {
        try {
            writeLock();
            items.remove(itemId);
        } finally {
            writeUnlock();
        }
    }

    public void removeItem(long itemId) throws SQLException {
        BaseModel item = getById(itemId);
        if (item != null) {
//            dataManager.removeObject(baseClass, itemId);
            removeCachedItem(itemId);
        }
    }

    public final Collection<T> getItems(Set<Long> itemIds) {
//    	refreshItems(); // Para siempre traer los items de la BD
        Collection<T> result = new LinkedList<>();
        for (long itemId : itemIds) {
            T item = getById(itemId);
            if (item != null) {
                result.add(item);
            }
        }
        return result;
    }

    public Set<Long> getAllItems() {
        try {
//        	refreshItems(); // Para siempre traer los items de la BD
            readLock();
            return items.keySet();
        } finally {
            readUnlock();
        }
    }
    
    public void resolveMethod() {

            List<ResultSetProcessor<T>> processors = new LinkedList<>();

            Method[] methods = baseClass.getMethods();

            for (final Method method : methods) {
                if (method.getName().startsWith("set") && method.getParameterTypes().length == 0
                        && method.isAnnotationPresent(InvokeMethod.class)) {

                    final String name = method.getName().substring(3);                   
                    addProcessors(processors, null, method, name);
                }
            }
            
            if(databaseItems != null) {
	            Iterator<T> iterator = databaseItems.iterator();
	            // while loop
	            while (iterator.hasNext()) {
	            	
	                    T object = iterator.next();
	                    for (ResultSetProcessor<T> processor : processors) {
	                        processor.process(object);
	                    }   
	            	
	            }
            }
	            
	            
            
    }

            
       
    private interface ResultSetProcessor<T> {
        void process(T object);
    }
    
    
    private <T> void addProcessors(
            List<ResultSetProcessor<T>> processors,
            final Class<?> parameterType, final Method method, final String name) {

//        if (parameterType.equals(boolean.class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    method.invoke(object, resultSet.getBoolean(name));
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else if (parameterType.equals(int.class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    method.invoke(object, resultSet.getInt(name));
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else if (parameterType.equals(long.class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    method.invoke(object, resultSet.getLong(name));
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else if (parameterType.equals(double.class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    method.invoke(object, resultSet.getDouble(name));
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else if (parameterType.equals(String.class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    method.invoke(object, resultSet.getString(name));
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else if (parameterType.equals(Date.class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    Timestamp timestamp = resultSet.getTimestamp(name);
//                    if (timestamp != null) {
//                        method.invoke(object, new Date(timestamp.getTime()));
//                    }
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else if (parameterType.equals(byte[].class)) {
//            processors.add((object, resultSet) -> {
//                try {
//                    method.invoke(object, resultSet.getBytes(name));
//                } catch (IllegalAccessException | InvocationTargetException error) {
//                    LOGGER.warn("Set property error", error);
//                }
//            });
//        } else {
//            processors.add((object, resultSet) -> {
//                String value = resultSet.getString(name);
//                if (value != null && !value.isEmpty()) {
//                    try {
//                        method.invoke(object, Context.getObjectMapper().readValue(value, parameterType));
//                    } catch (InvocationTargetException | IllegalAccessException | IOException error) {
//                        LOGGER.warn("Set property error", error);
//                    }
//                }
//            });
//        }
    	
    	
    	processors.add((object) -> {
//          String value = resultSet.getString(name);
//          if (value != null && !value.isEmpty()) {
              try {
                  method.invoke(object);
              } catch (InvocationTargetException | IllegalAccessException  error) {
                  LOGGER.warn("Set property error", error);
              }
//          }
      });
    }

	public Map<Long, T> getItems() {
		return items;
	}

	public void setItems(Map<Long, T> items) {
		this.items = items;
	}

	public Collection<T> getDatabaseItems() {
		return databaseItems;
	}

	public void setDatabaseItems(Collection<T> databaseItems) {
		this.databaseItems = databaseItems;
	}

	public DataManager getDataManager() {
		return dataManager;
	}
    
    


}
