package com.seguritech.dispatch.share;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class MessageResolver {
	
	public static String RECORD_NOT_FOUND_EXCEPTION;
	public static String CORPORATION_NOT_FOUND_EXCEPTION;
	public static String CORPORATION_REQUERID_EXCEPTION;
	public static String GEOFENCE_ID_REQUERID_EXCEPTION;
	public static String HOST_IP_REQUERID_EXCEPTION;
	public static String CORPORATION_FOLLOWUP_OBSERVATION_INVALID_EXCEPTION;
	public static String CORPORATION_FOLLOWUP_RESEARCH_FOLDER_INVALID_EXCEPTION;
	public static String INCIDENT_CORPORATION_ASSOCIATED_EXCEPTION;
	public static String INCIDENT_CORPORATION_CODE_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_CORPORATION_CODE_REQUIRED_EXCEPTION;
	public static String INCIDENT_CORPORATION_FOLLOWUP_REASON_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_CORPORATION_FOLLOWUP_REASON_REQUIRED_ID_EXCEPTION;
	public static String INCIDENT_CORPORATION_FOLLOWUP_STATUS_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_CORPORATION_FOLLOWUP_STATUS_REQUIRED_ID_EXCEPTION;
	public static String INCIDENT_CORPORATION_NOT_CLOSED_EXCEPTION;
	public static String INCIDENT_CORPORATION_NOT_ASSIGNED_EXCEPTION;
	public static String INCIDENT_CORPORATION_NOT_DISSOCIATED_EXCEPTION;
	public static String INCIDENT_CORPORATION_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_CORPORATION_NOT_OPEN_EXCEPTION;
	public static String INCIDENT_CORPORATION_OBSERVATION_INVALID_EXCEPTION;
	public static String INCIDENT_CORPORATION_REASON_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_CORPORATION_REASON_REQUERID_EXCEPTION;
	public static String INCIDENT_CORPORATION_REASON_CLOSE_REQUERID_EXCEPTION;
	public static String INCIDENT_CORPORATIONS_REQUERID_EXCEPTION;
	public static String INCIDENT_CORPORATION_STATUS_INVALID_EXCEPTION;
	public static String INCIDENT_CORPORATION_STATUS_REQUERID_EXCEPTION;
	public static String INCIDENT_CORPORATION_UNIT_NOT_CANCEL_EXCEPTION;
	public static String INCIDENT_ID_REQUERID_EXCEPTION;
	public static String INCIDENT_FOLIO_REQUERID_EXCEPTION;
	public static String INCIDENT_LOCATION_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_LOCATION_NOT_FOUND_EXCEPTION_2;
	public static String INCIDENT_LOCATION_REQUERID_EXCEPTION;
	public static String INCIDENT_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_NOT_FOUND_EXCEPTION_2;
	public static String INCIDENT_NOT_OPEN_EXCEPTION;
	public static String INCIDENT_PRIORITY_REQUERID_EXCEPTION;
	public static String INCIDENT_TYPE_REQUERID_EXCEPTION;
	public static String INCIDENT_TYPE_NOT_ASSIGNABLE_EXCEPTION;
	public static String INCIDENT_UNIT_FORCE_REQUERID_EXCEPTION;
	public static String INCIDENT_WITH_FORCE_UNIT_ASSIGNED_EXCEPTION;
	public static String INVOICE_REQUERID_EXCEPTION;
	public static String MANUAL_CORPORATION_ASSIGNMENT_NOT_ENABLED_EXCEPTION;
	public static String OPERATIVE_GROUP_REQUERID_EXCEPTION;
	public static String PROFILE_ID_REQUERID_EXCEPTION;
	public static String USER_REQUERID_EXCEPTION;
	public static String SOURCE_REQUERID_EXCEPTION;
	public static String INCIDENT_GEOFENCE_NOT_FOUND_EXCEPTION;
	
	public static String CHANGE_STAGE_EXCEPTION;
	public static String CORPORATION_NOT_FOUND_EXCEPTION_2;
	public static String CORPORATION_REQUERID_EXCEPTION_2;
	public static String EMERGENCY_ID_REQUERID_EXCEPTION;
	public static String INCIDENT_CORPORATION_EXCEPTION;
	public static String INCIDENT_IS_NOT_ASSIGNED_TO_THE_CORPORATION_EXCEPTION;
	public static String INCIDENT_IS_NOT_ASSIGNED_TO_THE_SECTOR_EXCEPTION;
	public static String INCIDENT_NOT_FOUND_EXCEPTION_3;
	public static String INCIDENT_SECTOR_NOT_FOUND_EXCEPTION;
	public static String INVALID_STAGE_TYPE_EXCEPTION;
	public static String LAST_PARENT_ID_REQUERID_EXCEPTION;
	public static String MISSION_ID_NOT_FOUND_EXCEPTION;
	public static String MISSION_ID_REQUERID_EXCEPTION;
	public static String MISSION_NOT_CONFIGURED_EXCEPTION;
	public static String MISSION_NOT_FOUND_EXCEPTION;
	public static String MISSION_STAGE_ID_REQUERID_EXCEPTION;
	public static String OBSERVATION_SIZE_EXCEPTION;
	public static String PARENT_ID_REQUERID_EXCEPTION;
	public static String PEOPLE_TRANSFER_REQUERID_EXCEPTION;
	public static String PERSONS_REQUERID_EXCEPTION;
	public static String REASON_CANCEL_ID_REQUERID_EXCEPTION;
	public static String REASON_CANCEL_NOT_FOUND_EXCEPTION;
	public static String SECTOR_NOT_FOUND_EXCEPTION;
	public static String SECTOR_REQUERID_EXCEPTION;
	public static String STAGE_NOT_CONFIGURED_EXCEPTION;
	public static String STAGE_PRECONFIGURED_NOT_FOUND_EXCEPTION;
	public static String STAGES_NOT_CONFIGURED_TO_MISSION_EXCEPTION;
	public static String STAGE_TRANSITION_ID_REQUERID_EXCEPTION;
	public static String STAGE_TRANSITION_NOT_FOUND_EXCEPTION;
	public static String STAGE_TYPE_NOT_SUPPORT_EXCEPTION;
	public static String STAGE_REQUIRED_EXCEPTION;
	public static String TRANSITION_STAGE_ID_REQUERID_EXCEPTION;
	public static String TRANSPORT_ID_REQUERID_EXCEPTION;
	public static String UNIT_CORPORATION_NOT_FOUND_EXCEPTION;
	public static String UNIT_FORCE_CONFIG_NOT_FOUND_EXCEPTION;
	public static String UNIT_FORCE_CONFIG_NOT_AVAILABLE_EXCEPTION;
	public static String UNIT_ID_REQUERID_EXCEPTION;
	public static String UNIT_MISSION_ID_REQUERID_EXCEPTION;
	public static String UNIT_MISSION_NOT_FOUND_EXCEPTION;
	public static String UNIT_MISSION_STATUS_ID_REQUERID_EXCEPTION;
	public static String UNIT_NOT_FOUND_EXCEPTION;
	public static String UNIT_SECTOR_NOT_FOUND_EXCEPTION;
	public static String USER_ID_REQUERID_EXCEPTION;
	public static String UNAVAILABILITY_REASON_REQUERID_EXCEPTION;
	public static String UNAVAILABILITY_REASON_NOT_FOUND_EXCEPTION;
	public static String CAT_ESTATUS_NOT_FOUND_EXCEPTION;
	public static String INCIDENT_CLOSE_EXCEPTION;
	public static String INCIDENT_CORPORATION_CLOSE_EXCEPTION;
	public static String INCIDENT_CORPORATION_EXIST_UNIT_MISSION_ASSING;
	
    @Value("${placeholder.record_not_found_exception}") 
    public void record_not_found_exception(String placeholder){
    	MessageResolver.RECORD_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.corporation_not_found_exception}")
    public void corporation_not_found_exception(String placeholder){
    	MessageResolver.CORPORATION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.corporation_requerid_exception}")
    public void corporation_requerid_exception(String placeholder){
    	MessageResolver.CORPORATION_REQUERID_EXCEPTION = placeholder;
    }
   
    @Value("${placeholder.geofence_id_requerid_exception}")
    public void geofence_id_requerid_exception(String placeholder){
    	MessageResolver.GEOFENCE_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.host_ip_requerid_exception}")
    public void host_ip_requerid_exception(String placeholder){
    	MessageResolver.HOST_IP_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.corporation_followup_observation_invalid_exception}")
    public void corporation_followup_observation_invalid_exception(String placeholder){
    	MessageResolver.CORPORATION_FOLLOWUP_OBSERVATION_INVALID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.corporation_followup_research_folder_invalid_exception}")
    public void corporation_followup_research_folder_invalid_exception(String placeholder){
    	MessageResolver.CORPORATION_FOLLOWUP_RESEARCH_FOLDER_INVALID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_associated_exception}")
    public void incident_corporation_associated_exception(String placeholder){
    	MessageResolver.INCIDENT_CORPORATION_ASSOCIATED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_code_not_found_exception}")
    public void incident_corporation_code_not_found_exception(String placeholder){
    	MessageResolver.INCIDENT_CORPORATION_CODE_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_code_required_exception}")
    public void incident_corporation_code_required_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_CODE_REQUIRED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_followup_reason_not_found_exception}")
    public void incident_corporation_followup_reason_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_REASON_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_followup_reason_required_exception}")
    public void incident_corporation_followup_reason_required_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_REASON_REQUIRED_ID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_followup_status_not_found_exception}")
    public void incident_corporation_followup_status_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_STATUS_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_followup_status_required_exception}")
    public void incident_corporation_followup_status_required_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_STATUS_REQUIRED_ID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_not_closed_exception}")
    public void incident_corporation_not_closed_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_NOT_CLOSED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_not_assigned_exception}")
    public void incident_corporation_not_assigned_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_NOT_ASSIGNED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_not_dissociated_exception}")
    public void incident_corporation_not_dissociated_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_NOT_DISSOCIATED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_not_found_exception}")
    public void incident_corporation_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_not_open_exception}")
    public void incident_corporation_not_open_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_NOT_OPEN_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_observation_invalid_exception}")
    public void incident_corporation_observation_invalid_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_OBSERVATION_INVALID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_reason_not_found_exception}")
    public void incident_corporation_reason_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_REASON_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_reason_requerid_exception}")
    public void incident_corporation_reason_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_REASON_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_reason_close_requerid_exception}")
    public void incident_corporation_reason_close_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_REASON_CLOSE_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporations_requerid_exception}")
    public void incident_corporations_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATIONS_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_status_invalid_exception}")
    public void incident_corporation_status_invalid_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_STATUS_INVALID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_status_requerid_exception}")
    public void incident_corporation_requerid_status_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_STATUS_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_unit_not_cancel_exception}")
    public void incident_corporation_unit_not_cancel_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_UNIT_NOT_CANCEL_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_id_requerid_exception}")
    public void incident_id_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_folio_requerid_exception}")
    public void incident_folio_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_FOLIO_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_location_not_found_exception}")
    public void incident_location_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_LOCATION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_location_not_found_exception_2}")
    public void incident_location_not_found_exception_2(String placeholder) {
    	MessageResolver.INCIDENT_LOCATION_NOT_FOUND_EXCEPTION_2 = placeholder;
    }
    
    @Value("${placeholder.incident_location_requerid_exception}")
    public void incident_location_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_LOCATION_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_not_found_exception}")
    public void incident_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_not_found_exception_2}")
    public void incident_not_found_exception_2(String placeholder) {
    	MessageResolver.INCIDENT_NOT_FOUND_EXCEPTION_2 = placeholder;
    }
    
    @Value("${placeholder.incident_not_open_exception}")
    public void incident_not_open_exception(String placeholder) {
    	MessageResolver.INCIDENT_NOT_OPEN_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_priority_requerid_exception}")
    public void incident_priority_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_PRIORITY_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_type_requerid_exception}")
    public void incident_type_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_TYPE_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_type_not_assignable_exception}")
    public void incident_type_not_assignable_exception(String placeholder) {
    	MessageResolver.INCIDENT_TYPE_NOT_ASSIGNABLE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_unit_force_requerid_exception}")
    public void incident_unit_force_requerid_exception(String placeholder) {
    	MessageResolver.INCIDENT_UNIT_FORCE_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_with_force_unit_assigned_exception}")
    public void incident_with_force_unit_assigned_exception(String placeholder) {
    	MessageResolver.INCIDENT_WITH_FORCE_UNIT_ASSIGNED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.invoice_requerid_exception}")
    public void invoice_requerid_exception(String placeholder) {
    	MessageResolver.INVOICE_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.manual_corporation_assignment_not_enabled_exception}")
    public void manual_corporation_assignment_not_enabled_exception(String placeholder) {
    	MessageResolver.MANUAL_CORPORATION_ASSIGNMENT_NOT_ENABLED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.operative_group_requerid_exception}")
    public void operative_group_requerid_exception(String placeholder) {
    	MessageResolver.OPERATIVE_GROUP_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.profile_id_requerid_exception}")
    public void profile_id_requerid_exception(String placeholder) {
    	MessageResolver.PROFILE_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.user_requerid_exception}")
    public void user_requerid_exception(String placeholder) {
    	MessageResolver.USER_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.source_requerid_exception}")
    public void source_requerid_exception(String placeholder) {
    	MessageResolver.SOURCE_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_geofence_not_found_exception}")
    public void incident_geofence_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_GEOFENCE_NOT_FOUND_EXCEPTION = placeholder;
    }
    //UNIT FORCE
    @Value("${placeholder.change_stage_exception}")
    public void change_stage_exception(String placeholder) {
    	MessageResolver.CHANGE_STAGE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.corporation_not_found_exception_2}")
    public void corporation_not_found_exception_2(String placeholder) {
    	MessageResolver.CORPORATION_NOT_FOUND_EXCEPTION_2 = placeholder;
    }
    
    @Value("${placeholder.corporation_requerid_exception_2}")
    public void corporation_requerid_exception_2(String placeholder) {
    	MessageResolver.CORPORATION_REQUERID_EXCEPTION_2 = placeholder;
    }
    
    @Value("${placeholder.emergency_id_requerid_exception}")
    public void emergency_id_requerid_exception(String placeholder) {
    	MessageResolver.EMERGENCY_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_exception}")
    public void incident_corporation_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_is_not_assigned_to_the_corporation_exception}")
    public void incident_is_not_assigned_to_the_corporation_exception(String placeholder) {
    	MessageResolver.INCIDENT_IS_NOT_ASSIGNED_TO_THE_CORPORATION_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_is_not_assigned_to_the_sector_exception}")
    public void incident_is_not_assigned_to_the_sector_exception(String placeholder) {
    	MessageResolver.INCIDENT_IS_NOT_ASSIGNED_TO_THE_SECTOR_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_not_found_exception_3}")
    public void incident_not_found_exception_3(String placeholder) {
    	MessageResolver.INCIDENT_NOT_FOUND_EXCEPTION_3 = placeholder;
    }
    
    @Value("${placeholder.incident_sector_not_found_exception}")
    public void incident_sector_not_found_exception(String placeholder) {
    	MessageResolver.INCIDENT_SECTOR_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.invalid_stage_type_exception}")
    public void invalid_stage_type_exception(String placeholder) {
    	MessageResolver.INVALID_STAGE_TYPE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stage_required_exception}")
    public void stage_required_exception(String placeholder) {
    	MessageResolver.STAGE_REQUIRED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.last_parent_id_requerid_exception}")
    public void last_parent_id_requerid_exception(String placeholder) {
    	MessageResolver.LAST_PARENT_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.mission_id_not_found_exception}")
    public void mission_id_not_found_exception(String placeholder) {
    	MessageResolver.MISSION_ID_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.mission_id_requerid_exception}")
    public void mission_id_requerid_exception(String placeholder) {
    	MessageResolver.MISSION_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.mission_not_configured_exception}")
    public void mission_not_configured_exception(String placeholder) {
    	MessageResolver.MISSION_NOT_CONFIGURED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.mission_not_found_exception}")
    public void mission_not_found_exception(String placeholder) {
    	MessageResolver.MISSION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.mission_stage_id_requerid_exception}")
    public void mission_stage_id_requerid_exception(String placeholder) {
    	MessageResolver.MISSION_STAGE_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.observation_size_exception}")
    public void observation_size_exception(String placeholder) {
    	MessageResolver.OBSERVATION_SIZE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.parent_id_requerid_exception}")
    public void parent_id_requerid_exception(String placeholder) {
    	MessageResolver.PARENT_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.people_transfer_requerid_exception}")
    public void people_transfer_requerid_exception(String placeholder) {
    	MessageResolver.PEOPLE_TRANSFER_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.persons_requerid_exception}")
    public void persons_requerid_exception(String placeholder) {
    	MessageResolver.PERSONS_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.reason_cancel_id_requerid_exception}")
    public void reason_cancel_id_requerid_exception(String placeholder) {
    	MessageResolver.REASON_CANCEL_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.reason_cancel_not_found_exception}")
    public void reason_cancel_not_found_exception(String placeholder) {
    	MessageResolver.REASON_CANCEL_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.sector_not_found_exception}")
    public void sector_not_found_exception(String placeholder) {
    	MessageResolver.SECTOR_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.sector_requerid_exception}")
    public void sector_requerid_exception(String placeholder) {
    	MessageResolver.SECTOR_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stage_not_configured_exception}")
    public void stage_not_configured_exception(String placeholder) {
    	MessageResolver.STAGE_NOT_CONFIGURED_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stage_preconfigured_not_found_exception}")
    public void stage_preconfigured_not_found_exception(String placeholder) {
    	MessageResolver.STAGE_PRECONFIGURED_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stages_not_configured_to_mission_exception}")
    public void stages_not_configured_to_mission_exception(String placeholder) {
    	MessageResolver.STAGES_NOT_CONFIGURED_TO_MISSION_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stage_transition_id_requerid_exception}")
    public void stage_transition_id_requerid_exception(String placeholder) {
    	MessageResolver.STAGE_TRANSITION_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stage_transition_not_found_exception}")
    public void stage_transition_not_found_exception(String placeholder) {
    	MessageResolver.STAGE_TRANSITION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.stage_type_not_support_exception}")
    public void stage_type_not_support_exception(String placeholder) {
    	MessageResolver.STAGE_TYPE_NOT_SUPPORT_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.transition_stage_id_requerid_exception}")
    public void transition_stage_id_requerid_exception(String placeholder) {
    	MessageResolver.TRANSITION_STAGE_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.transport_id_requerid_exception}")
    public void transport_id_requerid_exception(String placeholder) {
    	MessageResolver.TRANSPORT_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_corporation_not_found_exception}")
    public void unit_corporation_not_found_exception(String placeholder) {
    	MessageResolver.UNIT_CORPORATION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_force_config_not_available_exception}")
    public void unit_force_config_not_available_exception(String placeholder) {
    	MessageResolver.UNIT_FORCE_CONFIG_NOT_AVAILABLE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_force_config_not_found_exception}")
    public void unit_force_config_not_found_exception(String placeholder) {
    	MessageResolver.UNIT_FORCE_CONFIG_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_id_requerid_exception}")
    public void unit_id_requerid_exception(String placeholder) {
    	MessageResolver.UNIT_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_mission_id_requerid_exception}")
    public void unit_mission_id_requerid_exception(String placeholder) {
    	MessageResolver.UNIT_MISSION_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_mission_not_found_exception}")
    public void unit_mission_not_found_exception(String placeholder) {
    	MessageResolver.UNIT_MISSION_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_mission_status_id_requerid_exception}")
    public void unit_mission_status_id_requerid_exception(String placeholder) {
    	MessageResolver.UNIT_MISSION_STATUS_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_not_found_exception}")
    public void unit_not_found_exception(String placeholder) {
    	MessageResolver.UNIT_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unit_sector_not_found_exception}")
    public void unit_sector_not_found_exception(String placeholder) {
    	MessageResolver.UNIT_SECTOR_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.user_id_requerid_exception}")
    public void user_id_requerid_exception(String placeholder) {
    	MessageResolver.USER_ID_REQUERID_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unavailability_reason_not_found_exception}")
    public void unavailability_reason_not_found_exception(String placeholder) {
    	MessageResolver.UNAVAILABILITY_REASON_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.unavailability_reason_requerid_exception}")
    public void unavailability_reason_requerid_exception(String placeholder) {
    	MessageResolver.UNAVAILABILITY_REASON_REQUERID_EXCEPTION = placeholder;
    }
    @Value("${placeholder.cat_estatus_not_found_exception}")
    public void cat_estatus_not_found_exception(String placeholder) {
    	MessageResolver.CAT_ESTATUS_NOT_FOUND_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_close_exception}")
    public void incident_close_exception(String placeholder) {
    	MessageResolver.INCIDENT_CLOSE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_closed_exception}")
    public void incident_corporation_closed_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_CLOSE_EXCEPTION = placeholder;
    }
    
    @Value("${placeholder.incident_corporation_exist_unit_mission_assingn_exception}")
    public void incident_corporation_exist_unit_mission_assign_exception(String placeholder) {
    	MessageResolver.INCIDENT_CORPORATION_EXIST_UNIT_MISSION_ASSING = placeholder;
    }
}



