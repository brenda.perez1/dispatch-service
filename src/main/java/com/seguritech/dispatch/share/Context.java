package com.seguritech.dispatch.share;

import javax.ws.rs.ext.ContextResolver;

import com.fasterxml.jackson.databind.ObjectMapper;

public final class Context {

//    private static final Logger LOGGER = LoggerFactory.getLogger(Context.class);

//    private Context() {
//    }
//    
//    private static Config config;
//
//    public static Config getConfig() {
//        return config;
//    }

	private static ObjectMapper objectMapper;

	public static ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	private class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

		@Override
		public ObjectMapper getContext(Class<?> clazz) {
			return objectMapper;
		}

	}

}