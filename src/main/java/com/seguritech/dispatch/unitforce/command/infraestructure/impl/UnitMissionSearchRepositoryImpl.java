package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogSearch;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionSearch;
import com.seguritech.dispatch.unitforce.command.domain.model.StageModel;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionSearch;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitMissionSearchRepository;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitMissionSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionStageResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionTransitionResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceTransportResponse;
import com.seguritech.platform.application.http.Order;

@SuppressWarnings({"deprecation", "unchecked", "rawtypes"})
@Repository
public class UnitMissionSearchRepositoryImpl implements UnitMissionSearchRepository {

	private final EntityManager entityManager;

	public UnitMissionSearchRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public List<UnitMissionSearch> findAll(UnitMissionSearchRequest request) {

		StringBuilder sql = new StringBuilder();

		sql.append("   SELECT  ");
		sql.append("  	   um.id_emergency AS emergencyId,  ");
		sql.append("  	   um.id_unit_mission AS unitMissionId,  ");
		sql.append("  	   um.start_mission_date AS startMissionDate,  ");
		sql.append("  	   um.creation_date AS assignedAt,  ");
		sql.append("  	   um.created_by AS assignedByUser,  ");
		sql.append("  	   um.end_mission_date AS endMissionDate,  ");
		sql.append("  	   um.update_date AS lastUpdate, ");
		sql.append("  	   um.id_mission AS missionId,  ");
		sql.append("  	   cm.name AS missionName,  ");
		sql.append("  	   cm.description AS missionDescription,  ");
		sql.append("  	   um.id_unit_force_config AS unitForceConfigId,  ");
		sql.append("  	   ufc.alias AS unitForceConfigAlias,  ");
		sql.append("  	   um.persons_number as personsNumber,  ");
		sql.append("  	   ufc.id_corporation AS corporationId,  ");
		sql.append("  	   cc.corporation_name as corporationName,  ");
		sql.append("  	   ufc.visible AS unitForceConfigVisible, ");
		sql.append("  	   ct.id AS transportId,  ");
		sql.append("  	   ct.name AS transportName,  ");
		sql.append("  	   ct.description AS transportDescription,  ");
		sql.append("  	   ct.economic_number AS transportEconomicNumber, ");
		sql.append("  	   um.id_mission_stage_config AS idMissionStageConfig, ");
		sql.append("  	   cs.background_color AS backgroundColorActualStage, ");
		sql.append("  	   cs.icon AS iconActualStage, ");
		sql.append("  	   msc.id_stage AS stageId, ");
		sql.append("  	   cs.name AS stageName, ");
		sql.append("  	   cs.text_color AS textColorActualStage, ");
		sql.append("  	   msc.id_stage_transition AS transitionId, ");
		sql.append("  	   cst.stage_transition_desc AS stageTransitionDesc  ");
		sql.append("     FROM {h-schema}unit_mission um  ");
		sql.append("    INNER JOIN {h-schema}cat_mission cm ON um.id_mission = cm.id_mission ");
		sql.append("    INNER JOIN {h-schema}unit_force_config ufc ON um.id_unit_force_config = ufc.id_unit_force_config  ");
		sql.append("    INNER JOIN {h-schema}cat_transport ct ON ufc.id_transport = ct.id  ");
		sql.append("    INNER JOIN {h-schema}mission_stage_config msc ON um.id_mission_stage_config = msc.id_mission_stage_config ");
		sql.append("    INNER JOIN {h-schema}cat_stage cs ON msc.id_stage = cs.id_stage ");
		sql.append("    INNER JOIN {h-schema}cat_stage_transition cst ON msc.id_stage_transition = cst.id_stage_transition ");
		sql.append("    INNER JOIN {h-schema}cat_corporation cc ON ufc.id_corporation = cc.id_corporation   ");

		sql.append(this.getConditions(request));
		sql.append(this.getOrderBy(request.getOrderByField(), request.getOrder() != null ? request.getOrder().name() : Order.DESC.name()));

		sql = new StringBuilder(" WITH records AS (").append(sql).append("), ");
		sql.append("             total_records AS (SELECT COUNT(*) AS total FROM records), ");
		sql.append("          filtered_records AS (SELECT * FROM records " + this.getPagination(request) + ") ");

		sql.append("          SELECT r.*, ");

		/* Start - stageMissionLogs */
		sql.append("                 (SELECT JSONB_AGG(ROW_TO_JSON(logs)) FROM (SELECT  ");
		sql.append("                	 sml.id, ");
		sql.append("   					 sml.created_by AS createdBy, ");
		sql.append("   					 sml.creation_date AS creationDate, ");
		sql.append("   					 sml.id_emergency AS emergencyId, ");
		sql.append("   					 sml.id_mission AS missionId, ");
		sql.append("   					 sml.observations, ");
		sql.append("   					 sml.persons_number AS personsNumber, ");
		sql.append("   					 sml.id_reason_cancel AS reasonCancel, ");
		sql.append("   					 sml.id_stage AS stageId, ");
		sql.append("   					 cs.background_color AS stageBackGroundColor, ");
		sql.append("   					 cs.description AS stageDescription, ");
		sql.append("   					 cs.name AS stageName, ");
		sql.append("   					 cs.icon AS stageIcon, ");
		sql.append("   					 cs.text_color AS stageTextColor, ");
		sql.append("   					 sml.id_unit_force_config AS unitForceConfigId, ");
		sql.append("   					 sml.id_unit_mission AS unitMissionId, ");
		sql.append("   					 sml.update_date AS updateDate, ");
		sql.append("   					 sml.updated_by AS updatedBy, ");
		sql.append("   					 sml.id_stage_transition as idStageTransition ");
		sql.append("     			    FROM {h-schema}stage_mission_log sml  ");
		sql.append("    	      INNER JOIN {h-schema}cat_stage cs ON sml.id_stage = cs.id_stage  ");
		sql.append("                   WHERE sml.id_emergency = r.emergencyId) logs) AS stageMissionLogs, ");
		/* End - stageMissionLogs */

		/* Start - nextStages */
		sql.append("   				 (SELECT JSONB_AGG(ROW_TO_JSON(nextStages)) FROM (SELECT   ");
		sql.append("  	   				 msc.id_mission_stage_config AS idMissionStageConfig, ");
		sql.append("  	                 cs.background_color AS stageBackGroundColor, ");
		sql.append("  	                 cs.name AS stageName, ");
		sql.append("  	                 cs.icon AS stageIcon, ");
		sql.append("  	                 cs.text_color AS stageTextColor, ");
		sql.append("  	                 msc.id_stage_last AS lastStageId, ");
		sql.append("  	                 msc.id_stage_transition AS transitionId, ");
		sql.append("  	                 cst.stage_transition_desc AS stageTransitionDesc  ");
		sql.append("                    FROM {h-schema}mission_stage_config msc ");
		sql.append("                   INNER JOIN {h-schema}cat_stage cs ON msc.id_stage = cs.id_stage  ");
		sql.append("                   INNER JOIN {h-schema}cat_stage_transition cst ON msc.id_stage_transition = cst.id_stage_transition ");
		sql.append("                   WHERE msc.id_stage_last = r.stageId AND msc.id_mission = r.missionId) nextStages) AS nextStages, ");
		/* End - nextStages */

		sql.append("             (SELECT total FROM total_records) AS total ");
		sql.append("             FROM filtered_records r ");

		SQLQuery sqlQuery = entityManager.createNativeQuery(sql.toString()).unwrap(SQLQuery.class);

		this.mapper(sqlQuery);

		return sqlQuery.setResultTransformer(Transformers.aliasToBean(UnitMissionSearch.class)).getResultList();

	}

	private String getPagination(UnitMissionSearchRequest request) {
		StringBuilder pagination =  new StringBuilder();

		if(request.getPage() != null && request.getSize() != null) {
			pagination.append(" OFFSET " + (request.getPage() > 0 ? (request.getPage() * request.getSize()) : 0) + " ");	
		} else if (request.getPage() != null && request.getSize() == null) {
			pagination.append(" OFFSET " + (request.getPage() + " "));
		}

		if(request.getSize() != null) {
			pagination.append(" LIMIT " + request.getSize() + " ");
		}

		return pagination.toString();
	}

	@Override
	public PageDataProjectionDTO<IncidentUnitForceSearchResponse> search(UnitMissionSearchRequest request) {
		List<UnitMissionSearch> list = this.findAll(request);
		return new PageDataProjectionDTO<>(list.stream().map(u -> this.toResponse(u)).collect(Collectors.toList()), list.isEmpty() ? 0 : list.get(0).getTotal());
	}

	private String getConditions(UnitMissionSearchRequest request) {
		StringBuilder conditions = new StringBuilder(" WHERE 1 = 1 ");

		Optional.ofNullable(request.getIncidentId()).ifPresent(x -> conditions.append(" AND um.id_emergency = '" + x + "' "));
		Optional.ofNullable(request.getCorporationId()).ifPresent(x -> conditions.append(" AND ufc.id_corporation = " + x + " "));

		return conditions.toString();
	}

	private String getOrderBy(final String field, final String direction) {
		StringBuilder orderBy = new StringBuilder(" ORDER BY ");

		if(field == null || field.isEmpty()) {
			return orderBy.append(" um.creation_date ").append(Order.DESC.name()).toString();
		}

		switch (field) {
		case "unitMissionCreatedAt" : orderBy.append(" um.creation_date "); break;
		case "unitMissionStartAt" : orderBy.append(" um.start_mission_date "); break;
		case "emergencyId" : orderBy.append(" um.id_emergency "); break;
		default:
			orderBy.append(" um.creation_date ");
			break;
		}

		List<String> directions = Arrays.asList(Order.values()).stream().map(o -> String.valueOf(o)).collect(Collectors.toList());
		orderBy.append(directions.contains(direction != null ? direction.toUpperCase() : null) ? direction : Order.DESC.name());

		return orderBy.toString();
	}

	private void mapper(SQLQuery sqlQuery) {
		sqlQuery.addScalar("unitMissionId", StringType.INSTANCE); 
		sqlQuery.addScalar("startMissionDate", TimestampType.INSTANCE); 
		sqlQuery.addScalar("assignedAt", TimestampType.INSTANCE); 
		sqlQuery.addScalar("assignedByUser", StringType.INSTANCE); 
		sqlQuery.addScalar("endMissionDate", TimestampType.INSTANCE); 
		sqlQuery.addScalar("lastUpdate", TimestampType.INSTANCE); 
		sqlQuery.addScalar("missionId", LongType.INSTANCE); 
		sqlQuery.addScalar("missionName", StringType.INSTANCE); 
		sqlQuery.addScalar("missionDescription", StringType.INSTANCE); 
		sqlQuery.addScalar("unitForceConfigId", StringType.INSTANCE); 
		sqlQuery.addScalar("unitForceConfigAlias", StringType.INSTANCE); 
		sqlQuery.addScalar("personsNumber", LongType.INSTANCE); 
		sqlQuery.addScalar("corporationId", LongType.INSTANCE); 
		sqlQuery.addScalar("corporationName", StringType.INSTANCE); 
		sqlQuery.addScalar("unitForceConfigVisible", BooleanType.INSTANCE); 
		sqlQuery.addScalar("transportId", LongType.INSTANCE); 
		sqlQuery.addScalar("transportName", StringType.INSTANCE); 
		sqlQuery.addScalar("transportDescription", StringType.INSTANCE); 
		sqlQuery.addScalar("transportEconomicNumber", StringType.INSTANCE); 
		sqlQuery.addScalar("idMissionStageConfig", StringType.INSTANCE); 
		sqlQuery.addScalar("backgroundColorActualStage", StringType.INSTANCE); 
		sqlQuery.addScalar("iconActualStage", StringType.INSTANCE); 
		sqlQuery.addScalar("stageId", LongType.INSTANCE); 
		sqlQuery.addScalar("stageName", StringType.INSTANCE); 
		sqlQuery.addScalar("textColorActualStage", StringType.INSTANCE); 
		sqlQuery.addScalar("transitionId", LongType.INSTANCE); 
		sqlQuery.addScalar("stageTransitionDesc", StringType.INSTANCE); 
		sqlQuery.addScalar("stageMissionLogs", StringType.INSTANCE); 
		sqlQuery.addScalar("nextStages", StringType.INSTANCE); 
		sqlQuery.addScalar("total", LongType.INSTANCE); 
	}

	private IncidentUnitForceSearchResponse toResponse(UnitMissionSearch u) {

		List<StageMissionLogSearch> stageMissionLogs = this.jsonToList(u.getStageMissionLogs(), StageMissionLogSearch.class.getName());

		return Optional.ofNullable(u).map(x -> new IncidentUnitForceSearchResponse (
				x.getUnitMissionId(),
				x.getStartMissionDate(),
				x.getEndMissionDate(),
				x.getLastUpdate(),
				x.getAssignedAt(),
				x.getAssignedByUser(),
				x.getPersonsNumber(),
				Optional.ofNullable(x.getTransportId()).map(t -> new UnitForceTransportResponse(x.getTransportId(), x.getTransportName(), x.getTransportDescription(), x.getTransportEconomicNumber())).orElse(null),
				this.toUnitForceMissionResponse(u),   		
				new UnitForceResponse(u.getUnitForceConfigId(), u.getUnitForceConfigAlias(), u.getUnitForceConfigVisible(), u.getCorporationId(), u.getCorporationName()),
				stageMissionLogs != null && !stageMissionLogs.isEmpty() ? stageMissionLogs.stream().map(s -> this.toStageMissionLogModel(s)).collect(Collectors.toList()) : null
				)).orElse(null);
	}

	private UnitForceMissionResponse toUnitForceMissionResponse(UnitMissionSearch u) {
		List<StageMissionSearch> nextStages = this.jsonToList(u.getNextStages(), StageMissionSearch.class.getName());
		return Optional.ofNullable(u).map(x -> new UnitForceMissionResponse(
				x.getMissionId(),
				x.getMissionName(),
				x.getMissionDescription(),
				Optional.ofNullable(x.getTransitionId()).map(t -> new UnitForceMissionTransitionResponse(x.getTransitionId(), x.getStageTransitionDesc())).orElse(null),
				Optional.ofNullable(x.getStageId()).map(s -> new UnitForceMissionStageResponse(x.getIdMissionStageConfig(), x.getStageId(), x.getStageName(), x.getIconActualStage(), x.getTextColorActualStage(), x.getBackgroundColorActualStage())).orElse(null),
				nextStages != null && !nextStages.isEmpty() ? nextStages.stream().map(n -> this.toUnitForceMissionResponse(u, n)).collect(Collectors.toList()) : null
				)).orElse(null);
	}

	private UnitForceMissionStageResponse toUnitForceMissionResponse(UnitMissionSearch u, StageMissionSearch s) {
		return Optional.ofNullable(s).map(y -> {
			UnitForceMissionStageResponse unitForceMissionStageResponse =  new UnitForceMissionStageResponse(u.getIdMissionStageConfig(), y.getLastStageId(), y.getStageName(), y.getStageIcon(), y.getStageTextcolor(), y.getStageBackGroundColor());
			unitForceMissionStageResponse.setTransition(Optional.ofNullable(u.getTransitionId()).map(x -> new UnitForceMissionTransitionResponse(u.getTransitionId(), u.getStageTransitionDesc())).orElse(null));
			return unitForceMissionStageResponse;
		}).orElse(null);
	}

	private StageMissionLogModel toStageMissionLogModel(StageMissionLogSearch s) {
		return new StageMissionLogModel(
				s.getId(),
				s.getCreatedBy(),
				s.getCreationDate(),
				s.getUpdatedBy(), 
				s.getUpdateDate(),
				Optional.ofNullable(s.getStageId()).map(st ->  new StageModel(
						s.getStageId(),
						s.getStageName(),
						s.getStageDescription(),
						s.getStageIcon(),
						s.getStageTextColor(),
						s.getStageBackgroundColor(),
						null
						)).orElse(null),
				s.getIdStageTransition(),
				s.getPersonsNumber(), 
				s.getObservations(), 
				s.getReasonCancel(), 
				s.getMissionId(), 
				s.getEmergencyId(), 
				new UnitForceConfigId(s.getUnitForceConfigId()), 
				new UnitMissionId(s.getUnitMissionId()));
	}

	public <T> List<T> jsonToList(String json, String className) {
		Class<?> clazz = null;
		List<T> result = new ArrayList<>();

		try {
			clazz = Class.forName(className);	
		} catch (Exception e) {
			return result;
		}

		if(json != null && !json.isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			CollectionType type = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
			mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
			try {
				result = mapper.readValue(json, type);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}	
		}

		return result;
	}

}
