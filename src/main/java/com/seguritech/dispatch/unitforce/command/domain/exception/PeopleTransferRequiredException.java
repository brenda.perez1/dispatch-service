package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class PeopleTransferRequiredException extends BusinessException {
    public PeopleTransferRequiredException() {
        super(MessageResolver.PEOPLE_TRANSFER_REQUERID_EXCEPTION, 3057L);
    }
}
