package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;
//import com.domain.model.corporation.entity.CorporationOperative;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitCorporationRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;

@Repository
public class UnitCorporationRepositoryImpl implements UnitCorporationRepository {

    private final UnitConfigEntityRepository repository;

    public UnitCorporationRepositoryImpl (UnitConfigEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public CorporationOperativeId findCorporationOperativeId (UnitForceConfigId unitForceConfigId) {
        return this.repository
                .findById(unitForceConfigId.getValue())
                .map(UnitForceConfigEntity::getCorporationEntity)
                .map(CorporationOperative::getId)
                .map(CorporationOperativeId::required)
                .orElse(null);

    }

	@Override
	public CorporationId findCorporationId(UnitForceConfigId unitForceConfigId) {
		return this.repository
        .findById(unitForceConfigId.getValue())
        .map(UnitForceConfigEntity::getCorporation)
        .map(Corporation::getId)
        .map(CorporationId::required)
        .orElse(null);
	}

	@Override
	public UnitForceConfigEntity get(UnitForceConfigId unitForceConfigId) {
		 return this.repository.findById(unitForceConfigId.getValue()).orElse(null);
	}
}
