package com.seguritech.dispatch.unitforce.command.domain.event.listener;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SubStageMissionChangedEvent extends StageEvent{

}
