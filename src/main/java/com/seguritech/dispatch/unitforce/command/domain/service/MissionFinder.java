package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.MissionNotConfiguredException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;
import com.seguritech.platform.Service;

@Service
public class MissionFinder {

    private final CorporationMissionRepository repository;

    public MissionFinder(CorporationMissionRepository repository) {
        this.repository = repository;
    }

    public MissionId find(CorporationId corporationOperativeId) {
        MissionId missionId = this.repository.find(corporationOperativeId);

        if (missionId == null)
            throw new MissionNotConfiguredException(corporationOperativeId);

        return missionId;
    }

    public MissionId find(UnitForceConfigId unitForceConfigId) {
        MissionId missionId = this.repository.find(unitForceConfigId);

        if (missionId == null)
            throw new MissionNotConfiguredException();

        return missionId;
    }
}
