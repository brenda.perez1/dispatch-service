package com.seguritech.dispatch.unitforce.command.application.http.request;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class UnitForceChangeHttpRequest {
	
	@NotEmpty
	@NonNull
    private String status;
    private Long unavailabilityReasonId;
    private String observations;
}
