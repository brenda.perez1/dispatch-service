package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.domain.model.mission.entity.Mission;
import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.mission.entity.MissionStageConfigPk;
import com.domain.model.mission.entity.Stage;

public interface StageConfigEntityRepository extends JpaRepository<MissionStageConfig, MissionStageConfigPk>,
    JpaSpecificationExecutor<MissionStageConfig> {
	
    Optional<MissionStageConfig> findByMissionAndStage(Mission missionId, Stage lastStageId);
    
    Optional<MissionStageConfig> findByStageLastAndMissionAndStage(Long nextStageId, Mission missionId, Stage lastStageId);

    @Query("SELECT u, u.id, u.id.stage FROM MissionStageConfig u where u.stageLast = :nextStageId and u.id.mission = :missionId ")
    List<MissionStageConfig> findByIdStageLastAndIdMission( @Param("nextStageId") Long nextStageId,  @Param("missionId") Mission missionId);
    
}
