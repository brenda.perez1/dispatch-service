package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ReasonCancelNotFoundException extends BusinessException {
	
    public ReasonCancelNotFoundException(Object... reasonCancelId) {
        super(MessageResolver.REASON_CANCEL_NOT_FOUND_EXCEPTION, reasonCancelId, 3060L);
    }
}
