package com.seguritech.dispatch.unitforce.command.application.http;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.unitforce.command.application.http.request.UnitForceChangeHttpRequest;
import com.seguritech.dispatch.unitforce.command.application.usecase.UnitForceChangeUseCase;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.UnitForceChangeRequest;

import io.swagger.annotations.ApiOperation;

@RestController
public class UnitForceChangeController extends IncidentUnitMissionBaseController {

    private final UnitForceChangeUseCase unitForceChangeUseCase;
    
    public UnitForceChangeController(UnitForceChangeUseCase unitForceChangeUseCase) {
    	this.unitForceChangeUseCase = unitForceChangeUseCase;
    }

    @ApiOperation(value = "Status unit force change", nickname = "update", notes = "Status unit force change")
    @PatchMapping("unit-force/{unitforceConfig-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CadLiteAPIResponse<Object>> update(@RequestBody UnitForceChangeHttpRequest request,
                                                             @PathVariable("unitforceConfig-id") String unitforceConfigId,
                                                             UsernamePasswordAuthenticationToken user) {
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
        this.unitForceChangeUseCase.execute(new UnitForceChangeRequest(
        		unitforceConfigId,
        		request.getStatus(),
        		request.getUnavailabilityReasonId(),
        		request.getObservations(),
        		principal.getId()
        ));
        return CadLiteAPIResponse.build(CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.SUCCESS);
    }
    
    
}
