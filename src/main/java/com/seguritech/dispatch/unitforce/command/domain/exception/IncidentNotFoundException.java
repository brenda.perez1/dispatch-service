package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentNotFoundException extends BusinessException {
	
    public IncidentNotFoundException(Object... incidentId) {
        super(MessageResolver.INCIDENT_NOT_FOUND_EXCEPTION_3, incidentId, 3046L);
    }
}
