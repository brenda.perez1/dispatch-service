package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.domain.model.mission.entity.CorporationMission;
import com.domain.model.mission.entity.CorporationMissionPK;

public interface CorporationMissionEntityRepository extends JpaRepository<CorporationMission, CorporationMissionPK> {
	
	@Query(value = "FROM CorporationMission cm WHERE cm.id.corporation.id =:corporationId and cm.id.mission.id =:missionId")
    Optional<CorporationMission> findByCorporationAndMission(Long corporationId, Long missionId);
	
	@Query(value = "FROM CorporationMission cm WHERE cm.id.corporation.id =:corporationId and cm.active =:value")
    Optional<CorporationMission> findByCorporationAndPreconfiguredMission(Long corporationId, Boolean value);

}
