//package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;
//
//import java.util.Optional;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import com.domain.model.geofence.entity.Geofence;
//import com.domain.model.incident.entity.IncidentEntity;
//
//public interface DispatchIncidentLocationEntityRepository extends JpaRepository<IncidentLocationEntity, Long> {
//    Optional<IncidentLocationEntity> findByEmergencyAndGeofence(IncidentEntity incidentId, Geofence unitSectorId);
//
//}
//
