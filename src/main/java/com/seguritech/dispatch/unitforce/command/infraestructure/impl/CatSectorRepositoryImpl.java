package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatSectorRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CatSectorEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CatSectorRepositoryImpl implements CatSectorRepository {

    private final CatSectorEntityRepository catSectorEntityRepository;

    public CatSectorRepositoryImpl(CatSectorEntityRepository catSectorEntityRepository) {
        this.catSectorEntityRepository = catSectorEntityRepository;
    }

    @Override
    public boolean exist(SectorId sectorId) {
        return this.catSectorEntityRepository.findById(sectorId.getValue()).isPresent();
    }
}
