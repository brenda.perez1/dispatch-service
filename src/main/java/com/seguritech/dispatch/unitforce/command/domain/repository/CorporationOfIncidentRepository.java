package com.seguritech.dispatch.unitforce.command.domain.repository;


import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;

public interface CorporationOfIncidentRepository {

    boolean exist(IncidentId incidentId, CorporationId unitCorporationOperativeId);
    boolean exist(IncidentId incidentId, CorporationOperativeId unitCorporationOperativeId);
    IncidentCorporationEntity getById(IncidentId incidentId, CorporationOperativeId unitCorporationOperativeId);
    void save(IncidentCorporationEntity incidentCorporationEntity);

}
