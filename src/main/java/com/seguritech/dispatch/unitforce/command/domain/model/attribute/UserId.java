package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.UserIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class UserId {

    private String value;

    private UserId(String value) {
        this.value = Optional.ofNullable(value).orElseThrow(UserIdRequiredException::new);
    }

    public static UserId required(String id){
        return new UserId(id);
    }

}
