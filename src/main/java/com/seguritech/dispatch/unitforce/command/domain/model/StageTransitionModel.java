package com.seguritech.dispatch.unitforce.command.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StageTransitionModel {

	private Long id;
	private String description;
}
