package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageNotCofiguredException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;

public class StagePreconfiguredFinder {

    private final CorporationMissionRepository repository;

    public StagePreconfiguredFinder(CorporationMissionRepository repository) {
        this.repository = repository;
    }

    public StageId find(CorporationId corporationOperativeId, MissionId missionId) {
        StageId stageId = this.repository.findStageId(corporationOperativeId, missionId);

        if (stageId == null)
            throw new StageNotCofiguredException(missionId.getValue());

        return stageId;
    }

}
