package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ParentIdRequieredException extends BusinessException {
    public ParentIdRequieredException() {
        super(MessageResolver.PARENT_ID_REQUERID_EXCEPTION, 3056L);
    }
}
