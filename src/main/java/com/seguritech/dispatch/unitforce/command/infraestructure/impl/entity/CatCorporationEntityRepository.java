package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.corporation.entity.Corporation;

public interface CatCorporationEntityRepository extends JpaRepository<Corporation,Long> {
}
