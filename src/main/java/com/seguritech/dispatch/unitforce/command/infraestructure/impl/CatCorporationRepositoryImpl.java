package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatCorporationRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CatCorporationEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CatCorporationRepositoryImpl implements CatCorporationRepository {

    private final CatCorporationEntityRepository catCorporationEntityRepository;

    public CatCorporationRepositoryImpl(CatCorporationEntityRepository catCorporationEntityRepository) {
        this.catCorporationEntityRepository = catCorporationEntityRepository;
    }

    @Override
    public boolean exist(CorporationOperativeId corporationOperativeId) {
        return this.catCorporationEntityRepository.findById(corporationOperativeId.getValue()).isPresent();
    }
}
