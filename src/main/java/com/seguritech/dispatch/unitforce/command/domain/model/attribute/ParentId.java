package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.ParentIdRequieredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class ParentId {

    private Long value;

    private ParentId(Long value) {
        this.value = value;
    }

    public static ParentId required(Long id) {
        if (id == null)
            throw new ParentIdRequieredException();
        return new ParentId(id);
    }

    public static ParentId optional(Long id) {
        return new ParentId(id);
    }

}
