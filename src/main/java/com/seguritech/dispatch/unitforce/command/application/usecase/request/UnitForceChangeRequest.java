package com.seguritech.dispatch.unitforce.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UnitForceChangeRequest {

	private String unitForceConfigId;
	private String status;
	private Long unavailabilityReasonId;
	private String observations;
	private String user;
}
