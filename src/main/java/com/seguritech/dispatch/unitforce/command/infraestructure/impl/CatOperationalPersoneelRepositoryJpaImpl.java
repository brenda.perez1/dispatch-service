package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.seguritech.dispatch.unitforce.command.domain.repository.CatOperationalPersonnelRepositoryJpa;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CatOperationalPersoneelRepository;

@Repository
public class CatOperationalPersoneelRepositoryJpaImpl implements CatOperationalPersonnelRepositoryJpa {

	private final CatOperationalPersoneelRepository catOperationalPersoneelRepository;

	public CatOperationalPersoneelRepositoryJpaImpl(CatOperationalPersoneelRepository catOperationalPersoneelRepository) {
		this.catOperationalPersoneelRepository = catOperationalPersoneelRepository;
	}

	@Override
	public List<String> findOneSignalUsersIdByUnitForceConfigId(String unitForceConfigId) {
		List<String> oneSignalUsersId = catOperationalPersoneelRepository.findOneSignalUsersIdByUnitForceConfigId(unitForceConfigId);
		return Optional.ofNullable(oneSignalUsersId).orElse(new ArrayList<>()).stream().filter(x -> x != null).collect(Collectors.toList());
	}

}
