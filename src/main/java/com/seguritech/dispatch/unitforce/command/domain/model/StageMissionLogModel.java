package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.Date;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;

import lombok.Getter;

@Getter
public class StageMissionLogModel {
	
	public Long id;
    public String createdBy;
    public Date creationDate;
    public String updatedBy;
    public Date updateDate;
    public StageModel stage;
    public Long stageTransition;
    public Long personsNumber;
    public String observations;
    public Long reasonCancel;
    public Long missionId;
    public String emergencyId;
    public UnitForceConfigId unitForceConfigId;
    public UnitMissionId unitMissionId;
    
    public StageMissionLogModel(Long id, String createdBy, Date creationDate, String updatedBy, Date updateDate, StageModel stage, Long stageTransition, Long personsNumber,
			String observations, Long reasonCancel, Long missionId, String emergencyId, UnitForceConfigId unitForceConfigId,
			UnitMissionId unitMissionId) {
    	this.id = id;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
		this.updatedBy = updatedBy;
		this.updateDate = updateDate;
		this.stage = stage;
		this.stageTransition = stageTransition;
		this.observations = observations;
		this.reasonCancel = reasonCancel;
		this.missionId = missionId;
		this.emergencyId = emergencyId;
		this.unitForceConfigId = unitForceConfigId;
		this.unitMissionId = unitMissionId;
	}
    
}
