package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitMissionStatusIdRequiredException extends BusinessException {

    public UnitMissionStatusIdRequiredException() {
        super(MessageResolver.UNIT_MISSION_STATUS_ID_REQUERID_EXCEPTION, 3077L);
    }
}
