package com.seguritech.dispatch.unitforce.command.domain.event.listener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MissionCanceledEvent {
    private Long sectorId;
    private Long corporationId;
    private String unitMissionId;
    private Long missionId;
    private Long stageId;
    private String unitForceConfigId;
    private String incidentId;
    private String createdByUser;
    private String updatedByUser;
    private Date createdAt;
    private Date updateAt;
    private Date startMissionDate;
    private Date endMissionDate;
    private Long reasonCancelId;
    private String observations;
    private Long persons;
    private boolean active;
    private Long idStageTransition;
}
