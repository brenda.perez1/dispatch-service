package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ReasonCancelIdRequiredException extends BusinessException {
    public ReasonCancelIdRequiredException() {
        super(MessageResolver.REASON_CANCEL_ID_REQUERID_EXCEPTION, 3059L);
    }
}
