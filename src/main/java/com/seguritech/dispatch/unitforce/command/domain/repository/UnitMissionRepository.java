package com.seguritech.dispatch.unitforce.command.domain.repository;

import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;

public interface UnitMissionRepository {

    void save(UnitMissionModel unitMissionModel, StageId currentStage);

    boolean exist(UnitMissionId unitMissionId);

    UnitMissionModel find(UnitForceConfigId unitForceConfigId, IncidentId incidentId);
    
    UnitMissionModel find(UnitMissionId unitMissionId, IncidentId incidentId);
    
    List<UnitMissionModel> findByUnitForceAndIncident(UnitForceConfigId unitForceConfigId, IncidentId incidentId);
    
    List<String> find(IncidentId incidentId);
    
    Long countUnitMission(IncidentId incidentId, CorporationId corporationId);

}
