package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.Date;

import org.springframework.stereotype.Repository;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.mission.entity.Stage;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk;
import com.domain.model.unitmission.StageMissionLogEntity;
import com.seguritech.dispatch.unitforce.command.domain.model.BinnacleModel;
import com.seguritech.dispatch.unitforce.command.domain.repository.BinnacleRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.BinnacleEntityRepository;

@Repository
public class BinnacleRepositoryImpl implements BinnacleRepository {

    private final BinnacleEntityRepository binnacleEntityRepository;

    public BinnacleRepositoryImpl(BinnacleEntityRepository binnacleEntityRepository) {
        this.binnacleEntityRepository = binnacleEntityRepository;
    }

    @Override
    public void save(BinnacleModel model) {
        this.binnacleEntityRepository.save(toEntity(model));
    }

    private StageMissionLogEntity toEntity(BinnacleModel model) {
        return new StageMissionLogEntity(
                null,
                new Date(),
                model.getCreatedByUser().getValue(),
                new Date(),
                model.getUpdatedByUser().getValue(),
                new UnitMissionEntity(
                        new UnitMissionEntityPk(
                                model.getUnitMissionId().getValue(),
                                new IncidentEntity(model.getIncidentId().getValue()),
                                new UnitForceConfigEntity(model.getUnitForceConfigId().getValue())
                        )
                ),
                new Stage(model.getCatStageId().getValue()),
                model.getDateChangedStage(),
                model.getStageTransitionId().getValue(),
                model.getPersons().getValue(),
                model.getObservations().getValue(),
                model.getReasonCancelId().getValue(),
                model.getMissionId().getValue()
        );
    }
}
