package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitCorporationNotFoundException extends BusinessException {
	
    public UnitCorporationNotFoundException(Object... unitForceConfigId) {
        super(MessageResolver.UNIT_CORPORATION_NOT_FOUND_EXCEPTION, unitForceConfigId, 3071L);
    }
}
