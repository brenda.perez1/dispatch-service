package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.List;
import com.seguritech.dispatch.unitforce.command.domain.event.IncidentChangeUnitForceEvent;
import com.seguritech.platform.domain.RootAggregate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentAppCorporationModel extends RootAggregate {
	
	private String incidentId;
	private String incidentSheet;
	private List<String> unitForce;
	private Boolean status;
	private Long reasonId;
	private String reason;
	private String lastStageId;
	private String lastStage;
	private String userId;
	private String option;
	
	public static IncidentAppCorporationModel create (String incidentId, String incidentSheet, List<String> unitForceIds, Boolean status,
			Long reasonId, String reason,	String lastStageId,	String lastStage, String userId, String option) {
		IncidentAppCorporationModel model = new IncidentAppCorporationModel(
				incidentId, 
				incidentSheet, 
				unitForceIds, 
				status, 
				reasonId, 
				reason, 
				lastStageId, 
				lastStage, 
				userId, 
				option);
		model.record(new IncidentChangeUnitForceEvent(unitForceIds, incidentSheet, incidentId, status, reasonId, reason, lastStageId, lastStage, userId, option));
		return model;
	}
	
}
