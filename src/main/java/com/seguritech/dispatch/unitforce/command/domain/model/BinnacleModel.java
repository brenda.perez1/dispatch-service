package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.Date;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.event.LogMissionStageEvent;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.platform.domain.RootAggregate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BinnacleModel extends RootAggregate {
    private SectorId sectorId;
    private CorporationOperativeId corporationOperativeId;
    private UnitMissionId unitMissionId;
    private MissionId missionId;
    private CatStageId catStageId;
    private UnitForceConfigId unitForceConfigId;
    private IncidentId incidentId;
    private ReasonCancelId reasonCancelId;
    private StageTransitionId stageTransitionId;
    private UserId userId;
    private Observations observations;
    private Persons persons;
    private UserId createdByUser;
    private UserId updatedByUser;
    private Date createdAt;
    private Date updateAt;
    private Date dateChangedStage;
    private Boolean active;

    public static BinnacleModel create(
            SectorId sectorId,
            CorporationOperativeId corporationOperativeId,
            UnitMissionId unitMissionId,
            MissionId missionId,
            CatStageId catStageId,
            UnitForceConfigId unitForceConfigId,
            IncidentId incidentId,
            ReasonCancelId reasonCancelId,
            StageTransitionId stageTransitionId,
            UserId userId,
            Observations observations,
            Persons persons,
            Date dateChangedStage
    ) {
        BinnacleModel binnacleModel = new BinnacleModel(
                sectorId,
                corporationOperativeId,
                unitMissionId,
                missionId,
                catStageId,
                unitForceConfigId,
                incidentId,
                reasonCancelId,
                stageTransitionId,
                userId,
                observations,
                persons,
                userId,
                userId,
                new Date(),
                new Date(),
                dateChangedStage,
                true
        );

        binnacleModel.record(new LogMissionStageEvent(
                unitMissionId.getValue(),
                missionId.getValue(),
                catStageId.getValue(),
                incidentId.getValue(),
                new Date(),
                userId.getValue()
        ));
        return binnacleModel;
    }

}
