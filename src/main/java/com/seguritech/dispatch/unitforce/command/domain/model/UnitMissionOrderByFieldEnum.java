package com.seguritech.dispatch.unitforce.command.domain.model;

public enum UnitMissionOrderByFieldEnum {
	
	unitMissionCreatedAt, unitMissionStartAt, emergencyId

}
