package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.unit.entity.UnavailabilityReasonEntity;

public interface UnavailabilityReasonEntityRepository extends JpaRepository<UnavailabilityReasonEntity, Long> {	

}
