package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.mission.entity.StageTransition;

public interface StageTransitionEntityRepository extends JpaRepository<StageTransition, Long> {
	
}
