package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.domain.model.unit.entity.UnitForceConfigStatusLogEntity;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigStatusLogRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitForceConfigStatusLogEntityRepository;

@Repository
public class UnitForceConfigStatusLogRepositoryImpl implements UnitForceConfigStatusLogRepository {

    private final UnitForceConfigStatusLogEntityRepository repository;

    public UnitForceConfigStatusLogRepositoryImpl(UnitForceConfigStatusLogEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean ensurer(Long unitForceConfigStatusLogId) {
        return this.repository.findById(unitForceConfigStatusLogId).isPresent();
    }

	@Override
	public void save(UnitForceConfigStatusLogEntity unitForceConfigStatusLogModel) {
		this.repository.save(unitForceConfigStatusLogModel);
	}

}
