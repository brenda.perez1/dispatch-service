package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UserIdRequiredException extends BusinessException {

    public UserIdRequiredException() {
        super(MessageResolver.USER_ID_REQUERID_EXCEPTION, 3080L);
    }
}
