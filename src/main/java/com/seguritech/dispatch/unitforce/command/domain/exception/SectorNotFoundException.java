package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;

public class SectorNotFoundException extends BusinessException {
    public SectorNotFoundException(Object... sectorId) {
        super(MessageResolver.SECTOR_NOT_FOUND_EXCEPTION,sectorId, 3061L);
    }
}
