package com.seguritech.dispatch.unitforce.command.domain.repository;

import java.util.List;

public interface CatOperationalPersonnelRepositoryJpa {

    List<String> findOneSignalUsersIdByUnitForceConfigId(String unitForceConfigId);

}
