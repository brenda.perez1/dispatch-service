package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class StageTransitionIdRequieredException extends BusinessException {
    public StageTransitionIdRequieredException() {
        super(MessageResolver.STAGE_TRANSITION_ID_REQUERID_EXCEPTION, 3066L);
    }
}
