package com.seguritech.dispatch.unitforce.command.application.http.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
public class AssignIncidentToUnitMissionHttpRequest implements Serializable {
    private Long missionId;
}
