package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.incident.entity.IncidentEntity;

public interface UnitIncidentEntityRepository extends JpaRepository<IncidentEntity,String>{

}
