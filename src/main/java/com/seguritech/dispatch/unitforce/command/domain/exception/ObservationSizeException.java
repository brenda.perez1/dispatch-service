package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ObservationSizeException  extends BusinessException {
    public ObservationSizeException() {
        super(MessageResolver.OBSERVATION_SIZE_EXCEPTION, 3055L);
    }
}
