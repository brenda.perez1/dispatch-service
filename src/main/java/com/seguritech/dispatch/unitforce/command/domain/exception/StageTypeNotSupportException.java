package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;

public class StageTypeNotSupportException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public StageTypeNotSupportException(Object... catStageId) {
        super(MessageResolver.STAGE_TYPE_NOT_SUPPORT_EXCEPTION,catStageId, 3068L);
    }
	
}
