package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.ArrayList;
import java.util.List;

import com.domain.model.national.entity.IncidentClassificationEntity;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentAdditionalClassificationResponseMin;

public class IncidentAdditionalClassificationMapper {
	
	public static List<IncidentAdditionalClassificationResponseMin> toIncidentAdditionalClassificationResponse(List<IncidentClassificationEntity> additionalNationalSubTypes) {
		List<IncidentAdditionalClassificationResponseMin> list = new ArrayList<IncidentAdditionalClassificationResponseMin>();
		for (IncidentClassificationEntity entity : additionalNationalSubTypes) {
			IncidentAdditionalClassificationResponseMin response = new IncidentAdditionalClassificationResponseMin(
					entity.getId().getId(), 
					entity.getId().getNationalSubTypeId()
		);
			list.add(response);
		}
		return list;
	}

}
