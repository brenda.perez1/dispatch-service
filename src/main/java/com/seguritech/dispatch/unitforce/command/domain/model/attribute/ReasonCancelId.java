package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.ReasonCancelIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class ReasonCancelId {
    public static Long NOT_CANCELED = 0L;
    private Long value;

    private ReasonCancelId(Long value) {
        this.value = value;
    }

    public static ReasonCancelId required(Long id) {
        Optional.ofNullable(id).orElseThrow(ReasonCancelIdRequiredException::new);
        return new ReasonCancelId(id);
    }

    public static ReasonCancelId optional(Long id) {
        return new ReasonCancelId(Optional.ofNullable(id).orElse(NOT_CANCELED));
    }

    public static ReasonCancelId byDefault() {
        return new ReasonCancelId(NOT_CANCELED);
    }

}
