package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;

public interface StageMissionLogRepository {
	
	void save(StageMissionLogModel stageMissionLogModel);

}
