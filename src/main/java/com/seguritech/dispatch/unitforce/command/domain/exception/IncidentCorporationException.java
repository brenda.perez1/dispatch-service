package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public IncidentCorporationException(Object... incidentId) {
        super(MessageResolver.INCIDENT_CORPORATION_EXCEPTION, incidentId, 3043L);  
    }
	
}
