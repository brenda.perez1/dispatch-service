package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class CatStatusNotFoundException extends BusinessException {
	
    public CatStatusNotFoundException() {
        super(MessageResolver.CAT_ESTATUS_NOT_FOUND_EXCEPTION, 3081L);
    }
}
