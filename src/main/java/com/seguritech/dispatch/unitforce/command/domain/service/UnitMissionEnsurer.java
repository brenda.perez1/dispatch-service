package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitMissionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;

public class UnitMissionEnsurer {

    private final UnitMissionRepository repository;

    public UnitMissionEnsurer(UnitMissionRepository repository) {
        this.repository = repository;
    }

    public void ensurer(UnitMissionId unitMissionId) {
        if (!this.repository.exist(unitMissionId))
            throw new UnitMissionNotFoundException(unitMissionId.getValue());
    }

}
