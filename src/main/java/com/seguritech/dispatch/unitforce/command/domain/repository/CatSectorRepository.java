package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;

public interface CatSectorRepository {

    boolean exist(SectorId sectorId);

}
