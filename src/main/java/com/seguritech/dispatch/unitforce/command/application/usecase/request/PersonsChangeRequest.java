package com.seguritech.dispatch.unitforce.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PersonsChangeRequest {
	
	private String unitMissionId;
    private String incidentId;
    private String userId;
    private Long persons;

}
