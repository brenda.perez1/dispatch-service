package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageTypeNotSupportException;
import com.seguritech.dispatch.unitforce.command.domain.model.StageType;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;

public class StageTypeFinder {

    private final CatStageRepository repository;

    public StageTypeFinder(CatStageRepository repository) {
        this.repository = repository;
    }

    public StageType find(CatStageId catStageId) {
        Long stageType = this.repository.findByStageId(catStageId);

        if (stageType == null)
            throw new StageTypeNotSupportException(catStageId.getValue());

        return StageType.valueOf(stageType);
    }

}
