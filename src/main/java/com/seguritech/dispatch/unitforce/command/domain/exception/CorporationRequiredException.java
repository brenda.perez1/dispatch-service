package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class CorporationRequiredException extends BusinessException {
    public CorporationRequiredException() {
        super(MessageResolver.CORPORATION_REQUERID_EXCEPTION_2, 3041L);
    }
}
