package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import java.util.Optional;

import com.seguritech.dispatch.unitforce.command.domain.exception.CatStatusNotFoundException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CatStatusId {

    private String value;
    public static final String INACTIVE = "INACTIVE";
    private static final String ACTIVE = "ACTIVE";
    private static final String AVAILABLE = "DISPONIBLE";
    private static final String NO_AVAILABLE = "NO DISPONIBLE";
    
    private CatStatusId(String value) {
        this.value = value;
    }

    public static CatStatusId required(String value) {
    	Optional.ofNullable(value).orElseThrow(CatStatusNotFoundException::new);

    	if(!ACTIVE.equals(value.toUpperCase()) && !INACTIVE.equals(value.toUpperCase()))
    		throw new CatStatusNotFoundException();

    	String name = ACTIVE.equals(value.toUpperCase()) ? AVAILABLE : NO_AVAILABLE;
    	
    	return new CatStatusId(name);
    }

}
