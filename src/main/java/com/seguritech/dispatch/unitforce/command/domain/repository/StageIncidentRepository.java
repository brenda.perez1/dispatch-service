package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;

public interface StageIncidentRepository {

    boolean exist(IncidentId incidentId);
}
