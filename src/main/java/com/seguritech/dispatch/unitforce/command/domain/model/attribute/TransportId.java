package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.TransportIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class TransportId {
    private Long value;

    public TransportId(Long value) {
        this.value = Optional.ofNullable(value).orElseThrow(TransportIdRequiredException::new);
    }

    public static TransportId required(Long transportId) {
        return new TransportId(transportId);
    }

}
