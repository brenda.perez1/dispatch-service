package com.seguritech.dispatch.unitforce.command.application.usecase;

import java.util.List;

import javax.transaction.Transactional;

import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitStatus;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangerequest;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageType;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageMissionLogRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.service.IncidentEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.ReasonCancelEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.StageEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.StageFinder;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionFinder;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class StageChangeUseCase {

    private final UnitMissionFinder unitMissionFinder;
    private final UnitMissionRepository repository;
    private final StageMissionLogRepository logRepository;
    private final DomainEventPublisher domainEventPublisher;
    private final ReasonCancelEnsurer reasonCancelEnsurer;
    private final UnitMissionEnsurer unitMissionEnsurer;
    private final IncidentEnsurer incidentEnsurer;
    private final StageFinder nextStageFinder;
    private final StageEnsurer stageEnsurer;
    private final UnitConfigEntityRepository unitConfigEntityRepository;
    private final IncidentRepository incidentRepository;

    public StageChangeUseCase(
            UnitMissionRepository unitMissionRepository, 
            DomainEventPublisher domainEventPublisher, 
            ReasonCancelRepository reasonCancelEnsurer, 
            UnitForceConfigRepository forceConfigRepository, 
            StageIncidentRepository stageIncidentRepository, 
            StageRepository nextStageRepository, 
            CatStageRepository catStageRepository, 
            StageMissionLogRepository logRepository,
            UnitConfigEntityRepository unitConfigEntityRepository,
            IncidentRepository incidentRepository) {
        this.unitMissionFinder = new UnitMissionFinder(unitMissionRepository);
        this.repository = unitMissionRepository;
        this.domainEventPublisher = domainEventPublisher;
        this.reasonCancelEnsurer = new ReasonCancelEnsurer(reasonCancelEnsurer);
        this.nextStageFinder = new StageFinder(nextStageRepository);
        this.incidentEnsurer = new IncidentEnsurer(stageIncidentRepository);
        this.stageEnsurer = new StageEnsurer(catStageRepository);
        this.logRepository = logRepository;
        this.unitMissionEnsurer = new UnitMissionEnsurer(repository);
        this.unitConfigEntityRepository = unitConfigEntityRepository;
        this.incidentRepository = incidentRepository;
    }

    @Transactional
    public void execute(StageChangerequest request) {
        
        UnitMissionId unitMissionId = UnitMissionId.required(request.getUnitMissionId());

        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        IncidentBounded sheetEmergency = incidentRepository.find(incidentId);
        CatStageId nextStageId = CatStageId.required(request.getNextStageId());
        ReasonCancelId reasonCancelId = ReasonCancelId.optional(request.getReasonCancelId());
        Observations observations = Observations.optional(request.getObservations());
        List<String> unitForces =  this.repository.find(incidentId);
        
        this.ensurers(unitMissionId, incidentId, reasonCancelId, nextStageId);

        UnitMissionModel unitMissionModel = this.unitMissionFinder.find(unitMissionId, incidentId);
        StageId lastStageId = unitMissionModel.getCurrentStageId();
        MissionId missionId = unitMissionModel.getCurrentStageId().getMissionId();
        StageId nextStageIdConfig = this.nextStageFinder.find(missionId, lastStageId, nextStageId);
        
        unitMissionModel.changedToNextStage(
        		nextStageIdConfig,
                UserId.required(request.getUserId()),
                reasonCancelId,
                observations,
                unitMissionModel.getPersons(),
                sheetEmergency.getFolio(),
                "1",
                unitForces
                
        );	
        
        StageMissionLogModel stageMissionLogModel = new StageMissionLogModel(
        		null,
        		unitMissionModel.getCreatedByUser().getValue(),
        		null,
        		unitMissionModel.getUpdatedByUser().getValue(),
        		null,
        		new StageModel(nextStageIdConfig.getNextStageId().getValue()),
        		nextStageIdConfig.getStageTransitionId().getValue(),
        		unitMissionModel.getPersons().getValue(),
        		request.getObservations(),
        		request.getReasonCancelId(), 
        		missionId.getValue(), 
        		incidentId.getValue(),
        		unitMissionModel.getUnitForceConfigId(), 
        		unitMissionModel.getUnitMissionId()
        );
        this.logRepository.save(stageMissionLogModel);
        this.repository.save(unitMissionModel, lastStageId);
        updateStatusUnitForce(nextStageIdConfig, unitMissionModel);
        this.domainEventPublisher.publish(unitMissionModel.pullEvents());
        
        
    }

	private void ensurers(UnitMissionId unitMissionId, IncidentId  incidentId, ReasonCancelId reasonCancelId, CatStageId nextStageId) {
    	this.unitMissionEnsurer.ensurer(unitMissionId);
        this.incidentEnsurer.ensurer(incidentId);
        this.reasonCancelEnsurer.ensurer(reasonCancelId);
        this.stageEnsurer.ensurer(nextStageId);
    }
	
	private void updateStatusUnitForce(StageId nextStage, UnitMissionModel unitMissionModel) {
	     if(StageType.FINALIZE.getValue() == nextStage.getStageTransitionId().getValue()) {
	       this.unitConfigEntityRepository.updateStatusUnitForce(unitMissionModel.getUnitForceConfigId().getValue(), new StatusEntity(UnitStatus.AVAILABLE.getValue()));
	     } 
	}
	
}
