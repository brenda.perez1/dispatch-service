package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class MissionStageIdRequiredException extends BusinessException {

    public MissionStageIdRequiredException() {
        super(MessageResolver.MISSION_STAGE_ID_REQUERID_EXCEPTION, 3054L);
    }
}
