package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitNotFoundException extends BusinessException {

    public UnitNotFoundException(Object... unitForceConfigId) {
        super(MessageResolver.UNIT_NOT_FOUND_EXCEPTION, unitForceConfigId, 3078L);
    }
}
