package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;

public interface CatCorporationRepository {
    boolean exist (CorporationOperativeId corporationOperativeId);
}
