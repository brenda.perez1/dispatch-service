package com.seguritech.dispatch.unitforce.command.domain.service;

import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitCorporationNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitCorporationRepository;
import com.seguritech.platform.Service;

@Service
public class UnitCorporationFinder {

    private final UnitCorporationRepository unitCorporationRepository;

    public UnitCorporationFinder(UnitCorporationRepository unitCorporationRepository) {
        this.unitCorporationRepository = unitCorporationRepository;
    }

    public CorporationOperativeId findCorporationOperativeId(UnitForceConfigId unitForceConfigId) {
        CorporationOperativeId corporationOperativeId = this.unitCorporationRepository.findCorporationOperativeId(unitForceConfigId);

        if (corporationOperativeId == null)
            throw new UnitCorporationNotFoundException(unitForceConfigId.getValue());

        return corporationOperativeId;
    }
    
    public CorporationId findCorporationId(UnitForceConfigId unitForceConfigId) {
    	CorporationId corporationId = this.unitCorporationRepository.findCorporationId(unitForceConfigId);

        if (corporationId == null)
            throw new UnitCorporationNotFoundException(unitForceConfigId.getValue());

        return corporationId;
    }
    
    
    public UnitForceConfigEntity getUnitForceConfig(UnitForceConfigId unitForceConfigId) {
    	UnitForceConfigEntity unitForceConfigEntity = this.unitCorporationRepository.get(unitForceConfigId);

        if (unitForceConfigEntity == null)
            throw new UnitForceConfigNotFoundException(unitForceConfigId.getValue());

        return unitForceConfigEntity;
    }

}
