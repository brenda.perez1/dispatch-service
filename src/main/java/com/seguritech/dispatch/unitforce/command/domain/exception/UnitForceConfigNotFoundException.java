package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitForceConfigNotFoundException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public UnitForceConfigNotFoundException(Object... unitForceConfigId) {
        super(MessageResolver.UNIT_FORCE_CONFIG_NOT_FOUND_EXCEPTION, unitForceConfigId, 3073L);
    }
    
}
 