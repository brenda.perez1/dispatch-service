package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnavailabilityReasonNotFoundException extends BusinessException {
	
    public UnavailabilityReasonNotFoundException() {
        super(MessageResolver.UNAVAILABILITY_REASON_NOT_FOUND_EXCEPTION, 3081L);
    }
}
