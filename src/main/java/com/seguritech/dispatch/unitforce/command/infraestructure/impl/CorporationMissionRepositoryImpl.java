package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import javax.persistence.EntityNotFoundException;

import org.springframework.stereotype.Repository;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.mission.entity.CorporationMission;
import com.domain.model.mission.entity.CorporationMissionPK;
import com.domain.model.mission.entity.Mission;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CorporationMissionEntityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;

@Repository
public class CorporationMissionRepositoryImpl implements CorporationMissionRepository {

    private final CorporationMissionEntityRepository repository;
    private final UnitConfigEntityRepository unitConfigEntityRepository;

    public CorporationMissionRepositoryImpl(CorporationMissionEntityRepository repository, UnitConfigEntityRepository unitConfigEntityRepository) {
        this.repository = repository;
        this.unitConfigEntityRepository = unitConfigEntityRepository;
    }

    @Override
    public boolean exist(MissionId missionId, CorporationId unitCorporationOperativeId) {
        return this.repository.findByCorporationAndMission(unitCorporationOperativeId.getValue(), missionId.getValue()).isPresent();
    }

    @Override
    public StageId findStageId(CorporationId unitCorporationOperativeId, MissionId missionId) {
    	CorporationMission entity = 
    			this.repository.findByCorporationAndMission(unitCorporationOperativeId.getValue(), missionId.getValue())   			
    			.filter(CorporationMission::getActive).orElseThrow(() ->
    	         new EntityNotFoundException());
        
//    	
//    	MissionStageConfig missionStageConfig = entity.getMissionStageConfig();
//    	MissionStageConfigPk missionStageConfigPk = missionStageConfig.getId();
//        Stage stage = missionStageConfigPk.getStage();
//        Mission mission = missionStageConfigPk.getMission();
//        StageTransition stageTransition = missionStageConfigPk.getStageTransition();
        
        Long catStageId  =  entity.getMissionStageConfig().getStage().getId();
        String catStageName = entity.getMissionStageConfig().getStage().getName();
        Long missionId_  =   entity.getMissionStageConfig().getMission().getId();
        Long stageTransitionId =  Long.valueOf(entity.getMissionStageConfig().getStageTransition().getId());
        		
         return  StageId.required(
        		 catStageId,
        		 catStageName,
        		 missionId_,
        		 stageTransitionId
                );
    }

    @Override
    public MissionId find(CorporationId corporationOperativeId) {
        return this.repository.findByCorporationAndPreconfiguredMission(corporationOperativeId.getValue(), true)
                .map(CorporationMission::getId)
                .map(CorporationMissionPK::getMission)
                .map(Mission::getId)
                .map(MissionId::required).orElse(null);
    }

    @Override
    public MissionId find(UnitForceConfigId unitForceConfigId) {
        UnitForceConfigEntity entity = this.unitConfigEntityRepository
                .findById(unitForceConfigId.getValue()).orElse(null);

        if (entity == null)
            return null;

        return this.repository.findByCorporationAndPreconfiguredMission(entity.getCorporationEntity().getId(), true)
                .map(CorporationMission::getId)
                .map(CorporationMissionPK::getMission)
                .map(Mission::getId)
                .map(MissionId::required).orElse(null);
    }
    
}
