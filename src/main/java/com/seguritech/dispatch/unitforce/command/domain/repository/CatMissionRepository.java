package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;

public interface CatMissionRepository {

    boolean exist(MissionId missionId);

}
