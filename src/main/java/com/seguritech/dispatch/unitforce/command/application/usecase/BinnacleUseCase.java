package com.seguritech.dispatch.unitforce.command.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangeLogRequest;
import com.seguritech.dispatch.unitforce.command.domain.model.BinnacleModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.BinnacleRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatCorporationRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatSectorRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageTransitionRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.service.CorporationEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.IncidentEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.MissionEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.ReasonCancelEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.SectorEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.StageEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.StageTransitionEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitForceConfigEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class BinnacleUseCase {

    private final BinnacleRepository repository;
    private final DomainEventPublisher domainEventPublisher;
    private final UnitMissionEnsurer unitMissionEnsurer;
    private final StageEnsurer stageEnsurer;
    private final UnitForceConfigEnsurer forceConfigEnsurer;
    private final IncidentEnsurer incidentEnsurer;
    private final ReasonCancelEnsurer cancelEnsurer;
    private final SectorEnsurer sectorEnsurer;
    private final CorporationEnsurer corporationEnsurer;
    private final MissionEnsurer missionEnsurer;
    private final StageTransitionEnsurer stageTransitionEnsurer;

    public BinnacleUseCase(
            BinnacleRepository repository,
            DomainEventPublisher domainEventPublisher,
            UnitMissionRepository unitMissionRepository,
            CatStageRepository preconfiguredRepository,
            UnitForceConfigRepository forceConfigRepository,
            StageIncidentRepository incidentRepository,
            ReasonCancelRepository cancelRepository,
            CatSectorRepository catSectorRepository,
            CatCorporationRepository catCorporationRepository,
            CatMissionRepository catMissionRepository,
            StageTransitionRepository stageTransitionRepository) {
        this.domainEventPublisher = domainEventPublisher;
        this.repository = repository;
        this.unitMissionEnsurer = new UnitMissionEnsurer(unitMissionRepository);
        this.stageEnsurer = new StageEnsurer(preconfiguredRepository);
        this.forceConfigEnsurer = new UnitForceConfigEnsurer(forceConfigRepository);
        this.incidentEnsurer = new IncidentEnsurer(incidentRepository);
        this.cancelEnsurer = new ReasonCancelEnsurer(cancelRepository);
        this.sectorEnsurer = new SectorEnsurer(catSectorRepository);
        this.corporationEnsurer = new CorporationEnsurer(catCorporationRepository);
        this.missionEnsurer = new MissionEnsurer(catMissionRepository);
        this.stageTransitionEnsurer = new StageTransitionEnsurer(stageTransitionRepository);
    }

    public void create(StageChangeLogRequest request) {
        SectorId sectorId = SectorId.required(request.getSectorId());
        CorporationOperativeId corporationOperativeId = CorporationOperativeId.required(request.getCorporationId());
        UnitMissionId unitMissionId = UnitMissionId.required(request.getUnitMissionId());
        MissionId missionId = MissionId.required(request.getMissionId());
        CatStageId catStageId = CatStageId.required(request.getCatStageId());
        UnitForceConfigId unitForceConfigId = UnitForceConfigId.required(request.getUnitForceConfigId());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        ReasonCancelId reasonCancelId = ReasonCancelId.required(request.getReasonCancelId());
        StageTransitionId stageTransitionId = StageTransitionId.required(request.getIdStageTransition());

        this.ensurers(sectorId, corporationOperativeId, unitMissionId, missionId, catStageId, unitForceConfigId, incidentId, reasonCancelId, stageTransitionId);

        UserId userId = UserId.required(request.getCreatedByUser());
        Observations observations = Observations.optional(request.getObservations());
        Persons persons = Persons.optional(request.getPersons());

        BinnacleModel model = BinnacleModel.create(
                sectorId,
                corporationOperativeId,
                unitMissionId,
                missionId,
                catStageId,
                unitForceConfigId,
                incidentId,
                reasonCancelId,
                stageTransitionId,
                userId,
                observations,
                persons,
                request.getCreatedAt()
        );

        this.repository.save(model);
        this.domainEventPublisher.publish(model.pullEvents());
    }

    private void ensurers(SectorId sectorId, CorporationOperativeId corporationOperativeId, UnitMissionId unitMissionId, MissionId missionId, CatStageId catStageId, UnitForceConfigId unitForceConfigId, IncidentId incidentId, ReasonCancelId reasonCancelId, StageTransitionId stageTransitionId) {
        this.sectorEnsurer.ensurer(sectorId);
        this.corporationEnsurer.ensurer(corporationOperativeId);
        this.unitMissionEnsurer.ensurer(unitMissionId);
        this.missionEnsurer.ensurer(missionId);
        this.stageEnsurer.ensurer(catStageId);
        this.forceConfigEnsurer.ensurer(unitForceConfigId);
        this.incidentEnsurer.ensurer(incidentId);
        this.cancelEnsurer.ensurer(reasonCancelId);
        this.stageTransitionEnsurer.ensurer(stageTransitionId);

    }

}
