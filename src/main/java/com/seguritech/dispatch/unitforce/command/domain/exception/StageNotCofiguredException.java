package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class StageNotCofiguredException extends BusinessException {
	
	private static final long serialVersionUID = 1L;
    public StageNotCofiguredException(Object... missionId) {
        super(MessageResolver.STAGE_NOT_CONFIGURED_EXCEPTION,missionId, 3063L);
    }
	
}
