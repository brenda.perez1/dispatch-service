package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StagePreconfiguredNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageRepository;

public class StageFinder {

    private final StageRepository stageRepository;

    public StageFinder(StageRepository stageRepository) {
        this.stageRepository = stageRepository;
    }

    public StageId find(MissionId missionId, CatStageId nextStageId) {
        StageId stageId = this.stageRepository.find(missionId, nextStageId);

        if (stageId == null)
            throw new StagePreconfiguredNotFoundException(nextStageId.getValue());
        
        return stageId;
    }
    
    public StageId find(MissionId missionId, StageId lastStageId, CatStageId nextStageId ) {
        StageId stageId = this.stageRepository.find(missionId, lastStageId, nextStageId);

        if (stageId == null)
            throw new StagePreconfiguredNotFoundException(nextStageId.getValue());
        return stageId;
    }
    
}
