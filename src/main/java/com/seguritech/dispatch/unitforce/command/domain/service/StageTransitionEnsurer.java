package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageTrasitionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageTransitionRepository;

public class StageTransitionEnsurer {

    private final StageTransitionRepository stageTransitionRepository;

    public StageTransitionEnsurer(StageTransitionRepository stageTransitionRepository) {
        this.stageTransitionRepository = stageTransitionRepository;
    }

    public void ensurer(StageTransitionId stageTransitionId) {
        if (!this.stageTransitionRepository.exist(stageTransitionId))
            throw new StageTrasitionNotFoundException(stageTransitionId.getValue());
    }

}
