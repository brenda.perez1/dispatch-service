package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentSectorNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.repository.DispatchIncidentSectorRepository;
import com.seguritech.platform.Service;

@Service
public class IncidentSectorEnsurer {

    private final DispatchIncidentSectorRepository incidentSectorRepository;

    public IncidentSectorEnsurer(DispatchIncidentSectorRepository incidentSectorRepository) {
        this.incidentSectorRepository = incidentSectorRepository;
    }

    public void ensurer(IncidentId incidentId, SectorId unitSectorId) {
        if (!this.incidentSectorRepository.exist(incidentId, unitSectorId))
            throw new IncidentSectorNotFoundException(incidentId);
    }

}
