package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UnitMissionSearch {

	private String unitMissionId;
	private Date startMissionDate;
	private Date assignedAt;
	private String assignedByUser;
	private Date endMissionDate;
	private Date lastUpdate;
	private Long missionId;
	private String missionName;
	private String missionDescription;
	private String unitForceConfigId;
	private String unitForceConfigAlias;
	private Long corporationId;
	private String corporationName;
	private Long personsNumber;
	private Boolean unitForceConfigVisible;
	private Long transportId;
	private String transportName;
	private String transportDescription;
	private String transportEconomicNumber;
	private String idMissionStageConfig;
	private String backgroundColorActualStage;
	private String iconActualStage;
	private Long stageId;
	private String stageName;
	private String textColorActualStage;
	private Long transitionId;
	private String stageTransitionDesc;
	private String stageMissionLogs;
	private String nextStages;
	private Long total;

}
