package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;

public interface StageRepository {
	
    StageId find(MissionId missionId, CatStageId nextStageId);
    
    StageId find(MissionId missionId, StageId lastStageId, CatStageId nextStageId);

}
