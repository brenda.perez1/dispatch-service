package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.CorporationRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class CorporationOperativeId {
    private Long value;

    private CorporationOperativeId(Long value) {
        this.value = value;
    }

    public static CorporationOperativeId required(Long id) {
        Optional.ofNullable(id).orElseThrow(CorporationRequiredException::new);
        return new CorporationOperativeId(id);
    }

}
