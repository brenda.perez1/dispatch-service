package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;

public interface UnitCorporationRepository {
    CorporationOperativeId findCorporationOperativeId(UnitForceConfigId unitForceConfigId);
    CorporationId findCorporationId(UnitForceConfigId unitForceConfigId);
    UnitForceConfigEntity get(UnitForceConfigId unitForceConfigId);

    


}
