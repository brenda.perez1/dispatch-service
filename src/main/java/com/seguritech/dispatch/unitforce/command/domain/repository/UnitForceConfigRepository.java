package com.seguritech.dispatch.unitforce.command.domain.repository;

import java.util.List;

import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;

public interface UnitForceConfigRepository {

    boolean ensurer(UnitForceConfigId unitForceConfigId);

    boolean available(UnitForceConfigId unitForceConfigId);
    
    List<UnitForceConfigEntity> findCorporationOperatives(List<Long> corporationOperativesIds);
    
}
