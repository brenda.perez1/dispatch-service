package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.domain.model.mission.entity.StatusEntity;

public interface CatStatusRepository extends JpaRepository<StatusEntity, Long> {

	@Query(value = "SELECT se FROM StatusEntity se WHERE upper(se.name)=:name")
	StatusEntity findByName(String name);	

}

 