package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;

public interface CorporationIncidentEntityRepository extends JpaRepository<IncidentCorporationEntity, IncidentCorporationEntityPk> {
	
	Optional<IncidentCorporationEntity> findByIdAndStatus(IncidentCorporationEntityPk id, String status);
}
