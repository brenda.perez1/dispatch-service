package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.unitmission.StageMissionLogEntity;

public interface BinnacleEntityRepository extends JpaRepository<StageMissionLogEntity, Long> {
}
