package com.seguritech.dispatch.unitforce.command.application.usecase;

import javax.transaction.Transactional;

import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitStatus;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.PersonsChangeRequest;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageType;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageMissionLogRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionFinder;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class PersonChangeUseCase {
	
	private final UnitMissionFinder unitMissionFinder;
    private final UnitMissionRepository repository;
    private final StageMissionLogRepository logRepository;
    private final DomainEventPublisher domainEventPublisher;
    private final UnitConfigEntityRepository unitConfigEntityRepository;
    
    public PersonChangeUseCase(    		
            UnitMissionRepository repository,
            StageMissionLogRepository logRepository,
            DomainEventPublisher domainEventPublisher,
            UnitConfigEntityRepository unitConfigEntityRepository) {
        this.unitMissionFinder = new UnitMissionFinder(repository);
        this.repository = repository;
        this.logRepository = logRepository;        
        this.domainEventPublisher = domainEventPublisher;
        this.unitConfigEntityRepository = unitConfigEntityRepository;
    }
    
    @Transactional
    public void execute(PersonsChangeRequest request) {
    	
    	UnitMissionId unitMissionId = UnitMissionId.required(request.getUnitMissionId());
    	IncidentId incidentId = IncidentId.required(request.getIncidentId());
//    	CatStageId nextStageId = CatStageId.prePersonfigured();
        
        UnitMissionModel unitMissionModel = this.unitMissionFinder.find(unitMissionId, incidentId);
//    	MissionId missionId = unitMissionModel.getCurrentStageId().getMissionId();
//    	StageId nextStageIdConfig = new StageId(nextStageId, missionId, StageTransitionId.required(StageType.TRANSLATION.getValue()));
        
    	
        unitMissionModel.changedToPersonNextStage(
                UserId.required(request.getUserId()),
                Persons.optional(request.getPersons())
        );	
        
//        StageMissionLogModel stageMissionLogModel = new StageMissionLogModel(
//        		null,
//        		unitMissionModel.getCreatedByUser().getValue(),
//        		null,
//        		unitMissionModel.getUpdatedByUser().getValue(),
//        		null,
//        		new StageModel(nextStageIdConfig.getNextStageId().getValue()),
//        		nextStageIdConfig.getStageTransitionId().getValue(),
//        		request.getPersons(),
//        		null,
//        		null,
//        		missionId.getValue(), 
//        		incidentId.getValue(),
//        		unitMissionModel.getUnitForceConfigId(), 
//        		unitMissionModel.getUnitMissionId()
//        );
//        this.logRepository.save(stageMissionLogModel);
//        updateStatusUnitForce(unitMissionModel);
        this.repository.save(unitMissionModel,null);
        this.domainEventPublisher.publish(unitMissionModel.pullEvents());
    }
    
    private void updateStatusUnitForce(UnitMissionModel unitMissionModel) {
    	this.unitConfigEntityRepository.updateStatusUnitForce(unitMissionModel.getUnitForceConfigId().getValue(), new StatusEntity(UnitStatus.IN_SERVICE.getValue()));
	}

}
