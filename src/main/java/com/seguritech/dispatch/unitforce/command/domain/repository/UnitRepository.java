package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;

public interface UnitRepository {

    boolean exists (UnitForceConfigId unitForceConfigId);
}
