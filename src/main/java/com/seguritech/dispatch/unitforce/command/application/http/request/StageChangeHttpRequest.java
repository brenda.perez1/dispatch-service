package com.seguritech.dispatch.unitforce.command.application.http.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StageChangeHttpRequest {
	
    private Long missionId;
    private Long nextStageId;
    private Long reasonCancelId;
    private String observations;    
    
}
