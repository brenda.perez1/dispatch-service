package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.domain.model.geofence.entity.Geofence;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitSectorRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;

@Repository
public class UnitSectorRepositoryImpl implements UnitSectorRepository {

    private final UnitConfigEntityRepository repository;

    public UnitSectorRepositoryImpl(UnitConfigEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public SectorId find(UnitForceConfigId unitForceConfigId) {
        return this.repository.findById(unitForceConfigId.getValue())
                .map(UnitForceConfigEntity::getGeofence)
                .map(Geofence::getId)
                .map(id -> SectorId.required(id)).orElse(null);
    }
}
