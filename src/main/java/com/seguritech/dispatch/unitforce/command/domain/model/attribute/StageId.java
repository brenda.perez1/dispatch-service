package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageNotCofiguredException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@Getter
public class StageId {

    private CatStageId nextStageId;
    private String nextStageName;
    private MissionId missionId;
    private StageTransitionId stageTransitionId;

    public StageId(CatStageId nextStageId, String nextStageName, MissionId missionId, StageTransitionId stageTransitionId) {
        this.nextStageId = nextStageId;
        this.nextStageName = nextStageName;
        this.missionId = missionId;
        this.stageTransitionId = stageTransitionId;
    }

    public static StageId required(Long nextStageId, String nextStageName, Long missionId, Long stageTransitionId) {
    	
    	if ((nextStageId == null || nextStageId == 0L)
                || (missionId == null || missionId == 0L)
                || (stageTransitionId == null || stageTransitionId == 0L))
            throw new StageNotCofiguredException(missionId);

        return new StageId(
                CatStageId.required(nextStageId),
                nextStageName,
                MissionId.required(missionId),
                StageTransitionId.required(stageTransitionId)
        );
    }
    
}
