package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.mission.entity.Mission;
import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.MissionStageConfigEntityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitMissionEntityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitMissionMapper;

@Repository
public class UnitMissionRepositoryImpl implements UnitMissionRepository {

    private final UnitMissionEntityRepository repository;
    @Autowired
    private final MissionStageConfigEntityRepository stageConfigEntityRepository;

    public UnitMissionRepositoryImpl(UnitMissionEntityRepository repository, MissionStageConfigEntityRepository stageConfigEntityRepository) {
        this.repository = repository;
        this.stageConfigEntityRepository = stageConfigEntityRepository;
    }

    @Override
    public void save(UnitMissionModel unitMissionModel, StageId stageLastId) {
    	MissionStageConfig  missionStageConfig = null;
    	long missionId =  unitMissionModel.getCurrentStageId().getMissionId().getValue();
    	long stageId =  unitMissionModel.getCurrentStageId().getNextStageId().getValue();
    	long stageTransitionId =  unitMissionModel.getCurrentStageId().getStageTransitionId().getValue();
    	Long lastStageId = stageLastId == null ? null : stageLastId.getNextStageId().getValue();
    	missionStageConfig = stageConfigEntityRepository.findFirstByMissionIdAndStageIdAndStageTransitionId(missionId,stageId,stageTransitionId, lastStageId).get();
        this.repository.save(UnitMissionMapper.toEntity(unitMissionModel, missionStageConfig));
    }

    @Override
    public boolean exist(UnitMissionId unitMissionId) {
        return this.repository.findByIdUnitMissionId(unitMissionId.getValue()).isPresent();
    }

    @Override
    public UnitMissionModel find(UnitForceConfigId unitForceConfigId, IncidentId incidentId) {
        return this.repository.findByIdUnitForceConfigAndIdEmergencyId(
        		new UnitForceConfigEntity(unitForceConfigId.getValue()), 
    			new IncidentEntity(incidentId.getValue())
        )
        .filter(UnitMissionEntity::getActive)
        .map(entity -> this.toModel(entity)).orElse(null);
    }
    
    @Override
	public UnitMissionModel find(UnitMissionId unitMissionId, IncidentId incidentId) {
    	return this.repository.findByIdUnitMissionIdAndIdEmergencyId(unitMissionId.getValue(), new IncidentEntity(incidentId.getValue()))
                .filter(UnitMissionEntity::getActive)
                .map(entity -> this.toModel(entity)).orElse(null);
	}

	public UnitMissionModel toModel(UnitMissionEntity entity) {
        Long missionId = entity.getMissionStageConfig().getMission().getId();
        Long lastStageId = entity.getMissionStageConfig().getStage().getId();
        String lastStageName = entity.getMissionStageConfig().getStage().getName();
        
        List<MissionStageConfig>  missionStageConfigList = this.stageConfigEntityRepository
        		.findByMissionAndStageLast(new Mission(missionId), lastStageId)
        		.filter(Objects::nonNull)
        		.orElse(Collections.emptyList());
        
        List<StageId> stageList  = missionStageConfigList.stream()
                .map(m -> StageId.required(
                        m.getStage().getId(),
                        m.getStage().getName(),
                        m.getMission().getId(),
                        m.getStageTransition().getId() + 0L
                ))
                .collect(Collectors.toList());

        return new UnitMissionModel(
                UnitMissionId.required(entity.getId().getUnitMissionId()),
                null,//SectorId.required(entity.getId().getUnitForceConfig().getGeofence().getId()),
                CorporationId.required(entity.getId().getUnitForceConfig().getCorporation().getId()),
                CorporationOperativeId.required(entity.getId().getUnitForceConfig().getCorporationEntity().getId()),
                stageList                ,
                StageId.required(
                		lastStageId,
                		lastStageName,
                        missionId,
                        entity.getMissionStageConfig().getStageTransition().getId() + 0L
                ),
                UnitForceConfigId.required(entity.getId().getUnitForceConfig().getIdUnitForceConfig()),
                IncidentId.required(entity.getId().getEmergencyId().getId()),
                UserId.required(entity.getCreatedBy()),
                UserId.required(entity.getUpdatedBy()),
                entity.getCreationDate(),
                entity.getUpdateDate(),
                entity.getStartMissionDate(),
                entity.getEndMissionDate(),
                ReasonCancelId.required(entity.getReasonCancel().getId()),
                Observations.optional(entity.getObservations()),
                Persons.optional(entity.getPersonsNumber()),
                entity.getActive(),
                null, 
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

    private List<CatStageId> searchNextStages(Long parentId) {
        return new ArrayList<CatStageId>() {{
            add(CatStageId.required(2L));
            add(CatStageId.required(3L));
            add(CatStageId.required(4L));
            add(CatStageId.required(5L));
        }};
    }

	@Override
	public List<UnitMissionModel> findByUnitForceAndIncident(UnitForceConfigId unitForceConfigId,
			IncidentId incidentId) {
		// TODO Auto-generated method stub
		return this.repository.findAllByIdUnitForceConfigAndIdEmergencyIdAndActive(new UnitForceConfigEntity(unitForceConfigId.getValue()), new IncidentEntity(incidentId.getValue()), Boolean.TRUE).stream().map(this::toModel).collect(Collectors.toList());
	}

	@Override
	public List<String> find(IncidentId incidentId) {
		return Optional.ofNullable(this.repository.findByEmergencyId(new IncidentEntity(incidentId.getValue()))).orElse(null);
	}

	@Override
	public Long countUnitMission(IncidentId incidentId, com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId corporationId) {
		return Optional.ofNullable(this.repository.countUnitMission(new IncidentEntity(incidentId.getValue()), new Corporation(corporationId.getValue()))).orElse(null);
	}

}
