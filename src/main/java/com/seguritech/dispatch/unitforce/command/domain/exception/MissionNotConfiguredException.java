package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;

public class MissionNotConfiguredException extends BusinessException {

	public MissionNotConfiguredException(Object... corporationOperativeId) {
        super(MessageResolver.MISSION_NOT_CONFIGURED_EXCEPTION,corporationOperativeId, 3052L);
    }
    
    public MissionNotConfiguredException(CorporationId corporationOperativeId) {
        super(String.format("Misión no configurada para la corporación seleccionada %s", corporationOperativeId.getValue()), 0L);
    }

    public MissionNotConfiguredException() {
        super(String.format("Misión no configurada"), 0L);
    }
}
