package com.seguritech.dispatch.unitforce.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class StageChangeLogRequest {
    private Long sectorId;
    private Long corporationId;
    private String unitMissionId;
    private Long missionId;
    private Long catStageId;
    private String unitForceConfigId;
    private String incidentId;
    private String createdByUser;
    private String updatedByUser;
    private Date createdAt;
    private Date updateAt;
    private Date startMissionDate;
    private Date endMissionDate;
    private Long reasonCancelId;
    private String observations;
    private Long persons;
    private boolean active;
    private Long idStageTransition;
}
