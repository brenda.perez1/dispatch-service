package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class CatStageId {
    private static final Long INITIAL_STAGE_ID = 0L;
    private static final Long TRANSLATION_STAGE_ID = 28L;
    private static final Long CANCEL_STAGE_ID = 30L;

    private Long value;

    private CatStageId(Long value) {
        this.value = value;
    }

    public static CatStageId required(Long id) {
        return new CatStageId(id);
    }

    public static CatStageId preconfigured() {
        return new CatStageId(INITIAL_STAGE_ID);
    }
    
    public static CatStageId prePersonfigured() {
        return new CatStageId(TRANSLATION_STAGE_ID);
    }
    
    public static CatStageId preCancelconfigured() {
        return new CatStageId(CANCEL_STAGE_ID);
    }

    public static CatStageId optional(Long id) {
        return new CatStageId(Optional.ofNullable(id).orElse(INITIAL_STAGE_ID));
    }
}
