package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.domain.model.operationalpersonnel.entity.OperationalPersonnel;

public interface CatOperationalPersoneelRepository extends JpaRepository<OperationalPersonnel, Long> {

	@Query(value = " SELECT DISTINCT cop.one_signal_user FROM {h-schema}cat_operational_personnel cop WHERE cop.id_unit_force_config = :unitForceConfigId ", nativeQuery = true)
	List<String> findOneSignalUsersIdByUnitForceConfigId(String unitForceConfigId);	

}
