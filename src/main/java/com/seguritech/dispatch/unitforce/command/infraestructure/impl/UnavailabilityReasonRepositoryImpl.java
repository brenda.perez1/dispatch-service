package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnavailabilityReasonNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnavailabilityReasonId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnavailabilityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnavailabilityReasonEntityRepository;

import lombok.AllArgsConstructor;

@Repository
@AllArgsConstructor
public class UnavailabilityReasonRepositoryImpl implements UnavailabilityRepository {

    private final UnavailabilityReasonEntityRepository repository;

    @Override
    public void ensurer(UnavailabilityReasonId unavailabilityReasonId) {
        if(!this.repository.existsById(unavailabilityReasonId.getValue())) {
        	throw new UnavailabilityReasonNotFoundException();
        }        	
    }
    
}
