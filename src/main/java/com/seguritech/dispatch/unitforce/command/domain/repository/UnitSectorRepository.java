package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;

public interface UnitSectorRepository {

    SectorId find(UnitForceConfigId unitForceConfigId);

}
