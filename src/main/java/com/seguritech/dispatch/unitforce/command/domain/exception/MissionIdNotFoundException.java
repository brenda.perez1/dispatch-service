package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class MissionIdNotFoundException extends BusinessException {

    public MissionIdNotFoundException(Object... missionId) {
        super(MessageResolver.MISSION_ID_NOT_FOUND_EXCEPTION, missionId, 3050L);
    }
}
