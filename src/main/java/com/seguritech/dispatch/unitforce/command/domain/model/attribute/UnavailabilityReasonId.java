package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import java.util.Optional;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnavailabilityReasonRequiredException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class UnavailabilityReasonId {
	
    private Long value;
    
    public UnavailabilityReasonId(Long value) {
        this.value = value;
    }

    public static UnavailabilityReasonId required(Long id) {
        Optional.ofNullable(id).orElseThrow(UnavailabilityReasonRequiredException::new);
        return new UnavailabilityReasonId(id);
    }

}
