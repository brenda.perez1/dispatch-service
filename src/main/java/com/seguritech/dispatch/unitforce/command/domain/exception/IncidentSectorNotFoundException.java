package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentSectorNotFoundException extends BusinessException {
    
	public IncidentSectorNotFoundException(Object... incidentId) {
        super(MessageResolver.INCIDENT_SECTOR_NOT_FOUND_EXCEPTION, incidentId, 3047L);
    }
}
