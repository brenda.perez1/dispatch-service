package com.seguritech.dispatch.unitforce.command.application.listener;

//TODO:Validar si el registro en el evento de cambio de etapa será necesario persistirlo, dado que por cada tipo de etapa se persiste una bitácora
/*
@Component
@DomainSourceEventListener(domain = "incidents-service")
public class StageChangedListener {

    private final BinnacleUseCase useCase;

    public StageChangedListener(BinnacleUseCase useCase) {
        this.useCase = useCase;
    }

    @DomainEventListener(name = "stage-changed-event", version = "v1")
    public void onAssignedIncidents(DomainEventEnvelope<StageChangedEvent> evt) {
        StageChangedEvent event = evt.getPayload();
        useCase.create(new StageChangeLogRequest(
                event.getSectorId(),
                event.getCorporationId(),
                event.getUnitMissionId(),
                event.getMissionId(),
                event.getStageId(),
                event.getUnitForceConfigId(),
                event.getIncidentId(),
                event.getCreatedByUser(),
                event.getUpdatedByUser(),
                event.getCreatedAt(),
                event.getUpdateAt(),
                event.getStartMissionDate(),
                event.getEndMissionDate(),
                event.getReasonCancelId(),
                event.getObservations(),
                event.getPersons(),
                event.isActive(),
                event.getIdStageTransition()
        ));
    }

}
*/
