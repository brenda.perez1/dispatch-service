package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatMissionRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.MissionEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CatMissionRepositoryImpl implements CatMissionRepository {

    private final MissionEntityRepository missionEntityRepository;

    public CatMissionRepositoryImpl(MissionEntityRepository missionEntityRepository) {
        this.missionEntityRepository = missionEntityRepository;
    }

    @Override
    public boolean exist(MissionId missionId) {
        return this.missionEntityRepository.findById(missionId.getValue()).isPresent();
    }
}
