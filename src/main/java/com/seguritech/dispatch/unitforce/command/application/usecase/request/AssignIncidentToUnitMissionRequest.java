package com.seguritech.dispatch.unitforce.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AssignIncidentToUnitMissionRequest {
    private String unitForceId;
    private Long missionId;
    private String incidentId;
    private String userId;

}
