package com.seguritech.dispatch.unitforce.command.domain.model;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageRequiredException;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StageModel {

	private final Long id;
	private final String name;
	private final String description;
	private final String icon;
	private final String textColor;
	private final String backgroundColor;
	private final StageTransitionModel stageTransition;

	public static StageModel of(Long id, String name, String description, String icon, String textColor, String backgroundColor, StageTransitionModel stageTransition) {
		insureStageId(id);
		return new StageModel(id, name, description, icon, textColor, backgroundColor, stageTransition);
	}

	private static void insureStageId(Long id) {
		if (id == null) {
			throw new StageRequiredException();
		}
	}

	public static StageModel empty() {
		return new StageModel(null, null, null, null, null, null, null);
	}

	public boolean isEmpty() {
		return this.id == null && this.name == null && this.description == null && this.icon == null && this.textColor == null && this.backgroundColor == null;
	}

	public StageModel(Long id) {
		this.id= id;
		this.name = "";
		this.description = "";
		this.icon = "";
		this.textColor = "";
		this.backgroundColor = "";
		this.stageTransition = null;
	}

}
