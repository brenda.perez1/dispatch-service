package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentIsNotAssignedToTheCorporationException extends BusinessException {
    public IncidentIsNotAssignedToTheCorporationException(Object... incidentId) {
        super(MessageResolver.INCIDENT_CORPORATION_NOT_ASSIGNED_EXCEPTION,incidentId, 3044L);
    }
}
