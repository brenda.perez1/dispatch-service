package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;

public class MissionNotFoundException extends BusinessException {
    public MissionNotFoundException(Object... missionId) {
        super(MessageResolver.MISSION_ID_NOT_FOUND_EXCEPTION, 3053L);
    }
}
