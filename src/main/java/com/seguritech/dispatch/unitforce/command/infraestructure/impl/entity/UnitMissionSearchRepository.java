package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;

import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionSearch;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitMissionSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;

public interface UnitMissionSearchRepository {

	List<UnitMissionSearch> findAll(UnitMissionSearchRequest request);

	PageDataProjectionDTO<IncidentUnitForceSearchResponse> search(UnitMissionSearchRequest request);

}
