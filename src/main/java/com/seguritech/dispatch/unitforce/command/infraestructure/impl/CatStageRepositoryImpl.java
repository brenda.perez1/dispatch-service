package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CatStageEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CatStageRepositoryImpl implements CatStageRepository {

    private final CatStageEntityRepository catStageEntityRepository;

    public CatStageRepositoryImpl(CatStageEntityRepository catStageEntityRepository) {
        this.catStageEntityRepository = catStageEntityRepository;
    }

    @Override
    public Long findByStageId(CatStageId catStageId) {
        return 0L;//TODO: se obtiene de la tabla cat_stage_transition
    }

    @Override
    public boolean exist(CatStageId catStageId) {
        return this.catStageEntityRepository.findById(catStageId.getValue()).isPresent();
    }
}
