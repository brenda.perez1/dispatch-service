package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.PeopleTransferRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class PeopleTransfer {

    private Long value;

    public PeopleTransfer(Long value) {
        this.value = value;
    }

    public PeopleTransfer required(Long peopleTransfer) {
        Optional.ofNullable(peopleTransfer).orElseThrow(PeopleTransferRequiredException::new);
        return new PeopleTransfer(peopleTransfer);
    }

    public static PeopleTransfer optional(Long peopleTransfer) {
        return new PeopleTransfer(peopleTransfer);
    }

}
