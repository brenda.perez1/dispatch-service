package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.Optional;

import com.domain.model.incident.entity.IncidentSecundaryEntity;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentSecundaryResponse;

public class IncidentSecundaryMapper {

	public static IncidentSecundaryResponse toResponse(IncidentSecundaryEntity entity) {
		return Optional.ofNullable(entity).map(x -> new IncidentSecundaryResponse(x)).orElse(null); 
	}

}
