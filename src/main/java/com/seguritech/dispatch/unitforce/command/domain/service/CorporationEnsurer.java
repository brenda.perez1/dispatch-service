package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.CorporationNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatCorporationRepository;

public class CorporationEnsurer {

    private final CatCorporationRepository catCorporationRepository;

    public CorporationEnsurer(CatCorporationRepository catCorporationRepository) {
        this.catCorporationRepository = catCorporationRepository;
    }

    public void ensurer(CorporationOperativeId corporationOperativeId) {
        if (!this.catCorporationRepository.exist(corporationOperativeId))
            throw new CorporationNotFoundException(corporationOperativeId.getValue());
    }

}
