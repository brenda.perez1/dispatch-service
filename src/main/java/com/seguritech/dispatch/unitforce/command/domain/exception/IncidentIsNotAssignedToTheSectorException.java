package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentIsNotAssignedToTheSectorException extends BusinessException {
    public IncidentIsNotAssignedToTheSectorException(Object... incidentId) {
        super(MessageResolver.INCIDENT_IS_NOT_ASSIGNED_TO_THE_SECTOR_EXCEPTION,incidentId, 3045L);
    }
}
