package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.SectorRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class SectorId {

    private Long value;

    private SectorId(Long value) {
        this.value = value;
    }

    public static SectorId required(Long id) {
        Optional.ofNullable(id).orElseThrow(SectorRequiredException::new);
        return new SectorId(id);

    }

}
