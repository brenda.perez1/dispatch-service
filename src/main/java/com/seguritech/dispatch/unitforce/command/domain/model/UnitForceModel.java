package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.List;

import com.seguritech.dispatch.unitforce.command.domain.event.IncidentChangeUnitForceEvent;
import com.seguritech.platform.domain.RootAggregate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UnitForceModel extends RootAggregate {

	private List<String> unitForceIds;
	private Boolean status;
	private Long motivoId;
	private String motivo;
	private String userId;
	private String option;
	
	public static UnitForceModel create(List<String> unitForceIds, Boolean status, Long motivoId, String userId, String option) {
		UnitForceModel model =  new UnitForceModel(unitForceIds, status, motivoId, null, userId, option);
		model.record(new IncidentChangeUnitForceEvent(unitForceIds, "", "", status, motivoId, "", null, null, userId, option));
		return model;
	}
	
}