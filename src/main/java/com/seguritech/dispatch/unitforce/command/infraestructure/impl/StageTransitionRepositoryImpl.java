package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageTransitionRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageTransitionEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class StageTransitionRepositoryImpl implements StageTransitionRepository {
    private final StageTransitionEntityRepository stageTransitionEntityRepository;

    public StageTransitionRepositoryImpl(StageTransitionEntityRepository stageTransitionEntityRepository) {
        this.stageTransitionEntityRepository = stageTransitionEntityRepository;
    }

    @Override
    public boolean exist(StageTransitionId stageTransitionId) {
        return this.stageTransitionEntityRepository.findById(stageTransitionId.getValue()).isPresent();
    }
}
