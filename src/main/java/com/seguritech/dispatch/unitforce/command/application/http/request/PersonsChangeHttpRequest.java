package com.seguritech.dispatch.unitforce.command.application.http.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersonsChangeHttpRequest {
	
	private Long missionId;
    private Long nextStageId;
	private Long persons;

}
