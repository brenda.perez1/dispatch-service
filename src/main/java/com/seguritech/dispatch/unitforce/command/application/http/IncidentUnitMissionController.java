package com.seguritech.dispatch.unitforce.command.application.http;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.unitforce.command.application.http.request.AssignIncidentToUnitMissionHttpRequest;
import com.seguritech.dispatch.unitforce.command.application.usecase.AssignIncidentToUnitToStartAMissionUseCase;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.AssignIncidentToUnitMissionRequest;

import io.swagger.annotations.ApiOperation;

@RestController
public class IncidentUnitMissionController extends IncidentUnitMissionBaseController {

    private final AssignIncidentToUnitToStartAMissionUseCase useCase;

    public IncidentUnitMissionController(AssignIncidentToUnitToStartAMissionUseCase useCase) {
        this.useCase = useCase;
    }

    @ApiOperation(value = "assign incident to start mission", nickname = "created", notes = "assign incident to start mission")
    @PostMapping("{emergency-id}/unit-force/{unitforce-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@RequestBody AssignIncidentToUnitMissionHttpRequest request,
                       @PathVariable("emergency-id") String emergencyId,
                       @PathVariable("unitforce-id") String unitforceId,
                       UsernamePasswordAuthenticationToken user) {
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
        this.useCase.execute(new AssignIncidentToUnitMissionRequest(
                unitforceId,
                request.getMissionId(),
                emergencyId,
                principal.getId()
        ));
    }

}
