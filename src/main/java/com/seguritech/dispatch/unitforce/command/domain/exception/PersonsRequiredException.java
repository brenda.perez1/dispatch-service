package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class PersonsRequiredException extends BusinessException {

    public PersonsRequiredException() {
        super(MessageResolver.PERSONS_REQUERID_EXCEPTION, 3058L);
    }
}
