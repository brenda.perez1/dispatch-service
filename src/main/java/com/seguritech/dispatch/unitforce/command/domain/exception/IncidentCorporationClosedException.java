package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationClosedException extends BusinessException {
	
    public IncidentCorporationClosedException(Object... incidentId) {
        super(MessageResolver.INCIDENT_CORPORATION_CLOSE_EXCEPTION, incidentId, 3084L);
    }
}
