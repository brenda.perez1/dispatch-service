package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.SectorNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatSectorRepository;

public class SectorEnsurer {

    private final CatSectorRepository catSectorRepository;

    public SectorEnsurer(CatSectorRepository catSectorRepository) {
        this.catSectorRepository = catSectorRepository;
    }

    public void ensurer(SectorId sectorId) {
        if (!this.catSectorRepository.exist(sectorId))
            throw new SectorNotFoundException(sectorId);
    }

}
