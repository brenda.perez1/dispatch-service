package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.MissionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatMissionRepository;

public class MissionEnsurer {

    private final CatMissionRepository repository;

    public MissionEnsurer(CatMissionRepository repository) {
        this.repository = repository;
    }

    public void ensurer(MissionId missionId) {
        if (!this.repository.exist(missionId))
            throw new MissionNotFoundException(missionId);
    }

}
