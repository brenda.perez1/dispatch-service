package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.Date;
import java.util.Optional;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.mission.entity.Stage;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk;
import com.domain.model.unitmission.StageMissionLogEntity;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageTransitionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;

public class StageMissionLogMapper {
	
	public static StageMissionLogEntity toEntity(StageMissionLogModel model) {
		
		return new StageMissionLogEntity(
				new Date(),
				model.getCreatedBy(),
				new Date(),
				model.getUpdatedBy(),
				
				new UnitMissionEntity(
						new UnitMissionEntityPk(
								model.getUnitMissionId().getValue(),
								new IncidentEntity(model.getEmergencyId()),
								new UnitForceConfigEntity(model.getUnitForceConfigId().getValue())
						)
				),
				
				new Stage(model.getStage().getId()),
				new Date(),
				model.getStageTransition(),
				model.getPersonsNumber(),
				model.getObservations(),
				model.getReasonCancel(),
				model.getMissionId()
		);
	}
	
	public static StageMissionLogModel toModel(StageMissionLogEntity entity) {
		return new StageMissionLogModel(
				entity.getId(),
				entity.getCreatedBy(),
				entity.getCreationDate(),
				entity.getUpdatedBy(), 
				entity.getUpdateDate(),
				StageMissionLogMapper.toModel(entity.getStage()),
				entity.getStageTransition(), 
				entity.getPersonsNumber(), 
				entity.getObservations(), 
				entity.getReasonCancel(), 
				entity.getIdMission(), 
				entity.getUnitMission().getId().getEmergencyId().getId(), 
				new UnitForceConfigId(entity.getUnitMission().getId().getUnitForceConfig().getIdUnitForceConfig()), 
				new UnitMissionId(entity.getUnitMission().getId().getUnitMissionId()));
				
	}

	public static StageModel toModel(Stage entity) {
        return Optional.ofNullable(entity).map(x -> new StageModel(
                Optional.ofNullable(entity).map(Stage::getId).orElse(0L),
        			x.getName(),
        			x.getDescription(),
        			x.getIcon(),
        			x.getTextColor(),
        			x.getBackgroundColor(),
        			new StageTransitionModel(x.getStageTransition().getId(), x.getStageTransition().getStageTransitionDesc())
        )).orElse(StageModel.empty());
    }
	
}
