package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.domain.model.mission.entity.StatusEntity;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStatusId;

public interface CatStatusUnitRepository {

	StatusEntity ensurer(CatStatusId catStatusId);
	
}
