package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class StageRequiredException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public StageRequiredException () {
        super(MessageResolver.STAGE_REQUIRED_EXCEPTION, 8034L);
    }

}
