package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.MissionIdRequieredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class MissionId {

    private Long value;

    private MissionId(Long value) {
        this.value = Optional.ofNullable(value).orElseThrow(MissionIdRequieredException::new);
    }

    public static MissionId required(Long id){
        return new MissionId(id);
    }

}
