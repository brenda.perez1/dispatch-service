package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.ObservationSizeException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class Observations {

    private String value;

    private Observations(String value) {
        this.value = value;
    }

    public static Observations optional(String value) {

        if (value != null && value.length() > 1500)
            throw new ObservationSizeException();

        return new Observations(Optional.ofNullable(value).orElse(""));
    }
}
