package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnavailabilityReasonRequiredException extends BusinessException {
	
    public UnavailabilityReasonRequiredException() {
        super(MessageResolver.UNAVAILABILITY_REASON_REQUERID_EXCEPTION, 3080L);
    }
}
