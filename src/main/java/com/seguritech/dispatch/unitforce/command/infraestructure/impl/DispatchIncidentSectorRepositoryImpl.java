package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.repository.DispatchIncidentSectorRepository;

@Repository
public class DispatchIncidentSectorRepositoryImpl implements DispatchIncidentSectorRepository {

//    private final DispatchIncidentLocationEntityRepository repository;

    public DispatchIncidentSectorRepositoryImpl(
//    		DispatchIncidentLocationEntityRepository repository
    		) {
//        this.repository = repository;
    }


    @Override
    public boolean exist(IncidentId incidentId, SectorId unitSectorId) {
    	return false;
//        return this.repository.findByEmergencyAndGeofence(new IncidentEntity(incidentId.getValue()), new
//                Geofence(unitSectorId.getValue())).isPresent();
    }
}
