package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitForceConfigEntity;

@Repository
public interface UnitConfigEntityRepository extends JpaRepository<UnitForceConfigEntity, String>, JpaSpecificationExecutor<UnitForceConfigEntity> {
        
        Optional<UnitForceConfigEntity> findByIdUnitForceConfig(String unitForceId);
        
        @Modifying
        @Query(value="UPDATE FROM UnitForceConfigEntity ufc set ufc.idStatus=:idStatus WHERE ufc.idUnitForceConfig=:id")
		void updateStatusUnitForce(String id, StatusEntity idStatus);
        
//        @Query(value="FROM UnitForceConfigEntity ufc "
//        		+ "LEFT JOIN UnitMissionEntity ime ON ime.id.unitForceConfig.idUnitForceConfig = ufc.idUnitForceConfig "
//        		+ "WHERE ime.active = false "
//        		+ "AND ufc.corporationEntity.id IN :corporationEntityId")
        @Query(value = "SELECT ufc FROM UnitForceConfigEntity ufc "
        		+ "WHERE ufc.corporationEntity.id IN :corporationEntityId "
        		+ "AND ufc.idStatus = 1 "
        		+ "AND NOT EXISTS ( "
        		+ "SELECT 1 FROM UnitMissionEntity um WHERE um.active = true "
        		+ "AND ufc.idUnitForceConfig = um.id.unitForceConfig.idUnitForceConfig "
        		+ ")")
        List<UnitForceConfigEntity> findAllByCorporationEntityId(List<Long> corporationEntityId);
        
        Boolean existsByIdUnitForceConfigAndIdStatusNot(String idUnitForceConfig, StatusEntity idStatus);

}