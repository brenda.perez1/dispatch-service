package com.seguritech.dispatch.unitforce.command.application.http;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

@Api(tags = {"EMERGENCIES UNITS FORCE"})
@RequestMapping("/dispatch/")
public class IncidentUnitMissionBaseController {
}
