package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;

public class IncidentEnsurer {

    private final StageIncidentRepository repository;

    public IncidentEnsurer(StageIncidentRepository repository) {
        this.repository = repository;
    }

    public void ensurer(IncidentId incidentId) {
        if (!this.repository.exist(incidentId))
            throw new IncidentNotFoundException(incidentId.getValue());
    }

}
