package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;

public class CorporationNotFoundException extends BusinessException {
    public CorporationNotFoundException(Object... corporationOperativeId) {
        super(MessageResolver.CORPORATION_NOT_FOUND_EXCEPTION_2, corporationOperativeId, 3040L);
    }
    
    public CorporationNotFoundException() {
        super(MessageResolver.CORPORATION_NOT_FOUND_EXCEPTION_2, 3040L);
    }
}
