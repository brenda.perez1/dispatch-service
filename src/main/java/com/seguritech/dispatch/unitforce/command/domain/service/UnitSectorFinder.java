package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitSectorNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitSectorRepository;
import com.seguritech.platform.Service;

@Service
public class UnitSectorFinder {

    private final UnitSectorRepository unitSectorRepository;

    public UnitSectorFinder(UnitSectorRepository unitSectorRepository) {
        this.unitSectorRepository = unitSectorRepository;
    }

    public SectorId find(UnitForceConfigId unitForceConfigId){
        SectorId sectorId = this.unitSectorRepository.find(unitForceConfigId);

        if(sectorId==null)
            throw new UnitSectorNotFoundException(unitForceConfigId.getValue());

        return sectorId;
    }

}
