package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk;

public interface UnitMissionEntityRepository extends JpaRepository<UnitMissionEntity, UnitMissionEntityPk>,
    JpaSpecificationExecutor<UnitMissionEntity> {

    Optional<UnitMissionEntity> findByIdUnitMissionId(String unitMissionId);

    Optional<UnitMissionEntity> findByIdUnitForceConfigAndIdEmergencyId(UnitForceConfigEntity unitForceConfigId, IncidentEntity incidentId);
    
    List<UnitMissionEntity> findAllByIdUnitForceConfigAndIdEmergencyIdAndActive(UnitForceConfigEntity unitForceConfigId, IncidentEntity incidentId, boolean active);
    
    Optional<UnitMissionEntity> findByIdUnitMissionIdAndIdEmergencyId(String unitMissionId, IncidentEntity incidentId);

    
    @Query("SELECT u, u.id, u.id.unitForceConfig, u.id.unitForceConfig.transportEntity,  u.id.unitMissionId, "
    		+ " u.missionStageConfig, u.missionStageConfig.id, u.missionStageConfig.stage, u.missionStageConfig.mission, "
    		+ " u.missionStageConfig.stageTransition "
    		+ " FROM UnitMissionEntity u "
    		+ " WHERE u.id.unitForceConfig.corporationEntity.id IN :corporations"
    		+ " AND u.active = :active"
    		)
    List<UnitMissionEntity> findUnitMissionByOperativeGroup(@Param("corporations") List<Long> corporations, @Param("active") Boolean active);
    
    @Query("SELECT u, u.id, u.id.unitForceConfig, u.id.unitForceConfig.transportEntity,  u.id.unitMissionId, "
    		+ " u.missionStageConfig, u.missionStageConfig.id, u.missionStageConfig.stage, u.missionStageConfig.mission, "
    		+ " u.missionStageConfig.stageTransition "
    		+ " FROM UnitMissionEntity u "
    		+ " WHERE u.id.unitForceConfig.corporationEntity.id IN :corporations"
    		+ " AND u.id.unitMissionId = :unitmissionId "
    		+ " AND u.active = :active"
    		)
    List<UnitMissionEntity> findUnitMissionByOperativeGroupAndUnitmissionId(@Param("corporations") List<Long> corporations,
    		@Param("unitmissionId") String unitmissionId, @Param("active") Boolean active);
    
    @Query("SELECT u, u.id, u.id.unitForceConfig, u.id.unitForceConfig.transportEntity,  u.id.unitMissionId, "
    		+ " u.missionStageConfig, u.missionStageConfig.id, u.missionStageConfig.stage, u.missionStageConfig.mission, "
    		+ " u.missionStageConfig.stageTransition "
    		+ " FROM UnitMissionEntity u "
    		+ " WHERE u.id.unitForceConfig.corporationEntity.id IN :corporations"
    		+ " AND u.id.unitForceConfig.idUnitForceConfig = :unitforceId "
    		+ " AND u.active = :active"
    		)
    List<UnitMissionEntity> findUnitMissionByOperativeGroupAndUnitforceId(@Param("corporations") List<Long> corporations,
    		@Param("unitforceId") String unitforceId, @Param("active") Boolean active);
    
    @Query("SELECT u, u.id, u.id.unitForceConfig, u.id.unitForceConfig.transportEntity,  u.id.unitMissionId, "
    		+ " u.missionStageConfig, u.missionStageConfig.id, u.missionStageConfig.stage, u.missionStageConfig.mission, "
    		+ " u.missionStageConfig.stageTransition "
    		+ " FROM UnitMissionEntity u "
    		+ " WHERE u.id.unitForceConfig.corporationEntity.id IN :corporations"
    		+ " AND u.id.unitForceConfig.corporation.id IN :corporation"
    		+ " AND u.active = :active"
    		)
    List<UnitMissionEntity> findUnitMissionByOperativeGroupAndCorporation(@Param("corporations") List<Long> corporations, 
    		@Param("corporation") Long corporation, @Param("active") Boolean active);
    
    @Query("SELECT u, u.id, u.id.unitForceConfig, u.id.unitForceConfig.transportEntity,  u.id.unitMissionId, "
    		+ " u.missionStageConfig, u.missionStageConfig.id, u.missionStageConfig.stage, u.missionStageConfig.mission, "
    		+ " u.missionStageConfig.stageTransition "
    		+ " FROM UnitMissionEntity u "
    		+ " WHERE u.id.unitForceConfig.corporationEntity.id IN :corporations"
    		+ " AND u.id.unitForceConfig.corporation.id IN :corporation"
    		+ " AND u.missionStageConfig.stage.id IN :stage"
    		+ " AND u.active = :active"
    		)
    List<UnitMissionEntity> findUnitMissionByOperativeGroupAndCorporationAndStage(@Param("corporations") List<Long> corporations, 
    		@Param("corporation") Long corporation, @Param("stage") Long stage, @Param("active") Boolean active);
    
    @Query("SELECT u, u.id, u.id.unitForceConfig, u.id.unitForceConfig.transportEntity,  u.id.unitMissionId, "
    		+ " u.missionStageConfig, u.missionStageConfig.id, u.missionStageConfig.stage, u.missionStageConfig.mission, "
    		+ " u.missionStageConfig.stageTransition "
    		+ " FROM UnitMissionEntity u "
    		+ " WHERE u.id.unitForceConfig.corporationEntity.id IN :corporations"
    		+ " AND u.missionStageConfig.stage.id IN :stage"
    		+ " AND u.active = :active"
    		)    
    List<UnitMissionEntity> findUnitMissionByOperativeGroupAndStage(@Param("corporations") List<Long> corporations, 
    		@Param("stage") Long stage, @Param("active") Boolean active);

    @Query("FROM UnitMissionEntity unit WHERE unit.id.emergencyId =:idEmergency AND unit.id.unitForceConfig =:idUnitForce AND unit.active IS :active")
    UnitMissionEntity findUnitMissionByIdEmergencyAndIdUniteForce(@Param("idEmergency") IncidentEntity idEmergency, 
    		@Param("idUnitForce") UnitForceConfigEntity idUnitForce, @Param("active") Boolean active);
    
    @Query("FROM UnitMissionEntity unit WHERE unit.id.emergencyId =:idEmergency AND unit.id.unitForceConfig =:idUnitForce AND unit.id.unitMissionId =:unitMissionId")
    UnitMissionEntity findUnitMissionByIdEmergencyAndIdUniteForceAndUnitMission(@Param("idEmergency") IncidentEntity idEmergency, 
    		@Param("idUnitForce") UnitForceConfigEntity idUnitForce, @Param("unitMissionId") String unitMissionId);
    
    @Query("SELECT u.id.unitForceConfig.idUnitForceConfig FROM UnitMissionEntity u WHERE u.id.emergencyId =:idEmergency AND u.active IS TRUE ")
    List<String> findByEmergencyId(IncidentEntity idEmergency);
    
    @Query("SELECT COUNT(u) FROM UnitMissionEntity u WHERE u.active IS TRUE AND u.id.emergencyId =:incidentEntity AND u.id.unitForceConfig.corporation =:corporation ")
    Long countUnitMission(IncidentEntity incidentEntity, Corporation corporation);
    
    @Query("SELECT u FROM UnitMissionEntity u WHERE u.id.unitForceConfig.idUnitForceConfig = :unitForceConfigId AND u.active = true ORDER BY u.creationDate DESC")
    List<UnitMissionEntity> findUnitMissionByIdUnitForceConfig(String unitForceConfigId, Pageable pageable);
    
}
