package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.domain.model.incident.entity.IncidentTokenEntity;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentTokenResponse;


public class IncidentTokenMapper {

	public static List<IncidentTokenResponse> toResponse(List<IncidentTokenEntity> entities) {
		List<IncidentTokenResponse> list = new ArrayList<IncidentTokenResponse>();
		for (IncidentTokenEntity entity : entities) {
			Optional.ofNullable(entity)
					.map(x -> list.add(new IncidentTokenResponse(x.getTokenIncident(), x.isActive())))
					.orElse(null);
		}
		return list;
	}
	
	public static IncidentTokenResponse toResponse(IncidentTokenEntity entity) {
		return Optional.ofNullable(entity)
				.map(x -> new IncidentTokenResponse(entity.getTokenIncident(), entity.isActive()))
				.orElse(null);
	}

}
