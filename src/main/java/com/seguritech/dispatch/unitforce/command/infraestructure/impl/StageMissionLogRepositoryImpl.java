package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageMissionLogRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageMissionLogEntityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageMissionLogMapper;

@Repository
public class StageMissionLogRepositoryImpl implements StageMissionLogRepository{
	
	private final StageMissionLogEntityRepository repository;
	
	public StageMissionLogRepositoryImpl(StageMissionLogEntityRepository repository) {
		this.repository = repository;
	}

	@Override
	public void save(StageMissionLogModel stageMissionLogModel) {
		this.repository.save(StageMissionLogMapper.toEntity(stageMissionLogModel));
	}

}
