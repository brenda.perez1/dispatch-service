package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class StagePreconfiguredNotFoundException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public StagePreconfiguredNotFoundException(Object... catStageId) {
        super(MessageResolver.STAGE_PRECONFIGURED_NOT_FOUND_EXCEPTION, catStageId, 3064L);
    }
	
}
