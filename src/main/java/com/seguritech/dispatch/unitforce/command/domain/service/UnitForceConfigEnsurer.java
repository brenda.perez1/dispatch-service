package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.platform.Service;

@Service
public class UnitForceConfigEnsurer {

    private final UnitForceConfigRepository repository;

    public UnitForceConfigEnsurer(UnitForceConfigRepository repository) {
        this.repository = repository;
    }

    public void ensurer(UnitForceConfigId unitForceConfigId) {
        if (!this.repository.ensurer(unitForceConfigId))
            throw new UnitForceConfigNotFoundException(unitForceConfigId.getValue());
    }

}
