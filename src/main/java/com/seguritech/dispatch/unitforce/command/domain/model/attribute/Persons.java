package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.PersonsRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class Persons {
    private static Long EMPTY = 0L;
    private Long value;

    private Persons(Long value) {
        this.value = value;
    }

    public static Persons required(Long number) {

        if (number == null)
            throw new PersonsRequiredException();

        return new Persons(number);
    }

    public static Persons optional(Long number) {
        return new Persons(Optional.ofNullable(number).orElse(EMPTY));
    }

}
