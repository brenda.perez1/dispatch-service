package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;

public class StageTrasitionNotFoundException extends BusinessException {
    public StageTrasitionNotFoundException(Object... stageTransitionId) {
        super(MessageResolver.STAGE_TRANSITION_NOT_FOUND_EXCEPTION, stageTransitionId, 3067L);
    }
}
