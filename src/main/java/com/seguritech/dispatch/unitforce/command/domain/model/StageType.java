package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.HashMap;
import java.util.Map;

/*
* TODO:Refactorizar cargar desde un properties los tipos de etapa estandar
* */
public enum StageType {
	IN_THE_PLACE(8L), ON_THE_WAY(7L), TRANSFER_PEOPLE(6L), CANCEL(4L), FINALIZE(5L), INITIAL(1L), TRANSLATION(3L),SUBTRACKING(2L);

    private Long stageTransitionId;

    StageType(Long id){
     this.stageTransitionId = id;
    }

    private static Map map = new HashMap<>();

    static {
        for (StageType stageType : StageType.values()) {
            map.put(stageType.getValue(), stageType);
        }
    }

    public static StageType valueOf(long stageType) {
        return (StageType) map.get(stageType);
    }

    public Long getValue(){
        return this.stageTransitionId;
    }


}
