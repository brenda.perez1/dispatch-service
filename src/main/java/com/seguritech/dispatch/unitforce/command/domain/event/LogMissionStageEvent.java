package com.seguritech.dispatch.unitforce.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class LogMissionStageEvent implements DomainEvent {
    private String unitId;
    private Long missionId;
    private Long stageId;
    private String incidentId;
    private Date createdDate;
    private String createdByUser;

}
