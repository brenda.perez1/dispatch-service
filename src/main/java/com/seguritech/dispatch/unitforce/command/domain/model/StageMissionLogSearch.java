package com.seguritech.dispatch.unitforce.command.domain.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class StageMissionLogSearch {

	private Long id;
	private String createdBy;
	private Date creationDate;
	private String emergencyId;
	private Long missionId;
	private String observations;
	private Long personsNumber;
	private Long reasonCancel;
	private Long stageId;
	private String stageBackgroundColor;
	private String stageDescription;
	private String stageName;
	private String stageIcon;
	private String stageTextColor;
	private String unitForceConfigId;
	private String unitMissionId;
	private Date updateDate;
	private String updatedBy;
	private Long idStageTransition;

}
