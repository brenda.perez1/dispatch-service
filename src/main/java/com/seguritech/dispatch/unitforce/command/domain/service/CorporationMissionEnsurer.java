package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.MissionIdNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;


public class CorporationMissionEnsurer {

    private final CorporationMissionRepository repository;

    public CorporationMissionEnsurer(CorporationMissionRepository repository) {
        this.repository = repository;
    }

    public void ensurer(MissionId missionId, CorporationId unitCorporationOperativeId) {
        if (!this.repository.exist(missionId, unitCorporationOperativeId))
            throw new MissionIdNotFoundException(missionId);
    }

}
