package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentCorporationException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationOfIncidentRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CorporationIncidentEntityRepository;

@Repository
public class CorporationOfIncidentRepositoryImpl implements CorporationOfIncidentRepository {

    private final CorporationIncidentEntityRepository repository;

    public CorporationOfIncidentRepositoryImpl(CorporationIncidentEntityRepository repository) {
        this.repository = repository;
    }

	@Override
	public boolean exist(IncidentId incidentId, CorporationId unitCorporationOperativeId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean exist(IncidentId incidentId, CorporationOperativeId unitCorporationOperativeId) {
		 return this.repository.findById(new IncidentCorporationEntityPk(
	                new IncidentEntity(incidentId.getValue()),
	                new CorporationOperative(unitCorporationOperativeId.getValue())
	        )).isPresent();
	}

	@Override
	public IncidentCorporationEntity getById(IncidentId incidentId, CorporationOperativeId unitCorporationOperativeId) {
		// TODO Auto-generated method stub
		return this.repository.findByIdAndStatus(new IncidentCorporationEntityPk(
                new IncidentEntity(incidentId.getValue()),
                new CorporationOperative(unitCorporationOperativeId.getValue())
        ),
		IncidentCorporationStatusEnum.OPEN.getValue()).<IncidentCorporationException>orElseThrow(() -> {
			throw new IncidentCorporationException(unitCorporationOperativeId.getValue());
		});
	}

	@Override
	public void save(IncidentCorporationEntity incidentCorporationEntity) {
		// TODO Auto-generated method stub
		this.repository.save(incidentCorporationEntity);
	}


}
