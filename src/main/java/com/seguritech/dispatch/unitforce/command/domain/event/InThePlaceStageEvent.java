package com.seguritech.dispatch.unitforce.command.domain.event;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class InThePlaceStageEvent extends StageEvent {

}
