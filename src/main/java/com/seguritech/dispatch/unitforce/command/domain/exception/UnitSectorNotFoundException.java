package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitSectorNotFoundException extends BusinessException {

    public UnitSectorNotFoundException(Object... unitForceConfigId) {
        super(MessageResolver.UNIT_SECTOR_NOT_FOUND_EXCEPTION, unitForceConfigId, 3079L);
    }
}
