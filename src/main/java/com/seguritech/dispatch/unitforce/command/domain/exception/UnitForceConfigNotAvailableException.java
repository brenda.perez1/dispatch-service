package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;

public class UnitForceConfigNotAvailableException extends BusinessException {
    public UnitForceConfigNotAvailableException(Object... unitForceConfigId) {
        super(MessageResolver.UNIT_FORCE_CONFIG_NOT_AVAILABLE_EXCEPTION, 3072L);
    }
}
