package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageTransitionIdRequieredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class StageTransitionId {

    private Long value;

    private StageTransitionId(Long value) {
        this.value = value;
    }

    public static StageTransitionId optional(Long id) {
        return new StageTransitionId(id);
    }

    public static StageTransitionId required(Long id) {
        if (id == null)
            throw new StageTransitionIdRequieredException();

        return new StageTransitionId(id);
    }

    public static StageTransitionId none() {
        return new StageTransitionId(null);
    }

}
