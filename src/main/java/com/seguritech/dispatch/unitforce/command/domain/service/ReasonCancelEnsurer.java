package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.ReasonCancelNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;

public class ReasonCancelEnsurer {

    private final ReasonCancelRepository reasonCancelRepository;

    public ReasonCancelEnsurer(ReasonCancelRepository reasonCancelRepository) {
        this.reasonCancelRepository = reasonCancelRepository;
    }

    public void ensurer(ReasonCancelId reasonCancelId) {
        if (!this.reasonCancelRepository.exist(reasonCancelId)) {
            throw new ReasonCancelNotFoundException(reasonCancelId);
        }
    }

}
