package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitMissionNotFoundException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public UnitMissionNotFoundException(Object... unitMissionId) {
        super(MessageResolver.UNIT_MISSION_NOT_FOUND_EXCEPTION, unitMissionId, 3076L);
    }
	
  
}
