package com.seguritech.dispatch.unitforce.command.domain.event;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class TransferredPersonStageEvent extends StageEvent {
	
	private static final long serialVersionUID = 1L;
	
}
