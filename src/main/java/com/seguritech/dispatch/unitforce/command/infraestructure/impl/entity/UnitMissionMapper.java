package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.mission.entity.CatReasonMissionCancel;
import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;

public class UnitMissionMapper {

    public static UnitMissionEntity toEntity(UnitMissionModel unitMissionModel, MissionStageConfig missionStageConfig) {
    	
    	return new UnitMissionEntity(
                new UnitMissionEntityPk(
                        unitMissionModel.getUnitMissionId().getValue(),
                        new IncidentEntity(unitMissionModel.getIncidentId().getValue()),
                        new UnitForceConfigEntity(unitMissionModel.getUnitForceConfigId().getValue())
                ),
                unitMissionModel.isActive(),
                unitMissionModel.getCreatedByUser().getValue(),
                unitMissionModel.getCreatedAt(),
                unitMissionModel.getUpdatedByUser().getValue(),
                unitMissionModel.getUpdateAt(),
                missionStageConfig,
                unitMissionModel.getStartMissionDate(),
                unitMissionModel.getEndMissionDate(),
                new CatReasonMissionCancel(unitMissionModel.getReasonCancelId().getValue()),
                unitMissionModel.getObservations().getValue(),
                unitMissionModel.getPersons().getValue(),
                null,
                missionStageConfig.getStage(),
                missionStageConfig.getMission(),
                missionStageConfig.getStageTransition()
        );
    }

    public static UnitMissionModel toModel(UnitMissionEntity entity, List<StageId> nextStages) {
        return new UnitMissionModel(
                UnitMissionId.required(entity.getId().getUnitMissionId()),
                null,//SectorId.required(entity.getId().getUnitForceConfig().getCatSectorEntity().getId()),
                CorporationId.required(entity.getId().getUnitForceConfig().getCorporation().getId()),
                CorporationOperativeId.required(entity.getId().getUnitForceConfig().getCorporationEntity().getId()),
                nextStages,
                StageId.required(
                		0L, //Stage
                		null,
                		0L, //Mission
                		0L //StageTransition
                ),
                UnitForceConfigId.required(entity.getId().getUnitForceConfig().getIdUnitForceConfig()),
                IncidentId.required(entity.getId().getEmergencyId().getId()),
                UserId.required(entity.getCreatedBy()),
                UserId.required(entity.getUpdatedBy()),
                entity.getCreationDate(),
                entity.getUpdateDate(),
                entity.getStartMissionDate(),
                entity.getEndMissionDate(),
                ReasonCancelId.required(entity.getReasonCancel().getId()),
                Observations.optional(entity.getObservations()),
                Persons.optional(entity.getPersonsNumber()),
                entity.getActive(),
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

}
