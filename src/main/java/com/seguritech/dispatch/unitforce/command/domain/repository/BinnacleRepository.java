package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.BinnacleModel;

public interface BinnacleRepository {

    void save(BinnacleModel model);

}
