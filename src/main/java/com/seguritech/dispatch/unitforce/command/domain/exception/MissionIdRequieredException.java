package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class MissionIdRequieredException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public MissionIdRequieredException() {
        super(MessageResolver.MISSION_ID_REQUERID_EXCEPTION, 3051L);
    }
	
}
