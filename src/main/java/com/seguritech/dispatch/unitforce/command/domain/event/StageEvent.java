package com.seguritech.dispatch.unitforce.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StageEvent implements DomainEvent, Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long sectorId;
    private Long corporationId;
    private String unitMissionId;
    private Long missionId;
    private Long stageId;
    private String stageName;
    private String unitForceConfigId;
    private String incidentId;
    private String createdByUser;
    private String updatedByUser;
    private Date createdAt;
    private Date updateAt;
    private Date startMissionDate;
    private Date endMissionDate;
    private Long reasonCancelId;
    private String observations;
    private Long persons;
    private boolean active;
    private Long idStageTransition;
    private String incidentFolio;
    private List<String> oneSignalUsersId;
    private String subTypeName;
    private String priorityName;

    private String option;
    private String incidentSheet;
    private List<String> unitForceConfigIds;
    
}
