package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentCorporationException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationOfIncidentRepository;
import com.seguritech.platform.Service;

@Service
public class IncidentCorporationEnsurer {

    private final CorporationOfIncidentRepository corporationIncidentRepository;


    public IncidentCorporationEnsurer(CorporationOfIncidentRepository corporationIncidentRepository) {
        this.corporationIncidentRepository = corporationIncidentRepository;
    }

    public void ensurer(IncidentId incidentId, CorporationOperativeId unitCorporationOperativeId) {
        if (!this.corporationIncidentRepository.exist(incidentId, unitCorporationOperativeId))
            throw new IncidentCorporationException(incidentId.getValue());

    }

}
