package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;

public interface CatStageRepository {

    Long findByStageId(CatStageId nextStageId);

    boolean exist(CatStageId nextStageId);

}
