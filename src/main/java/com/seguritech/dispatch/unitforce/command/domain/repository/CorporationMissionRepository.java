package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;


public interface CorporationMissionRepository {

    boolean exist(MissionId missionId, CorporationId unitCorporationOperativeId);

    StageId findStageId(CorporationId unitCorporationOperativeId, MissionId missionId);

    MissionId find(CorporationId corporationOperativeId);

    MissionId find(UnitForceConfigId unitForceConfigId);

}
