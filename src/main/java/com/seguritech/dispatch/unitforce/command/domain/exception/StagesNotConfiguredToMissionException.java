package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;

public class StagesNotConfiguredToMissionException extends BusinessException {
    public StagesNotConfiguredToMissionException(Object... missionId) {
        super(MessageResolver.STAGES_NOT_CONFIGURED_TO_MISSION_EXCEPTION,missionId, 3065L);
    }
}
