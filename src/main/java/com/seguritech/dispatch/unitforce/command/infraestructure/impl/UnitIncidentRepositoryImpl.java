package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitIncidentEntityRepository;

@Repository
public class UnitIncidentRepositoryImpl implements StageIncidentRepository {

    private final UnitIncidentEntityRepository repository;

    public UnitIncidentRepositoryImpl(UnitIncidentEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean exist(IncidentId incidentId) {
        return this.repository.findById(incidentId.getValue()).isPresent();
    }
}
