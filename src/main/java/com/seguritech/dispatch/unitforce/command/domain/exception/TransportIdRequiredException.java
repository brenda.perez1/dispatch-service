package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class TransportIdRequiredException extends BusinessException {
    public TransportIdRequiredException() {
        super(MessageResolver.TRANSPORT_ID_REQUERID_EXCEPTION, 3070L);
    }
}
