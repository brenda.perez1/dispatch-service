package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitMissionIdRequiredException;
import com.seguritech.platform.domain.UUIDGenerator;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class UnitMissionId {

    private String value;

    public UnitMissionId(String value) {
        this.value = value;
    }

    public static UnitMissionId required(String id) {
        Optional.ofNullable(id).orElseThrow(UnitMissionIdRequiredException::new);
        return new UnitMissionId(id);
    }

    public static UnitMissionId generateId() {
        return new UnitMissionId(UUIDGenerator.random().toString());
    }

}
