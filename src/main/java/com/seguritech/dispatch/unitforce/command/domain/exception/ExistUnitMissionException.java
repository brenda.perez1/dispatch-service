package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ExistUnitMissionException extends BusinessException {

	public ExistUnitMissionException() {
        super(MessageResolver.INCIDENT_CORPORATION_EXIST_UNIT_MISSION_ASSING, 3083L);
    }
}
