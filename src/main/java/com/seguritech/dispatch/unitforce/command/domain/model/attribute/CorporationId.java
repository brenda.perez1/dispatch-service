package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.CorporationRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class CorporationId {
    private Long value;

    private CorporationId(Long value) {
        this.value = value;
    }

    public static CorporationId required(Long id) {
        Optional.ofNullable(id).orElseThrow(CorporationRequiredException::new);
        return new CorporationId(id);
    }

}
