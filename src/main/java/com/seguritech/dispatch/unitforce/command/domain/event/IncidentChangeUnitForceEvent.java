package com.seguritech.dispatch.unitforce.command.domain.event;

import java.util.List;

import com.seguritech.platform.domain.DomainEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentChangeUnitForceEvent implements DomainEvent {
	
	private List<String> unitForceIds;
	private String emergencySheet;
	private String emergencyId;
	private Boolean status;
	private Long reasonId;
	private String reason;
	private String lastStageId;
	private String lastStage;
	private String userCad;
	private String option;
	
}
