package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class SectorRequiredException extends BusinessException {
    public SectorRequiredException() {
        super(MessageResolver.SECTOR_REQUERID_EXCEPTION, 3062L);
    }
}
