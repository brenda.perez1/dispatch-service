package com.seguritech.dispatch.unitforce.command.application.usecase;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.transaction.Transactional;

import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitForceConfigStatusLogEntity;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.UnitForceChangeRequest;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitForceModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStatusId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnavailabilityReasonId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStatusUnitRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnavailabilityRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigStatusLogRepository;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitForceConfigEnsurer;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class UnitForceChangeUseCase {

    private final UnitConfigEntityRepository unitConfigEntityRepository;
    private final UnitForceConfigStatusLogRepository logRepository;
    private final UnavailabilityRepository unavailabilityRepository;
    private final CatStatusUnitRepository catStatusRepository;
	private final UnitForceConfigEnsurer unitForceConfigEnsurer;
	private final DomainEventPublisher domainEventPublisher;

	 public UnitForceChangeUseCase(
							 UnitConfigEntityRepository unitConfigEntityRepository, 
							 UnitForceConfigStatusLogRepository logRepository,
							 UnavailabilityRepository unavailabilityRepository,
							 CatStatusUnitRepository catStatusRepository,
							 UnitForceConfigEnsurer unitForceConfigEnsurer,
							 DomainEventPublisher domainEventPublisher) {
		this.unitConfigEntityRepository = unitConfigEntityRepository;
		this.logRepository = logRepository;
		this.unavailabilityRepository = unavailabilityRepository;
		this.catStatusRepository = catStatusRepository;
		this.unitForceConfigEnsurer = unitForceConfigEnsurer;
		this.domainEventPublisher = domainEventPublisher;
	}
	
	@Transactional
    public void execute(UnitForceChangeRequest request) {
        UnitForceConfigId unitForceConfigId = UnitForceConfigId.required(request.getUnitForceConfigId());
        CatStatusId catStatusId = CatStatusId.required(request.getStatus());
        StatusEntity status = this.catStatusRepository.ensurer(catStatusId);
        Observations observations = null;
        UnavailabilityReasonId unavailabilityReasonId = null;
        
        if(CatStatusId.INACTIVE.equals(request.getStatus().toUpperCase())) {
        	new UnavailabilityReasonId(request.getUnavailabilityReasonId());
        	unavailabilityReasonId = UnavailabilityReasonId.required(request.getUnavailabilityReasonId());
        	this.unavailabilityRepository.ensurer(unavailabilityReasonId);
        	observations = Observations.optional(request.getObservations());
        }
        
        this.unitForceConfigEnsurer.ensurer(unitForceConfigId);
        UserId user = UserId.required(request.getUser());

        UnitForceConfigStatusLogEntity unitForceConfigStatusLogModel = new UnitForceConfigStatusLogEntity(
        		new UnitForceConfigEntity(request.getUnitForceConfigId()),
        		status,
        		!Objects.isNull(unavailabilityReasonId) ? unavailabilityReasonId.getValue(): null,
        		!Objects.isNull(observations) ? observations.getValue(): null,
        		Boolean.TRUE,
        		user.getValue());
        List<String> unitForces = new ArrayList<>();
        unitForces.add(unitForceConfigId.getValue());
        UnitForceModel model = UnitForceModel.create(unitForces, !CatStatusId.INACTIVE.equals(request.getStatus().toUpperCase()), request.getUnavailabilityReasonId(), request.getUser(), "2");
        this.logRepository.save(unitForceConfigStatusLogModel);
        this.unitConfigEntityRepository.updateStatusUnitForce(unitForceConfigId.getValue(), status);
        this.domainEventPublisher.publish(model.pullEvents());
    }

}
