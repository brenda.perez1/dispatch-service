package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.domain.model.unit.entity.UnitForceConfigStatusLogEntity;

public interface UnitForceConfigStatusLogRepository {

    boolean ensurer(Long unitForceConfigStatusLogId);

	void save(UnitForceConfigStatusLogEntity unitForceConfigStatusLogModel);

}
