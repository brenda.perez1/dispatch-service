package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnavailabilityReasonId;

public interface UnavailabilityRepository {

    void ensurer(UnavailabilityReasonId unavailabilityReasonId);
    
}