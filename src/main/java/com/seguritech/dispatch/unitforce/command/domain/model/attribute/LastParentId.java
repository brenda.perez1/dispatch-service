package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.LastParentIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class LastParentId {
    private Long value;

    private LastParentId(Long value) {
        this.value = value;
    }

    public static LastParentId required(Long id) {
        if (id == null)
            throw new LastParentIdRequiredException();

        return new LastParentId(id);
    }

    public static LastParentId optional(Long id) {
        return new LastParentId(id);
    }
}
