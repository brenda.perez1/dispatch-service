package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.Objects;

import org.springframework.stereotype.Repository;

import com.domain.model.mission.entity.StatusEntity;
import com.seguritech.dispatch.unitforce.command.domain.exception.CatStatusNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStatusId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStatusUnitRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.CatStatusRepository;

@Repository
public class CatStatusUnitRepositoryImpl implements CatStatusUnitRepository {

    private final CatStatusRepository repository;

    public CatStatusUnitRepositoryImpl(CatStatusRepository repository) {
        this.repository = repository;
    }
    
    @Override
    public StatusEntity ensurer(CatStatusId statusId) {
    	
    	StatusEntity status = this.repository.findByName(statusId.getValue());
    	 if(Objects.isNull(status)) {
         	throw new CatStatusNotFoundException();
         }
    	
    	 return status;
    }
    
}
