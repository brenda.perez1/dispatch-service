package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class TransitionStageIdRequiredException extends BusinessException {
    public TransitionStageIdRequiredException() {
        super(MessageResolver.TRANSITION_STAGE_ID_REQUERID_EXCEPTION, 3069L);
    }
}
