package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class UnitForceConfigId {

    private String value;

    public UnitForceConfigId(String value) {
        this.value = Optional.ofNullable(value).orElseThrow(UnitIdRequiredException::new);
    }

    public static UnitForceConfigId required(String id){
        return new UnitForceConfigId(id);
    }
}
