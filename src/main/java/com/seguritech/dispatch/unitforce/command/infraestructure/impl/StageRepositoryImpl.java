package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import org.springframework.stereotype.Repository;

import com.domain.model.mission.entity.Mission;
import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.mission.entity.Stage;
import com.seguritech.dispatch.unitforce.command.domain.exception.StageTypeNotSupportException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageConfigEntityRepository;

@Repository
public class StageRepositoryImpl implements StageRepository {

    private final StageConfigEntityRepository stageConfigRepository;

    public StageRepositoryImpl(StageConfigEntityRepository stageConfigRepository) {
        this.stageConfigRepository = stageConfigRepository;
    }

    @Override
    public StageId find(MissionId missionId, CatStageId nextStageId) {
        return this.stageConfigRepository.findByMissionAndStage(
        				new Mission(missionId.getValue()),
        				new Stage(nextStageId.getValue())
        		)
                .map(m -> StageId.required(
                        m.getStage().getId(),
                        m.getStage().getName(),
                        m.getMission().getId(),
                        m.getStageTransition().getId()+0L
                )).orElseGet(null);
    }

	@Override
	public StageId find(MissionId missionId, StageId lastStageId, CatStageId nextStageId) {
		return this.stageConfigRepository.findByStageLastAndMissionAndStage(
						lastStageId.getNextStageId().getValue(),
        				new Mission(missionId.getValue()),
        				new Stage(nextStageId.getValue())
        )
        .map(m -> StageId.required(
        		m.getStage().getId(),
        		m.getStage().getName(),
                m.getMission().getId(),
                m.getStageTransition().getId() + 0L
        )).orElseThrow(() -> new StageTypeNotSupportException(nextStageId.getValue()));
	}
	
}
