package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;

public interface DispatchIncidentSectorRepository {

    boolean exist(IncidentId incidentId, SectorId unitSectorId);

}
