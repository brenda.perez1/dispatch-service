package com.seguritech.dispatch.unitforce.command.application.usecase;

import javax.transaction.Transactional;

import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitStatus;
import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangerequest;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageType;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageMissionLogRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.service.IncidentEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.ReasonCancelEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionFinder;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class CancelStageChangeUseCase {

    private final UnitMissionFinder unitMissionFinder;
    private final UnitMissionRepository repository;
    private final StageMissionLogRepository logRepository;
    private final DomainEventPublisher domainEventPublisher;
    private final ReasonCancelEnsurer reasonCancelEnsurer;
    private final UnitMissionEnsurer unitMissionEnsurer;
    private final IncidentEnsurer incidentEnsurer;
    private final EmergenciesClient client;
    private final IncidentOperatingCorporationRepository incidentOperatingCorporationrepository;
    private final UnitConfigEntityRepository unitConfigEntityRepository;

    public CancelStageChangeUseCase(
            UnitMissionRepository unitMissionRepository, 
            DomainEventPublisher domainEventPublisher, 
            ReasonCancelRepository reasonCancelEnsurer, 
            UnitForceConfigRepository forceConfigRepository, 
            StageIncidentRepository incidentRepository,
            StageMissionLogRepository logRepository,
            EmergenciesClient client,
            IncidentOperatingCorporationRepository incidentOperatingCorporationrepository,
            UnitConfigEntityRepository unitConfigEntityRepository) {
        this.unitMissionFinder = new UnitMissionFinder(unitMissionRepository);
        this.repository = unitMissionRepository;
        this.domainEventPublisher = domainEventPublisher;
        this.reasonCancelEnsurer = new ReasonCancelEnsurer(reasonCancelEnsurer);
        this.incidentEnsurer = new IncidentEnsurer(incidentRepository);
        this.logRepository = logRepository;
        this.unitMissionEnsurer = new UnitMissionEnsurer(repository);
        this.client = client;
        this.incidentOperatingCorporationrepository = incidentOperatingCorporationrepository;
        this.unitConfigEntityRepository = unitConfigEntityRepository;
    }

    @Transactional
    public void execute(String token, StageChangerequest request) {
        
        UnitMissionId unitMissionId = UnitMissionId.required(request.getUnitMissionId());

        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CatStageId nextStageId = CatStageId.preCancelconfigured();
        ReasonCancelId reasonCancelId = ReasonCancelId.optional(request.getReasonCancelId());
        Observations observations = Observations.optional(request.getObservations());

        UnitMissionModel unitMissionModel = this.unitMissionFinder.find(unitMissionId, incidentId);
        StageId lastStageId = unitMissionModel.getCurrentStageId();
        MissionId missionId = unitMissionModel.getCurrentStageId().getMissionId();
        StageId nextStageIdConfig = new StageId(nextStageId, null, missionId, StageTransitionId.required(StageType.CANCEL.getValue()));
        
        unitMissionModel.cancelToNextStage(
        		nextStageIdConfig,
                UserId.required(request.getUserId()),
                reasonCancelId,
                observations,
                unitMissionModel.getPersons()
        );	
        
        StageMissionLogModel stageMissionLogModel = new StageMissionLogModel(
        		null,
        		unitMissionModel.getCreatedByUser().getValue(),
        		null,
        		unitMissionModel.getUpdatedByUser().getValue(),
        		null,
        		new StageModel(nextStageIdConfig.getNextStageId().getValue()),
        		nextStageIdConfig.getStageTransitionId().getValue(),
        		unitMissionModel.getPersons().getValue(),
        		request.getObservations(),
        		request.getReasonCancelId(), 
        		missionId.getValue(), 
        		incidentId.getValue(),
        		unitMissionModel.getUnitForceConfigId(), 
        		unitMissionModel.getUnitMissionId()
        );
        this.logRepository.save(stageMissionLogModel);
        cancelStatusUnitForce(unitMissionModel);
        this.repository.save(unitMissionModel,null);
        this.domainEventPublisher.publish(unitMissionModel.pullEvents());
    }
    
    private void cancelStatusUnitForce(UnitMissionModel unitMission) {
    	this.unitConfigEntityRepository.updateStatusUnitForce(unitMission.getUnitForceConfigId().getValue(), new StatusEntity(UnitStatus.AVAILABLE.getValue()));
    }
    
}
