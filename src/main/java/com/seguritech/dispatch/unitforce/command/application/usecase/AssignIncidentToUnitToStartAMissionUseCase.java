package com.seguritech.dispatch.unitforce.command.application.usecase;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitStatus;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentFolio;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.AssignIncidentToUnitMissionRequest;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotAvailableException;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.domain.model.StageModel;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatOperationalPersonnelRepositoryJpa;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationOfIncidentRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.DispatchIncidentSectorRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageMissionLogRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitCorporationRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitSectorRepository;
import com.seguritech.dispatch.unitforce.command.domain.service.CorporationMissionEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.IncidentCorporationEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.IncidentEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.MissionFinder;
import com.seguritech.dispatch.unitforce.command.domain.service.StagePreconfiguredFinder;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitCorporationFinder;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitForceAvailableEnsurer;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitForceConfigEnsurer;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class AssignIncidentToUnitToStartAMissionUseCase {

    private final UnitForceConfigEnsurer unitForceConfigEnsurer;
    private final CorporationMissionEnsurer missionConfiguredToCorporationOfUnitForceEnsurer;
    private final IncidentEnsurer incidentEnsurer;
    private final UnitCorporationFinder unitCorporationFinder;
    private final IncidentCorporationEnsurer incidentAssignedToUnitCorporation;
    private final UnitMissionRepository repository;
    private final DomainEventPublisher domainEventPublisher;
    private final MissionFinder missionPreconfiguredFinder;
    private final StagePreconfiguredFinder initialStagePreconfiguredFinder;
    private final UnitForceAvailableEnsurer unitForceAvailableEnsurer;
    private final CorporationOfIncidentRepository corporationIncidentRepository;
    private final UnitConfigEntityRepository unitConfigEntityRepository;
    private final StageMissionLogRepository logRepository;
    private final IncidentRepository incidentRepository;
    private final CatOperationalPersonnelRepositoryJpa catOperationalPersonnelRepositoryJpa;

    public AssignIncidentToUnitToStartAMissionUseCase(
            UnitForceConfigRepository unitForceConfigRepository,
            CorporationMissionRepository missionRepository,
            StageIncidentRepository stageIncidentRepository,
            UnitSectorRepository unitSectorRepository,
            UnitCorporationRepository unitCorporationRepository,
            CorporationOfIncidentRepository corporationOfIncidentRepository,
            DispatchIncidentSectorRepository incidentSectorRepository,
            UnitMissionRepository repository,
            DomainEventPublisher domainEventPublisher,
            CorporationOfIncidentRepository corporationIncidentRepository,
            UnitConfigEntityRepository unitConfigEntityRepository,
            StageMissionLogRepository logRepository,
            IncidentRepository incidentRepository,
            CatOperationalPersonnelRepositoryJpa catOperationalPersonnelRepositoryJpa) {
        this.unitForceConfigEnsurer = new UnitForceConfigEnsurer(unitForceConfigRepository);
        this.missionConfiguredToCorporationOfUnitForceEnsurer = new CorporationMissionEnsurer(missionRepository);
        this.incidentEnsurer = new IncidentEnsurer(stageIncidentRepository);
        this.unitCorporationFinder = new UnitCorporationFinder(unitCorporationRepository);
        this.incidentAssignedToUnitCorporation = new IncidentCorporationEnsurer(corporationOfIncidentRepository);
        this.repository = repository;
        this.domainEventPublisher = domainEventPublisher;
        this.missionPreconfiguredFinder = new MissionFinder(missionRepository);
        this.initialStagePreconfiguredFinder = new StagePreconfiguredFinder(missionRepository);
        this.unitForceAvailableEnsurer = new UnitForceAvailableEnsurer(unitForceConfigRepository);
        this.corporationIncidentRepository = corporationIncidentRepository;
        this.unitConfigEntityRepository = unitConfigEntityRepository;
        this.logRepository = logRepository;
        this.incidentRepository = incidentRepository;
        this.catOperationalPersonnelRepositoryJpa = catOperationalPersonnelRepositoryJpa;
    }

    @Transactional
    public void execute(AssignIncidentToUnitMissionRequest request) {
    	
        UnitForceConfigId unitForceConfigId = UnitForceConfigId.required(request.getUnitForceId());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        UserId userId = UserId.required(request.getUserId());

        UnitForceConfigEntity unitForceEntity = this.unitCorporationFinder.getUnitForceConfig(unitForceConfigId);
        SectorId unitSectorId = null;
        CorporationId corporationId = CorporationId.required(unitForceEntity.getCorporation().getId());
        CorporationOperativeId corporationOperativeId = CorporationOperativeId.required(unitForceEntity.getCorporationEntity().getId());
        MissionId missionId = missionResolverPreconfigured(request.getMissionId(), corporationId);
        
        IncidentCorporationEntity incidentCorporationEntity = this.corporationIncidentRepository.getById(incidentId, corporationOperativeId);

        this.ensurers(unitForceConfigId, missionId, incidentId, unitSectorId, corporationId, corporationOperativeId);
        	
        IncidentBounded incidentBounded = this.incidentRepository.find(incidentId);
        
        IncidentFolio incidentFolio = IncidentFolio.required(incidentBounded.getFolio());
        
        List<String> oneSignalUsersId = catOperationalPersonnelRepositoryJpa.findOneSignalUsersIdByUnitForceConfigId(unitForceConfigId.getValue());
        
        StageId initialStageId = this.initialStagePreconfiguredFinder.find(corporationId, missionId);

        UnitMissionModel unitMissionModel = UnitMissionModel.incidentStartMission(
                incidentId,
                unitSectorId,
                corporationId,
                corporationOperativeId,
                initialStageId,
                unitForceConfigId,
                userId,
                incidentFolio,
                oneSignalUsersId,
                incidentBounded.getSubTypeName(),
                incidentBounded.getPriorityName()
        ); 
        
        this.repository.save(unitMissionModel,null);                
       
        if (incidentCorporationEntity != null) {
			incidentCorporationEntity.setAssignedUnitForceDate(new Date());
			incidentCorporationEntity.setUpdateDate(new Date());
			this.corporationIncidentRepository.save(incidentCorporationEntity);
		}

        changeStatusUnitForce(unitForceConfigId);
        
        StageMissionLogModel stageMissionLogModel = new StageMissionLogModel(
        		null,
        		unitMissionModel.getCreatedByUser().getValue(),
        		null,
        		unitMissionModel.getUpdatedByUser().getValue(),
        		null,
        		new StageModel(initialStageId.getNextStageId().getValue()),
        		initialStageId.getStageTransitionId().getValue(),
        		unitMissionModel.getPersons().getValue(),
        		null,
        		null, 
        		missionId.getValue(), 
        		incidentId.getValue(),
        		unitMissionModel.getUnitForceConfigId(), 
        		unitMissionModel.getUnitMissionId()
        );
        
        this.logRepository.save(stageMissionLogModel);
        this.domainEventPublisher.publish(unitMissionModel.pullEvents());
    }

    private void ensurers(UnitForceConfigId unitForceConfigId, MissionId missionId, IncidentId incidentId, SectorId unitSectorId,
    		CorporationId corporationId,
    		CorporationOperativeId unitCorporationOperativeId) {
        this.unitForceConfigEnsurer.ensurer(unitForceConfigId);
        this.unitForceAvailableEnsurer.ensurer(unitForceConfigId);
        List<UnitMissionModel> unitMissionModel = this.repository.findByUnitForceAndIncident(unitForceConfigId, incidentId);
        if (!unitMissionModel.isEmpty()) {
            throw new UnitForceConfigNotAvailableException(unitForceConfigId.getValue());
		}
        this.missionConfiguredToCorporationOfUnitForceEnsurer.ensurer(missionId, corporationId);
        this.incidentEnsurer.ensurer(incidentId);
        this.incidentAssignedToUnitCorporation.ensurer(incidentId, unitCorporationOperativeId);
    }

    private MissionId missionResolverPreconfigured(Long missionId, CorporationId corporationId) {
        if (missionId != null)
            return MissionId.required(missionId);

        return this.missionPreconfiguredFinder.find(corporationId);   

    }
        
    private void changeStatusUnitForce(UnitForceConfigId unitForceConfigId) {
    	this.unitConfigEntityRepository.updateStatusUnitForce(unitForceConfigId.getValue(), new StatusEntity(UnitStatus.IN_SERVICE.getValue()));
    }

}
