package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitMissionIdRequiredException extends BusinessException {
    public UnitMissionIdRequiredException() {
        super(MessageResolver.UNIT_MISSION_ID_REQUERID_EXCEPTION, 3075L);
    }
}
