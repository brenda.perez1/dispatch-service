package com.seguritech.dispatch.unitforce.command.domain.service;

import java.util.List;
import java.util.Optional;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitMissionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import com.seguritech.platform.Service;

@Service
public class UnitMissionFinder {

    private final UnitMissionRepository repository;

    public UnitMissionFinder(UnitMissionRepository repository) {
        this.repository = repository;
    }

    public UnitMissionModel find(UnitForceConfigId unitForceConfigId, IncidentId incidentId) {
        return Optional.ofNullable(this.repository.find(unitForceConfigId, incidentId))
                .orElseThrow(() -> new UnitForceConfigNotFoundException(unitForceConfigId.getValue()));
    }
    
    public UnitMissionModel find(UnitMissionId unitMissionId, IncidentId incidentId) {
        return Optional.ofNullable(this.repository.find(unitMissionId, incidentId))
                .orElseThrow(() -> new UnitMissionNotFoundException(unitMissionId.getValue()));
    }
    
    public Long countUnitMission (IncidentId incidentId, CorporationId corporationId) {
    	return Optional.ofNullable(this.repository.countUnitMission(incidentId, corporationId))
    			.orElseThrow(() -> new UnitMissionNotFoundException(incidentId.getValue()));
    }

}
