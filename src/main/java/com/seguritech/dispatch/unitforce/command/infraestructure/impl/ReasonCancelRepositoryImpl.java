package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.ReasonCancelEntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ReasonCancelRepositoryImpl implements ReasonCancelRepository {

    private final ReasonCancelEntityRepository reasonCancelEntityRepository;

    public ReasonCancelRepositoryImpl(ReasonCancelEntityRepository reasonCancelEntityRepository) {
        this.reasonCancelEntityRepository = reasonCancelEntityRepository;
    }

    @Override
    public boolean exist(ReasonCancelId reasonCancelId) {
        return this.reasonCancelEntityRepository.findById(reasonCancelId.getValue()).isPresent();
    }
}
