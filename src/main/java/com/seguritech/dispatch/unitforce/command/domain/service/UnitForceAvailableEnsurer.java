package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotAvailableException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;

public class UnitForceAvailableEnsurer {
    private final UnitForceConfigRepository repository;

    public UnitForceAvailableEnsurer(UnitForceConfigRepository repository) {
        this.repository = repository;
    }

    public void ensurer(UnitForceConfigId unitForceConfigId) {
        if (!this.repository.available(unitForceConfigId))
            throw new UnitForceConfigNotAvailableException(unitForceConfigId.getValue());
    }

}
