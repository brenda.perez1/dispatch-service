package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class LastParentIdRequiredException extends BusinessException {

    public LastParentIdRequiredException() {
        super(MessageResolver.LAST_PARENT_ID_REQUERID_EXCEPTION, 3049L);
    }
}
