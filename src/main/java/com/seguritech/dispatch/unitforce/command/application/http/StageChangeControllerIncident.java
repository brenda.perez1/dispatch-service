package com.seguritech.dispatch.unitforce.command.application.http;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.unitforce.command.application.http.request.PersonsChangeHttpRequest;
import com.seguritech.dispatch.unitforce.command.application.http.request.StageChangeHttpRequest;
import com.seguritech.dispatch.unitforce.command.application.usecase.CancelStageChangeUseCase;
import com.seguritech.dispatch.unitforce.command.application.usecase.PersonChangeUseCase;
import com.seguritech.dispatch.unitforce.command.application.usecase.StageChangeUseCase;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.PersonsChangeRequest;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangerequest;

import io.swagger.annotations.ApiOperation;

@RestController
public class StageChangeControllerIncident extends IncidentUnitMissionBaseController {

    private final StageChangeUseCase stageChangeUseCase;
    private final CancelStageChangeUseCase cancelStageChangeUseCase;
    private final PersonChangeUseCase personChangeUseCase;
    private static final String AUTHORIZATION_HEADER = "AUTHORIZATION";
	private static final String AUTHORIZATION_BEARER = "Bearer ";

    public StageChangeControllerIncident(StageChangeUseCase stageChangeUseCase, CancelStageChangeUseCase cancelStageChangeUseCase, PersonChangeUseCase personChangeUseCase) {
        this.stageChangeUseCase = stageChangeUseCase;
        this.cancelStageChangeUseCase = cancelStageChangeUseCase;
        this.personChangeUseCase = personChangeUseCase;
    }
    
    @ApiOperation(value = "Stage change", nickname = "update", notes = "Stage change")
    @PatchMapping("{emergency-id}/unit-force/{unitmission-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CadLiteAPIResponse<Object>> update(@RequestBody StageChangeHttpRequest request,
                                                             @PathVariable("emergency-id") String emergencyId,
                                                             @PathVariable("unitmission-id") String unitmissionId,
                                                             UsernamePasswordAuthenticationToken user) {
    	
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
        this.stageChangeUseCase.execute(new StageChangerequest(
        		unitmissionId,
                emergencyId,
                request.getNextStageId(),
                principal.getId(),
                request.getReasonCancelId(),
                request.getObservations()
        ));
        return CadLiteAPIResponse.build(CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.SUCCESS);
    }
    
    @ApiOperation(value = "Persons change", nickname = "update", notes = "Person change")
    @PatchMapping("{emergency-id}/unit-force/{unitmission-id}/persons")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CadLiteAPIResponse<Object>> updatePerson(@RequestBody PersonsChangeHttpRequest request,
                                                             @PathVariable("emergency-id") String emergencyId,
                                                             @PathVariable("unitmission-id") String unitmissionId,
                                                             UsernamePasswordAuthenticationToken user) {
    	
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
        this.personChangeUseCase.execute(new PersonsChangeRequest(
        		unitmissionId,
                emergencyId,
                principal.getId(),
                request.getPersons()
        ));
        return CadLiteAPIResponse.build(CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.SUCCESS);
    }
    
    @ApiOperation(value = "Cancel stage change", nickname = "update", notes = "Stage change")
    @PatchMapping("{emergency-id}/unit-force/{unitmission-id}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<CadLiteAPIResponse<Object>> cancel(@RequestBody StageChangeHttpRequest request,
                                                             @PathVariable("emergency-id") String emergencyId,
                                                             @PathVariable("unitmission-id") String unitmissionId,
                                                             UsernamePasswordAuthenticationToken user,
                                                             HttpServletRequest http) {
    	
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
        this.cancelStageChangeUseCase.execute(AUTHORIZATION_BEARER.concat(http.getHeader(AUTHORIZATION_HEADER)), new StageChangerequest(
        		unitmissionId,
                emergencyId,
                null,
                principal.getId(),
                request.getReasonCancelId(),
                request.getObservations()
        ));
        return CadLiteAPIResponse.build(CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.SUCCESS);
    }

}
