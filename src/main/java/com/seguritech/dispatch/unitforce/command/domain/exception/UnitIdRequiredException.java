package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UnitIdRequiredException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public UnitIdRequiredException() {
        super(MessageResolver.UNIT_ID_REQUERID_EXCEPTION, 3074L);
    }
	
}
