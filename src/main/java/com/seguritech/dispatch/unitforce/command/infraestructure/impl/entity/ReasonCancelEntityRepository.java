package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.mission.entity.CatReasonMissionCancel;

public interface ReasonCancelEntityRepository extends JpaRepository<CatReasonMissionCancel, Long> {
}
