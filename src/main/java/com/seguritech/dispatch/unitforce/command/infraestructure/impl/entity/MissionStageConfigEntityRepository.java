package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.domain.model.mission.entity.Mission;
import com.domain.model.mission.entity.MissionStageConfig;

@Repository
public interface MissionStageConfigEntityRepository extends JpaRepository<MissionStageConfig, String> {
    Optional<List<MissionStageConfig>> findByMissionAndStageLast(Mission missionId, Long stageParent);
    @Query("FROM MissionStageConfig msc "
    		+ "JOIN msc.mission m "
    		+ "JOIN msc.stage s "
    		+ "JOIN msc.stageTransition st "
    		+ "WHERE m.id = :missionId "
    		+ "AND s.id = :stageId "
    		+ "AND st.id = :transitionId "
    		+ "AND (:lastStage IS NULL OR msc.stageLast = :lastStage)")
    Optional<MissionStageConfig> findFirstByMissionIdAndStageIdAndStageTransitionId(@Param("missionId") Long missionId,@Param("stageId") Long stageId, @Param("transitionId") Long transitionId, @Param("lastStage") Long lastStage);
    Optional<MissionStageConfig> findFirstByMissionIdAndStageIdAndStageTransitionIdAndStageLast(Long missionId, Long stageId, Long transitionId, Long stageParent);
    
    @Query("SELECT DISTINCT msc FROM MissionStageConfig msc WHERE msc.mission.id =:idMission")
    List<MissionStageConfig>findMissionStageConfigByIdMission(@Param("idMission") Long idMission);

}
