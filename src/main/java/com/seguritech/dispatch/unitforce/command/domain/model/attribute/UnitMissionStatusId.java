package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitMissionStatusIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class UnitMissionStatusId {

    private Long value;

    private UnitMissionStatusId(Long value) {
        this.value = Optional.ofNullable(value).orElseThrow(UnitMissionStatusIdRequiredException::new);
    }

    public static UnitMissionStatusId required(Long id){
        return new UnitMissionStatusId(id);
    }

}
