package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;

public interface StageTransitionRepository {

    boolean exist (StageTransitionId stageTransitionId);

}
