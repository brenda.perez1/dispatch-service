package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;

public class ChangeStageException extends BusinessException {

	private static final long serialVersionUID = 1L;

    public ChangeStageException(Object... catStageId) {
        super(MessageResolver.CHANGE_STAGE_EXCEPTION,catStageId, 3039L);
    }
	
}
