package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class EmergencyIdRequiredException extends BusinessException {
    public EmergencyIdRequiredException() {
        super(MessageResolver.EMERGENCY_ID_REQUERID_EXCEPTION, 3042L);
    }
}
