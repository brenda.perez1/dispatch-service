package com.seguritech.dispatch.unitforce.command.infraestructure.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.domain.model.mission.entity.StatusEntity;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitStatus;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitConfigEntityRepository;

@Repository
public class UnitForceConfigRepositoryImpl implements UnitForceConfigRepository {

    private final UnitConfigEntityRepository repository;

    public UnitForceConfigRepositoryImpl(UnitConfigEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean ensurer(UnitForceConfigId unitForceConfigId) {
        return this.repository.findById(unitForceConfigId.getValue()).isPresent();
    }

    @Override
    public boolean available(UnitForceConfigId unitForceConfigId) {
        return Optional.ofNullable(this.repository.existsByIdUnitForceConfigAndIdStatusNot(unitForceConfigId.getValue(), new StatusEntity(UnitStatus.NOT_AVAILABLE.getValue()))).orElse(false);
    }

	@Override
	public List<UnitForceConfigEntity> findCorporationOperatives(List<Long> corporationOperativesIds) {
		// TODO Auto-generated method stub
		return this.repository.findAllByCorporationEntityId(corporationOperativesIds);
	}
    
}
