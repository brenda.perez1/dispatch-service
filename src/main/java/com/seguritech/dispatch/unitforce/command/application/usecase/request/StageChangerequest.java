package com.seguritech.dispatch.unitforce.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StageChangerequest {
	
    private String unitMissionId;
    private String incidentId;
    private Long nextStageId;
    private String userId;
    private Long reasonCancelId;
    private String observations;
    
}
