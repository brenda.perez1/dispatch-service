package com.seguritech.dispatch.unitforce.command.domain.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentFolio;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.event.InThePlaceStageEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.IncidentAssignedToForceUnitEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.MissionCanceledEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.MissionEndedEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.OnTheWayStageEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.StageChangedEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.StageEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.SubStageMissionChangedEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.TransferPeopleStageEvent;
import com.seguritech.dispatch.unitforce.command.domain.event.TransferredPersonStageEvent;
import com.seguritech.dispatch.unitforce.command.domain.exception.ChangeStageException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.platform.domain.RootAggregate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class UnitMissionModel extends RootAggregate {
	
    private final UnitMissionId unitMissionId;
    private SectorId sectorId;
    private CorporationId corporationId;
    private CorporationOperativeId corporationOperativeId;
    private List<StageId> nextStages;
    private StageId currentStageId;
    private UnitForceConfigId unitForceConfigId;
    private IncidentId incidentId;
    private UserId createdByUser;
    private UserId updatedByUser;
    private Date createdAt;
    private Date updateAt;
    private Date startMissionDate;
    private Date endMissionDate;
    private ReasonCancelId reasonCancelId;
    private Observations observations;
    private Persons persons;
    private boolean active;
    private IncidentFolio incidentFolio;
    private List<String> oneSignalUsersId;
    private String subTypeName;
    private String priorityName;
    private String option;
    private String sheetEmergency;
    private List<String> unitForceIds;

    public void changedToNextStage(StageId lastStageId, UserId userId, ReasonCancelId reasonCancelId, Observations observations, 
    		Persons persons, String incidentSheet, String option, List<String> unitForceIds) {
        this.checkIfTheStageIsInTheFollowingStages(lastStageId);
        StageType stageType = StageType.valueOf(lastStageId.getStageTransitionId().getValue());

        this.currentStageId = lastStageId;
        this.updatedByUser = userId;
        this.reasonCancelId = reasonCancelId;
        this.observations = observations;
        this.persons = persons;
        this.option = option;
        this.sheetEmergency = incidentSheet;
        this.unitForceIds = unitForceIds;
        switch (stageType) {
            case INITIAL:
                this.startMissionDate = new Date();
                IncidentAssignedToForceUnitEvent incidentAssignedToForceUnitEvent = factory(this, IncidentAssignedToForceUnitEvent.class);
                this.record(incidentAssignedToForceUnitEvent);
                break;
            case CANCEL:
                this.endMissionDate = new Date();
                this.active = false;
                MissionCanceledEvent missionCanceledEvent = factory(this, MissionCanceledEvent.class);
                this.record(missionCanceledEvent);
                break;
            case FINALIZE:
                this.endMissionDate = new Date();
                this.active = false;
                MissionEndedEvent missionEndedEvent = factory(this, MissionEndedEvent.class);
                super.record(missionEndedEvent);
                break;
            case TRANSLATION:
                TransferredPersonStageEvent transferredPersonStageEvent = factory(this, TransferredPersonStageEvent.class);
                super.record(transferredPersonStageEvent);
                break;
            case SUBTRACKING:
                SubStageMissionChangedEvent subStageMissionChangedEvent = factory(this, SubStageMissionChangedEvent.class);
                super.record(subStageMissionChangedEvent);
                break;
            case TRANSFER_PEOPLE:
            	TransferPeopleStageEvent transferPeopleStageEvent = factory(this, TransferPeopleStageEvent.class);
                super.record(transferPeopleStageEvent);
            	break;
            case ON_THE_WAY:
            	OnTheWayStageEvent onTheWayStageEvent = factory(this, OnTheWayStageEvent.class);
                super.record(onTheWayStageEvent);
            	break;
            case IN_THE_PLACE:
            	InThePlaceStageEvent inThePlaceStageEvent = factory(this, InThePlaceStageEvent.class);
                super.record(inThePlaceStageEvent);
            	break;
        }
        StageChangedEvent stageChangedEvent = factory(this, StageChangedEvent.class);
        this.record(stageChangedEvent);
        this.updateAt = new Date();
    }
    
    public void cancelToNextStage(StageId lastStageId, UserId userId, ReasonCancelId reasonCancelId, Observations observations, Persons persons) {
        StageType stageType = StageType.valueOf(lastStageId.getStageTransitionId().getValue());

        this.currentStageId = lastStageId;
        this.updatedByUser = userId;
        this.reasonCancelId = reasonCancelId;
        this.observations = observations;
        this.persons = persons;
        List<String> idsUnitForce = new ArrayList<>();
        if ((this.unitForceConfigId != null && this.unitForceConfigId.getValue() != null)) {
        	idsUnitForce.add(this.unitForceConfigId.getValue());
            this.unitForceIds = idsUnitForce;
		}
        switch (stageType) {
            case CANCEL:
                this.endMissionDate = new Date();
                this.active = false;
                MissionCanceledEvent missionCanceledEvent = factory(this, MissionCanceledEvent.class);
                this.record(missionCanceledEvent);
                break;
        }
        StageChangedEvent stageChangedEvent = factory(this, StageChangedEvent.class);
        this.record(stageChangedEvent);
        this.updateAt = new Date();
    }

    public <T extends StageEvent> T factory(Class<? extends T> stageClass) {
        return this.factory(this, stageClass);
    }

    private <T extends StageEvent> T factory(UnitMissionModel unitMissionModel, Class<? extends T> stageClass) {
        StageEvent stageEvent = null;
        try {
            stageEvent = stageClass.newInstance();

            setValue(stageEvent, "sectorId", null);
            setValue(stageEvent, "corporationId", unitMissionModel.getCorporationOperativeId().getValue());
            setValue(stageEvent, "unitMissionId", unitMissionModel.getUnitMissionId().getValue());
            setValue(stageEvent, "missionId", unitMissionModel.getCurrentStageId().getMissionId().getValue());
            setValue(stageEvent, "idStageTransition", unitMissionModel.getCurrentStageId().getStageTransitionId().getValue());
            setValue(stageEvent, "stageId", unitMissionModel.getCurrentStageId().getNextStageId().getValue());
            setValue(stageEvent, "stageName", unitMissionModel.getCurrentStageId().getNextStageName());
            setValue(stageEvent, "unitForceConfigId", unitMissionModel.getUnitForceConfigId().getValue());
            setValue(stageEvent, "incidentId", unitMissionModel.getIncidentId().getValue());
            setValue(stageEvent, "createdByUser", unitMissionModel.getCreatedByUser().getValue());
            setValue(stageEvent, "updatedByUser", unitMissionModel.getUpdatedByUser().getValue());
            setValue(stageEvent, "createdAt", unitMissionModel.getCreatedAt());
            setValue(stageEvent, "updateAt", unitMissionModel.getUpdateAt());
            setValue(stageEvent, "startMissionDate", unitMissionModel.getStartMissionDate());
            setValue(stageEvent, "endMissionDate", unitMissionModel.getEndMissionDate());
            setValue(stageEvent, "reasonCancelId", unitMissionModel.getReasonCancelId().getValue());
            setValue(stageEvent, "observations", unitMissionModel.getObservations().getValue());
            setValue(stageEvent, "persons", unitMissionModel.getPersons().getValue());
            setValue(stageEvent, "active", unitMissionModel.isActive());
            setValue(stageEvent, "incidentFolio", unitMissionModel.incidentFolio != null ? unitMissionModel.incidentFolio.getValue() : null);
            setValue(stageEvent, "oneSignalUsersId", unitMissionModel.getOneSignalUsersId() != null ? unitMissionModel.getOneSignalUsersId() : null);
            setValue(stageEvent, "subTypeName", unitMissionModel.getSubTypeName() != null ? unitMissionModel.getSubTypeName() : null);
            setValue(stageEvent, "priorityName", unitMissionModel.getPriorityName() != null ? unitMissionModel.getPriorityName() : null);
            setValue(stageEvent, "option", unitMissionModel.getOption());
            setValue(stageEvent, "incidentSheet", unitMissionModel.getSheetEmergency());
            setValue(stageEvent, "unitForceConfigIds", unitMissionModel.getUnitForceIds() != null ? unitMissionModel.getUnitForceIds() : null);

            
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return (T) stageEvent;
    }

    private void recordEvent(Class<? extends StageEvent> stageClass) {
        this.record(this.factory(this, stageClass));
    }

    private void setValue(StageEvent stageEvent, String fieldName, Object value) throws IllegalAccessException, NoSuchFieldException {
        Field fieldUnitMissionId = stageEvent.getClass().getSuperclass().getDeclaredField(fieldName);
        fieldUnitMissionId.setAccessible(true);
        fieldUnitMissionId.set(stageEvent, value);
        fieldUnitMissionId.setAccessible(false);
    }

    private void checkIfTheStageIsInTheFollowingStages(StageId currentStageId) {

        boolean isPresent = this.nextStages.stream()
                .filter(stageModel -> stageModel.equals(currentStageId))
                .findFirst().isPresent();

        if (!isPresent)
            throw new ChangeStageException(currentStageId);
    }

    public static UnitMissionModel incidentStartMission(IncidentId incidentId, SectorId unitSectorId, CorporationId corporationId, CorporationOperativeId unitCorporationOperativeId ,StageId lastStageId, UnitForceConfigId unitForceConfigId, UserId userId, IncidentFolio incidentFolio, List<String> oneSignalUsersId, String subTypeName, String priorityName) {
        UnitMissionModel model = new UnitMissionModel(
                UnitMissionId.generateId(),
                unitSectorId,
                corporationId,
                unitCorporationOperativeId,
                new ArrayList<>(),
                lastStageId,
                unitForceConfigId,
                incidentId,
                userId,
                userId,
                new Date(),
                new Date(),
                new Date(),
                new Date(),
                ReasonCancelId.byDefault(),
                Observations.optional(null),
                Persons.optional(null),
                true,
                incidentFolio,
                oneSignalUsersId,
                subTypeName,
                priorityName,
                null,
                null,
                null
        );

        model.record(model.factory(IncidentAssignedToForceUnitEvent.class));
        return model;
    }

	public void changedToPersonNextStage(UserId userId, Persons persons) {
		
//	        this.currentStageId = lastStageId;
	        this.updatedByUser = userId;
	        this.persons = persons;
	        
	        StageChangedEvent stageChangedEvent = factory(this, StageChangedEvent.class);
	        this.record(stageChangedEvent);
		
	}
	
}
