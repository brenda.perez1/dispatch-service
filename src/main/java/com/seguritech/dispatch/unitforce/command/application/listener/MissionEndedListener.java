package com.seguritech.dispatch.unitforce.command.application.listener;

import com.seguritech.dispatch.unitforce.command.application.usecase.BinnacleUseCase;
import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangeLogRequest;
import com.seguritech.dispatch.unitforce.command.domain.event.listener.MissionEndedEvent;
import com.seguritech.platform.application.messagging.DomainEventListener;
import com.seguritech.platform.application.messagging.DomainSourceEventListener;
import com.seguritech.platform.infrastructure.messaging.DomainEventEnvelope;
import org.springframework.stereotype.Component;

@Component
@DomainSourceEventListener(domain = "incidents-service")
public class MissionEndedListener {

    private final BinnacleUseCase useCase;

    public MissionEndedListener(BinnacleUseCase useCase) {
        this.useCase = useCase;
    }

    @DomainEventListener(name = "mission-ended", version = "v1")
    public void onAssignedIncidents(DomainEventEnvelope<MissionEndedEvent> evt) {
        MissionEndedEvent event = evt.getPayload();
        useCase.create(new StageChangeLogRequest(
                event.getSectorId(),
                event.getCorporationId(),
                event.getUnitMissionId(),
                event.getMissionId(),
                event.getStageId(),
                event.getUnitForceConfigId(),
                event.getIncidentId(),
                event.getCreatedByUser(),
                event.getUpdatedByUser(),
                event.getCreatedAt(),
                event.getUpdateAt(),
                event.getStartMissionDate(),
                event.getEndMissionDate(),
                event.getReasonCancelId(),
                event.getObservations(),
                event.getPersons(),
                event.isActive(),
                event.getIdStageTransition()
        ));
    }

}
