package com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity;


import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.geofence.entity.Geofence;

public interface CatSectorEntityRepository extends JpaRepository<Geofence, Long> {

}
