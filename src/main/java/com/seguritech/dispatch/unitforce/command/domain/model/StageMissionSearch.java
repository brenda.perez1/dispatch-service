package com.seguritech.dispatch.unitforce.command.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class StageMissionSearch {

	private String idMissionStageConfig;
	private String stageBackGroundColor;
	private String stageName;
	private String stageIcon;
	private String stageTextcolor;
	private Long lastStageId;
	private Long transitionId;
	private String stageTransitionDesc;

}
