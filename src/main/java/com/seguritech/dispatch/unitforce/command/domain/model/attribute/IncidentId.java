//package com.seguritech.dispatch.unitforce.command.domain.model.attribute;
//
//import com.seguritech.dispatch.unitforce.command.domain.exception.EmergencyIdRequiredException;
//import lombok.EqualsAndHashCode;
//import lombok.Getter;
//
//import java.util.Optional;
//
//@Getter
//@EqualsAndHashCode
//public class IncidentId {
//
//    private String value;
//
//    private IncidentId(String value) {
//        this.value = Optional.ofNullable(value).orElseThrow(EmergencyIdRequiredException::new);
//    }
//
//    public static IncidentId required(String id){
//        return new IncidentId(id);
//    }
//}
