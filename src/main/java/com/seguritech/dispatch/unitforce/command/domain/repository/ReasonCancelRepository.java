package com.seguritech.dispatch.unitforce.command.domain.repository;

import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;

public interface ReasonCancelRepository {

    boolean exist(ReasonCancelId reasonCancelId);

}
