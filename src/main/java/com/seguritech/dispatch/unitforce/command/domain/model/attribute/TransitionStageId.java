package com.seguritech.dispatch.unitforce.command.domain.model.attribute;

import com.seguritech.dispatch.unitforce.command.domain.exception.TransitionStageIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class TransitionStageId {
    private Long value;

    public TransitionStageId(Long value) {
        this.value = Optional.ofNullable(value).orElseThrow(TransitionStageIdRequiredException::new);
    }

    public static TransitionStageId required(Long id) {
        return new TransitionStageId(id);
    }
}
