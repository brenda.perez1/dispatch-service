package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitRepository;
import com.seguritech.platform.Service;

@Service
public class UnitEnsurer {

    private final UnitRepository repository;

    public UnitEnsurer(UnitRepository repository) {
        this.repository = repository;
    }

    public void ensurer(UnitForceConfigId unitForceConfigId) {
        if (!this.repository.exists(unitForceConfigId))
            throw new UnitForceConfigNotFoundException(unitForceConfigId.getValue());
    }

}
 