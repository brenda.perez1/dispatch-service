package com.seguritech.dispatch.unitforce.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;
import com.seguritech.dispatch.unitforce.command.domain.model.StageType;

public class InvalidStageType extends BusinessException {

    public InvalidStageType(Object... stageType) {
        super(MessageResolver.INVALID_STAGE_TYPE_EXCEPTION,stageType, 3048L);
    }
}
