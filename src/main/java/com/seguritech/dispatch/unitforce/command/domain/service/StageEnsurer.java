package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StagePreconfiguredNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;

public class StageEnsurer {

    private final CatStageRepository repository;

    public StageEnsurer(CatStageRepository repository) {
        this.repository = repository;
    }

    public void ensurer(CatStageId nextStageId) {
        if (!this.repository.exist(nextStageId))
            throw new StagePreconfiguredNotFoundException(nextStageId.getValue());
    }

}
