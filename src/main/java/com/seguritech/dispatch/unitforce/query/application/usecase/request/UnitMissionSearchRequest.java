package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import com.seguritech.platform.application.http.Order;
import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;

@Getter
public class UnitMissionSearchRequest extends AbstractSearcherRequest {

	private final String incidentId;
	private final Long corporationId;

	public UnitMissionSearchRequest(Integer size, Integer page, String incidentId, Long corporationId, String orderByField, Order order) {
		super(size, page, null);
		this.incidentId = incidentId;
		this.corporationId = corporationId;
		this.orderByField = orderByField;
		this.order = order;
	}

}
