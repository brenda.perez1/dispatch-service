package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SuggestUnitForceRequest {
	
	private List<Long> corporationOperativesIds;
	private Long profile;

}
