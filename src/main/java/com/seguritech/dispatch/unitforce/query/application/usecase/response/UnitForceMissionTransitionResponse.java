package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public class UnitForceMissionTransitionResponse {

    private final Long transitionId;
    private final String transition;

}
