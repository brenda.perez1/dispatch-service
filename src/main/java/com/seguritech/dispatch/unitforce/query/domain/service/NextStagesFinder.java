package com.seguritech.dispatch.unitforce.query.domain.service;

import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.NextStageResponse;
import com.seguritech.dispatch.unitforce.query.domain.model.MissionId;
import com.seguritech.dispatch.unitforce.query.domain.model.StageId;
import com.seguritech.dispatch.unitforce.query.domain.repository.NextStagesRepository;
import com.seguritech.platform.Service;

@Service
public class NextStagesFinder {

    private final NextStagesRepository repository;

    public NextStagesFinder(NextStagesRepository repository) {
        this.repository = repository;
    }

    public PageDataProjectionDTO<NextStageResponse> find(MissionId missionId, StageId stageId) {
        return this.repository.search(missionId, stageId);
    }

}
