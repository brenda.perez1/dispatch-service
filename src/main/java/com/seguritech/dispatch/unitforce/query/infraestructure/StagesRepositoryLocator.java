package com.seguritech.dispatch.unitforce.query.infraestructure;

import com.seguritech.dispatch.unitforce.query.application.usecase.response.NextStageResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.HashMap;

@Configuration
public class StagesRepositoryLocator {

    @Bean
    public HashMap<Long, HashMap<Long, ArrayList<NextStageResponse>>> stages() {
        return new HashMap<Long, HashMap<Long, ArrayList<NextStageResponse>>>() {{
            put(1L, new HashMap<Long, ArrayList<NextStageResponse>>() {
                {
                    put(1L, new ArrayList<NextStageResponse>() {{
                        add(new NextStageResponse(1L, "Inicial"));
                    }});
                    put(2L, new ArrayList<NextStageResponse>() {{
                        add(new NextStageResponse(2L, "En camino"));
                        add(new NextStageResponse(3L, "Cancelar"));
                    }});
                    put(3L, new ArrayList<NextStageResponse>() {{
                        add(new NextStageResponse(4L, "Finalizar"));
                        add(new NextStageResponse(5L, "Cancelar"));
                    }});
                }
            });
        }};
    }

}
