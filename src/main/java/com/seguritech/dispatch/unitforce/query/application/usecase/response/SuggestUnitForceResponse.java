package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SuggestUnitForceResponse {

	private String id;
	private Double distance;
	private Double duration;

}
