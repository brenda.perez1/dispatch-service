package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UnitForceSearchStatusResponse {

	private Long id;
	private String name;

}
