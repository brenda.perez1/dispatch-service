package com.seguritech.dispatch.unitforce.query.application.api;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ProfileIdRequierdException;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.query.application.usecase.IncidentUnitForceSearchUseCase;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitForceSearchDetailRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceSearchResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

//TODO: REfactorizar el Path de despacho
@RestController
@Api(tags = { "UNIT MISSION" })
@RequestMapping("/dispatch/unit-mission")
public class UnitMissionStageBaseController {

	private final IncidentUnitForceSearchUseCase searchUseCase;

	public UnitMissionStageBaseController(IncidentUnitForceSearchUseCase searchUseCase) {
		this.searchUseCase = searchUseCase;
	}

	@ApiOperation("Search all units force mission by profile")
	@GetMapping
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> search(
			// Pagination
			@ApiParam(value = "Result page  delimiter by size , it starts at 0 ") @RequestParam(value = "page", defaultValue = "0") Integer page,
			@ApiParam(value = "Result page  delimiter size of results") @RequestParam(value = "size", defaultValue = "25") Integer size,
			UsernamePasswordAuthenticationToken user) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		
		return CadLiteAPIResponse
				.build(this.searchUseCase.searchByProfile(new UnitForceSearchRequest(size, page, Long.parseLong(activeProfile))));
	}

	@ApiOperation("Search unit mission by id")
	@GetMapping("{unitmission-id}/by-unitmission")
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> searchByUnitMission(
			@PathVariable("unitmission-id") String unitMissionId,
			// Pagination
			@ApiParam(value = "Result page  delimiter by size , it starts at 0 ") @RequestParam(value = "page", defaultValue = "0") Integer page,
			@ApiParam(value = "Result page  delimiter size of results") @RequestParam(value = "size", defaultValue = "25") Integer size,
			@RequestBody UnitForceSearchRequest request,
			UsernamePasswordAuthenticationToken user) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		request.setProfileId(Long.parseLong(activeProfile));
		request.setUnitMissionId(unitMissionId);

		return CadLiteAPIResponse
				.build(this.searchUseCase.searchById(request));
	}
	
	@ApiOperation("Search unit mission by unit force")
	@GetMapping("{unitforce-id}/by-unitforce")
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> searchByUnitForce(
			@PathVariable("unitforce-id") String unitforceId,
			// Pagination
			@ApiParam(value = "Result page  delimiter by size , it starts at 0 ") @RequestParam(value = "page", defaultValue = "0") Integer page,
			@ApiParam(value = "Result page  delimiter size of results") @RequestParam(value = "size", defaultValue = "25") Integer size,
			@RequestBody UnitForceSearchRequest request,
			UsernamePasswordAuthenticationToken user) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		request.setProfileId(Long.parseLong(activeProfile));
		request.setUnitforceId(unitforceId);

		return CadLiteAPIResponse
				.build(this.searchUseCase.searchById(request));
	}
	
	@ApiOperation("Search unit mission detail by unit force")
	@GetMapping("/find-unit-mission")
	public ResponseEntity<CadLiteAPIResponse<UnitForceSearchResponse>> findUnitMission(@RequestBody UnitForceSearchDetailRequest request) {
		return CadLiteAPIResponse.build(this.searchUseCase.findUnitMissionByIdUnitForceConfig(request.getUnitForceId()));
	}
	
	@ApiOperation("Search units mission by filter")
	@PostMapping("by-filter")
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> searchByCorporation(
			@RequestBody UnitForceSearchRequest request,
			UsernamePasswordAuthenticationToken user) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		request.setProfileId(Long.parseLong(activeProfile));
		return CadLiteAPIResponse
				.build(this.searchUseCase.searchByProfile(request));
	}
	
	@ApiOperation("Search units mission by filter")
	@GetMapping("by-emergency")
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> searchByEmergency(
			@RequestBody IncidentUnitForceSearchRequest request) {
		return CadLiteAPIResponse.build(this.searchUseCase.search(request));
	}
	
}
