package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UnitForceSearchResponse {

	private UnitForceSearchStatusResponse status;
	private String emergencyId;
	private String folio;
	private String unitMissionId;
	private Boolean active;
	
}
