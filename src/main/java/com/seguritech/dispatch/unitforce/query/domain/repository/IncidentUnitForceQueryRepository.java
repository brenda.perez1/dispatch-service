package com.seguritech.dispatch.unitforce.query.domain.repository;

import java.util.List;

import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentAndUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.SuggestUnitForceRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceSearchResponse;

public interface IncidentUnitForceQueryRepository {

    PageDataProjectionDTO<IncidentUnitForceSearchResponse> search(IncidentUnitForceSearchRequest request);
    
    IncidentUnitForceSearchResponse search(IncidentAndUnitForceSearchRequest request, Boolean active, String unitMissionId);
    
    PageDataProjectionDTO<IncidentUnitForceSearchResponse> searchByFilter(UnitForceSearchRequest request);
    
    PageDataProjectionDTO<IncidentUnitForceSearchResponse> searchById(UnitForceSearchRequest request);
    
    List<IncidentUnitForceSearchResponse> searchByOperativeGroups(SuggestUnitForceRequest request);

	UnitForceSearchResponse findUnitMissionByIdUnitForceConfig(String unitForceConfigId);

}
