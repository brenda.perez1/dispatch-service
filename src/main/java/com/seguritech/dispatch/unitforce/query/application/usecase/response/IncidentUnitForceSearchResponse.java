package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import java.util.Date;
import java.util.List;

import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentUnitForceSearchResponse {

	private final String unitMissionId;
    private final Date startMissionDate;
    private final Date endMissionDate;
    private final Date lastUpdate;
    private final Date assignedAt;
    private final String assignedByUser;
    private final Long persons;
    private final UnitForceTransportResponse transport;
    private final UnitForceMissionResponse mission;
    private final UnitForceResponse unitForce;
    private final List<StageMissionLogModel> stageMissionLogs;

}
