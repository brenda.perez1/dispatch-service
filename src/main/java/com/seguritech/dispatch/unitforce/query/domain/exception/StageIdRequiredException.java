package com.seguritech.dispatch.unitforce.query.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;

public class StageIdRequiredException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public StageIdRequiredException() {
        super("El ID de la etapa es requerido", 0L);
    }
	
}
