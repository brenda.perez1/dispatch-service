package com.seguritech.dispatch.unitforce.query.application.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.application.http.IncidentUnitMissionBaseController;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionOrderByFieldEnum;
import com.seguritech.dispatch.unitforce.query.application.usecase.IncidentUnitForceSearchUseCase;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentAndUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.SuggestUnitForceRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitMissionSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.SuggestUnitForceResponse;
import com.seguritech.platform.application.http.Order;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class IncidentUnitMissionQueryController extends IncidentUnitMissionBaseController {

	private final IncidentUnitForceSearchUseCase searchUseCase;

	public IncidentUnitMissionQueryController(IncidentUnitForceSearchUseCase searchUseCase) {
		this.searchUseCase = searchUseCase;
	}

	@GetMapping("/{emergency-id}/unit-force")
	@ApiOperation("Search units force assigned to incident")
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> search(
			@PathVariable("emergency-id") String emergencyId,
			//Pagination
			@ApiParam(value = "Result page  delimiter by size , it starts at 0 ") @RequestParam(value = "page", defaultValue = "0")
			Integer page,
			@ApiParam(value = "Result page  delimiter size of results") @RequestParam(value = "size", defaultValue = "25")
			Integer size) {
		return CadLiteAPIResponse.build(this.searchUseCase.search(new IncidentUnitForceSearchRequest(size, page, emergencyId)));
	}
	
	@GetMapping("/{emergency-id}/unit-force/{unitforce-id}")
	@ApiOperation("Search unit force assigned to incident")
	public ResponseEntity<CadLiteAPIResponse<IncidentUnitForceSearchResponse>> searchUniteForce(
			@PathVariable("emergency-id") String emergencyId,
			@PathVariable("unitforce-id") String unitForceId,
			@ApiParam(value = "Unit Mission") 
            @RequestParam(value = "unit-mission-id", required = false) String unitMissionId,
			@ApiParam(value = "Active") 
            @RequestParam(value = "active", defaultValue = "true") Boolean active) {
		return CadLiteAPIResponse.build(this.searchUseCase.search(new IncidentAndUnitForceSearchRequest(emergencyId, unitForceId), active, unitMissionId));
	}

	@GetMapping("/unit-force")
	@ApiOperation("Search units force")
	public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentUnitForceSearchResponse>>> search(
			@ApiParam(value = "Corporation id ") @RequestParam(required = false) Long corporationId,
			@ApiParam(value = "Emergency id ") @RequestParam(required = false) String emergencyId,
			@ApiParam(value = "Result page  delimiter by size , it starts at 0 ") @RequestParam(required = false) Integer page,
			@ApiParam(value = "Result page  delimiter size of results") @RequestParam(required = false) Integer size,
			@ApiParam(value = "Field for ordering") @RequestParam(required = false) UnitMissionOrderByFieldEnum orderBy,
			@ApiParam(value = "Ordering direction") @RequestParam(required = false) Order order) {
		return CadLiteAPIResponse.build(this.searchUseCase.search(new UnitMissionSearchRequest(size, page, emergencyId, corporationId, orderBy != null ? orderBy.name() : null, order)));
	}
	
	@GetMapping("/{emergency-id}/suggest-units")
	@ApiOperation("Suggest units of force to an incidentt")
	public ResponseEntity<CadLiteAPIResponse<List<SuggestUnitForceResponse>>> searchSuggest(
			@PathVariable("emergency-id") String emergencyId,
			UsernamePasswordAuthenticationToken user
	){
		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		return CadLiteAPIResponse.build(this.searchUseCase.suggestUnitForce(emergencyId, new SuggestUnitForceRequest(null, Long.valueOf(principal.getActiveProfile().get()))));
	}

}