package com.seguritech.dispatch.unitforce.query.application.usecase;

import java.util.List;

import org.springframework.stereotype.Service;

import com.seguritech.dispatch.unitforce.query.application.usecase.response.MissionStageResponse;
import com.seguritech.dispatch.unitforce.query.domain.repository.UniteForceStageQueryRepository;

@Service
public class UniteForceStageSearchUseCase {
	
	private final UniteForceStageQueryRepository uniteForceStageQueryRepository;
	
	public UniteForceStageSearchUseCase(UniteForceStageQueryRepository uniteForceStageQueryRepository) {
		this.uniteForceStageQueryRepository = uniteForceStageQueryRepository;
	}
	
	public List<MissionStageResponse> search(Long request) {
        return this.uniteForceStageQueryRepository.getStageMission(request);
    }
}
