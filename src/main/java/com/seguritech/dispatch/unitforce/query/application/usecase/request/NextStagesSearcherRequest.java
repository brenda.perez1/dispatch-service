package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import com.seguritech.platform.application.http.Order;
import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;

import java.util.Date;

@Getter
public class NextStagesSearcherRequest extends AbstractSearcherRequest {

    private final Long missionId;
    private final Long lastStageId;

    public NextStagesSearcherRequest(Integer size, Integer page, String query, Long missionId, Long lastStageId) {
        super(size, page, query);
        this.missionId = missionId;
        this.lastStageId = lastStageId;
    }

    public NextStagesSearcherRequest(Integer size, Integer page, String query, String periodDateField, Date periodStartDate, Date periodEndDate, Order order, String orderByField, Long missionId, Long lastStageId) {
        super(size, page, query, periodDateField, periodStartDate, periodEndDate, order, orderByField);
        this.missionId = missionId;
        this.lastStageId = lastStageId;
    }
    
}
