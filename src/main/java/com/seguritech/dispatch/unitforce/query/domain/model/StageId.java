package com.seguritech.dispatch.unitforce.query.domain.model;

import com.seguritech.dispatch.unitforce.query.domain.exception.StageIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class StageId {
    private final Long value;

    public StageId(Long value) {
        this.value = Optional.ofNullable(value).orElseThrow(StageIdRequiredException::new);
    }

    public static StageId required(Long id) {
        return new StageId(id);
    }
    
}
