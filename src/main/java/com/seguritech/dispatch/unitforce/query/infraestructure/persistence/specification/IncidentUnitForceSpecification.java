package com.seguritech.dispatch.unitforce.query.infraestructure.persistence.specification;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.domain.model.corporation.entity.Corporation_;
import com.domain.model.incident.entity.IncidentEntity_;
import com.domain.model.unit.entity.UnitForceConfigEntity_;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk_;
import com.domain.model.unit.entity.UnitMissionEntity_;

public class IncidentUnitForceSpecification {


    /**
     * Filter by Incident Id
     **/

    public static Specification<UnitMissionEntity> incidentIdEq(String incidentId) {

        return (Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
            .equal(root.get(UnitMissionEntity_.ID).get(UnitMissionEntityPk_.EMERGENCY_ID).get(IncidentEntity_.ID), incidentId);
    }
    
    /**
     * Filter by Corporation Id
     **/
    public static Specification<UnitMissionEntity> corporationIdEq(String corporationId) {

        return (Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
            .equal(root
            		.get(UnitMissionEntity_.ID)
            		.get(UnitMissionEntityPk_.UNIT_FORCE_CONFIG)
            		.get(UnitForceConfigEntity_.CORPORATION)
            		.get(Corporation_.ID)
            		, corporationId);
    }

	public static Sort SortByEventDateAtDesc() {
		return Sort.by(Sort.Direction.ASC, UnitMissionEntity_.END_MISSION_DATE);
	}

}
