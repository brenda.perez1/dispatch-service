package com.seguritech.dispatch.unitforce.query.infraestructure.persistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.mission.entity.Stage;
import com.domain.model.mission.entity.StageTransition;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.MissionStageConfigEntityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageConfigEntityRepository;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.MissionStageResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionTransitionResponse;
import com.seguritech.dispatch.unitforce.query.domain.repository.UniteForceStageQueryRepository;
import com.seguritech.dispatch.unitforce.query.infraestructure.persistence.specification.StageConfigSpecification;

@Service
public class UniteForceStageQueryRepositoryImpl implements UniteForceStageQueryRepository {

	private final StageConfigEntityRepository stageConfigEntityRepository;
	private final MissionStageConfigEntityRepository missionStageConfigEntityRepository;
	
	public UniteForceStageQueryRepositoryImpl(StageConfigEntityRepository stageConfigEntityRepository,
			MissionStageConfigEntityRepository missionStageConfigEntityRepository) {
		this.stageConfigEntityRepository = stageConfigEntityRepository;
		this.missionStageConfigEntityRepository = missionStageConfigEntityRepository;
	}
	
	@Override
	public List<MissionStageResponse> getStageMission(Long mission) {
		List<MissionStageConfig> missionEntities = this.missionStageConfigEntityRepository.findMissionStageConfigByIdMission(mission);
		return missionEntities.stream().map(this::toResponse).collect(Collectors.toList());
	}

	private MissionStageResponse toResponse(MissionStageConfig entity) {
        return Optional.ofNullable(entity).flatMap(x -> Optional.ofNullable(x).map(MissionStageConfig::getStage)
            .map(y -> new MissionStageResponse(x.getId(),
            		y.getId(), 
            		y.getName(), 
            		y.getIcon(), 
            		y.getTextColor(), 
            		y.getBackgroundColor(),
            		toResponse(y.getStageTransition()),
            		this.stageConfigEntityRepository.findAll(Specification.where(StageConfigSpecification.stageIdEq(y.getId()))
                         .and(StageConfigSpecification.missionIdEq(x.getMission().getId()))).stream()
                         .map(this::toUnitForceMissionResponse).collect(Collectors.toList()))
            )).orElse(null);
    }
	
	private MissionStageResponse toUnitForceMissionResponse(MissionStageConfig entity) {
        return Optional.ofNullable(entity).map(y -> {
        	Stage x = y.getStage();
        	StageTransition transition = y.getStageTransition();
        	MissionStageResponse missionStageResponse=  new MissionStageResponse(y.getId(), x.getId(), x.getName(), x.getIcon(), x.getTextColor(), x.getBackgroundColor(),null);
        	missionStageResponse.setTransition(toResponse(transition));
        	return missionStageResponse;
        }).orElse(null);
    }
	
	private UnitForceMissionTransitionResponse toResponse(StageTransition entity) {
        return Optional.ofNullable(entity).map(x -> new UnitForceMissionTransitionResponse(Long.valueOf(x.getId()), x.getStageTransitionDesc()))
            .orElse(null);
    }
}
