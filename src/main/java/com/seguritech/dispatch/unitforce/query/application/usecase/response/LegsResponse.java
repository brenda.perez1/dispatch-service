package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LegsResponse {
	
	private List<String> stepsResponse;
    private String summary;
    private Double weight;
    private Double duration;
    private Double distance;

}
