package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UnitForceTransportResponse {

    private final Long transportId;
    private final String name;
    private final String description;
    private final String economicNumber;

}
