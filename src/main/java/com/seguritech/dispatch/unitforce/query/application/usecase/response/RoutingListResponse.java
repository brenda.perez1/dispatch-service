package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class RoutingListResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<RoutesResponse> routes;

}
