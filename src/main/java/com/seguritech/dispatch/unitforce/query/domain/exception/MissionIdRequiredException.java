package com.seguritech.dispatch.unitforce.query.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;

public class MissionIdRequiredException extends BusinessException {
    public MissionIdRequiredException() {
        super("Mission ID is required", 0L);
    }
}
