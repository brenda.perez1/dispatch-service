package com.seguritech.dispatch.unitforce.query.infraestructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;

import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.NextStageResponse;
import com.seguritech.dispatch.unitforce.query.domain.model.MissionId;
import com.seguritech.dispatch.unitforce.query.domain.model.StageId;
import com.seguritech.dispatch.unitforce.query.domain.repository.NextStagesRepository;
import com.seguritech.platform.Service;

//TODO: Refactor implementación de repositorio para buscar las siguientes etapas, actualmente MOCK
@Service
public class NextStagesRepositoryImpl implements NextStagesRepository {

    private final HashMap<Long, HashMap<Long, ArrayList<NextStageResponse>>> stages;

    public NextStagesRepositoryImpl(HashMap<Long, HashMap<Long, ArrayList<NextStageResponse>>> stages) {
        this.stages = stages;
    }

    @Override
    public PageDataProjectionDTO<NextStageResponse> search(MissionId missionId, StageId stageId) {
    	
        ArrayList<NextStageResponse> response = this.search(this.stages.get(missionId.getValue()), stageId);

        if (response == null)
            response = new ArrayList<>();

        return new PageDataProjectionDTO<>(
                response.stream().map(this::toDto).collect(Collectors.toList()),
                new Long(response.size()));
    }

    private ArrayList<NextStageResponse> search(HashMap<Long, ArrayList<NextStageResponse>> stages, StageId stageId) {
        if (stages == null)
            return new ArrayList<>();

        return stages.get(stageId.getValue());
    }

    private NextStageResponse toDto(NextStageResponse stage) {
        return stage;
    }

}
