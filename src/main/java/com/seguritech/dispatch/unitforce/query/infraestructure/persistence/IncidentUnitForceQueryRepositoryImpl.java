package com.seguritech.dispatch.unitforce.query.infraestructure.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.mission.entity.Stage;
import com.domain.model.mission.entity.StageTransition;
import com.domain.model.transport.entity.Transport;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unitmission.StageMissionLogEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.domain.model.StageMissionLogModel;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageConfigEntityRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.StageMissionLogMapper;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitMissionEntityRepository;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentAndUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.SuggestUnitForceRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionStageResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceMissionTransitionResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceSearchStatusResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceTransportResponse;
import com.seguritech.dispatch.unitforce.query.domain.repository.IncidentUnitForceQueryRepository;
import com.seguritech.dispatch.unitforce.query.infraestructure.persistence.specification.IncidentUnitForceSpecification;
import com.seguritech.dispatch.unitforce.query.infraestructure.persistence.specification.StageConfigSpecification;
import com.seguritech.platform.Service;

@Service
public class IncidentUnitForceQueryRepositoryImpl implements IncidentUnitForceQueryRepository {

    private final UnitMissionEntityRepository entityRepository;
    private final StageConfigEntityRepository stageConfigEntityRepository;

    public IncidentUnitForceQueryRepositoryImpl(UnitMissionEntityRepository entityRepository,
                                                StageConfigEntityRepository stageConfigEntityRepository) {
        this.entityRepository = entityRepository;
        this.stageConfigEntityRepository = stageConfigEntityRepository;            
    }

    @Override
    public PageDataProjectionDTO<IncidentUnitForceSearchResponse> search(IncidentUnitForceSearchRequest request) {
    	
    	Integer page = Objects.isNull(request.getPage()) ? 0 : request.getPage();
    	Integer size = Objects.isNull(request.getSize()) ? 10 : request.getSize();
    	
        Page<UnitMissionEntity> result = this.entityRepository.findAll(toSpecification(request),
            PageRequest.of(page, size, this.toSort(request)));
        return new PageDataProjectionDTO<>(result.get().map(this::toResponse).collect(Collectors.toList()),
            result.getTotalElements());
    }
    
    @Override
	public IncidentUnitForceSearchResponse search(IncidentAndUnitForceSearchRequest request, Boolean active, String unitMissionId) {
    	UnitMissionEntity result;
    	if (unitMissionId != null) {
    		result = this.entityRepository.findUnitMissionByIdEmergencyAndIdUniteForceAndUnitMission(
    				new IncidentEntity(request.getIncidentId()), 
    				new UnitForceConfigEntity(request.getUnitForceId()),
    				unitMissionId);
		} else {			
			result = this.entityRepository.findUnitMissionByIdEmergencyAndIdUniteForce(
					new IncidentEntity(request.getIncidentId()), 
					new UnitForceConfigEntity(request.getUnitForceId()),
					active);
		}
		
		return toResponse(result);
	}
    
    @Override
    public PageDataProjectionDTO<IncidentUnitForceSearchResponse> searchByFilter(UnitForceSearchRequest request) {  
    	
    	List<Long> corporationOperativeLongs = request.getCorporationOperativeIds().stream()
    		.map(CorporationOperativeId::getValue).map(Long::new)
    		.collect(Collectors.toList());
    	
    	Collection<UnitMissionEntity> result = new ArrayList<>();
    	if(request.getCorporationId() != null && request.getStageId() != null) 
    		result = this.entityRepository
    			.findUnitMissionByOperativeGroupAndCorporationAndStage(corporationOperativeLongs, request.getCorporationId(), request.getStageId(), request.getActive());
    	else if(request.getCorporationId() == null && request.getStageId() != null)
    		result = this.entityRepository
			.findUnitMissionByOperativeGroupAndStage(corporationOperativeLongs, request.getStageId(), request.getActive());
    	else if(request.getCorporationId() != null && request.getStageId() == null)
    		result = this.entityRepository.findUnitMissionByOperativeGroupAndCorporation(corporationOperativeLongs, request.getCorporationId(),  request.getActive());    
    	else 
    		result = this.entityRepository.findUnitMissionByOperativeGroup(corporationOperativeLongs, request.getActive());     
        List<IncidentUnitForceSearchResponse> result1= result.stream().map(this::toResponse).collect(Collectors.toList());
        
        return new PageDataProjectionDTO<IncidentUnitForceSearchResponse>(result1);
    }
    
	@Override
	public PageDataProjectionDTO<IncidentUnitForceSearchResponse> searchById(UnitForceSearchRequest request) {
		
    	List<Long> corporationOperativeLongs = request.getCorporationOperativeIds().stream()
    		.map(CorporationOperativeId::getValue).map(Long::new)
    		.collect(Collectors.toList());
    	
    	Collection<UnitMissionEntity> result = new ArrayList<>();
    	if(request.getUnitforceId() != null) 
    		result = this.entityRepository
    			.findUnitMissionByOperativeGroupAndUnitforceId(corporationOperativeLongs, request.getUnitforceId(), request.getActive());
    	else
    		result = this.entityRepository
			.findUnitMissionByOperativeGroupAndUnitmissionId(corporationOperativeLongs, request.getUnitMissionId(), request.getActive());
    	
    	List<IncidentUnitForceSearchResponse> result1= result.stream().map(this::toResponse).collect(Collectors.toList());
        
        return new PageDataProjectionDTO<IncidentUnitForceSearchResponse>(result1);
	}
	
	@Override
	public UnitForceSearchResponse findUnitMissionByIdUnitForceConfig(String unitForceConfigId) {
		List<UnitMissionEntity> entitys = this.entityRepository.findUnitMissionByIdUnitForceConfig(unitForceConfigId, PageRequest.of(0,1));
		return this.toDTO(entitys != null && !entitys.isEmpty() ? entitys.stream().findFirst().get() : null);
	}

	public UnitForceSearchResponse toDTO(UnitMissionEntity entity) {
		return Optional.ofNullable(entity).map(e -> 
			new UnitForceSearchResponse(
					Optional.ofNullable(e.getId().getUnitForceConfig().getIdStatus()).map(x -> new UnitForceSearchStatusResponse(x.getId(), x.getName())).orElse(null), 
					e.getId().getEmergencyId().getId(), 
					e.getId().getEmergencyId().getFolio(), 
					e.getId().getUnitMissionId(), 
					e.getActive()
		)).orElse(null);
	}
	
	private Sort toSort(final IncidentUnitForceSearchRequest request) {
		return IncidentUnitForceSpecification.SortByEventDateAtDesc();
    }
	
	private Specification<UnitMissionEntity> toSpecification(IncidentUnitForceSearchRequest request) {
		
        List<Specification<UnitMissionEntity>> specifications = new ArrayList<>();
        Optional.ofNullable(request.getIncidentId()).ifPresent(x -> specifications.add(IncidentUnitForceSpecification.incidentIdEq(x)));
      
        //Specification behaviour is immutable
        Specification<UnitMissionEntity> temp = (root, query, criteriaBuilder) -> null;
        for (Specification<UnitMissionEntity> specification : specifications) {
            temp = temp.and(specification);
        }
        
        return temp;
    }

    private IncidentUnitForceSearchResponse toResponse(UnitMissionEntity entity) {

        return Optional.ofNullable(entity).flatMap(x -> Optional.ofNullable(x.getId().getUnitForceConfig())
            .map(y -> new IncidentUnitForceSearchResponse(
            		entity.getId().getUnitMissionId(),
            		x.getStartMissionDate(),
            		x.getEndMissionDate(),
            		x.getUpdateDate(),
            		x.getCreationDate(),
            		x.getCreatedBy(),
            		x.getPersonsNumber(),
            		toResponse(y.getTransportEntity()),
            		toResponse(x.getMissionStageConfig()),   		
            		toResponse(y),
            		toResponseStageMission(x.getStageMissionLogs())
            ))).orElse(null);

    }

    private List<StageMissionLogModel> toResponseStageMission(List<StageMissionLogEntity> list) {
    	return list.stream().map(x -> StageMissionLogMapper.toModel(x)).collect(Collectors.toList());
    }

	private UnitForceMissionResponse toResponse(MissionStageConfig entity) {
        return Optional.ofNullable(entity).flatMap(x -> Optional.ofNullable(x).map(MissionStageConfig::getMission)
            .map(y -> new UnitForceMissionResponse(y.getId(),
                y.getName(),
                y.getDescription(),
                Optional.ofNullable(x).map(MissionStageConfig::getStageTransition).map(this::toResponse)
                    .orElse(null),
                Optional.ofNullable(x).map(this::toActualStageResponse).orElse(null),
              this.stageConfigEntityRepository.findAll(Specification.where(StageConfigSpecification.stageIdEq(x.getStage().getId()))
                  .and(StageConfigSpecification.missionIdEq(y.getId()))).stream()
                  .map(this::toUnitForceMissionResponse).collect(Collectors.toList())


            ))).orElse(null);
    }
    
    private UnitForceMissionStageResponse toUnitForceMissionResponse(MissionStageConfig entity) {
    	

        return Optional.ofNullable(entity).map(y -> {
        	Stage x = y.getStage();
        	StageTransition transition = y.getStageTransition();
        	UnitForceMissionStageResponse unitForceMissionStageResponse=  new UnitForceMissionStageResponse(y.getId(), x.getId(), x.getName(), x.getIcon(), x.getTextColor(), x.getBackgroundColor());
        	unitForceMissionStageResponse.setTransition(toResponse(transition));
        	return unitForceMissionStageResponse;
        }).orElse(null);

    }


    private UnitForceMissionTransitionResponse toResponse(StageTransition entity) {
        return Optional.ofNullable(entity).map(x -> new UnitForceMissionTransitionResponse(Long.valueOf(x.getId()), x.getStageTransitionDesc()))
            .orElse(null);
    }


    private UnitForceTransportResponse toResponse(Transport transportEntity) {
        return Optional.ofNullable(transportEntity)
            .map(x -> new UnitForceTransportResponse(x.getId(), x.getName(), x.getDescription(), x.getEconomicNumber()))
            .orElse(null);
    }

    private UnitForceMissionStageResponse toActualStageResponse(MissionStageConfig entity) {

        return Optional.ofNullable(entity).map(MissionStageConfig::getStage).map(x -> new UnitForceMissionStageResponse
        		(entity.getId().toString(),x.getId(), x.getName(), x.getIcon(), x.getTextColor(), x.getBackgroundColor())).orElse(null);

    }
    
    private UnitForceResponse toResponse(UnitForceConfigEntity entity) {
    	return new UnitForceResponse(entity.getIdUnitForceConfig(), entity.getAlias(), entity.getVisible(), entity.getCorporation().getId(), null);
    }

	@Override
	public List<IncidentUnitForceSearchResponse> searchByOperativeGroups(SuggestUnitForceRequest requests) {
		// TODO Auto-generated method stub
	    	
	    	Collection<UnitMissionEntity> result = new ArrayList<>();
	    	result = this.entityRepository.findUnitMissionByOperativeGroup(requests.getCorporationOperativesIds(), Boolean.TRUE);     
	    	return result.stream().map(this::toResponse).collect(Collectors.toList());
	}

}
