package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import lombok.Getter;

@Getter
public class IncidentAndUnitForceSearchRequest {

	 private final String incidentId;
	 private final String unitForceId;
	 
	 public IncidentAndUnitForceSearchRequest(String incidentId, String unitForceId) {
		 this.incidentId = incidentId;
		 this.unitForceId = unitForceId;
	 }
}
