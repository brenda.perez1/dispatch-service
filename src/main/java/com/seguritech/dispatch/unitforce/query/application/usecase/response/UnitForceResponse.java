package com.seguritech.dispatch.unitforce.query.application.usecase.response;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UnitForceResponse {
	
	private final String unitId;
    private final String alias;
    private final boolean isVisible;
    private final Long corporationId;
    private final String corporationName;

}
