package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RoutesResponse {
	
	private List<LegsResponse> legsResponse;
	private String weight_name;
	private Double weight;
	private Double distance;
	private Double duration;

}
