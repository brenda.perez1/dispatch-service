package com.seguritech.dispatch.unitforce.query.application.usecase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.RequestOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;

import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.seguritech.dispatch.client.RoutingFeignClient;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentCorporationOperativeEntityRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.UnitMissionSearchRepository;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentAndUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.IncidentUnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.SuggestUnitForceRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitForceSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.request.UnitMissionSearchRequest;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.IncidentUnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.RoutingListResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.SuggestUnitForceResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.UnitForceSearchResponse;
import com.seguritech.dispatch.unitforce.query.domain.repository.IncidentUnitForceQueryRepository;
import com.seguritech.platform.Service;

import feign.FeignException.BadRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class IncidentUnitForceSearchUseCase {

    private final IncidentUnitForceQueryRepository repository;
	private final ProfileCorporationConfigRepository profile;
	private final UnitMissionSearchRepository unitMissionSearchRepository;
	private final UnitForceConfigRepository unitForceConfigRepository;
	private final IncidentRepository incidentRepository;
	private final IncidentCorporationOperativeEntityRepository incidentCorporationOperativeRepository;
    private final RoutingFeignClient client;
    
	private String elasticDocumentProtocol;
    private String elasticDocumentDomain;
    private Integer elasticDocumentPort;
    private String elasticDocumentUser;
    private String elasticDocumentSecret;
    private String elasticDocumentIndex;

    public IncidentUnitForceSearchUseCase(IncidentUnitForceQueryRepository repository,
    		ProfileCorporationConfigRepository profile, 
    		UnitMissionSearchRepository unitMissionSearchRepository,
    		UnitForceConfigRepository unitForceConfigRepository,
    		IncidentRepository incidentRepository,
    		IncidentCorporationOperativeEntityRepository incidentCorporationOperativeRepository,
    		RoutingFeignClient client,
    		@Value("${feign.incident.rx.suspect.output.protocol}") String elasticDocumentProtocol,
            @Value("${feign.incident.rx.suspect.output.domain}") String elasticDocumentDomain,
            @Value("${feign.incident.rx.suspect.output.port}") Integer elasticDocumentPort,
            @Value("${feing.elastic.user}") String elasticDocumentUser,
            @Value("${feing.elastic.password}") String elasticDocumentSecret,
            @Value("${feign.incident.rx.suspect.output.index}") String elasticDocumentIndex) {
        this.repository = repository;
        this.profile = profile;
        this.unitMissionSearchRepository = unitMissionSearchRepository;
        this.unitForceConfigRepository = unitForceConfigRepository;
        this.incidentRepository = incidentRepository;
        this.incidentCorporationOperativeRepository = incidentCorporationOperativeRepository;
        this.client = client;
        this.elasticDocumentProtocol = elasticDocumentProtocol;
        this.elasticDocumentDomain = elasticDocumentDomain;
        this.elasticDocumentPort = elasticDocumentPort;
        this.elasticDocumentUser = elasticDocumentUser;
        this.elasticDocumentSecret = elasticDocumentSecret;
        this.elasticDocumentIndex = elasticDocumentIndex;
    }

    public PageDataProjectionDTO<IncidentUnitForceSearchResponse> search(IncidentUnitForceSearchRequest request) {
        return this.repository.search(request);
    } 
    
    public IncidentUnitForceSearchResponse search(IncidentAndUnitForceSearchRequest request, Boolean active, String unitMissionId) {
        return this.repository.search(request, active, unitMissionId);
    }
    
    public PageDataProjectionDTO<IncidentUnitForceSearchResponse> search(UnitMissionSearchRequest request) {
        return this.unitMissionSearchRepository.search(request);
    }
    
    public PageDataProjectionDTO<IncidentUnitForceSearchResponse> searchByProfile(UnitForceSearchRequest request) {
    	List<CorporationOperativeId>  corporationOperativeIds = profile.getCorporationOperativeIds( ProfileId.required(request.getProfileId()));
    	request.setCorporationOperativeIds(corporationOperativeIds);
        return this.repository.searchByFilter(request);
    }
    
    public PageDataProjectionDTO<IncidentUnitForceSearchResponse> searchById(UnitForceSearchRequest request) {
    	List<CorporationOperativeId>  corporationOperativeIds = profile.getCorporationOperativeIds(ProfileId.required(request.getProfileId()));
    	request.setCorporationOperativeIds(corporationOperativeIds);
        return this.repository.searchById(request);
    }
    
    public UnitForceSearchResponse findUnitMissionByIdUnitForceConfig(String unitForceConfigId) {
    	return this.repository.findUnitMissionByIdUnitForceConfig(unitForceConfigId);
    }
    
    public List<SuggestUnitForceResponse> suggestUnitForce(String emergencyId, SuggestUnitForceRequest request) {
    	ProfileId.required(request.getProfile());
    	List<Long> listCorpOpe = this.incidentCorporationOperativeRepository.findAllOperativeGroupsWhitEmergency(request.getProfile(), emergencyId);
    	request.setCorporationOperativesIds(listCorpOpe);
    	List<UnitForceConfigEntity> listUnitForce = this.unitForceConfigRepository.findCorporationOperatives(request.getCorporationOperativesIds());
    	//Finder location in incident
    	IncidentLocationBounded incident = this.incidentRepository.findLocation(new IncidentId(emergencyId));
    	return getDataElastic(listUnitForce, incident);
    }
    
    private List<SuggestUnitForceResponse> getDataElastic(List<UnitForceConfigEntity> listUnitForce, IncidentLocationBounded incident) {
    	final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(elasticDocumentUser, elasticDocumentSecret));
        RestClientBuilder builder = RestClient.builder(new HttpHost(elasticDocumentDomain,elasticDocumentPort,elasticDocumentProtocol)).setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
        RestHighLevelClient client = new RestHighLevelClient(builder);
        List<SuggestUnitForceResponse> listResp = new ArrayList<>();
        
        listUnitForce.forEach(unitforce -> {
        	
        	GetRequest getRequest = new GetRequest(elasticDocumentIndex, unitforce.getIdUnitForceConfig());            
            try {
            	GetResponse resp = client.get(
                		getRequest, 
                		RequestOptions.DEFAULT
                );
                //Coordenadas de las unidades de fuerza
            	if (resp.isExists()) {
            		Map<String, Object> objResp = resp.getSource();
            		
            		double longitudUnitForce = Double.valueOf(objResp.get("lng").toString());
            		double latitudeUnitForce = Double.valueOf(objResp.get("lat").toString());
                	
            		ResponseEntity<RoutingListResponse> response = this.client.getRouting(longitudUnitForce, latitudeUnitForce, incident.getCoordinates().getLongitude(), incident.getCoordinates().getLatitude());
            		
            		double distance = response.getBody().getRoutes().get(0).getDistance().doubleValue();
            		double duration = response.getBody().getRoutes().get(0). getDuration().doubleValue();
            		
            	   	listResp.add(new SuggestUnitForceResponse(objResp.get("idUnidadFuerza").toString(), distance, duration));
            	}          	
            }
            catch (IOException e) {
            	e.printStackTrace();
            } catch (BadRequest ec) {
            	ec.getMessage();
            }catch (Exception ex) {
            	log.error(ex.getMessage());
            }
            
        });
        
        List<SuggestUnitForceResponse> listResp2 = listResp.stream().sorted(Comparator.comparingDouble(SuggestUnitForceResponse::getDistance)).collect(Collectors.toList());
        return listResp2.size() >= 4 ? listResp2.subList(0, 3) : listResp2;
    }
    
}
