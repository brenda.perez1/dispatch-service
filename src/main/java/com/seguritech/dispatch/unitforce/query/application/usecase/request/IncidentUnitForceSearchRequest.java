package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;

@Getter
public class IncidentUnitForceSearchRequest extends AbstractSearcherRequest {

    private final String incidentId;

    public IncidentUnitForceSearchRequest(Integer size, Integer page, String incidentId) {
        super(size, page, null);
        this.incidentId = incidentId;
    }
}
