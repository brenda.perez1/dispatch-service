package com.seguritech.dispatch.unitforce.query.domain.repository;

import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.NextStageResponse;
import com.seguritech.dispatch.unitforce.query.domain.model.MissionId;
import com.seguritech.dispatch.unitforce.query.domain.model.StageId;

public interface NextStagesRepository {

    PageDataProjectionDTO<NextStageResponse> search(MissionId missionId, StageId stageId);

}
