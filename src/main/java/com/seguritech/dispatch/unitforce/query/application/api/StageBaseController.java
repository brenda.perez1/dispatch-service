package com.seguritech.dispatch.unitforce.query.application.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.unitforce.query.application.usecase.UniteForceStageSearchUseCase;
import com.seguritech.dispatch.unitforce.query.application.usecase.response.MissionStageResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = { "MISSION STAGE" })
@RequestMapping("/dispatch/mission-stage")
public class StageBaseController {
	
	private final UniteForceStageSearchUseCase forceStageSearchUseCase;
	
	public StageBaseController (UniteForceStageSearchUseCase forceStageSearchUseCase) {
		this.forceStageSearchUseCase = forceStageSearchUseCase;
	}

	@ApiOperation("Search all stage associated to mission")
	@GetMapping("/{idMission}")
	public ResponseEntity<CadLiteAPIResponse<List<MissionStageResponse>>> getStage(@PathVariable("idMission") Long idMission) {
		return CadLiteAPIResponse
				.build(this.forceStageSearchUseCase.search(idMission));
	}
}
