package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnitForceSearchDetailRequest {

	private String unitForceId;

}
