package com.seguritech.dispatch.unitforce.query.domain.model;

import com.seguritech.dispatch.unitforce.query.domain.exception.MissionIdRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class MissionId {
    private final Long value;

    private MissionId(Long missionId) {
        this.value = Optional.ofNullable(missionId).orElseThrow(MissionIdRequiredException::new);
    }

    public static MissionId required(Long id) {
        return new MissionId(id);
    }

}
