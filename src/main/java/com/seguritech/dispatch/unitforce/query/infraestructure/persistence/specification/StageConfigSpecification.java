package com.seguritech.dispatch.unitforce.query.infraestructure.persistence.specification;

import org.springframework.data.jpa.domain.Specification;

import com.domain.model.mission.entity.MissionStageConfig;
import com.domain.model.mission.entity.MissionStageConfigPk_;
import com.domain.model.mission.entity.MissionStageConfig_;
import com.domain.model.mission.entity.Mission_;

public class StageConfigSpecification {


    /**
     * Filter by Stage Id
     **/
    public static Specification<MissionStageConfig> stageIdEq(Long stageId) {
        return (Specification<MissionStageConfig>) (root, query, criteriaBuilder) -> criteriaBuilder
            .equal(root.get(MissionStageConfig_.STAGE_LAST), stageId);
    }

    /**
     * Filter by Mission Id
     **/
    public static Specification<MissionStageConfig> missionIdEq(Long missionId) {
        return (Specification<MissionStageConfig>) (root, query, criteriaBuilder) -> criteriaBuilder
            .equal(root.get(MissionStageConfig_.MISSION).get(Mission_.ID),
                missionId);
    }
}
