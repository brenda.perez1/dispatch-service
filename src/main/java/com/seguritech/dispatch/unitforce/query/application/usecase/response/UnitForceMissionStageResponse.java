package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class UnitForceMissionStageResponse {

	private String idMissionStageConfig;
    private Long lastStageId;
    private String stageName;
    private String icon;
    private String textColor;
    private String backgroundColor;
    private UnitForceMissionTransitionResponse transition;

	public UnitForceMissionStageResponse(String idMissionStageConfig, Long lastStageId, String stageName, String icon, String textColor, String backgroundColor) {
		super();
		this.idMissionStageConfig = idMissionStageConfig;
		this.lastStageId = lastStageId;
		this.stageName = stageName;
		this.icon = icon;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
	}
    
}
