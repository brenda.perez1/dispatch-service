package com.seguritech.dispatch.unitforce.query.application.usecase.request;

import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnitForceSearchRequest extends AbstractSearcherRequest {

    private Long profileId;
    private Long corporationId;
    private Long stageId;
    private String unitforceId;
    private String unitMissionId;
    private Boolean active=true;

    private List<CorporationOperativeId>  corporationOperativeIds ;
    

    public UnitForceSearchRequest(Integer size, Integer page, Long profile) {
		super(size, page, null);
        this.profileId = profile;
    }
}
