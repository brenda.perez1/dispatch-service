package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UnitForceMissionResponse {

    private final Long missionId;
    private final String name;
    private final String description;

    private final UnitForceMissionTransitionResponse transition;

    private final UnitForceMissionStageResponse actualStage;

    private final List<UnitForceMissionStageResponse> nextStages;

}
