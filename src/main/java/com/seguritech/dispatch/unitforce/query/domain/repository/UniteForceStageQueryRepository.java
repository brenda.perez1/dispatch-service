package com.seguritech.dispatch.unitforce.query.domain.repository;

import java.util.List;

import com.seguritech.dispatch.unitforce.query.application.usecase.response.MissionStageResponse;

public interface UniteForceStageQueryRepository {

	public List<MissionStageResponse> getStageMission(Long mission);
}
