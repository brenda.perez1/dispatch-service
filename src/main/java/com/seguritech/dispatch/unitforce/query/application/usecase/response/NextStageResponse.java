package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NextStageResponse {
    private final Long stageId;
    private final String stageName;
}
