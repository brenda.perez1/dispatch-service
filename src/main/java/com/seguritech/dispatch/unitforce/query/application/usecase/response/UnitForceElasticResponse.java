package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UnitForceElasticResponse {
	
	private final Long idOperativo;
	private final String idUnidadFuerza;
	private final Double lat;
	private final Double lng;
	private final Long phone;

}
