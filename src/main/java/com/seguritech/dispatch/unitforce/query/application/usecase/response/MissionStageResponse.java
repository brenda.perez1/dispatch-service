package com.seguritech.dispatch.unitforce.query.application.usecase.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Setter
public class MissionStageResponse {

	private String idMissionStageConfig;
	private Long idStage;
    private String stageName;
    private String icon;
    private String textColor;
    private String backgroundColor;
    private UnitForceMissionTransitionResponse transition;
	private List<MissionStageResponse> nextStages;
	
	public MissionStageResponse(String idMissionStageConfig, Long idStage, String stageName, String icon, 
			String textColor, String backgroundColor, List<MissionStageResponse> nextStages
			) {
		super();
		this.idStage = idStage;
		this.stageName = stageName;
		this.icon = icon;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
		this.nextStages = nextStages;
		this.idMissionStageConfig = idMissionStageConfig;
	}
}
