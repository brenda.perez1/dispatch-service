package com.seguritech.dispatch.specification;


import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Sort;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.exception.InfrastructureException;


public class SpecificationSortFieldResolver {

    private final Map<String, String> mapping;


    public SpecificationSortFieldResolver(Map<String, String> mapping) {
        this.mapping = Optional.ofNullable(mapping)
            .orElseThrow(() -> new InfrastructureException("Field for mapping are required",null));
    }

    public Sort resolve(String field, String order) {
        return Sort.by(this.insureDirection(order), this.insureField(field));
    }


    private String insureField(String fieldName) {
        return Optional.ofNullable(fieldName).map(this.mapping::get)
            .orElseThrow(() -> new BusinessException(String.format("Invalid field [%s]  for sorting", fieldName), 0L));
    }

    private Sort.Direction insureDirection(String direction) {
        try {
            return Sort.Direction.fromString(direction.toLowerCase());
        } catch (IllegalArgumentException exception) {
            throw new BusinessException(String.format("Invalid Direction [%s]  for sorting", direction), 0L);
        }
    }
}
