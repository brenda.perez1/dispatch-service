package com.seguritech.dispatch.specification;

import java.util.StringJoiner;

public class SortFieldBuilderSpecification {


    private final StringJoiner builder;

    public static SortFieldBuilderSpecification builder() {
        return new SortFieldBuilderSpecification(".");
    }

    private SortFieldBuilderSpecification(String separator) {
        this.builder = new StringJoiner(separator);
    }


    public SortFieldBuilderSpecification get(String value) {
        this.builder.add(value);
        return this;
    }

    public String build() {
        return this.builder.toString();
    }


}
