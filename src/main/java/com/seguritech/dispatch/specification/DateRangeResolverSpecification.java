package com.seguritech.dispatch.specification;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.exception.InfrastructureException;


public class DateRangeResolverSpecification<T> {

    private final Map<String, String> mapping;


    public DateRangeResolverSpecification(Map<String, String> mapping) {
        this.mapping = Optional.ofNullable(mapping)
            .orElseThrow(() -> new InfrastructureException("Field for mapping are required",null));
    }

    public Specification<T> resolve(String field, Date gt, Date lt) {
        this.ensureDates(gt, lt);
        return (Specification<T>) (root, query, criteriaBuilder) -> criteriaBuilder.between(root.get(insureField(field)), gt, lt);
    }

    private void ensureDates(Date gt, Date lt) {
        if (gt == null || lt == null) {
            throw new BusinessException("Date Range periodStartDate , periodEndDate are required", 0L);
        }
        if (!gt.before(lt)) {
            throw new BusinessException("Date Range periodEndDate must be grater than periodStartDate", 0L);
        }
    }
    
    public Specification<T> resolveStartDate(String field, Date gt) {
        return (Specification<T>) (root, query, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get(insureField(field)), gt);
    }
    
    public Specification<T> resolveEndDate(String field, Date lt) {
        return (Specification<T>) (root, query, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get(insureField(field)), lt);
    }

    private String insureField(String fieldName) {
        return Optional.ofNullable(fieldName).map(this.mapping::get)
            .orElseThrow(() -> new BusinessException(String.format("Invalid field [%s]  for sorting", fieldName), 0L));
    }

    private Sort.Direction insureDirection(String direction) {
        try {
            return Sort.Direction.fromString(direction.toLowerCase());
        } catch (IllegalArgumentException exception) {
            throw new BusinessException(String.format("Invalid Direction [%s]  for sorting", direction), 0L);
        }
    }
}
