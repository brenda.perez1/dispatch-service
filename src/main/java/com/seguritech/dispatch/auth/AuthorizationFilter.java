package com.seguritech.dispatch.auth;


import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;

public class AuthorizationFilter extends BasicAuthenticationFilter {
    
	Environment environment;
	private static final String AUTHORITIES_KEY = "auth";
	private static final String CLAIM_KEY_DATA = "data";
	private static final String CLAIM_KEY_PROFILE = "profile";
	public static final String CLAIM_KEY_ACTIVE_PROFILE = "active_profile";
	public static final String CLAIM_KEY_SUBCENTER = "subcenter";

	ObjectMapper mapper = new ObjectMapper();
	
    public AuthorizationFilter(AuthenticationManager authManager, Environment environment) {
        super(authManager);
        this.environment = environment;
    }
    
    @Override
    protected void doFilterInternal(HttpServletRequest req,
            HttpServletResponse res,
            FilterChain chain) throws IOException, ServletException {

        String authorizationHeader = req.getHeader(environment.getProperty("authorization.token.header.name"));

        if (authorizationHeader == null || !authorizationHeader.startsWith(environment.getProperty("authorization.token.header.prefix"))) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req, res);
       
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }  
    
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String authorizationHeader = req.getHeader(environment.getProperty("authorization.token.header.name"));
   
         if (authorizationHeader == null) {
             return null;
         }

         String token = authorizationHeader.replace(environment.getProperty("authorization.token.header.prefix"), "");
         
         Claims claims = null;
         
         try {
        	 claims = Jwts.parser()
        			 .setSigningKey(environment.getProperty("token.secret"))
        			 .parseClaimsJws(token)
        			 .getBody();
         } catch (ExpiredJwtException e) {
        	 claims = e != null ? e.getClaims() : null;
        	 res.setContentType("application/json"); 
        	 res.setStatus(HttpStatus.FORBIDDEN.value());
        	 res.getWriter().append("{ \"error\" : \"EXPIRED_TOKEN\" , \"expiredAt\" : \"" + (claims != null ? claims.getExpiration() : null) + "\", \"status\" : " + HttpStatus.FORBIDDEN.value() + " }");
        	 res.flushBuffer();
        	 return null;
         }
         
         String id = claims.get(CLAIM_KEY_DATA).toString();
         
         String profile = claims.get(CLAIM_KEY_PROFILE).toString();
         
         String subcenterId = claims.containsKey(CLAIM_KEY_SUBCENTER) ? claims.get(CLAIM_KEY_SUBCENTER).toString() : null;
       
         String userId = claims.getSubject();
         
         String activeProfile = claims.get(CLAIM_KEY_ACTIVE_PROFILE).toString();
         
         Collection<? extends AuthUserProfileResponse> profiles = 
        		 mapper.readValue(profile, new TypeReference<List<AuthUserProfileResponse>>(){});
         
         Collection<? extends GrantedAuthority> authorities =
                 Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());

         UserDetails authUserResponse= new AppUserPrincipal(id, userId, activeProfile, null, null, authorities,profiles, false, true,  claims.getExpiration(), null, null, subcenterId != null ? Long.valueOf(subcenterId) : null);

         if (userId == null) {
             return null;
         }
   
         return new UsernamePasswordAuthenticationToken(authUserResponse , token, authorities);
     }
    
}
