package com.seguritech.dispatch.auth;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class AppUserPrincipal implements UserDetails {

	private static final long serialVersionUID = 1236903435799179994L;
	
	private final String id;
    private final String nickName;
    private final String activeProfile;
    private final String activeExtension;
    private final String accessMethod;
    private Collection<? extends GrantedAuthority> authorities;
    private Collection<? extends AuthUserProfileResponse> profiles;
    private final Boolean temporalPass;
    private final Boolean active;
    private final Date expiration;
    private final String password;
    private final String extension;
    private final Long subCenter;
    
    @JsonCreator
    public AppUserPrincipal(
    	@JsonProperty("id") String id,
    	@JsonProperty("nickName") String nickName,
    	@JsonProperty("activeProfile") String activeProfile,
    	@JsonProperty("activeExtension") String activeExtension,
    	@JsonProperty("accessMethod") String accessMethod,
    	@JsonProperty("authorities") Collection<? extends GrantedAuthority> authorities,
    	@JsonProperty("profiles") Collection<? extends AuthUserProfileResponse> profiles,
    	@JsonProperty("temporalPass") Boolean temporalPass, 
    	@JsonProperty("active") Boolean active,
    	@JsonProperty("expiration") Date date,
    	@JsonProperty("password") String password,
    	@JsonProperty("extension") String extension,
    	@JsonProperty("subCenter") Long subCenter
    ) {
        
		this.id = id;
        this.nickName = nickName;
        this.activeProfile = activeProfile;
        this.activeExtension = activeExtension;
        this.accessMethod = accessMethod;
        this.authorities = authorities;
        this.temporalPass = temporalPass;
        this.active = active;
        this.expiration = date;
        this.password = password;
        this.extension = extension;
        this.profiles = profiles;
        this.subCenter = subCenter;
    }

    @Override
    public String getUsername () {
        return this.nickName;
    }

    @Override
    public boolean isAccountNonExpired () {
        return true;
    }

    @Override
    public boolean isAccountNonLocked () {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired () {
        return true;
    }

    @Override
    public boolean isEnabled () {
        return this.active;
    }

    public Optional<String> getActiveExtension () {
        return Optional.ofNullable(activeExtension);
    }

    public Optional<String> getActiveProfile () {
        return Optional.ofNullable(activeProfile);
    }

    public Optional<Date> getExpiration () {
        return Optional.ofNullable(expiration);
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

	public Long getSubCenter() {
		return this.subCenter;
	}
   
}
