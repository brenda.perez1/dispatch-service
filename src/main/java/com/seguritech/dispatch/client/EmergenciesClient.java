package com.seguritech.dispatch.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCloseRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.UpdateIncidentAssignCorporacionRequest;

@FeignClient(name = "${feign.emergencies-service.name}", url = "${feign.emergencies-service.url}")
public interface EmergenciesClient {
	
	static final String AUTHORIZATION = "Authorization";

	@RequestMapping(method = RequestMethod.POST, value = "/{emergency-id}/close", consumes = MediaType.APPLICATION_JSON_VALUE)
	void closeAnEmergency(
			@RequestHeader(AUTHORIZATION) String token, @PathVariable (value = "emergency-id") String idEmergency, @RequestBody EmergencyCloseRequest body);

	@RequestMapping(method = RequestMethod.PUT, value = "/incident-operating-corporation-assigned", consumes = MediaType.APPLICATION_JSON_VALUE)
	void updateEmergency(
			@RequestHeader(AUTHORIZATION) String token, @RequestBody UpdateIncidentAssignCorporacionRequest body);
}
