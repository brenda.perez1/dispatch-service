package com.seguritech.dispatch.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;

import com.seguritech.dispatch.unitforce.query.application.usecase.response.RoutingListResponse;

@FeignClient(name = "${cad-lite.routing.name}", url = "${cad-lite.routing.url}")
public interface RoutingFeignClient {
	
	 //TODO: path1=longitud origen (unidad de fuerza), path2=latitud origen (unidad de fuerza), path3=longitud destino (emergencia), path4=latitud destino (emergencia)
	 @RequestMapping(method = RequestMethod.GET, value = "{longitudUnitForce},{latitudeUnitForce};{longitudeEmergency},{latitudeEmergency}${cad-lite.routing.url.overview}", consumes = MediaType.APPLICATION_JSON_VALUE)                                 
	 ResponseEntity<RoutingListResponse> getRouting(@PathVariable("longitudUnitForce") Double longitudUnitForce, @PathVariable("latitudeUnitForce") Double latitudeUnitForce, @PathVariable("longitudeEmergency") Double longitudeEmergency, @PathVariable("latitudeEmergency") Double latitudeEmergency);

}
