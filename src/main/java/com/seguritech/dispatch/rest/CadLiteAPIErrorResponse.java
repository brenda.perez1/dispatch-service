package com.seguritech.dispatch.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CadLiteAPIErrorResponse {

    private final Long errorCode;
    @JsonProperty("error_description")
    private final String errorDescription;

    public CadLiteAPIErrorResponse(Long errorCode) {
        this.errorCode = errorCode;
        this.errorDescription = null;
    }
}
