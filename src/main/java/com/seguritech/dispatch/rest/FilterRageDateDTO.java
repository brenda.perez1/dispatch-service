package com.seguritech.dispatch.rest;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FilterRageDateDTO {
    private Date gt;
    private Date lt;
}
