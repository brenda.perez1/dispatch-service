package com.seguritech.dispatch.rest;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.util.List;
import java.util.Optional;

@Data
@AllArgsConstructor
@Slf4j
public class CadLiteAPIResponse<T> {

    private CadLiteAPIResponseStatusEnum status;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<T> errors;

    public static <T> ResponseEntity<CadLiteAPIResponse<T>> build() {
        CadLiteAPIResponseStatusEnum status = CadLiteAPIResponseStatusEnum.SUCCESS;
        return build(null, status.httpStatus.getReasonPhrase(), status);
    }


    public static <T> ResponseEntity<CadLiteAPIResponse<T>> build(CadLiteAPIResponseStatusEnum status) {
        return build(null, status.httpStatus.getReasonPhrase(), status);
    }


    public static <T> ResponseEntity<CadLiteAPIResponse<T>> build(T data) {
        CadLiteAPIResponseStatusEnum status = CadLiteAPIResponseStatusEnum.SUCCESS;
        return build(data, status.httpStatus.getReasonPhrase(), status);
    }


    public static <T> ResponseEntity<CadLiteAPIResponse<T>> build(T data, CadLiteAPIResponseStatusEnum status) {
        return build(data, status.httpStatus.getReasonPhrase(), status);
    }

    public static <T> ResponseEntity<CadLiteAPIResponse<T>> build(T data, String message, CadLiteAPIResponseStatusEnum status) {
        CadLiteAPIResponse<T> response = new CadLiteAPIResponse<>(status, data, message,null);
        return new ResponseEntity<CadLiteAPIResponse<T>>(response, status.httpStatus);
    }

    public static <T> ResponseEntity<CadLiteAPIResponse<T>> buildErrors(List<T> errors, String message, CadLiteAPIResponseStatusEnum status) {
        CadLiteAPIResponse<T> response = new CadLiteAPIResponse<>(status, null, message,errors);
        return new ResponseEntity<CadLiteAPIResponse<T>>(response, status.httpStatus);
    }

    public static <T> ResponseEntity<CadLiteAPIResponse<SourceCreatedResponse<T>>> buildCreated(T sourceIdValue) {
        return build(new SourceCreatedResponse(sourceIdValue), CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.CREATED);
    }

    public static <T> ResponseEntity<CadLiteAPIResponse<T>> buildOne(Optional<T> value) {
        if (!value.isPresent()) {
            return build(CadLiteAPIResponseStatusEnum.NOT_FOUND);
        }
        return build(value.get(), CadLiteAPIResponseStatusEnum.SUCCESS);
    }

    public static ResponseEntity<InputStreamResource> download(File file) throws IOException {
        String mediaType = URLConnection.guessContentTypeFromName(file.getName());
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        HttpHeaders headers = new HttpHeaders();
        String content = String.format("attachment; filename=\"%s\"", file.getName());
        headers.add("Content-Disposition", content);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        ResponseEntity<InputStreamResource> response = ResponseEntity.ok().headers(headers).contentLength(file.length())
            .contentType(MediaType.parseMediaType(mediaType)).body(resource);
        return response;
    }

    public static ResponseEntity<InputStreamResource> toResponse(File file) {
        return Optional.ofNullable(file).map(x -> {
            try {
                return download(file);
            } catch (Exception e) {
                log.error("[Application ] File to Response Entity ", e);
                return null;
            }
        }).orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    public static ResponseEntity<InputStreamResource> download(String filePath) throws IOException {
        return download(new File(filePath));
    }

    public enum CadLiteAPIResponseStatusEnum {
        SUCCESS(HttpStatus.OK),
        CREATED(HttpStatus.CREATED),
        ERROR(HttpStatus.CONFLICT),
        ERROR_INFRASTRUCTURE(HttpStatus.INTERNAL_SERVER_ERROR),
        NO_CONTENT(HttpStatus.NO_CONTENT),
        NOT_FOUND(HttpStatus.NOT_FOUND),
        BAD_REQUEST(HttpStatus.BAD_REQUEST),
        UNAUTHORIZED(HttpStatus.UNAUTHORIZED);

        private HttpStatus httpStatus;

        CadLiteAPIResponseStatusEnum(HttpStatus httpStatus) {
            this.httpStatus = httpStatus;
        }


        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

}
