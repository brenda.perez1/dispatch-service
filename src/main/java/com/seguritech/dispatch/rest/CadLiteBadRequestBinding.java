package com.seguritech.dispatch.rest;

import lombok.Data;

public class CadLiteBadRequestBinding {

    private final static String FIELD_REGX = "(?=\\p{Upper})";

    public static String resolve (String field, String defaultMessage) {
        String[] words = field.split(FIELD_REGX);
        String format = "";

        for (int i = 0; i < words.length; i++)
            format = i == 0
                    ? words[i].substring(0, 1).toUpperCase() + words[i].substring(1) + " "
                    : format.concat(words[i] + " ");

        return format.concat(defaultMessage);
    }


    @Data
    public static class InvalidParam {

        private final String name;
        private final String reason;

        public static InvalidParam of (String field, String defaultMessage) {
            return new InvalidParam(field, resolve(field, defaultMessage));
        }

    }

}
