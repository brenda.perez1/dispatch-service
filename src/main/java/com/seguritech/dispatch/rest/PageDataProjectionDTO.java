package com.seguritech.dispatch.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class PageDataProjectionDTO<T> {

    List<T> items;
    Long total;

    public PageDataProjectionDTO (List<T> items) {
        this.items = items;
        this.total = (long) items.size();
    }

}
