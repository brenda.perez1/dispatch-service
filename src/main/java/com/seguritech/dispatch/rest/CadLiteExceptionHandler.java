package com.seguritech.dispatch.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ResponseStatusException;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.exception.InfrastructureException;
import com.seguritech.dispatch.exception.NotAuthorizedException;
import com.seguritech.dispatch.exception.NotFoundException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class CadLiteExceptionHandler {

	private final MessageSource messageSource;

	@Autowired
	public CadLiteExceptionHandler(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity<CadLiteAPIResponse<CadLiteAPIErrorResponse>> business (BusinessException exception) {
    	
		String errorMessage = messageSource.getMessage(exception.getMessage(), exception.getArgs(),
				LocaleContextHolder.getLocale());
		
        if (exception instanceof NotFoundException) {
            return CadLiteAPIResponse.build(
                    Optional.ofNullable(exception.getBusinessError()).map(errorCode -> {
                        if (errorCode == null || errorCode.longValue() == 0)
                            return new CadLiteAPIErrorResponse(404L);
                        return new CadLiteAPIErrorResponse(errorCode);
                    })
                            .orElse(null),
                    exception.getMessage(),
                    CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.NOT_FOUND
            );
        }

        if (exception instanceof NotAuthorizedException) {
            return CadLiteAPIResponse.build(
                    new CadLiteAPIErrorResponse(401L),
                    exception.getMessage(),
                    CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.UNAUTHORIZED
            );
        }

        return CadLiteAPIResponse.build(
                Optional.ofNullable(exception.getBusinessError()).map(CadLiteAPIErrorResponse::new)
                        .orElse(null),
                errorMessage,
                CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.ERROR
        );
    }

	@ExceptionHandler(value = ResponseStatusException.class)
	public ResponseEntity<CadLiteAPIResponse<CadLiteAPIErrorResponse>> notFoundException(
			ResponseStatusException exception) {
		log.error(exception.getMessage(), exception);
		return CadLiteAPIResponse.build(new CadLiteAPIErrorResponse(404L), exception.getMessage(),
				CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.NOT_FOUND);
	}

	@ExceptionHandler(value = InfrastructureException.class)
	public ResponseEntity<CadLiteAPIResponse<CadLiteAPIErrorResponse>> infrastructure(
			InfrastructureException exception) {
		return CadLiteAPIResponse.build(null, exception.getMessage(),
				CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.ERROR_INFRASTRUCTURE);
	}

	@ExceptionHandler(value = WebExchangeBindException.class)
	public ResponseEntity<CadLiteAPIResponse<CadLiteBadRequestBinding.InvalidParam>> dtoValidationException(
			WebExchangeBindException ex) {

		List<CadLiteBadRequestBinding.InvalidParam> errors = new ArrayList<>();
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {

			errors.add(CadLiteBadRequestBinding.InvalidParam.of(error.getField(), error.getDefaultMessage()));
		}

		return CadLiteAPIResponse.buildErrors(errors, "Invalid request",
				CadLiteAPIResponse.CadLiteAPIResponseStatusEnum.BAD_REQUEST);
	}

}
