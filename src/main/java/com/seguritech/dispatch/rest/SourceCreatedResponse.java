package com.seguritech.dispatch.rest;

import lombok.Getter;

@Getter
public class SourceCreatedResponse<T> {

    private T sourceId;

    public SourceCreatedResponse(T sourceId) {
        this.ensurePrimitive(sourceId);
        this.sourceId = sourceId;
    }

    private void ensurePrimitive(T sourceId) {
        if (sourceId instanceof String) return;
        if (sourceId instanceof Long) return;
        if (sourceId instanceof Integer) return;
        if (sourceId instanceof Double) return;
        if (sourceId instanceof Float) return;
        throw new IllegalArgumentException(String.format("[%s] Type is not supported as a resource identifier", sourceId.getClass().getName()));
    }

}
