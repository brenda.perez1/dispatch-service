package com.seguritech.dispatch.exception;

public class NotAuthorizedException extends   BusinessException {

    public NotAuthorizedException(String message, long errorCode) {
        super(message, errorCode);
    }

    public NotAuthorizedException(String message, Throwable cause) {
        this(message, 0L, cause);
    }

    public NotAuthorizedException(String message, Long errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }
}
