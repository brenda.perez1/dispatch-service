package com.seguritech.dispatch.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class InfrastructureException extends RuntimeException {
	
	private String message;
    private Object[] args;

}
