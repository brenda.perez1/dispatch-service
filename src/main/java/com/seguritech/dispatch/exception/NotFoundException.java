package com.seguritech.dispatch.exception;

public class NotFoundException extends BusinessException {

    public NotFoundException(String message, long errorCode) {
        super(message, errorCode);
    }

    public NotFoundException(String message, Throwable cause) {
        this(message, 0L, cause);
    }

    public NotFoundException(String message, Long errorCode, Throwable cause) {
        super(message, errorCode, cause);
    }

}
