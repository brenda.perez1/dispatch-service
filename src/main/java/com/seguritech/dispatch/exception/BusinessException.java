package com.seguritech.dispatch.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BusinessException extends RuntimeException {

  private String message;
  private Object[] args;
  private Long businessError;

  public BusinessException() {
    super();
  }

  public BusinessException(String message, Long code) {
    super(message);
    this.message = message;
    this.businessError = code;
  }

  public BusinessException(String message, Throwable cause) {
    super(message, cause);
    this.message = message;
  }

  public BusinessException(String message, Long code, Throwable cause) {
    super(message, cause);
    this.businessError = code;
    this.message = message;
  }
  

}
