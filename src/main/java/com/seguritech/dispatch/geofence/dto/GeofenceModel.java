package com.seguritech.dispatch.geofence.dto;

import java.text.ParseException;
import java.time.LocalDateTime;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.WKTReader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.seguritech.dispatch.share.BaseModel;
import com.seguritech.dispatch.share.InvokeMethod;

import lombok.Data;

/**
 * @author edgar.alarcon
 * @version 1.0.0
 * @cad lite API
 * @Seguritech 9 jun. 2021
 */

@Data
public class GeofenceModel extends BaseModel {

	public static final String TYPE_GEOFENCE_CILCLE = "geofenceCircle";
	public static final String TYPE_GEOFENCE_POLYGON = "geofencePolygon";
	public static final String TYPE_GEOFENCE_POLYLINE = "geofencePolyline";
	
	public static final String TYPE_POLYLINE = "POLYGON";
	public static final String TYPE_CIRCLE = "CIRCLE";


//    private Long id;
	private String name;
	private String type;
	private String color;
	private String description;
	private String coordinates;
	private Boolean active;
	private Boolean byDefault;
	private String createdBy;
	private LocalDateTime createdDate;
	private String lastModifiedBy;
	private LocalDateTime lastModifiedDate;

	@JsonIgnore
	private String wkt;
	@JsonIgnore
	private GeofenceGeometry geometry;

//    @JsonIgnore
//    public String getArea() {
//        return area;
//    }

	public String getName() {
		return this.name;
//        return this.name.substring(0, 1).toUpperCase() + this.name.substring(1);
	}

	@InvokeMethod
	public void setArea() throws ParseException {
		if (type.toUpperCase().startsWith(TYPE_CIRCLE)) {
			geometry = new GeofenceCircle(coordinates);
		} else if (type.toUpperCase().startsWith(TYPE_POLYLINE)) {
			geometry = new GeofencePolygon(coordinates);
//        } else if (area.startsWith("LINESTRING")) {
//            final double distance = getDouble("polylineDistance");
//            geometry = new GeofencePolyline(area, distance > 0 ? distance
//                    : Context.getConfig().getDouble("geofence.polylineDistance", 25));
		} else {
			throw new ParseException("Unknown geometry type", 0);
		}

//        this.area = area;

// 		TODO: OPCION 2 PARA CREAR LA GEOCERCA, SOLO USAR EN CASO DE ERROR
//      Geometry oaxaca = new WKTReader().read("POLYGON((16.398377446436896 -98.96527290344238,18.487342352543493 -98.41119289398192,17.433732617381608 -93.6555290222168,15.282901789577224 -94.58155632019042,16.398377446436896 -98.96527290344238))");
//      Geometry pointx = new GeometryFactory().createPoint(new Coordinate(17.064885, -96.730239));
//   	System.out.println("Point within g1: " + oaxaca.contains(pointx));
	}

//    @QueryIgnore
//    @JsonIgnore
	public GeofenceGeometry getGeometry() {
		return geometry;
	}

//    @QueryIgnore
//    @JsonIgnore
	public void setGeometry(GeofenceGeometry geometry) {
		wkt = geometry.toWkt();
		this.geometry = geometry;
	}

}
