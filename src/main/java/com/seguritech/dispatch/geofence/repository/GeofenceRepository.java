package com.seguritech.dispatch.geofence.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.domain.model.geofence.entity.Geofence;


@Repository
public interface GeofenceRepository extends JpaRepository<Geofence, Long> {

    Geofence getById(Integer id);

    /**
     * @param word
     * @param paging
     * @return Page<CatGeofence>
     */
    @Query("FROM Geofence g WHERE g.description LIKE %:word% OR g.name LIKE %:word%")
    Page<Geofence> findByFilter(@Param("word") String word, Pageable paging);
    
    @Query(value="FROM Geofence g WHERE g.active = true ORDER BY g.name")
    List<Geofence> findAllByActiveOrderByName();

}
