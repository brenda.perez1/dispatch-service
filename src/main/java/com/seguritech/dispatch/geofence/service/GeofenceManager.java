package com.seguritech.dispatch.geofence.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seguritech.dispatch.geofence.dto.GeofenceModel;
import com.seguritech.dispatch.geofence.service.impl.GeofenceServiceImpl;
import com.seguritech.dispatch.share.BaseObjectManager;

@Service
public class GeofenceManager extends BaseObjectManager<GeofenceModel> {

//	private final GeofenceServiceImpl geofenceService;
	private List<GeofenceModel> geofences;

	@Autowired
	public GeofenceManager(GeofenceServiceImpl geofenceService) {
        super(geofenceService,GeofenceModel.class);
//		this.geofenceService = geofenceService;
	}

	@PostConstruct
	private void postConstruct() {
        refreshItems();

//		Collection<GeofenceModel> gg = getDatabaseItems();
//		geofences = gg.stream().collect(Collectors.toList());

	}

//	public List<GeofenceModel> getGeofences() {
//		return geofences;
//	}
//
//	public void setGeofences(List<GeofenceModel> geofences) {
//		this.geofences = geofences;
//	}
	
	

//    @Override
//    public final void refreshExtendedPermissions() {
//        super.refreshExtendedPermissions();
//        recalculateDevicesGeofences();
//    }
//
//    public List<Long> getCurrentDeviceGeofences(
////    		Position position
//    		) {
//        List<Long> result = new ArrayList<>();
////        for (long geofenceId : getAllDeviceItems(position.getDeviceId())) {
////            Geofence geofence = getById(geofenceId);
////            if (geofence != null && geofence.getGeometry() != null && geofence.getGeometry()
////                    .containsPoint(position.getLatitude(), position.getLongitude())) {
////                result.add(geofenceId);
////            }
////        }
//        return result;
//    }

//    public void recalculateDevicesGeofences() {
////        for (Device device : Context.getDeviceManager().getAllDevices()) {
////            List<Long> deviceGeofenceIds = device.getGeofenceIds();
////            if (deviceGeofenceIds == null) {
////                deviceGeofenceIds = new ArrayList<>();
////            } else {
////                deviceGeofenceIds.clear();
////            }
////            Position lastPosition = Context.getIdentityManager().getLastPosition(device.getId());
////            if (lastPosition != null && getAllDeviceItems(device.getId()) != null) {
////                deviceGeofenceIds.addAll(getCurrentDeviceGeofences(lastPosition));
////            }
////            device.setGeofenceIds(deviceGeofenceIds);
////        }
//    }

}
