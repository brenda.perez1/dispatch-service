package com.seguritech.dispatch.geofence.service;

import java.util.List;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import com.domain.model.PaginatedResultResponse;
import com.domain.model.geofence.dto.GeofenceRequest;
import com.domain.model.geofence.dto.GeofenceResponse;

/**
 * @author edgar.alarcon
 * @version 1.0.0
 * @cad lite API
 * @Seguritech 9 jun. 2021
 */

public interface GeofenceService {

    /**
     * @param request
     */
    void create(GeofenceRequest request, UsernamePasswordAuthenticationToken user);

    /**
     * @param request
     * @param id
     */
    void update(GeofenceRequest request, Long id, UsernamePasswordAuthenticationToken user);

    /**
     * @param id
     */
    void delete(Long id, UsernamePasswordAuthenticationToken user);

    /**
     * @param id
     * @return
     */
    GeofenceResponse getById(Long id);

    /**
     * @return List<GeofenceResponse>
     */
    List<GeofenceResponse> retrieve();

    /**
     * @param page
     * @param size
     * @param word
     * @param sort
     * @param column
     * @return PaginatedResultResponse<?>
     */
    PaginatedResultResponse<List<GeofenceResponse>> retrieve(Integer page, Integer size, String word, String sort, String column);
    

}
