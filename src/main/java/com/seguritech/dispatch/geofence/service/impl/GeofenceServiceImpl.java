package com.seguritech.dispatch.geofence.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.PaginatedResultResponse;
import com.domain.model.geofence.dto.GeofenceRequest;
import com.domain.model.geofence.dto.GeofenceResponse;
import com.domain.model.geofence.entity.Geofence;
import com.seguritech.dispatch.exception.NoDataFoundException;
import com.seguritech.dispatch.geofence.dto.GeofenceModel;
import com.seguritech.dispatch.geofence.repository.GeofenceRepository;
import com.seguritech.dispatch.geofence.service.GeofenceService;
import com.seguritech.dispatch.share.DataManager;
import com.seguritech.dispatch.share.MessageResolver;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author edgar.alarcon
 * @cad lite API 
 * @Seguritech
 * 9 jun. 2021
 * @version 1.0.0
 */
@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class GeofenceServiceImpl implements GeofenceService, DataManager<GeofenceModel> {
	
	@Autowired
	private final GeofenceRepository repository;

	@Override
	@CacheEvict(value ="geofence-catalog", allEntries = true)
	public void create(GeofenceRequest request, UsernamePasswordAuthenticationToken user) {
		log.info("New record creation: {}", request);
		request.setName(request.getName().toLowerCase());
		request.setActive(Boolean.TRUE);
		this.repository.save(this.toEntity(request));
	}

	@Override
	@CacheEvict(value ="geofence-catalog", allEntries = true)
	public void update( GeofenceRequest request, Long id, UsernamePasswordAuthenticationToken user) {
		log.info("Record update by Id: {} {}", request, id);
		Geofence entity = this.repository.findById(id).<NoDataFoundException>orElseThrow(() -> {
			throw new NoDataFoundException(MessageResolver.RECORD_NOT_FOUND_EXCEPTION);
		});
		entity.setId(id);
		entity.setName(request.getName().toLowerCase());
		entity.setType(request.getType());
		entity.setColor(request.getColor());
		entity.setDescription(request.getDescription());
		entity.setCoordinates(request.getCoordinates());
		entity.setActive(request.getActive());
		this.repository.save(entity);
	}

	@Override
	@CacheEvict(value ="geofence-catalog", allEntries = true)
	public void delete(Long id, UsernamePasswordAuthenticationToken user) {
		log.info("Record deletion by Id: " + id);
		Geofence entity = this.repository.findById(id).<NoDataFoundException>orElseThrow(() -> {
			throw new NoDataFoundException(MessageResolver.RECORD_NOT_FOUND_EXCEPTION);
		});
		entity.setActive(Boolean.FALSE);
		this.repository.save(entity);
	}

	@Override
	@Cacheable(value ="geofence-catalog")
	public GeofenceResponse getById(Long id) {
		log.info("Get record by Id");
		return this.repository.findById(id).map(this::toDTO).<NoDataFoundException>orElseThrow(() -> {
			throw new NoDataFoundException(MessageResolver.RECORD_NOT_FOUND_EXCEPTION);
		});
	}

	@Override
	@Cacheable(value ="geofence-catalog")
	public List<GeofenceResponse> retrieve() {
		log.info("Get record by Id");
		return this.repository.findAllByActiveOrderByName().stream().map(this::toDTO).collect(Collectors.toList());
	}

	@Override
	public PaginatedResultResponse<List<GeofenceResponse>> retrieve(Integer page, Integer size, String word,
																	String sort, String column) {
		log.info("Get record query and search by filter");
		Pageable paging = PageRequest.of(page - 1, size, Sort.by(sort.toUpperCase().equals("DESC") ? Sort.Direction.DESC : Sort.Direction.ASC, column));
		Page<Geofence> response = this.repository.findByFilter(word.toUpperCase(), paging);
		List<GeofenceResponse> result = response.getContent().stream().map(this::toDTO).collect(Collectors.toList());
		return new PaginatedResultResponse<>(result, response.getTotalElements());
	}
	
	@Override
	@Cacheable(value ="geofence-catalog-1")
	public List<GeofenceModel> getObjects() {
		log.info("Get record by Id");
		return this.repository.findAllByActiveOrderByName().stream().map(this::toDTOModel).collect(Collectors.toList());
	}
	
	GeofenceResponse toDTO(Geofence entity) {
		return new ModelMapper().map(entity, GeofenceResponse.class);
	}
	
	Geofence toEntity(GeofenceRequest request) {
		return new ModelMapper().map(request, Geofence.class);
	}
	
	GeofenceModel toDTOModel(Geofence entity) {
		return new ModelMapper().map(entity, GeofenceModel.class);
	}
}
