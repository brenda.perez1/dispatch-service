package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentResponseMin {
	
	private final String id;
	private final String folio;
	private final String status;
    private final String description;
	private final String senseDetection;
	private final boolean isManual;	
	private final Date eventDate;
	private final Date createdAt;
	private final Date updatedAt;
	private final NationalSubTypeResponseMin nationalSubType;
	private final PriorityResponse priority;
	private final IncidentSourceResponse source;
	private final IncidentLocationResponseMin location;
	private final AppUserResponse createdByUser;
	private final Set<IncidentAssociatedResponseMin> associates;
	private List<IncidentCorporationSearchDetailResponseMin> emergencyCorporations;
	private final IncidentTokenResponse token;
	private final IncidentSecundaryResponse secundary;
	private List<IncidentAdditionalClassificationResponseMin> additionalNationalSubTypes;
	private final boolean isAsociation;
	
	public void setEmergencyCorporations(
			List<IncidentCorporationSearchDetailResponseMin> emergencyCorporations) {
		this.emergencyCorporations = emergencyCorporations;
	}
	
	
}
