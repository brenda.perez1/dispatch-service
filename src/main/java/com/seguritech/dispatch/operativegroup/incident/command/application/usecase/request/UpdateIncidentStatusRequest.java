package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;


import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UpdateIncidentStatusRequest {

    private String incidentId;
    private final String status;
    private Collection<Long> corporationIds;
    private String userId;
    private Long profileId;

}

