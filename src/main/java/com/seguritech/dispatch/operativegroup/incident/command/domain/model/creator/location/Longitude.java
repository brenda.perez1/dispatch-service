package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class Longitude {

    private Double value;

    public static Longitude optional(Double value) {
        return new Longitude(value);
    }
}
