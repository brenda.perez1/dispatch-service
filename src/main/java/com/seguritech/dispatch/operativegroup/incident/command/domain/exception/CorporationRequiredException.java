package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class CorporationRequiredException extends BusinessException {

  public CorporationRequiredException() {
    super(MessageResolver.CORPORATION_REQUERID_EXCEPTION, 3002L);
  }
}
