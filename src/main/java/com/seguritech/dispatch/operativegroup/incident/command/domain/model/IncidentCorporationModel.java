package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.seguritech.dispatch.operativegroup.incident.command.domain.event.AceptedSupportRequestEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.CreateNoteIncidentOperatingCorporationAssignedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentCorporationClosedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentCorporationClosureRecommendationEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingCorporationAssignedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingCorporationCancelEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingCorporationReopenedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingCorporationTransferredEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingCorporationOperativeCancelEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingSupportGroupEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingSupportRequestEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.OperatingCorporationIncidentClosedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.OperatingCorporationIncidentTransferedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.OperatingCorporationOperativeIncidentClosedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.RejectedSupportRequestEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotAssignedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotClosedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotOpenException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationUnitNotCancelException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationsRequiredException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentWithForceUnitAsiggnedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.unit.UnitForceList;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentCorporationStatusChangedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOperatingCorporationAssignedCorpEvent;
import com.seguritech.platform.domain.RootAggregate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncidentCorporationModel extends RootAggregate {

    private final IncidentId incidentId;
    private final CorporationOperativeId corporationOperativeId;
    private CorporationId corporationId;
    private UnitForceList unitForceList;
    private IncidentCorporationStatus status;
    private IncidentCorporationReasonId reasonId;
    private IncidentCorporationObservation observation;
    private final UserId createdByUser;
    private final Date createdAt;
    private UserId cancelByUser;
    private Date cancelAt;
    private UserId closedByUser;
    private Date closedAt;
    private UserId traferedByUser;
    private Date traferedAt;
    private GeofenceId geofence;
    private boolean byDefault;
    private IncidentLocationId location;
    private Date assignedUnitForceDate;
    private IncidentCorporationReasonCloseId reasonCloseId;
    private IncidentCorporationObservation observationClose;
    private IncidentCorporationFollowUpStatusId followUpId;
    private UserId updatedBy;
    private Date updateAt;
    
    public IncidentCorporationModel(IncidentId incidentId, CorporationOperativeId corporationOperativeId, CorporationId corporationId, UnitForceList unitForceList, IncidentCorporationStatus status, IncidentCorporationReasonId reasonId,
    		IncidentCorporationObservation observation, UserId createdByUser, Date createdAt,
    		UserId cancelByUser, Date cancelAt, UserId closedByUser, Date closedAt, UserId traferedByUser, Date traferedAt,
    		IncidentCorporationReasonCloseId reasonCloseId, IncidentCorporationObservation observationClose, IncidentCorporationFollowUpStatusId followUpId, UserId updatedBy, Date updateAt
    	) {
        this.incidentId = incidentId;
        this.corporationOperativeId = corporationOperativeId;
        this.corporationId = corporationId;
        this.unitForceList = unitForceList;
        this.status = status;
        this.reasonId = reasonId;
        this.observation = observation;
        this.createdByUser = createdByUser;
        this.createdAt = createdAt;
        this.cancelByUser = cancelByUser;
        this.cancelAt = cancelAt;
        this.closedByUser = closedByUser;
        this.closedAt = closedAt;
        this.traferedByUser = traferedByUser;
        this.traferedAt = traferedAt;
        this.reasonCloseId = reasonCloseId;
        this.observationClose = observationClose;
        this.followUpId = followUpId;
        this.updatedBy = updatedBy;
		this.updateAt = updateAt;
    }

    public static IncidentCorporationModel assign(IncidentId incidentId,
                                                  CorporationOperativeId corporationOperativeId,
                                                  CorporationId corporationId,
                                                  UserId createdByUser) {
        IncidentCorporationModel model = new IncidentCorporationModel(
                incidentId,
                corporationOperativeId,
                corporationId,
                UnitForceList.optional(null),
                IncidentCorporationStatus.initial(),
                null,//cuando se asigna la corporación, la razón es null
                IncidentCorporationObservation.empty(),
                createdByUser,
                new Date(),
                UserId.empty(),
                new Date(),
                UserId.empty(),
                new Date(),
                UserId.empty(),
                new Date(),
                null,
                IncidentCorporationObservation.empty(),
                null,
                UserId.empty(),
                new Date()
        );        
        model.record(new IncidentOperatingCorporationAssignedEvent (
        		model.incidentId.getValue(),
                model.corporationOperativeId.getValue(),
                model.createdByUser.getValue(),
                model.createdAt,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        ));
        return model;
    }
    
    public static IncidentCorporationModel dispatch(    		
    		IncidentId incidentId,
            CorporationOperativeId corporationOperativeId,
            CorporationId corporationId,
            UserId createdByUser, 
            GeofenceId geofence,
            IncidentLocationId location,
            String folio,
            String sourceName,
            Long sourceId,
            String subTypeName,
            Boolean secondaryInvoice,
            Boolean isManual,
            Boolean playSound) {
				IncidentCorporationModel model = new IncidentCorporationModel(
					incidentId,
					corporationOperativeId,
					corporationId,
					UnitForceList.optional(null),
					IncidentCorporationStatus.initial(),
					null,//cuando se asigna la corporación, la razón es null
					IncidentCorporationObservation.empty(),
					createdByUser,
					new Date(),//createdAt
					UserId.empty(),//cancelByUser
					null,//cancelAt
					UserId.empty(),//closedByUser
					null,//closedAt
					UserId.empty(),//traferedByUser
					null,//traferedAt
					null,//closeRasonId
					IncidentCorporationObservation.empty(),
					null,//followUpId
					UserId.empty(),//updatedBy
					null//updatedBy
				);
				model.setGeofence(geofence);
				model.setLocation(location);
				model.record(new IncidentOperatingCorporationAssignedEvent(
						model.incidentId.getValue(),
						model.corporationId.getValue(),
						model.createdByUser.getValue(),
						model.createdAt,
						folio,
						sourceName,
						sourceId,
						isManual,
						subTypeName,
						null,
						Optional.ofNullable(secondaryInvoice).orElse(Boolean.FALSE),
						model.createdByUser.getValue(),
						model.createdAt,
						model.corporationOperativeId.getValue(),
						playSound
				));
			return model;
	}
    
    public static IncidentCorporationModel dispatchCorp(    		
    		IncidentId incidentId,
            List<CorporationOperativeId> listCorporationOperativeId,
            CorporationId corporationId,
            UserId createdByUser, 
            GeofenceId geofence,
            IncidentLocationId location,
            String folio,
            String sourceName,
            Long sourceId,
            String subTypeName,
            Boolean secondaryInvoice,
            Boolean isManual,
            Boolean playSound) {
				IncidentCorporationModel model = new IncidentCorporationModel(
					incidentId,
					null,
					corporationId,
					UnitForceList.optional(null),
					IncidentCorporationStatus.initial(),
					null,//cuando se asigna la corporación, la razón es null
					IncidentCorporationObservation.empty(),
					createdByUser,
					new Date(),//createdAt
					UserId.empty(),//cancelByUser
					null,//cancelAt
					UserId.empty(),//closedByUser
					null,//closedAt
					UserId.empty(),//traferedByUser
					null,//traferedAt
					null,//closeRasonId
					IncidentCorporationObservation.empty(),
					null,//followUpId
					UserId.empty(),//updatedBy
					null//updatedBy
				);
				model.setGeofence(geofence);
				model.setLocation(location);
				model.record(new IncidentOperatingCorporationAssignedCorpEvent(
						model.incidentId.getValue(),
						model.corporationId.getValue(),
						model.createdByUser.getValue(),
						model.createdAt,
						folio,
						sourceName,
						sourceId,
						isManual,
						subTypeName,
						null,
						Optional.ofNullable(secondaryInvoice).orElse(Boolean.FALSE),
						model.createdByUser.getValue(),
						model.createdAt,
						listCorporationOperativeId.stream().map(CorporationOperativeId::getValue).collect(Collectors.toList()),
						playSound
				));
			return model;
	}

    public static IncidentCorporationModel supportRequest(IncidentId incidentId,
                                                          CorporationOperativeId corporationOperativeId,
                                                          UserId requestByUser) {
        IncidentCorporationModel model = new IncidentCorporationModel(
                incidentId,
                corporationOperativeId,
                null,
                UnitForceList.optional(null),
                IncidentCorporationStatus.onHold(),
                IncidentCorporationReasonId.empty(),
                IncidentCorporationObservation.empty(),
                requestByUser,
                new Date(),
                UserId.empty(),
                new Date(),
                UserId.empty(),
                new Date(),
                UserId.empty(),
                new Date(),
                IncidentCorporationReasonCloseId.empty(),
                IncidentCorporationObservation.empty(),
                null,
                UserId.empty(),
                new Date());
        model.record(new IncidentOperatingSupportRequestEvent(
                model.incidentId.getValue(),
                model.corporationOperativeId.getValue(),
                model.createdByUser.getValue(),
                model.createdAt)
        );
        return model;
    }
    
    public static IncidentCorporationModel supportGroup(IncidentId incidentId, CorporationOperativeId corporationOperativeId, CorporationId corporationId, UserId requestByUser, String folio, String sourceName, String subTypeName) {
    	
    	IncidentCorporationModel model = new IncidentCorporationModel(
    			incidentId,
    			corporationOperativeId,
    			corporationId,
				UnitForceList.optional(null),
				IncidentCorporationStatus.initial(),
				null,
				IncidentCorporationObservation.empty(),
				requestByUser,
				new Date(),
				UserId.empty(),
				null,
				UserId.empty(),
				null,
				UserId.empty(),
				null,
				null,
				IncidentCorporationObservation.empty(),
				null,
				UserId.empty(),
				null
		);
    	
    	model.record(new IncidentOperatingSupportGroupEvent(
    			model.incidentId.getValue(),
    			model.corporationOperativeId.getValue(),
    			model.createdByUser.getValue(),
    			model.createdAt,
    			folio,
    			sourceName, 
    			subTypeName)
    	);
    	
    	return model;
    }

    public void reactivate(UserId reactivatedBy) {
    	
        if (!this.status.equals(new IncidentCorporationStatus(IncidentCorporationStatusEnum.CLOSED))) {
            throw new IncidentCorporationNotClosedException(this.corporationOperativeId.getValue());
        }
        
        this.status = new IncidentCorporationStatus(IncidentCorporationStatusEnum.OPEN);

        this.record(new IncidentOperatingCorporationReopenedEvent(
        		this.incidentId.getValue(),
                this.corporationOperativeId.getValue(),
                reactivatedBy.getValue(),
                new Date()
        ));
    }

    public void acceptSupport(UserId reactivatedBy) {
        
    	if (!this.status.equals(new IncidentCorporationStatus(IncidentCorporationStatusEnum.ON_HOLD))) {
            throw new IncidentCorporationNotAssignedException(this.incidentId.getValue());
        }
        
        this.status = new IncidentCorporationStatus(IncidentCorporationStatusEnum.OPEN);

        this.record(new AceptedSupportRequestEvent(
                this.incidentId.getValue(),
                this.corporationOperativeId.getValue(),
                reactivatedBy.getValue(),
                new Date()
        ));
    }

    public void rejectSupport(UserId RejectedBy) {
        
    	if (!this.status.equals(new IncidentCorporationStatus(IncidentCorporationStatusEnum.ON_HOLD))) {
            throw new IncidentCorporationNotAssignedException(this.incidentId.getValue());
        }
    	
        this.status = new IncidentCorporationStatus(IncidentCorporationStatusEnum.REJECTED);

        this.record(new RejectedSupportRequestEvent(
                this.incidentId.getValue(),
                this.corporationOperativeId.getValue(),
                RejectedBy.getValue(),
                new Date()
        ));
    }

    public void cancel(IncidentCorporationReasonBounded reason, IncidentCorporationReasonId reasonId) {
    	
        if (!this.status.getValue().equals(IncidentCorporationStatusEnum.OPEN)) {
            throw new IncidentCorporationNotOpenException();
        }
        
        if (!this.unitForceList.isEmpty())
            throw new IncidentWithForceUnitAsiggnedException(this.getIncidentId());

        this.status = new IncidentCorporationStatus(IncidentCorporationStatusEnum.CANCELED);
        this.reasonId = reasonId;
        this.cancelByUser = reason.getCancelByUser();
        this.updatedBy = reason.getCancelByUser();
        this.cancelAt = reason.getCancelAt();
        this.updateAt = new Date();
        this.observation = reason.getObservation();
        
        this.record(new IncidentOperatingCorporationCancelEvent(
                this.incidentId.getValue(),
                this.corporationId.getValue(),
                reason.getReasonId().getValue(),
                reason.getObservation().getValue(),
                reason.getCancelByUser().getValue(),
                reason.getCancelAt(),
                this.corporationOperativeId.getValue()
        ));
        
        this.record(new IncidentOperatingCorporationOperativeCancelEvent(
    			this.incidentId.getValue(),
    			this.corporationId.getValue(),
    			reason.getReasonId().getValue(),
    			reason.getObservation().getValue(),
    			reason.getCancelByUser().getValue(),
    			reason.getCancelAt()
    	));
    }
    
    public void close(IncidentCorporationReasonCloseId closeId, IncidentCorporationReasonCloseBounded reason, UserId userId) {
       
    	if (!this.unitForceList.isEmpty())
            throw new IncidentCorporationUnitNotCancelException(this.corporationOperativeId, this.incidentId);
    	
        this.status = IncidentCorporationStatus.of(IncidentCorporationStatusEnum.CLOSED.toString());
        this.closedByUser = userId;
        this.updatedBy = userId;
        this.closedAt = new Date();
        this.reasonCloseId = closeId;
        this.observationClose = reason.getObservation();

        this.record(new OperatingCorporationIncidentClosedEvent(
                this.incidentId.getValue(),
                this.corporationId.getValue(),
                this.status.getValue().toString(),
                this.closedByUser.getValue(),
                this.getClosedAt(),
                reason.getReasonId().getValue(),
                reason.getObservation().getValue(),
                this.corporationOperativeId.getValue()
        ));
        
        this.record(new OperatingCorporationOperativeIncidentClosedEvent(
                this.incidentId.getValue(),
                this.corporationId.getValue(),
                this.corporationOperativeId.getValue(),
                this.status.getValue().toString(),
                this.closedByUser.getValue(),
                this.getClosedAt(),
                reason.getReasonId().getValue(),
                reason.getObservation().getValue()
        ));
        this.updateAt = new Date();
    }
    
    public void changeStatus( IncidentCorporationStatus status, String updByUser) {
		
    	if (!this.unitForceList.isEmpty())
            throw new IncidentCorporationsRequiredException();
    	
		this.status = status;
		this.updatedBy = UserId.from(updByUser);
		this.updateAt = new Date();
	
		this.record(new IncidentCorporationStatusChangedEvent(
				this.incidentId.toString(),
				this.corporationId.getValue(),
				this.status.getValue().toString(),
				this.getUpdatedBy().getValue(),
				this.getUpdateAt()
		));
	}
    
    public void transfer(IncidentId incidentId,CorporationOperativeId oldCorporationOperativeId,
			 CorporationOperativeId newCorporationOperativeId, UserId transferedBy) {

    	//RN3: Solo se trasferira si no tiene unidades de fuerza asignadas
        if (!this.unitForceList.isEmpty())
            throw new IncidentWithForceUnitAsiggnedException(this.getIncidentId());

        this.traferedByUser = transferedBy;
        this.traferedAt = new Date();

        this.record(new OperatingCorporationIncidentTransferedEvent(
                this.incidentId.getValue(),
                oldCorporationOperativeId.getValue(),
                newCorporationOperativeId.getValue(), // this.corporationOperativeId.getValue(),
                this.traferedByUser.getValue(),
                this.traferedAt
        ));
        this.updateAt = new Date();
    }

    public void validateCanBeClosedIncident(UserId userId) {
        
    	if (!this.unitForceList.isEmpty())
            return;
    	
        this.record(new IncidentCorporationClosureRecommendationEvent(
                this.incidentId.getValue(),
                this.corporationOperativeId.getValue(),
                this.status.toString(),
                new Date(),
                userId.getValue()
        ));
    }
    
    public void updateFollowUp(IncidentCorporationFollowUpStatusId followUpId,
    		UserId updatedBy) {
		this.followUpId = followUpId;
		this.updatedBy = updatedBy;
		this.updateAt = new Date();
	}
    
    public void closeCorporation(CorporationId corporationId,UserId userId) {
    	this.updatedBy = userId;
    	this.corporationId = corporationId;
    	this.incidentId.getValue();
    	
    	this.record(new IncidentCorporationClosedEvent(
    				this.incidentId.getValue(),
    			    this.updatedBy.getValue(),
    			    this.corporationId.getValue()
    			    ));
    }

    public void createNotaAssignCorporation(IncidentId incidentId,
            CorporationId corporationId,
            UserId createdByUser) {
    	this.record(new CreateNoteIncidentOperatingCorporationAssignedEvent(incidentId.getValue(), corporationId.getValue(), createdByUser.getValue(), new Date()));
    	
    }
}
