package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;

public interface CorporationFollowUpStatusRepository {

	IncidentCorporationFollowUpStatusId findById(IncidentCorporationFollowUpStatusId reasonId);

}
