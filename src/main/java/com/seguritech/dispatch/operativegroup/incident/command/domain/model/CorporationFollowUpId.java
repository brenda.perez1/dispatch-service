package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.Optional;

import com.seguritech.platform.domain.UUIDGenerator;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CorporationFollowUpId {

	private final String value;
	
	public CorporationFollowUpId(String value) {
		this.value = value;
	}
	
	public static CorporationFollowUpId create() {
		return new CorporationFollowUpId(UUIDGenerator.random().toString());
	}

	public static CorporationFollowUpId required(String id) {
        return Optional.ofNullable(id).map(CorporationFollowUpId::new).orElse(null);
    }

}
