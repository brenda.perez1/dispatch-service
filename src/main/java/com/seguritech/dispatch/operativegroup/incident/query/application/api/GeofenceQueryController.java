package com.seguritech.dispatch.operativegroup.incident.query.application.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.geofence.service.GeofenceManager;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.FilterDTO;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ProfileIdRequierdException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Api(tags = {"GEOFENCE"})
@RequestMapping("/geofence")
public class GeofenceQueryController {

  
    private final GeofenceManager geofenceManager;
    
    public GeofenceQueryController(GeofenceManager geofenceManager) {       
        this.geofenceManager = geofenceManager;
    }

	@GetMapping("/refresh")
	public  ResponseEntity<CadLiteAPIResponse<String>> refresh() {
		geofenceManager.refreshItems();
		
		return CadLiteAPIResponse
				.build();
		

	}



}
