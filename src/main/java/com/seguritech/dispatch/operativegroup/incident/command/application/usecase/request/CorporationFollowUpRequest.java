package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CorporationFollowUpRequest {

    private String userId;
    private Long profileId;
	private String incidentId;
    private Long corporationId;
	private Long followUpStatus;
    private Long followUpReason;
	private String researchFolder;
    private String observation;

}
