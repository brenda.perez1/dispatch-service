package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationStatusInvalidException extends BusinessException {

    public IncidentCorporationStatusInvalidException(Object... value) {

        super(MessageResolver.INCIDENT_CORPORATION_STATUS_INVALID_EXCEPTION, value, 3017L);
    }
}
