package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class IncidentOperatingCorporationReopenedEvent implements DomainEvent {

    private final String incidentId;
    private final Long corporationId;
    private final String reactivatedBy;
    private final Date reactivationDate;
}
