package com.seguritech.dispatch.operativegroup.incident.query.domain;

public enum OpGpIncidentOrderFieldEnum {

	emergencyId,
    priority,
    invoice,
    type,
    event_date,
    nationalSubType,

}
