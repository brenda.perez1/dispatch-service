package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.Invoice;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCreatorResponse {
    private IncidentId incidentId;
    private Invoice invoice;
}
