package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.domain.model.incident.dto.IncidentStatus.IncidentStatusEnum;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incident.entity.IncidentLocationEntity;
import com.domain.model.national.entity.IncidentNationalSubtypeEntity;
import com.domain.model.priority.entity.Priority;
import com.domain.model.source.entity.IncidentSource;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCoordinatesBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentPriority;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentStatusBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentLocationEntityRepository;

@Service
//TODO
public class IncidentOperatingCorporationBoundaryRepositoryImpl implements IncidentRepository {

    private final IncidentEntityRepository repository;
    private final IncidentLocationEntityRepository locationEntityRepository;

//    private final ReactivateIncidentUseCase reactivateIncidentUseCase;

    public IncidentOperatingCorporationBoundaryRepositoryImpl(
    		IncidentEntityRepository repository,
    		IncidentLocationEntityRepository locationEntityRepository
//                                                              ReactivateIncidentUseCase reactivateIncidentUseCase
                                                              ) {
        this.repository = repository;
        this.locationEntityRepository  = locationEntityRepository;
//        this.reactivateIncidentUseCase = reactivateIncidentUseCase;
    }

    @Override
    public Boolean exist(IncidentId id) {
        return this.repository.existsById(id.getValue());
    }

    @Override
    public IncidentBounded find(IncidentId id) {
        return this.repository.findById(id.getValue()).map(this::toModel).orElse(null);
    }
    
	@Override
	public IncidentLocationBounded findLocation(IncidentId id) {
		return this.locationEntityRepository.findByEmergency(new IncidentEntity(id.getValue()))
				.map(this::toModel).orElse(null);
	}

    @Override
    public void reactivate(IncidentId id, IncidentCorporationReasonBounded reactivationReasonBounded) {
//        this.reactivateIncidentUseCase.reactivate(new ReactivateIncidentRequest(id.getValue(),
//            reactivationReasonBounded.getReactivatedByUser().getValue(),
//            reactivationReasonBounded.getObservation().getValue(),
//            reactivationReasonBounded.getReasonId().getValue(),
//            reactivationReasonBounded.getReactivatedAt()));
    }

    private IncidentBounded toModel(IncidentEntity incidentEntity) {
        return new IncidentBounded(IncidentId.required(incidentEntity.getId()),
            Optional.ofNullable(incidentEntity.getNationalSubType())
                .map(IncidentNationalSubtypeEntity::getNationalSubTypeId).map(IncidentTypeIdBounded::new)
                .orElse(null),
            IncidentStatusBounded.of(incidentEntity.getStatus()),
            Optional.ofNullable(incidentEntity.getPriority())
            .map(Priority::getId)
            .map(IncidentPriority::from)
            .orElse(IncidentPriority.none()),
            Optional.ofNullable(incidentEntity.getFolio()).orElse(null),
            Optional.ofNullable(incidentEntity.getSource()).map(IncidentSource::getName).orElse(null),
            Optional.ofNullable(incidentEntity.getSource()).map(IncidentSource::getId).orElse(null),
            Optional.ofNullable(incidentEntity.getNationalSubType()).map(IncidentNationalSubtypeEntity::getName).orElse(null),
            Optional.ofNullable(incidentEntity.getPriority()).map(Priority::getName).orElse(null),
            Optional.ofNullable(incidentEntity.getIsManual()).orElse(null),
            incidentEntity.getSource().getPlaySound()
        );
    }
    
    
    private IncidentLocationBounded toModel(IncidentLocationEntity incidentLocationEntity) {
        return new IncidentLocationBounded(IncidentId.required(incidentLocationEntity.getEmergency().getId()),
           new IncidentCoordinatesBounded(incidentLocationEntity.getLatitude(), incidentLocationEntity.getLongitude(), incidentLocationEntity.getIdEmergencyLocation())
        );
    }

	@Override
	public void updateStatus(IncidentId id, IncidentStatusEnum status) {
		IncidentEntity entity = this.repository.getById(id.getValue());
		if (entity != null && !entity.getStatus().equalsIgnoreCase("CLOSED") && !entity.getStatus().equalsIgnoreCase("CANCELLED")) {
			entity.setStatus(status.name());
			this.repository.save(entity);
		}
	}

	@Override
	public IncidentEntity getIncident(IncidentId id) {
		return this.repository.getById(id.getValue());
	}
    
    
//    public  IncidentModel toModel(IncidentEntity incidentEntity) {
//
//        return new IncidentModel(new IncidentId(incidentEntity.getId()),
//                incidentEntity.getFolio(),
//                SourceMapper.toModel(incidentEntity.getSource()),
//                TypeMapper.toModel(incidentEntity.getNationalEmergencySubSet()),
//                PriorityMapper.toModel(incidentEntity.getPriority()),
//                new IncidentStatus(incidentEntity.getStatus()),
//                IncidentEntityMapper.toModel(incidentEntity.getPhone()),
//                IncidentEntityMapper.toModel(incidentEntity.getReporterEntity()),
//                LocationMapper.toModel(incidentEntity.getLocation()),
//                incidentEntity.getEventDate(),
//                IncidentDescription.optional(incidentEntity.getDescription()),
//                IncidentSenseDetection.optional(incidentEntity.getSenseDetection()),
//                IncidentEntityMapper.toModel(incidentEntity.getIsManual()),
//                toModel(incidentEntity.getCreatedByUser()),
//                toModel(incidentEntity.getUpdatedByUser()),
//                incidentEntity.getCreatedAt(),
//                incidentEntity.getUpdatedAt(),
//                incidentEntity.getDateAttention(),
//                IncidentEntityMapper.toModel(incidentEntity.getCancellationReason()),
//                toModel(incidentEntity.getClosedReason()),
//                toModel(incidentEntity.getReactivationReason()));
//    }

}
