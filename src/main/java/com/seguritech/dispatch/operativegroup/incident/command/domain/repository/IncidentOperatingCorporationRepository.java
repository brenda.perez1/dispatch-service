package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import java.util.Collection;
import java.util.List;

import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.FilterDTO;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;

public interface IncidentOperatingCorporationRepository {

    IncidentCorporationModel save(IncidentCorporationModel model);
    
    Collection<IncidentCorporationModel> saveAll(Collection<IncidentCorporationModel> model);

    void save(List<IncidentCorporationModel> model);

    void remove(IncidentCorporationModel model);

    IncidentCorporationModel findById(CorporationOperativeId corporationId, IncidentId incidentId);
    
    IncidentCorporationModel findByIdAndStatus(CorporationOperativeId corporationId, IncidentId incidentId, IncidentCorporationStatusEnum status);
    
    List<IncidentCorporationModel> findById(CorporationId corporationId, IncidentId incidentId);
    
    List<IncidentCorporationModel> findByIncident(IncidentId id);
    
    List<IncidentCorporationModel> findByIncidentAndStatus(IncidentId id, String status);
    
    List<IncidentCorporationModel> findByIncidentAndStatusAndCorporation(IncidentId id, String status,CorporationId corporationId);

    List<String> searchUnitForce(CorporationOperativeId corporationId, IncidentId incidentId);
    
    List<String> searchUnitForce(CorporationId corporationId, IncidentId incidentId);
    
    List<IncidentCorporationEntity> findAll(List<CorporationOperativeId> corporationOperativeIds, FilterDTO status);
    
    IncidentCorporationEntity getByIncident(List<CorporationOperativeId> corporationOperativeIds, IncidentId incidentId);
    
    List<IncidentCorporationEntity> getByIncidentAndCorporationsOperative(List<CorporationOperativeId> corporationOperativeIds, IncidentId incidentId);
    
	void updateCorporationOperativeByIncidentIdAndCorporationOperativeId(IncidentId incidentId, CorporationOperativeId currentCorporationOperativeId, CorporationOperativeId newCorporationOperativeId);

    IncidentCorporationModel findByIdIncidentAndCorporation(CorporationId corporationId, IncidentId incidentId);
    
    Long countByIncidentAndStatus(IncidentId id, String status);
    
    Long countCorporationAssign(IncidentId id);
}