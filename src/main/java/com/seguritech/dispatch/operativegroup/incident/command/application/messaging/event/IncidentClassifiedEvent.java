package com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class IncidentClassifiedEvent {
    private String id;
    private Long typeId;
    private Long subTypeId;
    private String updatedBy;
    private Date updatedAt;
}
