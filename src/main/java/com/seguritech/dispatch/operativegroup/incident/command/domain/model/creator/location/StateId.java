package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class StateId {

    private Long value;

    public static StateId optional(Long id) {
        return new StateId(id);
    }

}
