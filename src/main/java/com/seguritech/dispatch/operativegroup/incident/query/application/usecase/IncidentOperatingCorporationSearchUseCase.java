package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentToOperativeGroupsResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOperatingCorporationQueryRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.platform.Service;

@Service
public class IncidentOperatingCorporationSearchUseCase {
	
	private final IncidentOperatingCorporationQueryRepository repository;
	
	public IncidentOperatingCorporationSearchUseCase(IncidentOperatingCorporationQueryRepository repository) {
		this.repository = repository;
	}
	
	public PageDataProjectionDTO<IncidentToOperativeGroupsResponse> execute (IncidentToOperativeGroupsSearchRequest request) {
    	return this.repository.search(request);
    }

}
