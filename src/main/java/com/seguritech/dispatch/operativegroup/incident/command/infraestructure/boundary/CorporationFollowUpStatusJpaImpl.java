package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import com.domain.model.corporation.followup.status.entity.CorporationFollowUpStatusEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationFollowUpStatusRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.CorporationFollowUpStatusEntityRepository;
import com.seguritech.platform.Service;

@Service
public class CorporationFollowUpStatusJpaImpl implements CorporationFollowUpStatusRepository {

	private final CorporationFollowUpStatusEntityRepository repository;

	public CorporationFollowUpStatusJpaImpl(CorporationFollowUpStatusEntityRepository repository) {
		this.repository = repository;
	}

	@Override
	public IncidentCorporationFollowUpStatusId findById(IncidentCorporationFollowUpStatusId statusId) {
		return this.repository.findById(statusId.getValue()).map(x -> x.getActive() ? x : null).map(this::toModel)
				.orElse(null);
	}

	private IncidentCorporationFollowUpStatusId toModel(CorporationFollowUpStatusEntity entity) {
		return IncidentCorporationFollowUpStatusId.required(entity.getId());
	}

}
