package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporation.followup.entity.CorporationFollowUpEntity;
import com.domain.model.corporation.followup.status.entity.CorporationFollowUpStatusEntity;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.CorporationFollowUpEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpFollowUpSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.CorporationFollowUpStatusResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.CorporationOperativeResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.CorporationResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpFollowUpSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpFollowUpQueryRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;

@Service
@Transactional(readOnly = true)
public class OpGpFollowUpQueryQueryRepositoryJpaImpl implements OpGpFollowUpQueryRepository {

	private static final String EMPTY_STRING = "";

	private final CorporationFollowUpEntityRepository repository;

	public OpGpFollowUpQueryQueryRepositoryJpaImpl(CorporationFollowUpEntityRepository repository) {
		this.repository = repository;
	}

	@Override
	public PageDataProjectionDTO<OpGpFollowUpSearchResponse> search(OperativeGpFollowUpSearchRequest request) {
		Page<CorporationFollowUpEntity> result = this.repository.findAllByIncidentId(request.getIncidentId(),
				PageRequest.of(request.getPage(), request.getSize(), Direction.fromString(request.getOrder().name()),
						request.getOrderByField()));
		return new PageDataProjectionDTO<>(result.get().map(this::toResponse).collect(Collectors.toList()),
				result.getTotalElements());
	}

	private OpGpFollowUpSearchResponse toResponse(CorporationFollowUpEntity entity) {
		return new OpGpFollowUpSearchResponse(entity.getId(),
				Optional.ofNullable(entity.getCorporationOperative()).map(this::toResponse).orElse(null),
				Optional.ofNullable(entity.getFollowUpStatus()).map(this::toResponse).orElse(null),
				entity.getFollowUpReason() != null ? entity.getFollowUpReason().getName() : EMPTY_STRING,
				entity.getResearchFolder(), entity.getObservation(), entity.getCreatedBy().getAlias(),
				entity.getCreationDate());
	}

	private CorporationOperativeResponse toResponse(CorporationOperative entity) {
		return new CorporationOperativeResponse(entity.getId(), entity.getName(), entity.getClave(), entity.getActive(),
				Optional.ofNullable(entity.getCorporation()).map(this::toResponse).orElse(null));
	}

	private CorporationResponse toResponse(Corporation entity) {
		return new CorporationResponse(entity.getId(), entity.getName(), entity.getClave(), entity.getDescription(),
				entity.getIcon(), entity.getActive());
	}

	private CorporationFollowUpStatusResponse toResponse(CorporationFollowUpStatusEntity entity) {
		return new CorporationFollowUpStatusResponse(entity.getId(), entity.getName(), entity.getDescription());
	}

}
