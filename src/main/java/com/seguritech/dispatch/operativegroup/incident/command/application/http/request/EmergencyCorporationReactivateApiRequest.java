package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationReactivateApiRequest {

    private Long reactivationReasonId;
    private String observation;
    private Long profileId;

}
