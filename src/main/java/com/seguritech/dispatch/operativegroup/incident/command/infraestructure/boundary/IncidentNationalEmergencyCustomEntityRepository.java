package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.domain.model.national.entity.IncidentNationalTypeEntity;

public interface IncidentNationalEmergencyCustomEntityRepository extends
    JpaRepository<IncidentNationalTypeEntity, Long>,
    JpaSpecificationExecutor<IncidentNationalTypeEntity> {}
