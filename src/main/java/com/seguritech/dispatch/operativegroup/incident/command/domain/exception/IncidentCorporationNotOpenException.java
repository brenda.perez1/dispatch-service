package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationNotOpenException extends BusinessException {

    public IncidentCorporationNotOpenException() {
        super(MessageResolver.INCIDENT_CORPORATION_NOT_OPEN_EXCEPTION, 3012L);
    }

}
