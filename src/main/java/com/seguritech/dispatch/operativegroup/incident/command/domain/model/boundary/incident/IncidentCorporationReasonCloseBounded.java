package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import java.util.Date;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;

import lombok.Getter;

@Getter
public class IncidentCorporationReasonCloseBounded {
	
	private final IncidentCorporationReasonCloseId reasonId;
    private final IncidentCorporationObservation observation;
    private final UserId reactivatedByUser;
    private final Date reactivatedAt;
    private final UserId cancelByUser;
    private final Date cancelAt;
    
	public IncidentCorporationReasonCloseBounded(IncidentCorporationReasonCloseId reasonId,
			IncidentCorporationObservation observation, UserId reactivatedByUser,
			UserId cancelByUser) {
		
		this.reasonId = reasonId;
		this.observation = observation;
		this.reactivatedByUser = reactivatedByUser;
		this.reactivatedAt = new Date();
		this.cancelByUser = cancelByUser;
		this.cancelAt = new Date();
	}
	
	public static IncidentCorporationReasonCloseBounded empty() {
        return new IncidentCorporationReasonCloseBounded(
                IncidentCorporationReasonCloseId.empty(),
                IncidentCorporationObservation.empty(),
                UserId.empty(),
                UserId.empty()
        );
    }

    public boolean isEmpty() {
        return this.reasonId.isEmpty();
    }

}
