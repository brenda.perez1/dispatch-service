package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class IncidentAdditionalClassificationResponse {

	private final Long id;
    private final String name;
    private final String clave;
    private final String description;
    private final NationalTypeArrayResponse nationalType;
    private final NationalCategorySubtypeResponse categoryType;
    private final Boolean isSubtype;

}
