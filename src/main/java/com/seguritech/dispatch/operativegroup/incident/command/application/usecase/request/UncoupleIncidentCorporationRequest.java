package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UncoupleIncidentCorporationRequest {
    private final String incidentId;
    private final Long corporationId;
    private final String userId;
}
