package com.seguritech.dispatch.operativegroup.incident.command.application.http.response;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentManualCreateApiResponse {
    private String emergencyId;
    private String invoice;
}
