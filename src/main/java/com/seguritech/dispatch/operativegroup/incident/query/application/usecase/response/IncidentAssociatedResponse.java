package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IncidentAssociatedResponse {
	
	private String id;
	private String folio;
	private NationalSubTypeResponseMin nationalSubtype;

}
