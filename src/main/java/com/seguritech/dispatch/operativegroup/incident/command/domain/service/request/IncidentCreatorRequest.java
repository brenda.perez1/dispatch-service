package com.seguritech.dispatch.operativegroup.incident.command.domain.service.request;

import java.util.Date;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.Description;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.HostIp;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.IncidentTypeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.PhoneNumber;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.PhoneNumberCountryId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.SourceId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Latitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Longitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.StateId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.TownId;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCreatorRequest {
    private SourceId sourceId;
    private PhoneNumber phoneNumber;
    private PhoneNumberCountryId phoneNumberCountryId;
    private Date eventDate;
    private IncidentTypeId incidentTypeId;
    private IncidentLocationSearch locationRequest;
    private HostIp createdFrom;
    private Description description;
    private UserId userId;
    private Boolean isManual;

    public static IncidentCreatorRequest build(Long sourceId, String phoneNumber, Long phoneNumberCountryId,
                                                                                                       Date eventDate, String createdFrom, String description, String userId,
                                                                                                       Long incidentTypeId, Long stateId, Long townId, Double latitude, Double longitude

    ) {
        return new IncidentCreatorRequest(
                SourceId.required(sourceId),
                PhoneNumber.optional(phoneNumber),
                PhoneNumberCountryId.optional(phoneNumberCountryId),
                eventDate,
                IncidentTypeId.optional(incidentTypeId),
                new IncidentLocationSearch(
                        StateId.optional(stateId),
                        TownId.optional(townId),
                        Latitude.optional(latitude),
                        Longitude.optional(longitude)
                ),
                HostIp.required(createdFrom),
                Description.optional(description),
                UserId.required(userId),
                true
        );
    }
}

