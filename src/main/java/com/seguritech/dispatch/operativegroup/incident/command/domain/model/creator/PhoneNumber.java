package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class PhoneNumber {
    private final String value;

    private PhoneNumber(String value) {
        this.value = value;
    }

    public static PhoneNumber optional(String value) {
        return new PhoneNumber(value);
    }
}
