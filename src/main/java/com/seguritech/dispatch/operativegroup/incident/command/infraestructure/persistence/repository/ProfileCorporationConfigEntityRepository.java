package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.domain.model.corporationoperativeprofile.entity.CorporationOperativeProfile;
import com.domain.model.corporationoperativeprofile.entity.CorporationOperativeProfilePK;

public interface ProfileCorporationConfigEntityRepository extends JpaRepository<CorporationOperativeProfile, CorporationOperativeProfilePK> {
	
	@Query(value= "FROM CorporationOperativeProfile cop WHERE cop.id.profile.id = :profileId and cop.active = :active ")
	List<CorporationOperativeProfile> findByProfile(@Param("profileId") Long profileId, @Param("active") Boolean active);
	
}
