package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IncidentOperatingCorporationBoundedFinder {

    private final IncidentOperatingCorporationRepository repository;

    public IncidentOperatingCorporationBoundedFinder(IncidentOperatingCorporationRepository repository) {
        this.repository = repository;
    }

    public IncidentCorporationModel findById(CorporationOperativeId corporationId,
                                             IncidentId incidentId) {
        return Optional.ofNullable(this.repository.findById(corporationId, incidentId))
                .orElseThrow(() ->
                        new IncidentCorporationNotFoundException(corporationId.getValue(), incidentId.getValue()));
    }

}
