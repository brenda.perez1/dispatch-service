package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.domain.model.incident.dto.IncidentStatus.IncidentStatusEnum;
import com.domain.model.incident.entity.IncidentEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;

public interface IncidentRepository {

    Boolean exist(IncidentId id);

    IncidentBounded find(IncidentId id);
    
    IncidentLocationBounded findLocation(IncidentId id);

    void reactivate(IncidentId id, IncidentCorporationReasonBounded reactivationReasonBounded);
    
    IncidentEntity getIncident(IncidentId id);
    
    void updateStatus(IncidentId id, IncidentStatusEnum status);

}
