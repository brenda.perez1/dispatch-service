package com.seguritech.dispatch.operativegroup.incident.query.domain;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupsResponse;

import java.util.List;

public interface IncidentOpGpRepository {

    List<IncidentOperativeGroupsResponse> search (IncidentOperativeGroupSearchRequest request);

}
