package com.seguritech.dispatch.operativegroup.incident.command.application.http;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

@RestController
@Api(tags = {"OPERATIVE GROUP EMERGENCIES"})
@RequestMapping("/dispatch/operative-groups/{operative-group-id}/emergencies")
public class OperativeGroupIncidentBaseController{
}

