package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class IncidentId {

    private final String value;

    public IncidentId(String value) {
        this.value = value;
    }

    public static IncidentId required(String incidentId) {
        return Optional.ofNullable(incidentId).map(IncidentId::new).orElseThrow(IncidentRequiredException::new);
    }

    public static IncidentId from(String incidentId) {
        return new IncidentId(incidentId);
    }

}
