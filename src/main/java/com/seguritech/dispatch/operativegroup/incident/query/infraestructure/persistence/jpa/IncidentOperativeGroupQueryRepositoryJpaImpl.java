package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.incident.entity.IncidentAssociationEntity;
import com.domain.model.incident.entity.IncidentAttendedEntity;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incident.entity.IncidentLocationEntity;
import com.domain.model.incident.entity.IncidentLocationStreetviewEntity;
import com.domain.model.incident.entity.LocationMunicipalityEntity;
import com.domain.model.incident.entity.LocationSettlementEntity;
import com.domain.model.incident.entity.LocationStateEntity;
import com.domain.model.incidentcancellation.entity.IncidentCancellationEntity;
import com.domain.model.incidentclose.entity.IncidentCloseEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;
import com.domain.model.national.entity.IncidentClassificationEntity;
import com.domain.model.national.entity.IncidentNationalClassificationEntity;
import com.domain.model.national.entity.IncidentNationalSubtypeEntity;
import com.domain.model.national.entity.IncidentNationalTypeEntity;
import com.domain.model.priority.entity.Priority;
import com.domain.model.source.entity.IncidentSource;
import com.domain.model.user.entity.UserEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentCorporationOperativeEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentToOperativeGroupsSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.AppUserResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.ClassificationResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.CorporationOperativeResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.CorporationResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentAdditionalClassificationResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentAssociatedResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentCancellationResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentCloseResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentLocationResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentLocationStreetviewResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentSourceResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentToOperativeGroupsPkResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentToOperativeGroupsResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.NationalSubTypeResponseMin;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.NationalTypeArrayResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.NationalTypeResponseMin;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.PriorityResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOperatingCorporationQueryRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.IncidentSecundaryMapper;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.IncidentTokenMapper;
import com.seguritech.platform.Service;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class IncidentOperativeGroupQueryRepositoryJpaImpl implements IncidentOperatingCorporationQueryRepository {

	private final IncidentCorporationOperativeEntityRepository repository;
	
	public IncidentOperativeGroupQueryRepositoryJpaImpl(IncidentCorporationOperativeEntityRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public PageDataProjectionDTO<IncidentToOperativeGroupsResponse> search(IncidentToOperativeGroupsSearchRequest request) {
		Page<IncidentCorporationEntity> result = this.repository.findAll(
				Specification.where(toSpecifications(request)),
                PageRequest.of(request.getPage(), request.getSize(), IncidentOperativeCorporationDetails.SortByCreatedAtDesc()));
        return new PageDataProjectionDTO<>(result.get().map(this::toDTO).collect(Collectors.toList()),result.getTotalElements());
	}
	
	private Specification<IncidentCorporationEntity> toSpecifications(IncidentToOperativeGroupsSearchRequest dto) {
			
		CorporationId.required(dto.getCorporationId());
		List<Specification<IncidentCorporationEntity>> specifications = new ArrayList<>();
		Optional.ofNullable(dto.getEmergencyId()).ifPresent(x -> specifications.add(IncidentOperativeCorporationDetails.incidentIdEq(x)
				.and(IncidentOperativeCorporationDetails.corporationIdEq(dto.getCorporationId()))));		
		
		Specification<IncidentCorporationEntity> temp = (root, query, criteriaBuilder) -> null;
	     for (Specification<IncidentCorporationEntity> specification : specifications) {
	    	 temp = temp.and(specification);
	     }
	     return temp;
	}
	
	private IncidentToOperativeGroupsResponse toDTO(IncidentCorporationEntity entity) {
	        return new IncidentToOperativeGroupsResponse(
	        	this.toResponse(entity.getId()),
	            entity.getStatus(),
	            entity.getObservationClose(),
	            entity.getObservationCancel(),
	            entity.getCreatedBy(),
	            entity.getUpdateBy(),
	            entity.getClosedBy(),
	            entity.getCancelBy(),
	            entity.getTransferedBy(),
	            entity.getMission(),	            
	            entity.getCreationDate(),
	            entity.getUpdateDate(),
	            entity.getClosedDate(), 
	            entity.getCancelDate(),
	            entity.getTransferedDate(),
	            entity.getAssignedUnitForceDate(),
	            Optional.ofNullable(entity.getReasonClose()).map(this::toResponse).orElse(null),	
	            Optional.ofNullable(entity.getReasonCancel()).map(this::toResponse).orElse(null),
	            this.toResponse(entity.getLocation())
	        );
	}
	
	private IncidentLocationResponse toResponse(IncidentLocationEntity entity) {
		return new IncidentLocationResponse(
				entity.getIdEmergencyLocation(), 
				entity.getHouseNumber(),
				entity.getStreet(),
				entity.getBwnStreet1(),
				entity.getBwnStreet2(),
			    entity.getLatitude(),
				entity.getLongitude(),
				entity.getReference(),
				entity.getZipCode(),
				Optional.ofNullable(entity.getState()).map(LocationStateEntity::getId).orElse(null),
				Optional.ofNullable(entity.getSettlement()).map(LocationSettlementEntity::getId).orElse(null),
				Optional.ofNullable(entity.getMunicipality()).map(LocationMunicipalityEntity::getId).orElse(null),
				entity.getCreatedAt(),
				entity.getCountryAlternative(),
				entity.getStateAlternative(),
				entity.getMunicipalityAlternative(),
				entity.getSettlementAlternative(),
				entity.getAddressAlternative(),
				entity.getStateAlternativeId(),
				entity.getMunicipalityAlternativeId(),
				entity.getSettlementAlternativeId()
		);
	}
	
	private IncidentLocationStreetviewResponse toResponse(IncidentLocationStreetviewEntity entity) {
		return new IncidentLocationStreetviewResponse(
				entity.getLatitude(),
				entity.getLongitude(), 
				entity.getHeading(), 
				entity.getPitch(), 
				entity.getPanoId()
				);
	}

	private IncidentCloseResponse toResponse(IncidentCloseEntity entity) {
		return new IncidentCloseResponse(
				entity.getId(),
	        	entity.getName()
		);
	}

	private IncidentCancellationResponse toResponse(IncidentCancellationEntity entity) {
		return new IncidentCancellationResponse(
				entity.getId(),
	        	entity.getName()
		);
	}

	private CorporationOperativeResponse toResponse(CorporationOperative entity) {
		return new CorporationOperativeResponse(
	        	entity.getId(),
	        	entity.getName(),
				entity.getClave(),
				entity.getActive(),
	        	this.toResponse(entity.getCorporation())
	    );
	}

	private CorporationResponse toResponse(Corporation entity) {
		return new CorporationResponse(
	        	entity.getId(),
	        	entity.getName(),
				entity.getDescription(),
				entity.getClave(),
				entity.getIcon(),
				entity.getActive()
	    );
	}
	
	private IncidentResponse toResponse(IncidentEntity entity) {
		return new IncidentResponse(
	        	entity.getId(),
	        	entity.getFolio(),
	        	entity.getStatus(),
	        	entity.getDescription(),
	        	entity.getSenseDetection(),
	        	entity.getIsManual(),
	        	entity.getEventDate(),
	        	entity.getCreatedAt(),
	        	entity.getUpdatedAt(),
	        	Optional.ofNullable(entity.getAttention()).map(IncidentAttendedEntity::getAttendDate).orElse(null),
	        	this.toResponse(entity.getSource()),
	        	toResponseMin(entity.getNationalSubType()),
	        	toResponse(entity.getPriority()),
	        	toResponse(entity.getLocation()),
	        	null,//phoneReporter
	        	Optional.ofNullable(entity.getStreetview()).map(this::toResponse).orElse(null),
	        	toResponse(entity.getCreatedByUser()),
	        	Optional.ofNullable(entity.getAttention()).map(IncidentAttendedEntity::getAttendBy).map(this::toResponse).orElse(null),
	        	this.toResponse(entity.getAssociates()),
	        	null,
	        	!entity.getTokens().isEmpty() ? IncidentTokenMapper.toResponse(entity.getTokens().stream().filter(z -> z.isActive()).findFirst().get()) : null,
		        entity.getSecundaryIncident() != null ? IncidentSecundaryMapper.toResponse(entity.getSecundaryIncident()) : null,
		        toResponse(entity.getAdditionalNationalSubTypes())			       
	    );
	}
	
	private IncidentSourceResponse toResponse(IncidentSource entity) {
		return new IncidentSourceResponse(
				entity.getId(),
				entity.getName(),
				entity.getClave(),
				entity.getDescription(),
				entity.getIcon(),
				entity.getShowAlertIdentifier(),
				entity.getPlaySound()
		);
	}
	
	private AppUserResponse toResponse(UserEntity entity) {
		return new AppUserResponse(
				entity.getIdUser(), 
				entity.getAlias());
	}
	
	private NationalSubTypeResponseMin toResponseMin(IncidentNationalSubtypeEntity entity) {
		return new NationalSubTypeResponseMin(
				entity.getNationalSubTypeId(),
				entity.getName(), 
				entity.getClave(),
				entity.isSubtype(),
				toResponseMin(entity.getNationalType())
		);
	}
	
	private NationalTypeResponseMin toResponseMin(IncidentNationalTypeEntity entity) {
		return new NationalTypeResponseMin(
				entity.getNationalTypeId(),
				entity.getName()
		);
	}
	
	private PriorityResponse toResponse(Priority entity) {
		return new PriorityResponse(
				entity.getId(), 
				entity.getName(), 
				entity.getDescription(), 
				entity.getTextColor(), 
				entity.getBackColor()
		);
	}
	
	private Set<IncidentAssociatedResponse> toResponse(Set<IncidentAssociationEntity> associates) {
		return associates.stream().map(this::toDTO).collect(Collectors.toSet());
	}

    private IncidentAssociatedResponse toDTO(IncidentAssociationEntity entity) {
    	return new IncidentAssociatedResponse(
    			entity.getId().getIdEmergencySecond().getId(),
    			entity.getId().getIdEmergencySecond().getFolio(),
    			toResponseMin(entity.getId().getIdEmergencySecond().getNationalSubType())
    	);
    }
    
	private IncidentToOperativeGroupsPkResponse toResponse(IncidentCorporationEntityPk entity) {
		return new IncidentToOperativeGroupsPkResponse(
				this.toResponse(entity.getIncident()),
				this.toResponse(entity.getCorporationOperative())
	    );
	}
	
	public static List<IncidentAdditionalClassificationResponse> toResponse(List<IncidentClassificationEntity> nationalSubTypes) {
		return Optional.ofNullable(nationalSubTypes).orElseGet(Collections::emptyList).stream()
				.map(x -> new IncidentAdditionalClassificationResponse(
						x.getNationalSubTypeId().getNationalSubTypeId(),
						x.getNationalSubTypeId().getName(),
						x.getNationalSubTypeId().getClave(),
						x.getNationalSubTypeId().getDescription(),
						toResponseTypeArray(x.getNationalSubTypeId().getNationalType()),
						null,
						x.getNationalSubTypeId().isSubtype()
				)).collect(Collectors.toList());
	}
	
	public static NationalTypeArrayResponse toResponseTypeArray(IncidentNationalTypeEntity emergencySubTypeEntity) {
        return Optional.ofNullable(emergencySubTypeEntity).map(x -> new NationalTypeArrayResponse(
                x.getNationalTypeId(),
                x.getName(),
                x.getClave(),
                x.getDescription(),
                toResponse(x.getNationalClassification())
        )).orElse(null);
	}
	
	public static ClassificationResponse toResponse (IncidentNationalClassificationEntity emergencyTypeEntity) {
		 return Optional.ofNullable(emergencyTypeEntity).map(x -> new ClassificationResponse(
				 x.getNationalClassificationId(),
				 x.getName(),
				 x.getClave(),
				 x.getDescription()
	     )).orElse(null);
	}

}
