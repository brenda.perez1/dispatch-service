package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class OperatingCorporationIncidentClosedEvent implements DomainEvent {

    private String emergencyId;
    private Long corporationId;
    private String status;
    private String closedByUser;
    private Date closedAt;
    private Long reasonCloseId;
    private String observations;
    private Long corporationOperativeId;

}