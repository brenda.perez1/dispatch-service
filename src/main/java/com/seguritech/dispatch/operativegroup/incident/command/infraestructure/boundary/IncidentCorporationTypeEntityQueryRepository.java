package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.domain.model.corporationnational.subtype.entity.CorporationNationalSubType;
import com.domain.model.corporationnational.subtype.entity.CorporationNationalSubTypePk;

public interface IncidentCorporationTypeEntityQueryRepository extends
    JpaRepository<CorporationNationalSubType, CorporationNationalSubTypePk> {

    @Query("SELECT COUNT(u) FROM CorporationNationalSubType u  WHERE u.id.nationalSubType.nationalSubTypeId =:typeId and u.active = :active")
    Long countByIncidentType(@Param("typeId") Long typeId, @Param("active") Boolean status);

}
