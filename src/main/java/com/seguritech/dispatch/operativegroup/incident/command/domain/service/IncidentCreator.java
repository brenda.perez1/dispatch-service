package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.IncidentCreatorResponse;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentCreatorRequest;
import com.seguritech.platform.Service;

@Service
public class IncidentCreator {

    private final IncidentBoundedRepository repository;

    public IncidentCreator(IncidentBoundedRepository repository) {
        this.repository = repository;
    }

    public IncidentCreatorResponse generate(IncidentCreatorRequest request) {
        return this.repository.create(request);
    }
}
