package com.seguritech.dispatch.operativegroup.incident.query.domain;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentSearchResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;

public interface OpGpIncidentQueryRepository {

    PageDataProjectionDTO<OpGpIncidentSearchResponse> search (OperativeGpIncidentSearchRequest request);

}
