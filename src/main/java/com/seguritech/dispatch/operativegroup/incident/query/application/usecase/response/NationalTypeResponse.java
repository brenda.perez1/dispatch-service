package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class NationalTypeResponse {
	
	private final Long nationalTypeId;
	private final String name;
	private final String description;
	private final String clave;
	
}
