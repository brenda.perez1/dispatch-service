package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;


import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class AssignIncidentCorporationsRequest {

    private final String incidentId;
    private final List<String> corporationsCode;
    private final String assignedBy;

}

