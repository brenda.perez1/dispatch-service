package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationCode;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;

public interface CorporationRepository {

    Boolean exist(CorporationOperativeId id);

    CorporationOperativeId find(IncidentCorporationCode code);
    
    CorporationOperative findById(CorporationOperativeId id);

}
