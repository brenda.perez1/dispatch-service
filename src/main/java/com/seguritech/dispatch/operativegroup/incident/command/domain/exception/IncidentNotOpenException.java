package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentNotOpenException  extends BusinessException {

  public IncidentNotOpenException() {
    super(MessageResolver.INCIDENT_NOT_OPEN_EXCEPTION, 3024L);
  }
}
