package com.seguritech.dispatch.operativegroup.incident.command.application.messaging;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.ClosingOperatingCorporationRecommendationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosingRecommendationIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.MissionCanceledEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.MissionEndedEvent;
import com.seguritech.platform.application.messagging.DomainEventListener;
import com.seguritech.platform.application.messagging.DomainSourceEventListener;
import com.seguritech.platform.infrastructure.messaging.DomainEventEnvelope;
import org.springframework.stereotype.Component;

@Component
@DomainSourceEventListener(domain = "incidents-service")
public class OperatingCorporationClosureRecommendationListener {

    private final ClosingOperatingCorporationRecommendationUseCase useCase;

    public OperatingCorporationClosureRecommendationListener(ClosingOperatingCorporationRecommendationUseCase useCase) {
        this.useCase = useCase;
    }

    @DomainEventListener(name = "mission-canceled", version = "v1")
    public void onMissionCanceled(DomainEventEnvelope<MissionCanceledEvent> evt) {
        MissionCanceledEvent event = evt.getPayload();
        this.useCase.execute(new ClosingRecommendationIncidentCorporationRequest(
                event.getIncidentId(),
                event.getCorporationId(),
                event.getCreatedByUser()
        ));
    }

    @DomainEventListener(name = "mission-ended", version = "v1")
    public void onMissionEnded(DomainEventEnvelope<MissionEndedEvent> evt) {
        MissionEndedEvent event = evt.getPayload();
        this.useCase.execute(new ClosingRecommendationIncidentCorporationRequest(
                event.getIncidentId(),
                event.getCorporationId(),
                event.getCreatedByUser()
        ));
    }

}
