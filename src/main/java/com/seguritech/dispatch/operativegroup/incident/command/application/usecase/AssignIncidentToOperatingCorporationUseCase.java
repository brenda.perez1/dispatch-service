package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.seguritech.platform.domain.DomainEvent;
import org.springframework.stereotype.Component;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.geofence.entity.Geofence;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntity;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntityPk;
import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.geofence.dto.GeofenceModel;
import com.seguritech.dispatch.geofence.repository.GeofenceRepository;
import com.seguritech.dispatch.geofence.service.GeofenceManager;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.UpdateIncidentAssignCorporacionRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotDissociatedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentGeofenceNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.GeofenceId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationNoteModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.OperatingCorporationsRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentTypeChecker;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentsAssignationConfigurationChecker;
import com.seguritech.platform.domain.DomainEventPublisher;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AssignIncidentToOperatingCorporationUseCase {

    private final IncidentOperatingCorporationRepository repository;
    private final IncidentTypeChecker incidentChecker;
    private final DomainEventPublisher publisher;
    private final OperatingCorporationsRepository operatingCorporationsRepository;
    private final CorporationOperativeRepository corporationOperativeRepository;
    private final GeofenceManager geofenceManager;
    private final EmergenciesClient client;

    public AssignIncidentToOperatingCorporationUseCase(IncidentsAssignationConfigurationChecker incidentsAssignationConfigurationChecker,
                                                       ProfileCorporationConfigRepository profileCorporationConfigRepository, 
                                                       IncidentOperatingCorporationRepository repository,
                                                       IncidentTypeChecker incidentChecker, DomainEventPublisher publisher,
                                                       OperatingCorporationsRepository operatingCorporationsRepository,
                                                       IncidentRepository incidentRepository,
                                                       GeofenceManager geofenceManager,
                                                       CorporationOperativeRepository corporationOperativeRepository,
                                                       EmergenciesClient client
    		
    ) {
        this.repository = repository;
        this.incidentChecker = incidentChecker;
        this.operatingCorporationsRepository = operatingCorporationsRepository;
        this.corporationOperativeRepository = corporationOperativeRepository;
        this.geofenceManager = geofenceManager;
        this.publisher = publisher; 
        this.client = client;
    }

//    @Transactional
    public List<IncidentCorporationModel> execute(AssignIncidentCorporationRequest request, String token) {
    	
	    CorporationId corporationId = CorporationId.required(request.getCorporationId()); 
	    
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        
        IncidentBounded incident = this.incidentChecker.check(incidentId);
        
        IncidentLocationBounded incidentLocation = this.incidentChecker.getLocation(incidentId);
    
        //busca geocercas por grupo operativo
        List<OperativeGroupGeofencesEntity> operativeGroupGeofencesEntity = operatingCorporationsRepository.find(corporationId.getValue());
        
        List<IncidentCorporationModel> result = new ArrayList<>();
        
        for (OperativeGroupGeofencesEntity entity : operativeGroupGeofencesEntity) {
        	
        	long idGeofence = entity.getId().getGeofence().getId();
        	
            GeofenceModel geofence = this.geofenceManager.getById(idGeofence);
            
            if (geofence != null && geofence.getGeometry().containsPoint(incidentLocation.getCoordinates().getLatitude(), incidentLocation.getCoordinates().getLongitude())) {        	
            	Corporation corporation = entity.getId().getCorporationOperative().getCorporation();            	
            	CorporationOperative corporationOperative = entity.getId().getCorporationOperative();            	
            	Geofence geofence1 = entity.getId().getGeofence();           	
            	CorporationOperativeId corporationOperativeId = new CorporationOperativeId(corporationOperative.getId());
                IncidentCorporationModel model = IncidentCorporationModel.dispatch(
                		incidentId, 
                		corporationOperativeId, 
                		CorporationId.required(corporation.getId()),
                		new UserId(request.getAssignedBy()),
                		GeofenceId.required(geofence1.getId()),
                		IncidentLocationId.required(incidentLocation.getCoordinates().getLocationId()),
                		incident.getFolio(),
                		incident.getSourceName(),
                		incident.getSourceId(),
                		incident.getSubTypeName(),
                		request.getSecondaryInvoice(),
                		incident.getIsManual(),
                		incident.getPlaySound()
                );
                
                IncidentCorporationModel aux = this.repository.findById(model.getCorporationOperativeId(), incidentId);
            	
            	if(aux != null && !aux.getStatus().getValue().equals(IncidentCorporationStatusEnum.CLOSED) && !aux.getStatus().getValue().equals(IncidentCorporationStatusEnum.CANCELED)) {
            		throw new IncidentCorporationNotDissociatedException();
            	}
            	
            	if(aux == null || !aux.getStatus().getValue().equals(IncidentCorporationStatusEnum.OPEN)) {
            		IncidentCorporationModel corporationModel = this.repository.save(model);
            		log.info(corporationModel.toString());
            		this.client.updateEmergency(token, new UpdateIncidentAssignCorporacionRequest(model.getIncidentId().getValue(), model.getCreatedByUser().getValue(), model.getCreatedAt(), model.getStatus().getValue().getValue(), Optional.ofNullable(request.getSecondaryInvoice()).orElse(Boolean.FALSE)));
            		this.publisher.publish(model.pullEvents());
            	}
                result.add(model);
            }
           
        }     
        
        if (!result.isEmpty()) {
        	 IncidentCorporationNoteModel modelNote =  IncidentCorporationNoteModel.createNotaAssignCorporation(incidentId, corporationId, new UserId(request.getAssignedBy()));
        	 this.publisher.publish(modelNote.pullEvents());
        }

		///Si no enceuntra geocerca busca gpos. operativos por default 
		if (result.isEmpty()) {	
			CorporationOperative corporationOperative = this.corporationOperativeRepository.findCorporationOperativeDefault(corporationId);
			
			if(corporationOperative == null) throw new IncidentGeofenceNotFoundException();
				
        	Corporation corporation = corporationOperative.getCorporation();
        	OperativeGroupGeofencesEntity geofence = this.operatingCorporationsRepository.findDefaultGeofence(corporationOperative.getId());
        	CorporationOperativeId corporationOperativeId = new CorporationOperativeId(corporationOperative.getId());
            IncidentCorporationModel model = IncidentCorporationModel.dispatch(
            		incidentId, 
            		corporationOperativeId, 
            		CorporationId.required(corporation.getId()),
            		new UserId(request.getAssignedBy()),
            		GeofenceId.optional(Optional.ofNullable(geofence).map(OperativeGroupGeofencesEntity::getId).map(OperativeGroupGeofencesEntityPk::getGeofence).orElse(null)),	                		
            		IncidentLocationId.required(incidentLocation.getCoordinates().getLocationId()),
            		incident.getFolio(),
            		incident.getSourceName(),
            		incident.getSourceId(),
            		incident.getSubTypeName(),
            		request.getSecondaryInvoice(),
            		incident.getIsManual(),
            		incident.getPlaySound()
            );
            result.add(model);	 
            
            IncidentCorporationModel aux = this.repository.findById(model.getCorporationOperativeId(), incidentId);
        	
        	if(aux != null && !aux.getStatus().getValue().equals(IncidentCorporationStatusEnum.CLOSED) && !aux.getStatus().getValue().equals(IncidentCorporationStatusEnum.CANCELED)) {
        		throw new IncidentCorporationNotDissociatedException();
        	}
        	
        	if(aux == null || !aux.getStatus().getValue().equals(IncidentCorporationStatusEnum.OPEN)) {
        		IncidentCorporationModel corporationModel = this.repository.save(model);
        		log.info(corporationModel.toString());
        		this.client.updateEmergency(token, new UpdateIncidentAssignCorporacionRequest(model.getIncidentId().getValue(), model.getCreatedByUser().getValue(), model.getCreatedAt(), model.getStatus().getValue().getValue(), Optional.ofNullable(request.getSecondaryInvoice()).orElse(Boolean.FALSE)));
        		model.createNotaAssignCorporation(incidentId, corporationId, new UserId(request.getAssignedBy()));
        		this.publisher.publish(model.pullEvents());
        	}
	    }
//		result.stream().forEach((m) -> this.publisher.publish(m.pullEvents()));
//		this.publisher.publish(model.pullEvents());
		if (!result.isEmpty()) {       	 
       	 IncidentCorporationModel modelNotification = IncidentCorporationModel.dispatchCorp(
            		incidentId, 
            		result.stream().map(IncidentCorporationModel::getCorporationOperativeId).collect(Collectors.toList()), 
            		CorporationId.required(corporationId.getValue()),
            		new UserId(request.getAssignedBy()),
            		null,
            		IncidentLocationId.required(incidentLocation.getCoordinates().getLocationId()),
            		incident.getFolio(),
            		incident.getSourceName(),
            		incident.getSourceId(),
            		incident.getSubTypeName(),
            		request.getSecondaryInvoice(),
            		incident.getIsManual(),
            		incident.getPlaySound()
            );
       	 this.publisher.publish(modelNotification.pullEvents());
       }
       return result;
            
    }
    
}



class Publisher extends Thread {

	private final DomainEventPublisher publisher;
	private final List<DomainEvent> events;

	Publisher(final DomainEventPublisher publisher, final List<DomainEvent> events) {
		this.publisher = publisher;
		this.events = events;
	}

	@Override
	public void run() {
		this.publisher.publish(events);
	}

}