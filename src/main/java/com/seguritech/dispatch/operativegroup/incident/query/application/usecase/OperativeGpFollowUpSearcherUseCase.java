package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpFollowUpSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpFollowUpQueryRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.platform.Service;

@Service
public class OperativeGpFollowUpSearcherUseCase {

	private final OpGpFollowUpQueryRepository repository;

	public OperativeGpFollowUpSearcherUseCase(OpGpFollowUpQueryRepository repository) {
		this.repository = repository;
	}

	public PageDataProjectionDTO<OpGpFollowUpSearchResponse> execute(
			OperativeGpFollowUpSearchRequest request) {
		return this.repository.search(request);
	}

}
