package com.seguritech.dispatch.operativegroup.incident.query.application.api;

import java.util.Date;
import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignSupportGroupsIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.FilterDTO;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ProfileIdRequierdException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.CorporationIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.CorporationIncidentSearcherUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperatingCorporationSearchUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentToOperativeGroupsSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpFollowUpSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpFollowUpSearcherUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.SupportAllGroupsIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.SupportAllGroupsIncidentCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.SupportGroupsIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.SupportGroupsIncidentCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.domain.AssignedStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOrderFieldEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentQueryStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentPeriodFilterEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryStatusEnum;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.seguritech.platform.application.http.Order;

@RestController
@Api(tags = {"CORPORATION EMERGENCIES"})
@RequestMapping("/dispatch")
public class CorporationIncidentQueryController {

    @Autowired
	private ResourceBundleMessageSource source;
	@Value("${placeholder.test}")
	private String greetings;
	
    private final IncidentOperatingCorporationFinder incidentOperatingCorporationFinder;
    private final IncidentOperatingCorporationSearchUseCase searchUseCase;
    private final CorporationIncidentSearcherUseCase corporationIncidentSearchUseCase;
    private final SupportGroupsIncidentCorporationUseCase supportGroupsIncidentCorporationUseCase;
    private final SupportAllGroupsIncidentCorporationUseCase supportAllGroupsIncidentCorporationUseCase;
    private final OperativeGpFollowUpSearcherUseCase operativeGpFollowUpIncidentSearcherUseCase;

    public CorporationIncidentQueryController(IncidentOperatingCorporationFinder incidentOperatingCorporationFinder, 
    										  IncidentOperatingCorporationSearchUseCase searchUseCase,
    										  CorporationIncidentSearcherUseCase corporationIncidentSearchUseCase,
    										  SupportGroupsIncidentCorporationUseCase supportGroupsIncidentCorporationUseCase,
    										  SupportAllGroupsIncidentCorporationUseCase supportAllGroupsIncidentCorporationUseCase,
    										  OperativeGpFollowUpSearcherUseCase operativeGpFollowUpIncidentSearcherUseCase
    ) {       
        this.incidentOperatingCorporationFinder = incidentOperatingCorporationFinder;
        this.searchUseCase = searchUseCase;
        this.corporationIncidentSearchUseCase = corporationIncidentSearchUseCase;
        this.supportGroupsIncidentCorporationUseCase = supportGroupsIncidentCorporationUseCase;
        this.supportAllGroupsIncidentCorporationUseCase = supportAllGroupsIncidentCorporationUseCase;
        this.operativeGpFollowUpIncidentSearcherUseCase = operativeGpFollowUpIncidentSearcherUseCase;
    }

    @ApiOperation(value = "List dispatch", nickname = "listDispatch",
            notes = "List dispatch by profile")
    @PostMapping("/emergencies")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<CadLiteAPIResponse<List<IncidentCorporationEntity>>> 
    list(UsernamePasswordAuthenticationToken user,		
    	@RequestBody FilterDTO request) {
    	
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
    	String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
        return CadLiteAPIResponse.build(
        		incidentOperatingCorporationFinder.findAll(ProfileId.required(Long.parseLong(activeProfile)), request)
        );
    }

    @ApiOperation(value = "Find dispatch by id")
    @GetMapping("/{emergency-id}")
    public ResponseEntity<CadLiteAPIResponse< PageDataProjectionDTO<IncidentSubSetResponse>>> getById(
    		@PathVariable("emergency-id") String emergencyId,
    		
    		@ApiParam(value = "Result page  delimiter by size , it starts at 0 ") 
    		@RequestParam(value = "page", defaultValue = "0")
            Integer page,
            
            @ApiParam(value = "Result page  delimiter size of results") 
    		@RequestParam(value = "size", defaultValue = "25")
            Integer size
    ){
    		return CadLiteAPIResponse.build(this.corporationIncidentSearchUseCase.execute(new IncidentSearchRequest(size, page, emergencyId)));
    }
    
    @ApiOperation(value = "Find all emergency-corporation by filters", notes = "Pageable result and optional filters")
    @GetMapping("/emergency-corporation")
    public ResponseEntity<CadLiteAPIResponse< PageDataProjectionExtendedDTO<IncidentResponseMin>>> getEmergencyCorporation(    	    

    		UsernamePasswordAuthenticationToken user,
    		
			@ApiParam(value = "Emergency-corporation priority for searching")
            @RequestParam(value = "priorityIds", required = false, defaultValue = "")List<Long> priorityIds,
            
            @ApiParam(value = "Emergency-corporation corporation for searching")
            @RequestParam(value = "corporationIds", required = false)List<Long> corporationIds,
            
            @ApiParam(value = "Emergency Corporation status ,it searches emergencies corporation by status")
            @RequestParam(value = "status", required = false) OpGpIncidentQueryStatusEnum status,    
            
            @ApiParam(value = "Emergency-corporation follow-up status corporation")
            @RequestParam(value = "followUpStatusIds", required = false) List<Long> followUpStatusIds,

            @ApiParam(value = "Filter emergencies-corporation by text in content ")
            @RequestParam(value = "query", required = false) String query,

            //Date Range Filter
            @RequestParam(value = "periodDateField", required = false) 
            @ApiParam(value = "Specify field to date range") OpGpIncidentPeriodFilterEnum periodDateField, 
            
            @RequestParam(value = "periodStartDate", required = false)
            @ApiParam(value = "Initial Date for filter", example = "yyyy-MM-dd HH:mm:ss")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodStartDate,
            
            @RequestParam(value = "periodEndDate", required = false)
            @ApiParam(value = "End Date for filter", example = "yyyy-MM-dd HH:mm:ss") 
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodEndDate,
            
            //Order
            @RequestParam(value = "order", required = false) 
            @ApiParam(value = "Ordering direction") Order order,
            
            @RequestParam(value = "orderBy", required = false) 
            @ApiParam(value = "Field for ordering") IncidentOrderFieldEnum orderByField,
            
            //Pagination
            @RequestParam(value = "page", defaultValue = "0") 
            @ApiParam(value = "Result page  delimiter by size , it starts at 0 ") Integer page,
            
            @RequestParam(value = "size", defaultValue = "10") 
            @ApiParam(value = "Result page  delimiter size of results") Integer size,
            
            @ApiParam(value = "Result delimiter N hours ago, creation Date") 
            @RequestParam(value = "maxHours", required = false) Integer maxHours,
            
            @ApiParam(value = "Emergency status, it searches emergencies by status")
            @RequestParam(value = "statusEmergency", required = false) IncidentQueryStatusEnum statusEmergency,
            
            @ApiParam(value = "Filter by created by ")
            @RequestParam(value = "createdBy", required = false) String createdBy,
            
            @ApiParam(value = "Filter by assigned ")
            @RequestParam(value = "assignedStatus", required = false) AssignedStatusEnum assignedStatus

        ) {
    	
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		
    	CorporationIncidentSearchRequest incidentSearchRequest = new CorporationIncidentSearchRequest(                
                size,
                page,
                query,
                periodDateField,
                periodStartDate,
                periodEndDate,
                order,
                orderByField,
                maxHours,
                status,
                priorityIds,     
                statusEmergency,
                principal.getSubCenter(),
                followUpStatusIds,
                corporationIds,
                createdBy,
                assignedStatus,
                null,
                null,
                null,
                null
         );
    	incidentSearchRequest.setProfileId(Long.parseLong(activeProfile));
    	
    	 return CadLiteAPIResponse.build(this.corporationIncidentSearchUseCase.execute(incidentSearchRequest));
    }
    
    @ApiOperation(value = "Find all emergency-corporation by filters", notes = "Pageable result and optional filters")
    @GetMapping("/emergency-corporation-advance")
    public ResponseEntity<CadLiteAPIResponse< PageDataProjectionExtendedDTO<IncidentResponseMin>>> getEmergencyCorporationAdvance(    	    

    		UsernamePasswordAuthenticationToken user,
    		
			@ApiParam(value = "Emergency-corporation priority for searching")
            @RequestParam(value = "priorityIds", required = false, defaultValue = "")List<Long> priorityIds,
            
            @ApiParam(value = "Emergency-corporation corporation for searching")
            @RequestParam(value = "corporationIds", required = false)List<Long> corporationIds,
            
            @ApiParam(value = "Emergency-corporation group operative for searching")
            @RequestParam(value = "corporationOperativeIds", required = false)List<Long> corporationOperativeIds,
            
            @ApiParam(value = "National SubType for searching")
            @RequestParam(value = "nationalSubTypeId", required = false) Long nationalSubTypeId,
            
            @ApiParam(value = "Emergency Corporation status ,it searches emergencies corporation by status")
            @RequestParam(value = "status", required = false) OpGpIncidentQueryStatusEnum status,   
            
            @ApiParam(value = "Emergency-corporation follow-up status corporation")
            @RequestParam(value = "followUpStatusIds", required = false) List<Long> followUpStatusIds,

            @ApiParam(value = "Filter emergencies-corporation by text in content ")
            @RequestParam(value = "query", required = false) String query,

            //Date Range Filter
            @RequestParam(value = "periodDateField", required = false) 
            @ApiParam(value = "Specify field to date range") OpGpIncidentPeriodFilterEnum periodDateField, 
            
            @RequestParam(value = "periodStartDate", required = false)
            @ApiParam(value = "Initial Date for filter", example = "yyyy-MM-dd HH:mm:ss")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodStartDate,
            
            @RequestParam(value = "periodEndDate", required = false)
            @ApiParam(value = "End Date for filter", example = "yyyy-MM-dd HH:mm:ss") 
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodEndDate,
            
            //Order
            @RequestParam(value = "order", required = false) 
            @ApiParam(value = "Ordering direction") Order order,
            
            @RequestParam(value = "orderBy", required = false) 
            @ApiParam(value = "Field for ordering") IncidentOrderFieldEnum orderByField,
            
            //Pagination
            @RequestParam(value = "page", defaultValue = "0") 
            @ApiParam(value = "Result page  delimiter by size , it starts at 0 ") Integer page,
            
            @RequestParam(value = "size", defaultValue = "10") 
            @ApiParam(value = "Result page  delimiter size of results") Integer size,
            
            @ApiParam(value = "Result delimiter N hours ago, creation Date") 
            @RequestParam(value = "maxHours", required = false) Integer maxHours,
            
            @ApiParam(value = "Emergency status, it searches emergencies by status")
            @RequestParam(value = "statusEmergency", required = false) IncidentQueryStatusEnum statusEmergency,
            
            @ApiParam(value = "Filter by created by ")
            @RequestParam(value = "createdBy", required = false) String createdBy,
            
            @ApiParam(value = "Filter by assigned ")
            @RequestParam(value = "assignedStatus", required = false) AssignedStatusEnum assignedStatus,
            
            @ApiParam(value = "Filter not associated as parent")
            @RequestParam(value = "isNotAssociatedAsParent", required = false, defaultValue = "false") Boolean isNotAssociatedAsParent,
            
            @ApiParam(value = "Filter not associated as child")
            @RequestParam(value = "isNotAssociatedAsChild", required = false, defaultValue = "false") Boolean isNotAssociatedAsChild
        ) {
    	
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		
    	CorporationIncidentSearchRequest incidentSearchRequest = new CorporationIncidentSearchRequest(                
                size,
                page,
                query,
                periodDateField,
                periodStartDate,
                periodEndDate,
                order,
                orderByField,
                maxHours,
                status,
                priorityIds,     
                statusEmergency,
                principal.getSubCenter(),
                followUpStatusIds,
                corporationIds,
                createdBy,
                assignedStatus,
                corporationOperativeIds,
                isNotAssociatedAsParent,
                isNotAssociatedAsChild,
                nationalSubTypeId
         );
    	incidentSearchRequest.setProfileId(Long.parseLong(activeProfile));
    	
    	 return CadLiteAPIResponse.build(this.corporationIncidentSearchUseCase.executeAdvance(incidentSearchRequest));
    }
    
    @GetMapping("/emergencies/{emergency-id}/corporations/{corporation-id}")
    @ApiOperation(value = "Find dispatch by emgergencyId To corporationId")
    public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<IncidentToOperativeGroupsResponse>>> getByEmergencyToCorporations(
    		UsernamePasswordAuthenticationToken user,
    		@ApiParam(value = "Emergency by Id for searching")
    		@PathVariable(value = "emergency-id") String emergencyId,
    		@ApiParam(value = "Corporation by Id for searching")
    		@PathVariable(value = "corporation-id") Long corporationId,
    		//Pagination
    		@ApiParam(value = "Result page  delimiter by size , it starts at 0 ")
    		@RequestParam(value = "page", defaultValue = "0") Integer page,
			@ApiParam(value = "Result page  delimiter size of results") 
			@RequestParam(value = "size", defaultValue = "10") Integer size) {

    	return CadLiteAPIResponse.build(this.searchUseCase.execute(new IncidentToOperativeGroupsSearchRequest(
    			size, 
    			page, 
    			emergencyId, 
    			corporationId
    	)));
    }
  
	@GetMapping("/get-message")
	public String getLocaleMessage(
			@RequestParam(name = "username", defaultValue = "", required = false) final String username) {
		return source.getMessage(greetings, new Object[] { username }, LocaleContextHolder.getLocale());
	}
	
	@ApiOperation(value = "Save support groups")
	@PostMapping("/emergencies/support-groups/{emergency-id}")
	public ResponseEntity<CadLiteAPIResponse<Object>> saveSupportGroups(@RequestBody AssignSupportGroupsIncidentCorporationRequest request,
			@ApiParam(value = "Emergency id to add support groups") @PathVariable(value = "emergency-id") String emergencyId, UsernamePasswordAuthenticationToken user) {
		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		this.supportGroupsIncidentCorporationUseCase.execute(new SupportGroupsIncidentCorporationRequest(emergencyId, principal.getId(), request.getOperativeGroups()));
		return CadLiteAPIResponse.build();    
	}
	
	@ApiOperation(value = "Save all support groups")
	@PostMapping("/emergencies/support-all-groups/{emergency-id}")
	public ResponseEntity<CadLiteAPIResponse<Object>> saveAllSupportGroups(@RequestBody AssignSupportGroupsIncidentCorporationRequest request,
			@ApiParam(value = "Emergency id to add support groups") @PathVariable(value = "emergency-id") String emergencyId, UsernamePasswordAuthenticationToken user) {
		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		this.supportAllGroupsIncidentCorporationUseCase.execute(new SupportAllGroupsIncidentCorporationRequest(emergencyId, principal.getId(), request.getOperativeGroups()));
		return CadLiteAPIResponse.build();    
	}
	
	@GetMapping("/emergencies/{emergency-id}/followup")
    @ApiOperation(value = "Find followup of selected corporation")
    public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<OpGpFollowUpSearchResponse>>> findFollowUpCorporation (
    		@PathVariable(value = "emergency-id") String incidentId,
    		@RequestParam(value = "size", defaultValue = "5")
            @ApiParam(value = "Result page delimiter size of results") Integer size,
    		@RequestParam(value = "page", defaultValue = "0")
    		@ApiParam(value = "Result page delimiter by size, it starts at 0") Integer page,
    		@RequestParam(value = "query", required = false)
    		@ApiParam(value = "Filter by text in content ") String query,
            @RequestParam(value = "periodDateField", required = false)
    		@ApiParam(value = "Specify field to date range") String periodDateField,
    		@RequestParam(value = "periodStartDate", required = false)
            @ApiParam(value = "Initial Date for filter", example = "yyyy-MM-dd HH:mm:ss")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodStartDate,
            @RequestParam(value = "periodEndDate", required = false)
            @ApiParam(value = "End Date for filter", example = "yyyy-MM-dd HH:mm:ss")
    		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodEndDate,
    		@RequestParam(value = "order", required = false)
    		@ApiParam(value = "Ordering direction") Order order,
            @RequestParam(value = "orderBy", required = false)
    		@ApiParam(value = "Field for ordering") String orderByField) {
    	return CadLiteAPIResponse.build(
    			this.operativeGpFollowUpIncidentSearcherUseCase.execute(
    					new OperativeGpFollowUpSearchRequest(
    							incidentId, size, page, query,
    							periodDateField, periodStartDate, periodEndDate, order, orderByField
    					))
    			);
    }

}
