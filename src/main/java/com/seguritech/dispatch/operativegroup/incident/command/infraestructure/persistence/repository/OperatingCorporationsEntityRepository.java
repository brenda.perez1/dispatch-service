package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntity;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntityPk;

@Repository
public interface OperatingCorporationsEntityRepository extends JpaRepository<OperativeGroupGeofencesEntity, OperativeGroupGeofencesEntityPk>,
        JpaSpecificationExecutor<OperativeGroupGeofencesEntity> {

	@Query("SELECT u FROM OperativeGroupGeofencesEntity u"
	+ " INNER JOIN u.id.geofence"
	+ " INNER JOIN u.id.corporationOperative co"
	+ " INNER JOIN co.corporation"
	+ " WHERE u.id.corporationOperative.active = :corporationOperativeActive"
	+ " AND u.id.corporationOperative.corporation.id = :corporation"
	+ " AND u.id.corporationOperative.isDefault = false"
	+ " AND u.id.corporationOperative.active = true"	
	+ " AND u.id.geofence.active = true"
	)
	Collection<OperativeGroupGeofencesEntity> findOperativeGroupGeofences (@Param("corporationOperativeActive") Boolean corporationOperativeActive, @Param("corporation") Long corporation);
	
	@Query("SELECT u FROM OperativeGroupGeofencesEntity u"
			+ " INNER JOIN u.id.geofence"
			+ " INNER JOIN u.id.corporationOperative co"
			+ " INNER JOIN co.corporation"
			+ " WHERE u.id.corporationOperative.active = :corporationOperativeActive"
			+ " AND u.id.corporationOperative.corporation.id = :corporation"
			+ " AND u.id.geofence.active = true"
			+ " AND u.id.geofence.byDefault = true"
			)
	Collection<OperativeGroupGeofencesEntity> findOperativeGroupGeofenceByDefault(@Param("corporationOperativeActive") Boolean corporationOperativeActive, @Param("corporation") Long corporation);
	
	@Query("SELECT u FROM OperativeGroupGeofencesEntity u"
			+ " INNER JOIN u.id.geofence"
			+ " INNER JOIN u.id.corporationOperative co"
			+ " INNER JOIN co.corporation"
			+ " WHERE u.id.corporationOperative.active = :corporationOperativeActive"
			+ " AND u.id.corporationOperative.id = :corporationOperative"
			+ " AND u.id.corporationOperative.isDefault = true"
			+ " AND u.id.corporationOperative.active = true"	
			+ " AND u.id.geofence.active = true"
			)
			OperativeGroupGeofencesEntity findDefaultOperativeGroupGeofences (@Param("corporationOperativeActive") Boolean corporationOperativeActive, @Param("corporationOperative") Long corporationOperative);
	
}
