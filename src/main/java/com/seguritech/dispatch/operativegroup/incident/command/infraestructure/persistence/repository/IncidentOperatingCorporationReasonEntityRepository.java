package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.incidentcancellation.entity.IncidentCancellationEntity;
import com.seguritech.platform.Service;

@Service
public interface IncidentOperatingCorporationReasonEntityRepository extends JpaRepository<IncidentCancellationEntity, Long> {

}
