package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AcceptSupportRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ReactivateCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentReactivationEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class AcceptSupportRequestUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentReactivationEnsurer incidentRepository;
    private final IncidentOperatingCorporationRepository incidentCorporationRepository;
    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final DomainEventPublisher publisher;


    public AcceptSupportRequestUseCase(IncidentOperatingCorporationFinder finder, IncidentReactivationEnsurer incidentRepository,
                                       IncidentOperatingCorporationRepository incidentCorporationRepository,
                                       ProfileCorporationConfigRepository profileCorporationConfigRepository, DomainEventPublisher publisher) {
        this.finder = finder;
        this.incidentRepository = incidentRepository;
        this.incidentCorporationRepository = incidentCorporationRepository;
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.publisher = publisher;
    }

    public void execute(AcceptSupportRequest request) {
        UserId userId = new UserId(request.getIncidentId());
        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getOperativeCorporationId());
        ProfileId profileId = ProfileId.required(request.getProfileId());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());

        this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);

        IncidentCorporationModel corporationModel = this.finder.findById(corporationId, incidentId);
        corporationModel.acceptSupport(userId);

        this.incidentCorporationRepository.save(corporationModel);
        this.publisher.publish(corporationModel.pullEvents());
    }

}
