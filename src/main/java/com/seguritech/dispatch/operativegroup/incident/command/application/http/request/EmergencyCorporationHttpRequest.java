package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationHttpRequest {

    private Long corporationId;
    private Boolean secondaryInvoice;

}
