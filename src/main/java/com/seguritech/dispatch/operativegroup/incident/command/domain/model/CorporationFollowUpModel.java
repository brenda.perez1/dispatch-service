package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.Date;
import java.util.Optional;

import com.seguritech.dispatch.operativegroup.incident.command.domain.event.CorporationFollowUpCreatedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.CorporationFollowUpObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.CorporationFollowUpResearchFolder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;

import com.seguritech.platform.domain.RootAggregate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorporationFollowUpModel extends RootAggregate {

	private final CorporationFollowUpId id;
	private final IncidentId incidentId;
	private final CorporationOperativeId corporationOperativeId;
	private final IncidentCorporationFollowUpReasonId incidentCorporationFollowUpReasonId;
	private final IncidentCorporationFollowUpStatusId incidentCorporationFollowUpStatusId;
	private final CorporationFollowUpObservation corporationFollowUpObservation;
	private final CorporationFollowUpResearchFolder corporationFollowUpResearchFolder;
	private final UserId createdByUser;
	private final Date createdAt;
	private final UserId updatedByUser;
	private final Date updatedAt;

	public CorporationFollowUpModel(CorporationFollowUpId id, IncidentId incidentId,
			CorporationOperativeId corporationOperativeId,
			IncidentCorporationFollowUpReasonId incidentCorporationFollowUpReasonId,
			IncidentCorporationFollowUpStatusId incidentCorporationFollowUpStatusId,
			CorporationFollowUpObservation corporationFollowUpObservation,
			CorporationFollowUpResearchFolder corporationFollowUpResearchFolder, UserId createdByUser, Date createdAt,
			UserId updatedByUser, Date updatedAt) {
		this.id = id;
		this.incidentId = incidentId;
		this.corporationOperativeId = corporationOperativeId;
		this.incidentCorporationFollowUpReasonId = incidentCorporationFollowUpReasonId;
		this.incidentCorporationFollowUpStatusId = incidentCorporationFollowUpStatusId;
		this.corporationFollowUpObservation = corporationFollowUpObservation;
		this.corporationFollowUpResearchFolder = corporationFollowUpResearchFolder;
		this.createdByUser = createdByUser;
		this.createdAt = createdAt;
		this.updatedByUser = updatedByUser;
		this.updatedAt = updatedAt;
	}

	public static CorporationFollowUpModel save(IncidentId incidentId, CorporationOperativeId corporationOperativeId,
			IncidentCorporationFollowUpReasonId incidentCorporationFollowUpReasonId,
			IncidentCorporationFollowUpStatusId incidentCorporationFollowUpStatusId,
			IncidentCorporationFollowUpStatusId oldincidentCorporationFollowUpStatusId,
			CorporationFollowUpObservation corporationFollowUpObservation,
			CorporationFollowUpResearchFolder corporationFollowUpResearchFolder, UserId userId) {
		Date newDate = new Date();
		CorporationFollowUpModel model = new CorporationFollowUpModel(CorporationFollowUpId.create(), incidentId,
				corporationOperativeId, incidentCorporationFollowUpReasonId, incidentCorporationFollowUpStatusId,
				corporationFollowUpObservation, corporationFollowUpResearchFolder, userId, newDate, userId, new Date());
		model.record(new CorporationFollowUpCreatedEvent(incidentId.getValue(), corporationOperativeId.getValue(),
				incidentCorporationFollowUpReasonId.getValue(), incidentCorporationFollowUpStatusId.getValue(),
				Optional.ofNullable(oldincidentCorporationFollowUpStatusId).map(IncidentCorporationFollowUpStatusId::getValue).orElse(null),
				corporationFollowUpObservation.getValue(), corporationFollowUpResearchFolder.getValue(),
				userId.getValue(), newDate));
		return model;
	}

}
