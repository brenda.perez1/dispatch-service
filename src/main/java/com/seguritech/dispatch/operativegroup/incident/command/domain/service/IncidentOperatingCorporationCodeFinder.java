package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationCodeNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationCode;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationRepository;
import com.seguritech.platform.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IncidentOperatingCorporationCodeFinder {

    private final CorporationRepository corporationRepository;

    public IncidentOperatingCorporationCodeFinder(CorporationRepository corporationRepository) {
        this.corporationRepository = corporationRepository;
    }


    public CorporationOperativeId find(IncidentCorporationCode code) {
        return Optional.ofNullable(code).map(this.corporationRepository::find)
            .orElseThrow(() -> new IncidentCorporationCodeNotFoundException(code.getValue()));
    }


    public List<CorporationOperativeId> find(List<IncidentCorporationCode> corporationCodes) {
        return corporationCodes.stream().map(this::find).collect(Collectors.toList());

    }
}
