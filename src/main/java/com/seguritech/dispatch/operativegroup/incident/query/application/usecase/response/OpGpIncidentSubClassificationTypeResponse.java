package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpGpIncidentSubClassificationTypeResponse {

    private final Long subClassificationTypeId;
    private final String name;


}
