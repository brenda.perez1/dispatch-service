package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.Date;

import com.seguritech.dispatch.operativegroup.incident.command.domain.event.CreateNoteIncidentOperatingCorporationAssignedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.platform.domain.RootAggregate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class IncidentCorporationNoteModel extends RootAggregate {

	private final IncidentId incidentId;
    private CorporationId corporationId;
    private final UserId createdByUser;
    private final Date createdAt;
    
    
    public static IncidentCorporationNoteModel createNotaAssignCorporation(IncidentId incidentId,
            CorporationId corporationId,
            UserId createdByUser) {
    	IncidentCorporationNoteModel model = new IncidentCorporationNoteModel(incidentId, corporationId, createdByUser, new Date());
    	model.record(new CreateNoteIncidentOperatingCorporationAssignedEvent(model.incidentId.getValue(), model.corporationId.getValue(), model.createdByUser.getValue(), new Date()));
    	return model;
    }
}
