package com.seguritech.dispatch.operativegroup.incident.query.application.api;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.operativegroup.incident.command.application.http.OperativeGroupIncidentBaseController;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpIncidentSearcherUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentOrderFieldEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentPeriodFilterEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryStatusEnum;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.platform.application.http.Order;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@RestController
public class OperativeGroupIncidentQueryController extends OperativeGroupIncidentBaseController {

    private final OperativeGpIncidentSearcherUseCase incidentSearchUseCase;

    public OperativeGroupIncidentQueryController (OperativeGpIncidentSearcherUseCase incidentSearchUseCase) {this.incidentSearchUseCase = incidentSearchUseCase;}

    @ApiIgnore
    @GetMapping
    @ApiOperation(value = "Find all emergencies by filters", notes = "Pageable result and optional filters")
    public ResponseEntity<CadLiteAPIResponse<PageDataProjectionDTO<OpGpIncidentSearchResponse>>> searchIncident (
            @ApiParam(value = "Emergencies assigned to operative groups")
            @PathVariable("operative-group-id") List<Long> operativeGroups,
            @ApiParam(value = "Emergency Folio for searching , it match text")
            @RequestParam(value = "folio", required = false) String folio,
            @ApiParam(value = "Emergency priority for searching")
            @RequestParam(value = "priorityId", required = false) Long priorityId,
            @ApiParam(value = "Emergency type for searching")
            @RequestParam(value = "emergencyTypeId", required = false) Long emergencyTypeId,
            @ApiParam(value = "Emergency Type id ,it searches emergency by type id")
            @RequestParam(value = "typeId", required = false) Long typeId,
            @ApiParam(value = "Emergency location for searching, name match for state, municipality, settlement, btwn" +
                    " street")
            @RequestParam(value = "location", required = false) String location,
            @ApiParam(value = "Emergency Location state id, it searches emergencies by location state id")
            @RequestParam(value = "stateId", required = false) Long stateId,
            @ApiParam(value = "Emergency location City/Municipality id ,it searches  emergencies by location " +
                    "city/municipality id")
            @RequestParam(value = "municipalityId", required = false) Long townId,
            @ApiParam(value = "Emergency location locality/settlement id ,it searches  emergencies by location " +
                    "locality/settlement id")
            @RequestParam(value = "localityId", required = false) Long localityId,
            @ApiParam(value = "Emergency location sector id")
            @RequestParam(value = "sectorId", required = false) Long sectorId,
            @ApiParam(value = "Emergency phoneNumber ,it searches  emergencies by reporter phoneNumber")
            @RequestParam(value = "phoneNumber", required = false) Long phoneNumber,
            @ApiParam(value = "Emergency status ,it searches emergencies by status")
            @RequestParam(value = "status", required = false) OpGpIncidentQueryStatusEnum status,
            @ApiParam(value = "Emergency source ,it searches emergencies by source id")
            @RequestParam(value = "sourceId", required = false) Long sourceId,
            @ApiParam(value = "Emergency assigned user ,it searches emergencies by assignedUser")
            @RequestParam(value = "assignedUserId", required = false) String assignedUserId,
            @ApiParam(value = "Emergency created by user ")
            @RequestParam(value = "createdUserId", required = false) String createdUserId,
            @ApiParam(value = "Filter emergencies by text in content ")
            @RequestParam(value = "query", required = false) String query,

            //Corporation
            @RequestParam(value = "corporationCode", required = false) @ApiParam(value = "Look for emergencies by " +
                    "Corporation Code")
                    String corporationCode,

            @RequestParam(value = "corporationDescription", required = false)
            @ApiParam(value = "Look for emergencies by Corporation Description") String corporationDescription,

            @RequestParam(value = "corporationId", required = false)
            @ApiParam(value = "Look for emergencies by Corporation related id") Long corporationId,

            //Date Range Filter
            @RequestParam(value = "periodDateField", required = false) @ApiParam(value = "Specify field to date range")
                    OpGpIncidentPeriodFilterEnum periodDateField, @RequestParam(value = "periodStartDate", required =
            false)
            @ApiParam(value = "Initial Date for filter", example = "yyyy-MM-dd HH:mm:ss")
            @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date periodStartDate,
            @RequestParam(value = "periodEndDate", required = false)
            @ApiParam(value = "End Date for filter", example = "yyyy-MM-dd HH:mm:ss") @DateTimeFormat(pattern = "yyyy" +
                    "-MM-dd HH:mm:ss")
                    Date periodEndDate,
            //Order
            @RequestParam(value = "order", required = false) @ApiParam(value = "Ordering direction") Order order,
            @RequestParam(value = "orderBy", required = false) @ApiParam(value = "Field for ordering")
                    OpGpIncidentOrderFieldEnum orderByField,
            //Pagination
            @RequestParam(value = "page", defaultValue = "0") @ApiParam(value = "Result page  delimiter by size , it " +
                    "starts at 0 ")
                    Integer page,
            @ApiParam(value = "Result page  delimiter size of results") @RequestParam(value = "size", defaultValue =
                    "20")
                    Integer size) {

        return CadLiteAPIResponse.build(this.incidentSearchUseCase.execute(new OperativeGpIncidentSearchRequest(
                size,
                page,
                operativeGroups,
                folio,
                priorityId,
                emergencyTypeId,
                location,
                status,
                phoneNumber,
                typeId,
                stateId,
                townId,
                localityId,
                sectorId,
                sourceId,
                assignedUserId,
                createdUserId,
                corporationCode,
                corporationDescription,
                corporationId,
                periodDateField,
                periodStartDate,
                periodEndDate,
                order,
                orderByField,
                query)));
    }

}
