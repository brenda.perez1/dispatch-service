package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentCorporationSearchPKResponse {

	private final CorporationOperativeResponse corporationOperative;

}
