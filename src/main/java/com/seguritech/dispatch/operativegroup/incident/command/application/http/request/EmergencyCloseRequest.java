package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmergencyCloseRequest {
	
	private String observation;
	private Long reasonId;

}
