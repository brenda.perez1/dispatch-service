package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import lombok.Getter;

import java.util.Optional;

@Getter
public class ReasonCancelId {
    private static final Long DEFAULT_VALUE = 0L;
    private Long value;

    private ReasonCancelId(Long value) {
        this.value = value;
    }

    public static ReasonCancelId optional(Long value) {
        return new ReasonCancelId(Optional.ofNullable(value).orElse(DEFAULT_VALUE));
    }

}
