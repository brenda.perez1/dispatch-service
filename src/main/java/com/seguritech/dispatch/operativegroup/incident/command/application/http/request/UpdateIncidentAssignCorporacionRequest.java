package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateIncidentAssignCorporacionRequest {

	private String incidentId;
	private String updateByUser;
	private Date updatedAt;
	private String status;
	private Boolean secondaryInvoice;
}
