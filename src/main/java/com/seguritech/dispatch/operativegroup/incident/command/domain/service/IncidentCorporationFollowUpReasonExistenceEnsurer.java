package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationFollowUpReasonNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationFollowUpReasonRepository;
import com.seguritech.platform.Service;

import java.util.Optional;

@Service
public class IncidentCorporationFollowUpReasonExistenceEnsurer {

    private final CorporationFollowUpReasonRepository repository;

    public IncidentCorporationFollowUpReasonExistenceEnsurer(CorporationFollowUpReasonRepository repository) {
        this.repository = repository;
    }

    public void ensure(IncidentCorporationFollowUpReasonId reasonId) {
        Optional.ofNullable(this.repository.findById(reasonId))
                .orElseThrow(IncidentCorporationFollowUpReasonNotFoundException::new);
    }

}
