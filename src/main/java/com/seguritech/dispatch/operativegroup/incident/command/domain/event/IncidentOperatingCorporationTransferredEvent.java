package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class IncidentOperatingCorporationTransferredEvent implements DomainEvent {
	
    private String incidentId;
    private Long corporationOperativeId;
    private String transferedByUser;
    private Date transferedAt;

}
