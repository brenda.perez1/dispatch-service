package com.seguritech.dispatch.operativegroup.incident.query.domain;

public enum IncidentOrderFieldEnum {
	
	/*priority("priority"),
    nationalSubType("nationalSubType"),
    corporation("corporation"),*/
    createdAt("createdAt");
    
	private String value;
	
    private IncidentOrderFieldEnum(String value) {
		this.value = value;
	}
    
    public static IncidentOrderFieldEnum Default() {
		return createdAt;
	}

	public String getValue() {
		return value;
	}

}
