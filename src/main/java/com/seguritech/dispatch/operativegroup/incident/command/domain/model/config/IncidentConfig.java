package com.seguritech.dispatch.operativegroup.incident.command.domain.model.config;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentPreconfigured;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class IncidentConfig {

    @Bean
    public IncidentPreconfigured addressPreconfigured(
            @Value("${cadlite.dispatch.incident.config.phone-number}") String phoneNumber,
            @Value("${cadlite.dispatch.incident.config.phone-number-country-id}") Long phoneNumberCountryId,
            @Value("${cadlite.dispatch.incident.config.incident-type-id}") Long incidentTypeId,
            @Value("${cadlite.dispatch.incident.config.state-id}") Long stateId,
            @Value("${cadlite.dispatch.incident.config.municipality-id}") Long townId,
            @Value("${cadlite.dispatch.incident.config.latitude}") Double latitude,
            @Value("${cadlite.dispatch.incident.config.longitude}") Double longitude
    ) {
        return new IncidentPreconfigured(phoneNumber, phoneNumberCountryId, incidentTypeId, stateId, townId, latitude, longitude);
    }

}

