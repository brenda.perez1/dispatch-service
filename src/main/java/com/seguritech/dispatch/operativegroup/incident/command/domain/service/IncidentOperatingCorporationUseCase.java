//package com.seguritech.dispatch.operativegroup.incident.command.domain.service;
//
//import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosureIncidentCorporationRequest;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
//import com.seguritech.platform.Service;
//import com.seguritech.platform.domain.DomainEventPublisher;
//
//@Service
//public class IncidentOperatingCorporationUseCase {
//
//    private final IncidentOperatingCorporationFinder finder;
////    private final IncidentOperatingCorporationRepository repository;
//    private final ProfileCorporationConfigRepository profile
//    private final DomainEventPublisher publisher;
//
//    public IncidentOperatingCorporationUseCase(IncidentOperatingCorporationFinder finder,
//                                              ProfileCorporationConfigRepository profileCorporationConfigRepository, DomainEventPublisher publisher) {
//        this.finder = finder;
////        this.repository = repository;
//        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
//        this.publisher = publisher;
//    }
//
//    public void execute(ClosureIncidentCorporationRequest request) {
//        IncidentId incidentId = IncidentId.required(request.getIncidentId());
//        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getCorporationId());
//        UserId userId = UserId.required(request.getUserId());
//        ProfileId profileId = ProfileId.required(request.getProfileId());
//
//        this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);
//
//        IncidentCorporationModel model = this.finder.findById(corporationId, incidentId);
//        model.close(userId);
////        this.repository.save(model);
//        this.publisher.publish(model.pullEvents());
//    }
//
//}
