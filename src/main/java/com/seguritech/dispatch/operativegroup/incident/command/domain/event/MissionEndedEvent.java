package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class MissionEndedEvent extends StageEvent {


}
