package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.geofence.entity.Geofence;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntity;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntityPk;
import com.domain.model.incident.dto.IncidentStatus.IncidentStatusEnum;
import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.geofence.dto.GeofenceModel;
import com.seguritech.dispatch.geofence.service.GeofenceManager;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.UpdateIncidentAssignCorporacionRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignIncidentCorporationListRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotDissociatedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentGeofenceNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.GeofenceId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationNoteModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.OperatingCorporationsRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentTypeChecker;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentsAssignationConfigurationChecker;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AssignIncidentToListOperatingCorporationUseCase {

    private final IncidentOperatingCorporationRepository repository;
    private final IncidentTypeChecker incidentChecker;
    private final OperatingCorporationsRepository operatingCorporationsRepository;
    private final CorporationOperativeRepository corporationOperativeRepository;
    private final GeofenceManager geofenceManager;
    private final EmergenciesClient client;
    private final ApplicationEventPublisher applicationEventPublisher;

    public AssignIncidentToListOperatingCorporationUseCase(IncidentsAssignationConfigurationChecker incidentsAssignationConfigurationChecker,
                                                       ProfileCorporationConfigRepository profileCorporationConfigRepository, 
                                                       IncidentOperatingCorporationRepository repository,
                                                       IncidentTypeChecker incidentChecker, 
                                                       OperatingCorporationsRepository operatingCorporationsRepository,
                                                       IncidentRepository incidentRepository,
                                                       GeofenceManager geofenceManager,
                                                       CorporationOperativeRepository corporationOperativeRepository,
                                                       EmergenciesClient client,
                                                       ApplicationEventPublisher applicationEventPublisher
    		
    ) {
        this.repository = repository;
        this.incidentChecker = incidentChecker;
        this.operatingCorporationsRepository = operatingCorporationsRepository;
        this.corporationOperativeRepository = corporationOperativeRepository;
        this.geofenceManager = geofenceManager;
        this.client = client;
        this.applicationEventPublisher = applicationEventPublisher;
    }
    
    @Transactional
    public List<IncidentCorporationModel> execute(AssignIncidentCorporationListRequest request, String token) {
    	
    	List<Object> events = new ArrayList<>();
    	
    	List<IncidentCorporationModel> notifications = new ArrayList<>();
    	List<IncidentCorporationNoteModel> notes = new ArrayList<>();
    	List<IncidentCorporationModel> corporations = new ArrayList<>();
    	
    	IncidentId incidentId = IncidentId.required(request.getIncidentId());
    	IncidentBounded incident = this.incidentChecker.check(incidentId);
    	for(Long id : request.getCorporationId()) {
    		CorporationId corporationId = CorporationId.required(id); 
    		
    		IncidentLocationBounded incidentLocation = this.incidentChecker.getLocation(incidentId);

    		//Busca geocercas por grupo operativo
    		List<OperativeGroupGeofencesEntity> operativeGroupGeofencesEntity = operatingCorporationsRepository.find(corporationId.getValue());

    		List<IncidentCorporationModel> result = new ArrayList<>();
    		boolean flagInserts = false;
    		for (OperativeGroupGeofencesEntity entity : operativeGroupGeofencesEntity) {

    			long idGeofence = entity.getId().getGeofence().getId();

    			GeofenceModel geofence = this.geofenceManager.getById(idGeofence);

    			if (geofence != null && geofence.getGeometry().containsPoint(incidentLocation.getCoordinates().getLatitude(), incidentLocation.getCoordinates().getLongitude())) {        	
    				Corporation corporation = entity.getId().getCorporationOperative().getCorporation();            	
    				CorporationOperative corporationOperative = entity.getId().getCorporationOperative();            	
    				Geofence geofence1 = entity.getId().getGeofence();           	
    				CorporationOperativeId corporationOperativeId = new CorporationOperativeId(corporationOperative.getId());
    				IncidentCorporationModel model = IncidentCorporationModel.dispatch(
    						incidentId, 
    						corporationOperativeId, 
    						CorporationId.required(corporation.getId()),
    						new UserId(request.getAssignedBy()),
    						GeofenceId.required(geofence1.getId()),
    						IncidentLocationId.required(incidentLocation.getCoordinates().getLocationId()),
    						incident.getFolio(),
    						incident.getSourceName(),
    						incident.getSourceId(),
    						incident.getSubTypeName(),
    						request.getSecondaryInvoice(),
    						incident.getIsManual(),
    						incident.getPlaySound()
    				);

    				IncidentCorporationModel aux = this.repository.findByIdAndStatus(model.getCorporationOperativeId(), incidentId, IncidentCorporationStatusEnum.OPEN);

    				if((aux != null && aux.getCorporationOperativeId().getValue() != entity.getId().getCorporationOperative().getId()) || aux == null) {
    					IncidentCorporationModel corporationModel = this.repository.save(model);
    					log.info(corporationModel.toString());
        				result.add(model);
        				if (!flagInserts) flagInserts = true;
    				}
    			}

    		}     

    		if (flagInserts) {
    			IncidentCorporationNoteModel modelNote =  IncidentCorporationNoteModel.createNotaAssignCorporation(incidentId, corporationId, new UserId(request.getAssignedBy()));
    			notes.add(modelNote);
    		}

    		///Si no encuentra geocerca busca gpos. operativos por default 
    		if (!flagInserts) {	
    			CorporationOperative corporationOperative = this.corporationOperativeRepository.findCorporationOperativeDefault(corporationId);

    			if(corporationOperative == null) throw new IncidentGeofenceNotFoundException();

    			Corporation corporation = corporationOperative.getCorporation();
    			OperativeGroupGeofencesEntity geofence = this.operatingCorporationsRepository.findDefaultGeofence(corporationOperative.getId());
    			CorporationOperativeId corporationOperativeId = new CorporationOperativeId(corporationOperative.getId());
    			IncidentCorporationModel model = IncidentCorporationModel.dispatch(
    					incidentId, 
    					corporationOperativeId, 
    					CorporationId.required(corporation.getId()),
    					new UserId(request.getAssignedBy()),
    					GeofenceId.optional(Optional.ofNullable(geofence).map(OperativeGroupGeofencesEntity::getId).map(OperativeGroupGeofencesEntityPk::getGeofence).orElse(null)),	                		
    					IncidentLocationId.required(incidentLocation.getCoordinates().getLocationId()),
    					incident.getFolio(),
    					incident.getSourceName(),
    					incident.getSourceId(),
    					incident.getSubTypeName(),
    					request.getSecondaryInvoice(),
    					incident.getIsManual(),
    					incident.getPlaySound()
    			);

    			IncidentCorporationModel aux = this.repository.findByIdAndStatus(model.getCorporationOperativeId(), incidentId, IncidentCorporationStatusEnum.OPEN);

    			if((aux != null && aux.getCorporationOperativeId().getValue() != corporationOperativeId.getValue()) || aux == null) {
					IncidentCorporationModel corporationModel = this.repository.save(model);
					log.info(corporationModel.toString());
    				result.add(model);
    				if (!flagInserts) flagInserts = true;
				}
    			
    		}

    		if (flagInserts) {       	 
    			IncidentCorporationModel modelNotification = IncidentCorporationModel.dispatchCorp(
    					incidentId, 
    					result.stream().map(IncidentCorporationModel::getCorporationOperativeId).collect(Collectors.toList()), 
    					CorporationId.required(corporationId.getValue()),
    					new UserId(request.getAssignedBy()),
    					null,
    					IncidentLocationId.required(incidentLocation.getCoordinates().getLocationId()),
    					incident.getFolio(),
    					incident.getSourceName(),
    					incident.getSourceId(),
    					incident.getSubTypeName(),
    					request.getSecondaryInvoice(),
    					incident.getIsManual(),
    					incident.getPlaySound()
    			 );

    			notifications.add(modelNotification);
    		}
    		
    		if (flagInserts)
    			corporations.addAll(result);

    	}
    	
    	if(!corporations.isEmpty()) {
    		this.client.updateEmergency(token, new UpdateIncidentAssignCorporacionRequest(incident.getId().getValue(), request.getAssignedBy(), new Date(), IncidentStatusEnum.DISPATCHING.name(), Optional.ofNullable(request.getSecondaryInvoice()).orElse(Boolean.FALSE)));
    	}
    	
    	events.addAll(notifications);
    	events.addAll(notes);
    	events.addAll(corporations);
    	
    	this.applicationEventPublisher.publishEvent(events);
    	
    	return corporations;
    	 
    }
    
}
