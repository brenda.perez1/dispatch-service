package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationReasonNotFound;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentCorporationReasonRepository;
import com.seguritech.platform.Service;

import java.util.Optional;

@Service
public class IncidentOperatingCorporationReasonExistenceEnsurer {

    private final IncidentCorporationReasonRepository reasonRepository;

    public IncidentOperatingCorporationReasonExistenceEnsurer(IncidentCorporationReasonRepository reasonRepository) {
        this.reasonRepository = reasonRepository;
    }

    public void ensure(IncidentCorporationReasonId reasonId) {
        Optional.ofNullable(this.reasonRepository.findById(reasonId))
                .orElseThrow(IncidentCorporationReasonNotFound::new);
    }

}
