package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationFollowUpModel;

public interface CorporationFollowUpRepository {

	void save(CorporationFollowUpModel model);

}