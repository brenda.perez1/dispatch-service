package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;


import java.util.Date;

import com.domain.model.incident.entity.IncidentSecundaryEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentSecundaryResponse {
    private final String folio;
    private  Date createdAt;

    public IncidentSecundaryResponse(String folio){
    
    	this.folio =folio;
    
    }

    public IncidentSecundaryResponse(IncidentSecundaryEntity x){
        this.folio = x.getFolio();
        this.createdAt = x.getCreatedAt();
    }

}
