package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.corporation.followup.reason.entity.CorporationFollowUpReasonEntity;
import com.seguritech.platform.Service;

@Service
public interface CorporationFollowUpReasonEntityRepository extends JpaRepository<CorporationFollowUpReasonEntity, Long> {

}
