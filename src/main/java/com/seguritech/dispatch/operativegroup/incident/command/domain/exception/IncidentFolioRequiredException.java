package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentFolioRequiredException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public IncidentFolioRequiredException() {
		super(MessageResolver.INCIDENT_FOLIO_REQUERID_EXCEPTION, 3020L);
	}
	
}
