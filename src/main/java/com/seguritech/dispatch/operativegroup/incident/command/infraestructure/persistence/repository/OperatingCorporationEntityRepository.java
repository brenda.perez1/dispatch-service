package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;

public interface OperatingCorporationEntityRepository extends JpaRepository<CorporationOperative,Long> {
	
	public CorporationOperative findByCorporationAndIsDefaultIsTrueAndActiveIsTrue(Corporation corporation);//Valida true gpo.operative
	
	@Query("SELECT CASE WHEN co.isDefault IS NULL THEN FALSE ELSE co.isDefault END FROM CorporationOperative co WHERE co.id =:corporationOperativeId AND co.active = TRUE  ")
	public Boolean isDefault(Long corporationOperativeId);
	
}
