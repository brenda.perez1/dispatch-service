package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationUnitNotCancelException extends BusinessException {

    public IncidentCorporationUnitNotCancelException(Object...args) {
        super(MessageResolver.INCIDENT_CORPORATION_UNIT_NOT_CANCEL_EXCEPTION, args, 3019L);
    }

}
