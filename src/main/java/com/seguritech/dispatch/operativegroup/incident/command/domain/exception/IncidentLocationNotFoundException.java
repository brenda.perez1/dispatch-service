package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentLocationNotFoundException extends BusinessException{
    public IncidentLocationNotFoundException(String message) {
        super(MessageResolver.INCIDENT_LOCATION_NOT_FOUND_EXCEPTION, 3035L);
    }
    
    public IncidentLocationNotFoundException(Object... incidentId) {
        super(MessageResolver.INCIDENT_LOCATION_NOT_FOUND_EXCEPTION_2, 3021L);
    }
}
