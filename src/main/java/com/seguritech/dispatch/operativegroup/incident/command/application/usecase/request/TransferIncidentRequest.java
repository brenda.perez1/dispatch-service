package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TransferIncidentRequest {
	
    private String emergencyId;
    private Long currentCorporationOperativeId;
    private Long newCorporationOperativeId;
    private String transferedBy;
    private Long profileId;
    
}
