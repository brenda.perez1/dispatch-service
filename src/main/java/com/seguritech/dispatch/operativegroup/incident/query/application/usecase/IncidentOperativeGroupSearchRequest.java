package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOperativeGroupStatus;
import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;

@Getter
public class IncidentOperativeGroupSearchRequest extends AbstractSearcherRequest {

    private final String incidentId;
    private final IncidentOperativeGroupStatus status;

    public IncidentOperativeGroupSearchRequest (String incidentId, IncidentOperativeGroupStatus status) {

        super(null, null, null);
        this.incidentId = incidentId;
        this.status = status;
    }

}
