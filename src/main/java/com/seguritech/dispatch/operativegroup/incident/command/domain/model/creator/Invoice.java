package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.InvoiceRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class Invoice {

    private final String value;

    private Invoice(String value) {
        this.value = value;
    }

    public static Invoice required(String invoice) {
        Optional.ofNullable(invoice).orElseThrow(InvoiceRequiredException::new);
        return new Invoice(invoice);
    }

}
