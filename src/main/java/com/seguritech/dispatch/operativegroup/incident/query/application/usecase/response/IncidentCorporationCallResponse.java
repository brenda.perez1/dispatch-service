package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IncidentCorporationCallResponse {

	private String id;
	private String folio;
	private String statusEmergency;
	private String description;
	private String senseDetection;
	private boolean isManual;	
	private Timestamp eventDate;
	private Timestamp createdAt;
	private Timestamp updatedAt;
	private Long nationalSubTypeId;
	private String nationalSubTypeName;
	private String nationalSubTypeClave;
	private Boolean nationalSubTypeIsSubType;
	private Long nationalTypeId;
	private String nationalTypeName;
	private Long priorityId;
	private String priorityName;
	private String priorityDescription;
	private String priorityTextColor;
	private String priorityBackColor;
	private Long sourceId;
	private String sourceName;
	private String sourceClave;
	private String sourceDescription;
	private String sourceIcon;
	private Boolean showAlertIdentifier;
	private Boolean playSound;
	private Long emergencyLocationId;
	private Double emergencyLocationLatitude;
	private Double emergencyLocationLongitude;
	private String emergencyLocationAddressAlternative;
	private String emergencyLocationMunicipalityAlternative;
	private String emergencyLocationStateAlternative;	
	private Timestamp emergencyLocationCreatedAt;
	private String userId;
	private String userName;
	private String associates;
	private String emergencyCorporations;
	private boolean isAsociation;
	private Long total;
	private String secundaryFolio;
	private String bwnStreet1;
	private String bwnStreet2;

}
