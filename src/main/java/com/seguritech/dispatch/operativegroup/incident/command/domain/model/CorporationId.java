package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationsRequiredException;
import lombok.Getter;

import java.util.Optional;

@Getter
public class CorporationId {
    private final Long value;

    private CorporationId(Long value) {
        this.value = value;
    }

    public static CorporationId required(Long value) {
        return Optional.ofNullable(value).map(CorporationId::new).orElseThrow(IncidentCorporationsRequiredException::new);
    }

}
