package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationValidatorModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;

public interface IncidentOperatingCorporationValidatorRepository {

    IncidentCorporationValidatorModel find(IncidentId incidentId);

}
