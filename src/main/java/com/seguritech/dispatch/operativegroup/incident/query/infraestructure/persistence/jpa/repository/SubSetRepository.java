package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository;

import com.domain.model.national.entity.SubSetEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubSetRepository extends JpaRepository<SubSetEntity, Long> {

    @Query("FROM SubSetEntity ss WHERE ss.nationalSubType.nationalSubTypeId = :nationalSubTypeId AND ss.active = true")
    List<SubSetEntity> findAllByNationalSubType(@Param("nationalSubTypeId") Long nationalSubTypeId);

}