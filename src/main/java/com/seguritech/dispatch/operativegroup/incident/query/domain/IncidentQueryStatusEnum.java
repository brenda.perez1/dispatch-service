package com.seguritech.dispatch.operativegroup.incident.query.domain;

public enum IncidentQueryStatusEnum {

    OPEN("OPEN"),
    DISPATCHING("DISPATCHING"),
    CLOSED("CLOSED");

    private String value;

    IncidentQueryStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
