package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import lombok.Getter;

import java.util.Date;

@Getter
public class IncidentCorporationReasonBounded {

    private final IncidentCorporationReasonId reasonId;
    private final IncidentCorporationObservation observation;
    private final UserId reactivatedByUser;
    private final Date reactivatedAt;
    private final UserId cancelByUser;
    private final Date cancelAt;

    public IncidentCorporationReasonBounded(IncidentCorporationReasonId reasonId,
                                            IncidentCorporationObservation observation,
                                            UserId reactivatedByUser,
                                            UserId cancelByUser) {
        this.reasonId = reasonId;
        this.observation = observation;
        this.reactivatedByUser = reactivatedByUser;
        this.reactivatedAt = new Date();
        this.cancelByUser = cancelByUser;
        this.cancelAt = new Date();
    }

    public static IncidentCorporationReasonBounded empty() {
        return new IncidentCorporationReasonBounded(
                IncidentCorporationReasonId.empty(),
                IncidentCorporationObservation.empty(),
                UserId.empty(),
                UserId.empty()
        );
    }

    public boolean isEmpty() {
        return this.reasonId.isEmpty();
    }

}
