package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import com.domain.model.incident.entity.IncidentEntity;

@Repository
public interface IncidentCorporationEntityRepository extends JpaRepository<IncidentEntity, String>, JpaSpecificationExecutor<IncidentEntity> {

    @Query("FROM IncidentEntity e WHERE e.createdByUser.idUser =:userId")
	Optional<IncidentEntity> findByIdUser(@Param("userId") String userId);
	
	@Query("SELECT COUNT(ec) FROM IncidentEntity e INNER JOIN IncidentCorporationEntity ec ON ec.status =:status")
    Long countByStatus(@Param("status") String status);	
	
}
