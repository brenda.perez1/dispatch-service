package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentUnitForceRequired extends BusinessException {

    public IncidentUnitForceRequired() {
        super(MessageResolver.INCIDENT_UNIT_FORCE_REQUERID_EXCEPTION, 3028L);
    }

}
