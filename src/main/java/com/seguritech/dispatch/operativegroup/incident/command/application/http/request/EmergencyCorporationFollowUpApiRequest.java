package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationFollowUpApiRequest {

	private Long followUpStatus;
    private Long followUpReason;
	private String researchFolder;
    private String observation;

}
