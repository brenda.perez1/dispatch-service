package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.event.IncidentOpenedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.OperatingCorporationIncidentUnassignedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.platform.domain.RootAggregate;

public class IncidentCorporationUnassingModel extends RootAggregate{
	
	private final List<IncidentCorporationModel> models;
	private final IncidentId incidentId;
	private CorporationId corporationId;
	private IncidentCorporationReasonCloseId closeId; 
	private IncidentCorporationReasonCloseBounded reason; 
	private UserId userId;
	
	public IncidentCorporationUnassingModel(List<IncidentCorporationModel> models, IncidentId incidentId,
			CorporationId corporationId, IncidentCorporationReasonCloseId closeId,
			IncidentCorporationReasonCloseBounded reason, UserId userId) {
		super();
		this.models = models;
		this.incidentId = incidentId;
		this.corporationId = corporationId;
		this.closeId = closeId;
		this.reason = reason;
		this.userId = userId;
	}
	
	public static IncidentCorporationUnassingModel unassign(List<IncidentCorporationModel> models, IncidentId incidentId,
			CorporationId corporationId, IncidentCorporationReasonCloseId closeId,
			IncidentCorporationReasonCloseBounded reason, UserId userId) {
		
		Date closeDate =  new Date();
		List<Long> corporationOperativeIds = new ArrayList<>();
		for (IncidentCorporationModel incidentCorporationModel : models) {
			incidentCorporationModel.setStatus(IncidentCorporationStatus.of(IncidentCorporationStatusEnum.CLOSED.toString()));
			incidentCorporationModel.setClosedByUser(userId);
			incidentCorporationModel.setClosedAt(closeDate);
			incidentCorporationModel.setReasonCloseId(closeId);
			incidentCorporationModel.setObservationClose(reason.getObservation());
			corporationOperativeIds.add(incidentCorporationModel.getCorporationOperativeId().getValue());
		}
		IncidentCorporationUnassingModel model =  new IncidentCorporationUnassingModel(models, incidentId, corporationId, closeId, reason, userId);
		model.record(new OperatingCorporationIncidentUnassignedEvent(
                model.incidentId.getValue(),
                model.corporationId.getValue(),
                corporationOperativeIds,
                IncidentCorporationStatus.of(IncidentCorporationStatusEnum.CLOSED.toString()).getValue().getValue(),
                userId.getValue(),
                closeDate,
                reason.getReasonId().getValue(),
                reason.getObservation().getValue()
        ));
		
		return model;
	}

	public void openIncident(String idIncident) {
    	this.record(new IncidentOpenedEvent(idIncident, idIncident));
    }
}
