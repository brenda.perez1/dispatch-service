package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested.SuggestedCorporationBoundedModel;

import java.util.List;

public interface SuggestedCorporationsRepository {
    List<SuggestedCorporationBoundedModel> findByIncidentTypeIdBounded(IncidentTypeIdBounded id);
}
