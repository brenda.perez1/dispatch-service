package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.unit;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentUnitForceRequired;
import lombok.Getter;
import lombok.ToString;

import java.util.*;

@Getter
@ToString
public class UnitForceList {

    public static final List<String> DEFAULT_VALUE = new ArrayList<>();
    private final List<String> value;

    private UnitForceList(List<String> value) {
        this.value = Collections.unmodifiableList(value);
    }

    public static UnitForceList optional(List<String> unitForceList) {
        return new UnitForceList(Optional.ofNullable(unitForceList)
                .orElse(DEFAULT_VALUE));
    }

    public static UnitForceList required(List<String> unitForceList) {
        return new UnitForceList(Optional.ofNullable(unitForceList)
                .orElseThrow(IncidentUnitForceRequired::new));

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        UnitForceList that = (UnitForceList) o;
        if (that.value.equals(DEFAULT_VALUE)) return true;
        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public boolean isEmpty() {
        return this.value.isEmpty();
    }

}
