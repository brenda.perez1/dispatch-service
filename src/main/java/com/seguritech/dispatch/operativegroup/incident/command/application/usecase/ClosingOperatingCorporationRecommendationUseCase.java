package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosingRecommendationIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class ClosingOperatingCorporationRecommendationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final DomainEventPublisher publisher;

    public ClosingOperatingCorporationRecommendationUseCase(IncidentOperatingCorporationRepository repository,
                                                            DomainEventPublisher publisher) {
        this.finder = new IncidentOperatingCorporationFinder(repository,null);
        this.publisher = publisher;
    }

    public IncidentCorporationModel execute(ClosingRecommendationIncidentCorporationRequest request) {
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getCorporationId());
        UserId userId = UserId.required(request.getUserId());

        IncidentCorporationModel model = this.finder.findById(corporationId, incidentId);
        //TODO: Analizar y mejorar proceso de sugerencias de cierre de incidente por corporación
        model.validateCanBeClosedIncident(userId);
        this.publisher.publish(model.pullEvents());
        return model;
    }

}
