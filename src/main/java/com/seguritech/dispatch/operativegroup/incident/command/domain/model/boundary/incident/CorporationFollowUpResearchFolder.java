package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.CorporationFollowUpResearchFolderInvalidException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class CorporationFollowUpResearchFolder {

    private final String value;
    private final static Long MAX_LENGTH = 100L;
    private static final String DEFAULT_VALUE = "";

    public static CorporationFollowUpResearchFolder optional(String value) {
        return Optional.ofNullable(value)
                .map(CorporationFollowUpResearchFolder::ensure)
                .map(CorporationFollowUpResearchFolder::new)
                .orElse(CorporationFollowUpResearchFolder.empty());
    }

    public static CorporationFollowUpResearchFolder empty() {
        return new CorporationFollowUpResearchFolder(DEFAULT_VALUE);
    }

    private static String ensure(String value) {
        if (value.length() > MAX_LENGTH) {
            throw new CorporationFollowUpResearchFolderInvalidException(MAX_LENGTH);
        }
        return value;
    }

    public boolean isEmpty() {
        return this.value == DEFAULT_VALUE;
    }

}
