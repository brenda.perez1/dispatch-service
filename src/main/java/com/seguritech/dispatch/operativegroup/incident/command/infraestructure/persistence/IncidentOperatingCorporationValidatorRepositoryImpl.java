package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence;

import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationValidatorModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationValidatorRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentOperatingCorporationEntityRepository;

@Repository
public class IncidentOperatingCorporationValidatorRepositoryImpl implements IncidentOperatingCorporationValidatorRepository {

    private static final String STATUS_INCIDENT_OPEN = "OPEN";
    private final IncidentOperatingCorporationEntityRepository repository;

    public IncidentOperatingCorporationValidatorRepositoryImpl(IncidentOperatingCorporationEntityRepository repository) {
        this.repository = repository;
    }

    @Override
    public IncidentCorporationValidatorModel find(IncidentId incidentId) {
        return new IncidentCorporationValidatorModel(
                incidentId,
                this.repository.findByIdIncidentAndStatus(new IncidentEntity(incidentId.getValue()), STATUS_INCIDENT_OPEN)
                        .map(x -> x.stream().map(this::toCorporation).collect(Collectors.toList())).orElse(Collections.emptyList())
        );
    }

    private CorporationId toCorporation(IncidentCorporationEntity entity) {
        return CorporationId.required(entity.getId().getCorporationOperative().getId());
    }

}
