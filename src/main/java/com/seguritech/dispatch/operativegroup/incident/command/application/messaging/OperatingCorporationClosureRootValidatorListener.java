package com.seguritech.dispatch.operativegroup.incident.command.application.messaging;

import com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event.IncidentOperatingCorporationCancelEvent;
import com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event.OperatingCorporationIncidentClosedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.RootIncidentValidatorOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.RootIncidentCancelValidatorRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.RootIncidentClosureValidatorRequest;
import com.seguritech.platform.application.messagging.DomainEventListener;
import com.seguritech.platform.application.messagging.DomainSourceEventListener;
import com.seguritech.platform.infrastructure.messaging.DomainEventEnvelope;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@DomainSourceEventListener(domain = "incidents-service")
public class OperatingCorporationClosureRootValidatorListener {

    private final RootIncidentValidatorOperatingCorporationUseCase useCase;

    public OperatingCorporationClosureRootValidatorListener(RootIncidentValidatorOperatingCorporationUseCase useCase) {
        this.useCase = useCase;
    }

    @DomainEventListener(name = "operating-corporation-incident-closed", version = "v1")
    public void onCorporationIncidentClosed(DomainEventEnvelope<OperatingCorporationIncidentClosedEvent> evt) {
        OperatingCorporationIncidentClosedEvent event = evt.getPayload();
        this.useCase.execute(new RootIncidentClosureValidatorRequest(
                event.getEmergencyId(),
                event.getClosedByUser()
        ));
    }

    @DomainEventListener(name = "incident-operating-corporation-cancel", version = "v1")
    public void onIncidentCorporationCancel(DomainEventEnvelope<IncidentOperatingCorporationCancelEvent> evt) {
        IncidentOperatingCorporationCancelEvent event = evt.getPayload();
        this.useCase.execute(new RootIncidentCancelValidatorRequest(
                event.getEmergencyId(),
                event.getCancelByUser(),
                event.getCancelReasonId(),
                event.getObservation()
        ));
    }

}
