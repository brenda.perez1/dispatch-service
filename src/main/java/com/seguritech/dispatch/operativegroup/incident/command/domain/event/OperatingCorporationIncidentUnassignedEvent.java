package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.List;

@Getter
@AllArgsConstructor
public class OperatingCorporationIncidentUnassignedEvent implements DomainEvent {

    private String emergencyId;
    private Long corporationId;
    private List<Long> coporationOperativeIds;
    private String status;
    private String unassignedByUser;
    private Date unassignedAt;
    private Long reasonCloseId;
    private String observations;

}