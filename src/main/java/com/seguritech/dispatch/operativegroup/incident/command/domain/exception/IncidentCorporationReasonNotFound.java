package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationReasonNotFound extends BusinessException {

    public IncidentCorporationReasonNotFound() {
        super(MessageResolver.INCIDENT_CORPORATION_REASON_NOT_FOUND_EXCEPTION, 3014L);
    }

}
