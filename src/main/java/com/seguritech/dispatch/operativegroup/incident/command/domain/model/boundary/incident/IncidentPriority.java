package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import java.util.Optional;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentPriorityRequiredException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class IncidentPriority {

    private final Long value;

    private IncidentPriority (Long value) {
        this.value = value;
    }


    public static IncidentPriority required (Long value) {
        return Optional.ofNullable(value)
                .map(IncidentPriority::new)
                .orElseThrow(IncidentPriorityRequiredException::new);
    }

    public static IncidentPriority from (Long value) {
        return new IncidentPriority(value);
    }

    public static IncidentPriority none () {
        return new IncidentPriority(null);
    }

    //Change to Default
    public static IncidentPriority Default () {
        return new IncidentPriority(1L);
    }

    public static IncidentPriority optional (Long priorityId) {
        return Optional.ofNullable(priorityId).map(IncidentPriority::new).orElse(IncidentPriority.none());
    }

    public boolean isEmpty () {
        return this.value == null;
    }

}
