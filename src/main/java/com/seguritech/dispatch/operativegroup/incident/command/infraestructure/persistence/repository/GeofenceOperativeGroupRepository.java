package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.domain.model.geofence.entity.Geofence;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntity;
import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntityPk;

public interface GeofenceOperativeGroupRepository extends JpaRepository<OperativeGroupGeofencesEntity, OperativeGroupGeofencesEntityPk> {

	@Query(value= "SELECT ogg.id.geofence FROM OperativeGroupGeofencesEntity ogg WHERE ogg.active = true AND ogg.id.geofence.active = true AND ogg.id.corporationOperative.id IN :corporationsOperative ORDER BY ogg.id.geofence.id ASC")
	List<Geofence> getByOperativeGroupId(@Param("corporationsOperative") List<Long> corporationsOperative);
	
	@Query("SELECT CASE WHEN count(ogg) > 0 then true else false END FROM OperativeGroupGeofencesEntity ogg WHERE ogg.id.geofence.id =:id ")
	Boolean isDefault(@Param("id") Long id);

}
