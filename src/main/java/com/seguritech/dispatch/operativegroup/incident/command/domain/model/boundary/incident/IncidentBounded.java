package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentBounded {

    private final IncidentId id;
    private final IncidentTypeIdBounded type;
    private final IncidentStatusBounded status;
    private final IncidentPriority priority;
    private final String folio;
    private final String sourceName;
    private final Long sourceId;
    private final String subTypeName;
    private final String priorityName;
    private final Boolean isManual;
    private final Boolean playSound;

}
