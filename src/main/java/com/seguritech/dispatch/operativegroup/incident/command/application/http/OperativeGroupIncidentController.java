package com.seguritech.dispatch.operativegroup.incident.command.application.http;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationAcceptSupportHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationCancelApiRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationCloseApiRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationFollowUpApiRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationManualIncidentHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationReactivateApiRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationRejectSupportHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationSupportHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyTransferHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.response.IncidentManualCreateApiResponse;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.AcceptSupportRequestUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.CancelOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.ClosureOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.ManualIncidentUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.OperatingCorporationFollowUpUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.ReactivateOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.SupportRequestUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.TransferIncidentUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AcceptSupportRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CancelIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosureIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CorporationFollowUpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CreateManualIncidentRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ReactivateCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.RejectSupportRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.SupportRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.TransferIncidentRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.CreateManualIncidentResponse;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.RejectSupportRequestUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ProfileIdRequierdException;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;

import io.swagger.annotations.ApiOperation;

@RestController
public class OperativeGroupIncidentController extends OperativeGroupIncidentBaseController {

	private static final String AUTHORIZATION_HEADER = "AUTHORIZATION";
	private static final String AUTHORIZATION_BEARER = "Bearer ";
    private final ReactivateOperatingCorporationUseCase reactivateOperatingCorporationUseCase;
    private final CancelOperatingCorporationUseCase cancelOperatingCorporationUseCase;
    private final ClosureOperatingCorporationUseCase closureOperatingCorporationUseCase;
    private final OperatingCorporationFollowUpUseCase operatingCorporationFollowUpUseCase;
    private final TransferIncidentUseCase transferIncidentUseCase;
    private final ManualIncidentUseCase manualIncidentUseCase;
    private final SupportRequestUseCase supportRequestUseCase;
    private final AcceptSupportRequestUseCase acceptSupportRequestUseCase;
    private final RejectSupportRequestUseCase rejectSupportRequestUseCase;

    public OperativeGroupIncidentController(ReactivateOperatingCorporationUseCase reactivateOperatingCorporationUseCase,
                                            CancelOperatingCorporationUseCase cancelOperatingCorporationUseCase, ClosureOperatingCorporationUseCase closureOperatingCorporationUseCase,
                                            OperatingCorporationFollowUpUseCase operatingCorporationFollowUpUseCase,
                                            TransferIncidentUseCase transferIncidentUseCase,
                                            ManualIncidentUseCase manualIncidentUseCase,
                                            SupportRequestUseCase supportRequestUseCase,
                                            AcceptSupportRequestUseCase acceptSupportRequestUseCase,
                                            RejectSupportRequestUseCase rejectSupportRequestUseCase) {
        this.reactivateOperatingCorporationUseCase = reactivateOperatingCorporationUseCase;
        this.cancelOperatingCorporationUseCase = cancelOperatingCorporationUseCase;
        this.closureOperatingCorporationUseCase = closureOperatingCorporationUseCase;
        this.operatingCorporationFollowUpUseCase = operatingCorporationFollowUpUseCase;
        this.transferIncidentUseCase = transferIncidentUseCase;
        this.manualIncidentUseCase = manualIncidentUseCase;
        this.supportRequestUseCase = supportRequestUseCase;
        this.acceptSupportRequestUseCase = acceptSupportRequestUseCase;
        this.rejectSupportRequestUseCase = rejectSupportRequestUseCase;
    }

    @ApiOperation(value = "To transfer emergency to another corporation",
            notes = "To transfer emergency to another corporation")
    @PatchMapping("/{emergency-id}/transfer")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void transfer(@PathVariable("emergency-id") String emergencyId,
                         @PathVariable("operative-group-id") Long corporationOperativeId,
                         @RequestBody EmergencyTransferHttpRequest request,
                         UsernamePasswordAuthenticationToken user) {
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
        this.transferIncidentUseCase.execute(new TransferIncidentRequest(
        		emergencyId,
        		request.getCorporationOperativeId(),
        		corporationOperativeId,
                principal.getId(),
                Long.parseLong(activeProfile)
        ));
    }

    @ApiOperation(value = "Reopen corporation for an emergency",
            notes = "Reopen corporation associated to emergency , if emergency is closed, emergency will reopen")
    @PatchMapping("/{emergency-id}/reopen")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void reactivateCorporation(@PathVariable("emergency-id") String incidentId,
                                      @PathVariable("operative-group-id") Long operativeGroupId,
                                      @RequestBody EmergencyCorporationReactivateApiRequest request,
                                      AppUserPrincipal principal) {
        this.reactivateOperatingCorporationUseCase.execute(new ReactivateCorporationRequest(
                incidentId,
                operativeGroupId,
                request.getObservation(),
                request.getReactivationReasonId(),
                principal.getId(),
                new Date(),
                request.getProfileId()));
    }

    @ApiOperation(value = "Cancel corporation to the emergency")
    @PatchMapping("/{emergency-id}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelIncidentCorporation(
            @PathVariable("emergency-id") String incidentId,
            @PathVariable("operative-group-id") Long operativeGroupId,
            @RequestBody @Valid EmergencyCorporationCancelApiRequest request,
            UsernamePasswordAuthenticationToken user,
            HttpServletRequest http
            ) {
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
    	
    	Long profileId = Long.parseLong(principal.getActiveProfile().orElse("-1"));
        this.cancelOperatingCorporationUseCase.execute(AUTHORIZATION_BEARER.concat(http.getHeader(AUTHORIZATION_HEADER)),new CancelIncidentCorporationRequest(
                incidentId,
                operativeGroupId,
                request.getCancelReasonId(),
                request.getObservation(),
                principal.getId(),
                profileId
        ));

    }

    @ApiOperation(value = "Close incident at the corporation")
    @PatchMapping("/{emergency-id}/close")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void closeIncidentCorporation(
            @PathVariable("emergency-id") String incidentId,
            @PathVariable("operative-group-id") Long operativeGroupId,
            @RequestBody @Valid EmergencyCorporationCloseApiRequest request,
            UsernamePasswordAuthenticationToken user,
            HttpServletRequest http
            ) {
    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
    	
    	Long profileId = Long.parseLong(principal.getActiveProfile().orElse("-1"));
        this.closureOperatingCorporationUseCase.execute(AUTHORIZATION_BEARER.concat(http.getHeader(AUTHORIZATION_HEADER)),new ClosureIncidentCorporationRequest(
                incidentId,
                operativeGroupId,
                principal.getId(),
                profileId,
                request.getCloseReasonId(),
                request.getObservation()
        ));
    }
    
    @PostMapping("/{emergency-id}/followup")
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation(value = "Create new followup record of corporation")
	public void addFollowUpCorporation(@PathVariable("emergency-id") String incidentId,
			@PathVariable("operative-group-id") Long operativeGroupId,
			@RequestBody @Valid EmergencyCorporationFollowUpApiRequest request,
			UsernamePasswordAuthenticationToken user) {
		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		Long profileId = Long.parseLong(principal.getActiveProfile().orElse("-1"));
		this.operatingCorporationFollowUpUseCase
			.execute(new CorporationFollowUpRequest(
				principal.getId(), profileId, incidentId,operativeGroupId, 
				request.getFollowUpStatus(), request.getFollowUpReason(),
				request.getResearchFolder(), request.getObservation()
			));
	}

    @ApiOperation(value = "Create Manual Incident From Corporation", nickname = "assignCorporation",
            notes = "Create Manual Incident")
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<CadLiteAPIResponse<IncidentManualCreateApiResponse>> assign(
            @PathVariable("operative-group-id") Long operativeGroupId,
            @RequestBody EmergencyCorporationManualIncidentHttpRequest request,
            AppUserPrincipal principal, ServerHttpRequest httpRequest) {
        CreateManualIncidentResponse response = this.manualIncidentUseCase.execute(
                new CreateManualIncidentRequest(
                        request.getSourceId(),
                        request.getPhoneNumber(),
                        request.getPhoneNumberCountryId(),
                        request.getDescription(),
                        principal.getId(),
                        request.getEventDate(),
                        httpRequest.getRemoteAddress().getHostString(),
                        operativeGroupId,
                        request.getProfileId()
                )
        );

        return CadLiteAPIResponse.build(new IncidentManualCreateApiResponse(
                response.getIncidentId(),
                response.getInvoice()
        ));
    }

    @ApiOperation(value = "Request for support to the operative group", nickname = "supportRequest",
            notes = "Request for support")
    @PostMapping("/{emergency-id}/support")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void supportRequest(
            @PathVariable("emergency-id") String incidentId,
            @PathVariable("operative-group-id") Long operativeGroupId,
            @RequestBody EmergencyCorporationSupportHttpRequest request,
            AppUserPrincipal principal) {
        this.supportRequestUseCase.execute(
                new SupportRequest(
                        principal.getId(),
                        operativeGroupId,
                        request.getProfileId(),
                        incidentId
                )
        );
    }

    @ApiOperation(value = "Accept support request", nickname = "supportRequest",
            notes = "Accept support request")
    @PatchMapping("/{emergency-id}/support-accept")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void acceptSupportRequest(
            @PathVariable("emergency-id") String incidentId,
            @PathVariable("operative-group-id") Long operativeGroupId,
            @RequestBody EmergencyCorporationAcceptSupportHttpRequest request,
            AppUserPrincipal principal) {
        this.acceptSupportRequestUseCase.execute(
                new AcceptSupportRequest(
                        operativeGroupId,
                        incidentId,
                        principal.getId(),
                        request.getProfileId()
                )
        );
    }

    @ApiOperation(value = "Reject support request", nickname = "supportRequest",
            notes = "Reject support request")
    @PatchMapping("/{emergency-id}/support-reject")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void rejectSupportRequest(
            @PathVariable("emergency-id") String incidentId,
            @PathVariable("operative-group-id") Long operativeGroupId,
            @RequestBody EmergencyCorporationRejectSupportHttpRequest request,
            AppUserPrincipal principal) {
        this.rejectSupportRequestUseCase.execute(
                new RejectSupportRequest(
                        operativeGroupId,
                        incidentId,
                        principal.getId(),
                        request.getProfileId()
                )
        );
    }
   

}
