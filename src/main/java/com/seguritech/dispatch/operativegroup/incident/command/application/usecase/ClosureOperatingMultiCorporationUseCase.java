package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCloseRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosureIncidentMultiCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCloseException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
@Slf4j
@Service
public class ClosureOperatingMultiCorporationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentOperatingCorporationRepository repository;
    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final DomainEventPublisher publisher;
    private final EmergenciesClient client;
    private final CorporationOperativeRepository corporationOperativeRepository;

    public ClosureOperatingMultiCorporationUseCase(IncidentOperatingCorporationRepository repository,
                                                   ProfileCorporationConfigRepository profileCorporationConfigRepository, DomainEventPublisher publisher,
                                                   EmergenciesClient client, CorporationOperativeRepository corporationOperativeRepository) {
        this.finder = new IncidentOperatingCorporationFinder(repository,null);
        this.repository = repository;
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.publisher = publisher;
        this.client = client;
        this.corporationOperativeRepository = corporationOperativeRepository;
    }

    @Transactional
    public void execute(final String token, final ClosureIncidentMultiCorporationRequest request) {

    	List<Long> idsCorporation = new ArrayList<>();
    	IncidentCorporationReasonCloseId closeId = IncidentCorporationReasonCloseId.required(request.getCloseReasonId());
    	IncidentId incidentId = IncidentId.required(request.getIncidentId());
    	UserId userId = UserId.required(request.getUserId());   	
    	ProfileId profileId = ProfileId.required(request.getProfileId());
    	
    	IncidentCorporationReasonCloseBounded reason = new IncidentCorporationReasonCloseBounded(
    			closeId,
    			IncidentCorporationObservation.optional(request.getObservation()),
    			UserId.required(request.getUserId()),
    			new UserId(request.getUserId()));

    	request.getOperativeGroupIds().forEach(id -> {
    		CorporationOperativeId corporationId = CorporationOperativeId.required(id);
    		this.check(profileId, corporationId);

    		IncidentCorporationModel model = this.finder.findById(corporationId, incidentId);
    		
    		//Se extraen las corporaciones
    		if(!idsCorporation.contains(model.getCorporationId().getValue())){
    			idsCorporation.add(model.getCorporationId().getValue());
    		}
  		
    		model.close(closeId, reason, userId);
    		this.repository.save(model);
    		this.publisher.publish(model.pullEvents());

    	});
	
    	/**Se iteran las corporaciones ejecutando "findByIncidentAndStatusAndCorporation" y ver si aun tiene grupos operativos*/
		for (Long idCorporation : idsCorporation) {

			CorporationId corporation = CorporationId.required(idCorporation);
			List<IncidentCorporationModel> listIncidentsOpen = this.repository.findByIncidentAndStatusAndCorporation(
					incidentId, IncidentCorporationStatusEnum.OPEN.toString(), corporation);
			
			//Se genera la nota automatica "cierre por una corporacion" siempre y cuando no haya grupos operativos abiertos
			if (listIncidentsOpen.size() == 0) {
				IncidentCorporationModel modelCorporation = this.finder.findByIdIncidentAndCorporation(corporation, incidentId);
				modelCorporation.closeCorporation(corporation, userId);
				this.publisher.publish(modelCorporation.pullEvents());
			}
		}
    	  		
    	try {
			//si ya no esta abierto ningun grupo operativo se manda cerrar el incidente principal
    		if (this.repository.countByIncidentAndStatus(incidentId, IncidentCorporationStatusEnum.OPEN.toString()) == 0) {
    			log.info("Cerrando incidente principal: " + incidentId.getValue());
    			client.closeAnEmergency(token, incidentId.getValue(), new EmergencyCloseRequest("Cierre automatico",0L));
    		}
    	} catch (Exception e) {
    		log.error(e.getMessage());
    		throw new IncidentCloseException();
    	}
    }

    public void check(ProfileId profileId, CorporationOperativeId corporationOperativeId) {
    	if (!this.corporationOperativeRepository.isCorporationOperativeDefault(corporationOperativeId.getValue())) {
    		this.profileCorporationConfigEnsurer.ensure(profileId, corporationOperativeId);
		}
    }
}
