package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incident.entity.IncidentLocationEntity;

public interface IncidentLocationEntityRepository extends JpaRepository<IncidentLocationEntity, Long> {

    Optional<IncidentLocationEntity> findByEmergency(IncidentEntity incidentEntity);

}
