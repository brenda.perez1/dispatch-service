package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import org.springframework.stereotype.Service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentTypedRequiredException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedTypeRepository;

@Service
public class IncidentBoundedTypeEnsurer {

    private final IncidentBoundedTypeRepository repository;

    public IncidentBoundedTypeEnsurer(IncidentBoundedTypeRepository repository) {
        this.repository = repository;
    }


    public void check(IncidentTypeIdBounded type) {

        IncidentTypeIdBounded unidentified = this.repository.findUnidentified();

        if (type == null || unidentified.equals(type)) {
            throw new IncidentTypedRequiredException();
        }
        //Check is possible corporation
//        if (!repository.isAssignable(type)) {
//            throw new IncidentTypeNotAssignableException();
//        }
    }
    
    

}
