package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ProfileIdRequierdException extends BusinessException {
    public ProfileIdRequierdException() {
        super(MessageResolver.PROFILE_ID_REQUERID_EXCEPTION, 3033L);
    }
}
