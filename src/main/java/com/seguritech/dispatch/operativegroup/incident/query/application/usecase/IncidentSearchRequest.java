package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.platform.application.usecase.AbstractSearcherRequest;

import lombok.Getter;

@Getter
public class IncidentSearchRequest extends AbstractSearcherRequest {

    private final String emergencyId;

    public IncidentSearchRequest(Integer size, Integer page, String emergencyId) {
        super(size, page, null);
        this.emergencyId = emergencyId;
    }

}
