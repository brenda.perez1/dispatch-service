package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class OpGpFollowUpSearchResponse {

	private String id;
	private CorporationOperativeResponse corporation;
	private CorporationFollowUpStatusResponse status;
	private String reason;
	private String researchFolder;
	private String observation;
	private String createdBy;
	private Date createdAt;

}
