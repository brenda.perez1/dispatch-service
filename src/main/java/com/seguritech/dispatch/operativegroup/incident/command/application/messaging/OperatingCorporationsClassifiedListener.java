package com.seguritech.dispatch.operativegroup.incident.command.application.messaging;

import com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event.IncidentClassifiedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.AutomaticOperatingCorporationAssignmentToClassifiedIncidentUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AutomaticCorporationAssignmentToClassifiedIncidentRequest;
import com.seguritech.platform.application.messagging.DomainEventListener;
import com.seguritech.platform.application.messagging.DomainSourceEventListener;
import com.seguritech.platform.infrastructure.messaging.DomainEventEnvelope;
import lombok.extern.slf4j.Slf4j;

@DomainSourceEventListener(domain = "incidents-service")
@Slf4j
public class OperatingCorporationsClassifiedListener {

    private final AutomaticOperatingCorporationAssignmentToClassifiedIncidentUseCase automaticCorporationAssignment;

    public OperatingCorporationsClassifiedListener(AutomaticOperatingCorporationAssignmentToClassifiedIncidentUseCase automaticCorporationAssignment) {
        this.automaticCorporationAssignment = automaticCorporationAssignment;
    }

    @DomainEventListener(name = "incident-classified", version = "v1")
    public void onIncidentCreatedEvent(DomainEventEnvelope<IncidentClassifiedEvent> event) {
        IncidentClassifiedEvent payload = event.getPayload();
        this.automaticCorporationAssignment.execute(new AutomaticCorporationAssignmentToClassifiedIncidentRequest(
                payload.getId(),
                payload.getTypeId(),
                payload.getUpdatedBy()
        ));
    }
}