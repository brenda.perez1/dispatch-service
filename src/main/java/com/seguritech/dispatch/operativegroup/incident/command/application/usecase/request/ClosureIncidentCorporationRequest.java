package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClosureIncidentCorporationRequest {

    private String incidentId;
    private Long corporationId;
    private String userId;
    private Long profileId;
    private Long closeReasonId;
    private String observation;
}
