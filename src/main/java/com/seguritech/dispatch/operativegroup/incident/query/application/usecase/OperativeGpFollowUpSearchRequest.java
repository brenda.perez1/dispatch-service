package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.platform.application.http.Order;
import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;

import java.util.Date;

@Getter
public class OperativeGpFollowUpSearchRequest extends AbstractSearcherRequest {

	private final String incidentId;

	public OperativeGpFollowUpSearchRequest(String incidentId, Integer size, Integer page, String query,
			String periodDateField, Date periodStartDate, Date periodEndDate, Order order, String orderByField) {

		super(size, page, query, periodDateField, periodStartDate, periodEndDate, order, orderByField);
		this.incidentId = incidentId;
	}

}
