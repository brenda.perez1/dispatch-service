package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class OpGpIncidentCorporationSearchResponse {

    private final Long operativeGroupId;
    private final Long corporationId;
    private final String name;
//    private final String description;
    private final Date assignedAt;
    private final String status;

}
