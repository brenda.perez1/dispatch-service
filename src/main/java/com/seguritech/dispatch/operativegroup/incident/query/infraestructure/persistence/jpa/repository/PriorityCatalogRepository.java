package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.domain.model.priority.entity.Priority;

@Repository
public interface PriorityCatalogRepository extends JpaRepository<Priority, Long> {
	
	@Query("SELECT cat.id FROM Priority cat")
	public List<Long> findIds();

}
