package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.specification;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.data.domain.Sort;
import javax.persistence.criteria.Order;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;

import com.domain.model.corporation.entity.CorporationEntity_;
import com.domain.model.corporation.entity.CorporationOperativeEntity_;
import com.domain.model.corporation.entity.EmergencyCorporationEntityPK_;
import com.domain.model.corporation.entity.EmergencyCorporationEntity_;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incident.entity.IncidentEntity_;
import com.domain.model.incident.entity.IncidentLocationEntity;
import com.domain.model.incident.entity.IncidentLocationEntity_;
import com.domain.model.incident.entity.IncidentSecundaryEntity_;
import com.domain.model.incident.entity.LocationMunicipalityEntity;
import com.domain.model.incident.entity.LocationMunicipalityEntity_;
import com.domain.model.incident.entity.LocationSettlementEntity;
import com.domain.model.incident.entity.LocationSettlementEntity_;
import com.domain.model.incident.entity.LocationStateEntity;
import com.domain.model.incident.entity.LocationStateEntity_;
import com.domain.model.national.entity.IncidentNationalSubtypeEntity_;
import com.domain.model.priority.entity.Priority_;
import com.domain.model.user.entity.UserEntity_;
import com.seguritech.dispatch.exception.NotFoundException;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.CorporationIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOrderFieldEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentPeriodFilterEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryStatusEnum;

public class IncidentSpecifications {	

	public static Specification<IncidentEntity> EmergencyIdEq(String emergencyId) {
	    	return (root, query, criteriaBuilder) -> criteriaBuilder
	    			.like(criteriaBuilder.lower(root.get(IncidentEntity_.ID)),  "%" + emergencyId.toLowerCase() + "%"
	                );
	}
	
    public static Specification<IncidentEntity> FolioLike(String folio) {
        return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.FOLIO)), "%" + folio.toLowerCase() + "%");
    }
    
	public static Specification<IncidentEntity> SecondaryFolioLike(String folio) {
		return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder.like(
				root.get(IncidentEntity_.SECUNDARY_INCIDENT).get(IncidentSecundaryEntity_.FOLIO),
				"%" + folio.toLowerCase() + "%");
	}
    
	public static Specification<IncidentEntity> statusEmergencyIq(String statusEmergency) {
		return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
				.equal(root.get(IncidentEntity_.STATUS), statusEmergency);
	};

    public static Specification<IncidentEntity> nationalSubTypeIdEq(String nationalSubTypeId) {
        return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
        		.equal(root.join(IncidentEntity_.NATIONAL_SUB_TYPE)
                        .get(IncidentNationalSubtypeEntity_.NATIONAL_SUB_TYPE_ID), nationalSubTypeId);
    }
    
    public static Specification<IncidentEntity> nationalSubTypeNameLike(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder
                .lower(root.get(IncidentEntity_.NATIONAL_SUB_TYPE)
                        .get(IncidentNationalSubtypeEntity_.NAME)),
        "%" + name.toLowerCase() + "%");
    }
    
    public static Specification<IncidentEntity> priorityIdEq(Long priorityId) {
        return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                .equal(root.get(IncidentEntity_.PRIORITY)
                        .get(Priority_.ID), priorityId);
    }
    
    public static Specification<IncidentEntity> priorityNamelike(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(criteriaBuilder
                .lower(root.get(IncidentEntity_.PRIORITY)
                        .get(Priority_.DESCRIPTION)),
        "%" + name.toLowerCase() + "%");
    }
    
    public static Specification<IncidentEntity> queryJoinEmergenciesCorporation(List<Long> corporationOperativeId, String status) {
		return (root, query, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();
			/*
			 * If you need any data from a child table add here...
			 */
			// Join tables first level
			Join<IncidentLocationEntity, IncidentEntity> incidentCorporations = root.join(IncidentEntity_.EMERGENCY_CORPORATIONS);
			predicates.add(criteriaBuilder.in(incidentCorporations.get(EmergencyCorporationEntity_.ID)
					.get(EmergencyCorporationEntityPK_.CORPORATION_OPERATIVE)
					.get(CorporationOperativeEntity_.ID)).value(corporationOperativeId));
			predicates.add(criteriaBuilder.equal(incidentCorporations.get(EmergencyCorporationEntity_.STATUS), Optional.ofNullable(status).isPresent() ? status : OpGpIncidentQueryStatusEnum.OPEN.name()));		
			return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
    
    public static Specification<IncidentEntity> statusIq(String status) {
		 return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
	                .equal(root.join(IncidentEntity_.EMERGENCY_CORPORATIONS).get(EmergencyCorporationEntity_.STATUS), status);
	};
	
	 public static Specification<IncidentEntity> corporationOperativeIdIn (List<Long> corporationOperativeId) {
		 return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
	                .in(root.join(IncidentEntity_.EMERGENCY_CORPORATIONS)
	                		.get(EmergencyCorporationEntity_.ID)
							.get(EmergencyCorporationEntityPK_.CORPORATION_OPERATIVE)
							.get(CorporationOperativeEntity_.ID)).value(corporationOperativeId);
	}
	 
	 public static  Specification<IncidentEntity> emergencyIdIn (Specification<IncidentEntity> subQuery) {

		 return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> {
			 Predicate predicate =subQuery.toPredicate(root, query, criteriaBuilder);
			 Subquery<IncidentEntity> subquery = query.subquery(IncidentEntity.class);
			 Root<IncidentEntity> coloums = subquery.from(IncidentEntity.class);
			 subquery.select(coloums.get(IncidentEntity_.ID))
			   .where(predicate);
			
			 
			 return criteriaBuilder
             .in(root
             		.get(IncidentEntity_.ID)).value(subquery);
		 };
	    }
	
	public static Specification<IncidentEntity> corporationIdEq(List<Long> corporationId) {
		  return (root, query, criteriaBuilder) -> criteriaBuilder
	                .in(root.join(IncidentEntity_.EMERGENCY_CORPORATIONS)
	                        .get(EmergencyCorporationEntity_.ID)
	                        .get(EmergencyCorporationEntityPK_.CORPORATION_OPERATIVE)
							.get(CorporationOperativeEntity_.CORPORATION)
	                        .get(CorporationEntity_.ID)
	                        )
	                .value(corporationId);
    }	

	public static Specification<IncidentEntity> corporationNameLike(String name) {
		return (Specification<IncidentEntity>) (root, query, criteriaBuilder) -> criteriaBuilder.like(
				criteriaBuilder.lower(root.join(IncidentEntity_.EMERGENCY_CORPORATIONS)
						.get(EmergencyCorporationEntity_.ID)
						.get(EmergencyCorporationEntityPK_.CORPORATION_OPERATIVE)
						.get(CorporationOperativeEntity_.CORPORATION)
						.get(CorporationEntity_.NAME)),
				"%" + name.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> countryAlternativeLike(String country) {
        return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.COUNTRY_ALTERNATIVE)),
                "%" + country.toLowerCase() + "%");
    }   
	
	public static Specification<IncidentEntity> stateAlternativeLike(String state) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.STATE_ALTERNATIVE)), 
                		"%" + state.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> municipalityAlternativeLike(String municipality) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.MUNICIPALITY_ALTERNATIVE)), 
                		"%" + municipality.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> settlementAlternativeLike(String settlement) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.SETTLEMENT_ALTERNATIVE)), 
                		"%" + settlement.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> streetLike(String street) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.STREET)), 
                		"%" + street.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> addressAlternativeLike(String address) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.ADDRESS_ALTERNATIVE)), 
                		"%" + address.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> bwnStreet1Like(String bwnStreet1) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.BWN_STREET1)), 
                		"%" + bwnStreet1.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> bwnStreet2Like(String bwnStreet2) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.BWN_STREET2)), 
                		"%" + bwnStreet2.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> referenceLike(String reference) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.REFERENCE)), 
                		"%" + reference.toLowerCase() + "%");
	}
	
	public static Specification<IncidentEntity> zipcodeLike(String zipcode) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.ZIP_CODE)),
                		"%" + zipcode + "%");
	}
	
	public static Specification<IncidentEntity> latitudeLike(Double latitude) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.LATITUDE)).as(String.class), 
                		"%" + latitude + "%" );
	}
	
	public static Specification<IncidentEntity> longitudeLike(Double longitude) {
		return (root, query, criteriaBuilder) -> criteriaBuilder
                .like(criteriaBuilder.lower(root.get(IncidentEntity_.LOCATION)
                        .get(IncidentLocationEntity_.LONGITUDE)).as(String.class), 
                		"%" + longitude + "%");
	}
	
	public static Specification<IncidentEntity> assignedUserIdEq(String userId) {
    	try {
    		return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(IncidentEntity_.createdByUser).get(UserEntity_.idUser), userId);
    	}
    	catch(NotFoundException e) {
    		throw new NotFoundException("Register not found", 0L);
    	}
    }
	
	public static Specification<IncidentEntity> queryJoin(String x) {
		return (root, query, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();
			/*
			 * If you need any data from a child table add here...
			 */
			// Join tables first level
			Join<IncidentLocationEntity, IncidentEntity> incidentLocation = root.join(IncidentEntity_.LOCATION,
					JoinType.LEFT);
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.LATITUDE).as(String.class)),
					"%" + x + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.LONGITUDE).as(String.class)),
					"%" + x + "%"));
			predicates.add(
					criteriaBuilder.like(criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.STREET)),
							"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.BWN_STREET1)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.BWN_STREET2)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(
					criteriaBuilder.like(criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.REFERENCE)),
							"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.COUNTRY_ALTERNATIVE)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.STATE_ALTERNATIVE)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.MUNICIPALITY_ALTERNATIVE)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.SETTLEMENT_ALTERNATIVE)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(incidentLocation.get(IncidentLocationEntity_.ADDRESS_ALTERNATIVE)),
					"%" + x.toLowerCase() + "%"));
			// Join tables second level
			Join<LocationStateEntity, IncidentLocationEntity> stateLocation = incidentLocation
					.join(IncidentLocationEntity_.STATE, JoinType.LEFT);
			Join<LocationMunicipalityEntity, IncidentLocationEntity> municipalityLocation = incidentLocation
					.join(IncidentLocationEntity_.MUNICIPALITY, JoinType.LEFT);
			Join<LocationSettlementEntity, IncidentLocationEntity> settlemantLocation = incidentLocation
					.join(IncidentLocationEntity_.SETTLEMENT, JoinType.LEFT);
			// Search second level
			predicates.add(criteriaBuilder.like(criteriaBuilder.lower(stateLocation.get(LocationStateEntity_.STATE)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(municipalityLocation.get(LocationMunicipalityEntity_.NAME)),
					"%" + x.toLowerCase() + "%"));
			predicates.add(criteriaBuilder.like(
					criteriaBuilder.lower(settlemantLocation.get(LocationSettlementEntity_.NAME)),
					"%" + x.toLowerCase() + "%"));
			return criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]));
		};
	}
    
    public static Specification<IncidentEntity> querySearch (String query) {
    	return Specification
    			.where(FolioLike(query)
    	                .or(EmergencyIdEq(query)))
    	              	.or(emergencyIdIn(nationalSubTypeNameLike(query))
    	              	.or(emergencyIdIn(SecondaryFolioLike(query))))
    	                .or(emergencyIdIn(queryJoin(query)))
    	                
    	            ;
    	
	}
    
    public static Specification<IncidentEntity> inLocationLike(String x) {
        return Specification
                .where(streetLike(x))
                .or(bwnStreet1Like(x))
                .or(bwnStreet2Like(x))
                .or(referenceLike(x))
                .or(countryAlternativeLike(x))
                .or(stateAlternativeLike(x))
                .or(municipalityAlternativeLike(x))
                .or(settlementAlternativeLike(x))
                .or(addressAlternativeLike(x));       
    }

    /**
     * Filter by Range createdAt
     **/
    public static Specification<IncidentEntity> createdAtDateRange(Date gt, Date lt) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.between(root.get(IncidentEntity_.CREATED_AT), gt, lt);
    }

    public static Map<String, String> dateRage() {
        return new HashMap<String, String>() {
        	
			private static final long serialVersionUID = 1L;

		{
            put(IncidentPeriodFilterEnum.creation_date.name(), IncidentEntity_.CREATED_AT);
        }};
    }
    
    public static Sort SortByCreatedAtDesc() {
        return Sort.by(Sort.Direction.DESC, IncidentEntity_.CREATED_AT);
    }
    	
   /* public static Sort SortByCreationDateDesc() {
		return Sort.by(Sort.Direction.DESC, EmergencyCorporationEntity_.CREATION_DATE);
	}*/
    
    public static Sort SortByUpdatedAtDesc() {
		return Sort.by(Sort.Direction.DESC, IncidentEntity_.UPDATED_AT);
	}
    
    public static Specification<IncidentEntity> filterSpecification(CorporationIncidentSearchRequest request) {    		
    		
    	return (root, query, cb) -> {
    		
    		query.distinct(true);
			
    		Expression<Object> e;
			List<Order> orders = new ArrayList<>();
			
			final IncidentOrderFieldEnum orderBy = Optional.ofNullable(IncidentOrderFieldEnum.valueOf(request.getOrderByField())).orElse(IncidentOrderFieldEnum.Default());
			final com.seguritech.platform.application.http.Order order = Optional.ofNullable(com.seguritech.platform.application.http.Order.valueOf(request.getOrder().name())).orElse(com.seguritech.platform.application.http.Order.DESC);

			switch (orderBy) {
			case createdAt:
				e = root.get(IncidentEntity_.CREATED_AT);
				break;
			default:
				e = root.get(IncidentEntity_.CREATED_AT);
			}
			
			query.orderBy(direction(cb, e, order, orders));
			return cb.conjunction();
		};
	}
    
    static List<Order> direction(CriteriaBuilder cb, Expression<Object> expression,
			com.seguritech.platform.application.http.Order order, List<Order> orders) {
		switch (order) {
		case ASC:
			orders.add(cb.asc(expression));
			break;
		case DESC:
			orders.add(cb.desc(expression));
			break;
		default:
			orders.add(cb.desc(expression));
		}
		return orders;
	}
    

}
