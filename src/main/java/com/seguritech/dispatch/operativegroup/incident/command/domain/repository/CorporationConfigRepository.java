package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

public interface CorporationConfigRepository {

    Boolean automaticAssignmentEnable();
}
