package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class CreateManualIncidentRequest {
    private Long sourceId;
    private String phoneNumber;
    private Long phoneNumberCountryId;
    private String description;
    private String userId;
    private Date eventDate;
    private String createdFrom;
    private Long operatingCorporationId;
    private Long profileId;

}
