package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationNotFoundException extends BusinessException {

    public IncidentCorporationNotFoundException(Object... args) {
        super(MessageResolver.INCIDENT_CORPORATION_NOT_FOUND_EXCEPTION, args, 3011L);
    }

}
