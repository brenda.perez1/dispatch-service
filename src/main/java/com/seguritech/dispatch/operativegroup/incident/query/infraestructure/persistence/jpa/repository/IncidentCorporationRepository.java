package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.domain.model.incident.entity.IncidentEntity;

@Repository
public interface IncidentCorporationRepository extends JpaRepository<IncidentEntity, String> {

	@Query("SELECT DISTINCT inc FROM IncidentEntity inc "
			+ "LEFT JOIN inc.emergencyCorporations ece "
			+ "LEFT JOIN inc.priority pri "
			+ "LEFT JOIN inc.location loc "
			+ "LEFT JOIN inc.nationalSubType nat "
			+ "LEFT JOIN loc.state st "
			+ "LEFT JOIN loc.settlement stl "
			+ "LEFT JOIN loc.municipality mun "
			+ "WHERE (:statusCorp IS NULL OR ece.status = :statusCorp) "
			+ "AND (coalesce(:idsOperativeCorporation) IS NULL OR ece.id.corporationOperative.id IN (:idsOperativeCorporation)) "
			+ "AND (coalesce(:idsCorporation) IS NULL OR ece.id.corporationOperative.corporation.id IN (:idsCorporation)) "
			+ "AND pri.id IN (:priority) "
			+ "AND (:status IS NULL OR inc.Status = :status) "
			+ "AND (cast(:ago AS timestamp) IS NULL OR ece.creationDate BETWEEN :ago AND :today) "
			+ "AND (:createdBy IS NULL OR inc.createdByUser.idUser = :createdBy) "
			+ "AND (:isAssigned IS NULL OR ece.isAssigned = :isAssigned) "
			//Filters for dates from the created_at field
			+ "AND (cast(:startPeriod AS timestamp) IS NULL OR inc.createdAt > :startPeriod) "
			+ "AND (cast(:endPeriod AS timestamp) IS NULL OR inc.createdAt < :endPeriod) "
			+ "AND (cast(:startDate AS timestamp) IS NULL OR inc.createdAt BETWEEN :startDate AND :endDate) "
			+ "AND ("
			+ ":query IS NULL "
			+ "OR UPPER(inc.folio) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(st.state) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(stl.name) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(mun.name) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.street) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.bwnStreet1) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.bwnStreet2) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR CONCAT(loc.latitude, '') LIKE %:query% "
			+ "OR CONCAT(loc.longitude, '') LIKE %:query% "
			+ "OR UPPER(loc.reference) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.countryAlternative) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.stateAlternative) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.municipalityAlternative) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.settlementAlternative) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(loc.addressAlternative) LIKE UPPER(concat('%',:query,'%')) "
			+ "OR UPPER(nat.name) LIKE UPPER(concat('%',:query,'%')) "
			+ ") "
			)
	Page<IncidentEntity> getIncidente(
			@Param("statusCorp")String statusCorp,
			@Param("query")String query, 
			@Param("priority") List<Long> priority,
			@Param("idsOperativeCorporation")List<Long> idsOperativeCorporation, 
			@Param("idsCorporation")List<Long> idsCorporation, 
			@Param("status")String status,
			@Param("startDate")Date startDate,
			@Param("endDate")Date endDate,
			@Param("startPeriod")Date startPeriod, 
			@Param("endPeriod") Date endPeriod,
			@Param("ago")Date ago, 
			@Param("today")Date today, 
			@Param("createdBy") String createdBy,
			@Param("isAssigned") Boolean isAssigned,
			Pageable pageable);
	
}
