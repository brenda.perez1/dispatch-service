package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import java.util.Date;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CorporationFollowUpCreatedEvent implements DomainEvent {

	private final String incidentId;
	private final Long corporationId;
	private final Long reasonId;
	private final Long statusId;
	private final Long oldStatusId;
	private final String observation;
	private final String researchFolder;
	private final String createdBy;
	private final Date createdAt;

}
