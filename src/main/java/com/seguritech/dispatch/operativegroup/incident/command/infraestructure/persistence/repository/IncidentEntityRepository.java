package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.domain.model.incident.entity.IncidentEntity;


public interface IncidentEntityRepository extends JpaRepository<IncidentEntity, String> ,
    JpaSpecificationExecutor<IncidentEntity> {}
