package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested;

import lombok.Getter;

@Getter
public class SuggestedCorporationBoundedModel {

    private final SuggestedCorporationBoundedId corporationBoundedId;

    public SuggestedCorporationBoundedModel(SuggestedCorporationBoundedId corporationBoundedId) {
        this.corporationBoundedId = corporationBoundedId;
    }
}
