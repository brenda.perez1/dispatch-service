package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.TemporalType;

import com.domain.model.incident.entity.*;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.*;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository.SubSetRepository;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.corporation.entity.CorporationEntity;
import com.domain.model.corporation.entity.CorporationOperativeEntity;
import com.domain.model.corporation.entity.EmergencyCancellationEntity;
import com.domain.model.corporation.entity.EmergencyCloseEntity;
import com.domain.model.corporation.entity.EmergencyCorporationEntity;
import com.domain.model.corporation.entity.EmergencyCorporationEntityPK;
import com.domain.model.corporation.entity.GeofenceEntity;
import com.domain.model.national.entity.IncidentClassificationEntity;
import com.domain.model.national.entity.IncidentNationalCategorySubtypeEntity;
import com.domain.model.national.entity.IncidentNationalClassificationEntity;
import com.domain.model.national.entity.IncidentNationalSubtypeEntity;
import com.domain.model.national.entity.IncidentNationalTypeEntity;
import com.domain.model.national.entity.SubSetEntity;
import com.domain.model.priority.entity.Priority;
import com.domain.model.source.entity.IncidentSource;
import com.domain.model.user.entity.UserEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.CorporationIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.domain.CorporationIncidentQueryRepository;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository.IncidentCorporationEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository.IncidentCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.specification.IncidentSpecifications;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.IncidentAdditionalClassificationMapper;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.IncidentSecundaryMapper;
import com.seguritech.dispatch.unitforce.command.infraestructure.impl.entity.IncidentTokenMapper;
import com.seguritech.platform.Service;

@Service
@Transactional(readOnly = true)
public class CorporationIncidentQueryRepositoryImpl implements CorporationIncidentQueryRepository {
	
	private final IncidentCorporationEntityRepository repository;
	private final IncidentCorporationRepository repositoryIncident;
	private final SubSetRepository subSetRepository;
	private final EntityManager entityManager;
    
	
	public CorporationIncidentQueryRepositoryImpl(IncidentCorporationEntityRepository repository, EntityManager entityManager,
												  IncidentCorporationRepository repositoryIncident, SubSetRepository subSetRepository) {
		this.repository = repository;		
		this.repositoryIncident = repositoryIncident;
		this.entityManager = entityManager;
		this.subSetRepository = subSetRepository;
	}

	@Override
	public PageDataProjectionExtendedDTO<IncidentResponseMin> search(CorporationIncidentSearchRequest request) {
		Pageable pageable = this.getPageable(request);
		Date dateE = null;
        Date date = null;
		if(!Objects.isNull(request.getMaxHours())){
        	if(request.getMaxHours ()>0) {
	        	LocalDateTime today = LocalDateTime.now();     
	        	dateE = java.sql.Timestamp.valueOf(today);
	            LocalDateTime minday = today.minusHours(request.getMaxHours());
	            date = java.sql.Timestamp.valueOf(minday);
        	}
        }
		String query = request.getQuery();
		if (request.getQuery() != null && this.isNumeric(request.getQuery())) {
			query = this.calculatedLenght(request.getQuery());
		}
		Date endPeriod = null;
		Date startPeriod = null;
		Date startBetween = null;
		Date endBetween = null;
		if (request.getPeriodDateField() != null && request.getPeriodDateField().equals("creation_date")) {
			if (request.getPeriodStartDate() != null && request.getPeriodEndDate() != null) {
				LocalDateTime local = LocalDateTime.ofInstant(request.getPeriodStartDate().toInstant(), ZoneId.systemDefault());
				startBetween = java.sql.Timestamp.valueOf(local);
				local = LocalDateTime.ofInstant(request.getPeriodEndDate().toInstant(), ZoneId.systemDefault());
				endBetween = java.sql.Timestamp.valueOf(local);;
			} else {
				startPeriod = request.getPeriodStartDate();
				endPeriod = request.getPeriodEndDate();
				if (startPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(startPeriod.toInstant(), ZoneId.systemDefault());
					startPeriod = java.sql.Timestamp.valueOf(local);
				}
				if (endPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(endPeriod.toInstant(), ZoneId.systemDefault());
					endPeriod = java.sql.Timestamp.valueOf(local);
				}
			}
		} else {
			if (request.getPeriodStartDate() != null && request.getPeriodEndDate() != null) {
				LocalDateTime local = LocalDateTime.ofInstant(request.getPeriodStartDate().toInstant(), ZoneId.systemDefault());
				startBetween = java.sql.Timestamp.valueOf(local);
				local = LocalDateTime.ofInstant(request.getPeriodEndDate().toInstant(), ZoneId.systemDefault());
				endBetween = java.sql.Timestamp.valueOf(local);;
			} else {
				startPeriod = request.getPeriodStartDate();
				endPeriod = request.getPeriodEndDate();
				if (startPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(startPeriod.toInstant(), ZoneId.systemDefault());
					startPeriod = java.sql.Timestamp.valueOf(local);
				}
				if (endPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(endPeriod.toInstant(), ZoneId.systemDefault());
					endPeriod = java.sql.Timestamp.valueOf(local);
				}
			}
		}
		
		Page<IncidentEntity> rs = this.repositoryIncident.getIncidente(
				request.getStatus() != null ? request.getStatus().getValue() : null,						
				query,
				request.getPriorityId(),
				request.getCorporationId() != null && !request.getCorporationId().isEmpty() ? null : request.getCorporationOperativeId(),
				request.getCorporationId() != null && !request.getCorporationId().isEmpty() ? request.getCorporationId() : null,
				request.getStatusEmergency() != null ? request.getStatusEmergency().getValue() : null,								
				startBetween,
				endBetween,
				startPeriod,
				endPeriod,
				date,
				dateE,
				request.getCreatedBy(),
				request.getAssignedStatus(),
				pageable);
		return new PageDataProjectionExtendedDTO<>(
				this.setResponseIncident(rs.get().map(this::toResponseMin).collect(Collectors.toList()), request.getStatus() == null ? null : request.getStatus().getValue()), 
				rs.getTotalElements()
		);
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public PageDataProjectionExtendedDTO<IncidentResponseMin> searchAdvance(CorporationIncidentSearchRequest request) {
		
		Pageable pageable = this.getPageable(request);
		Date dateE = null;
        Date date = null;
		if(!Objects.isNull(request.getMaxHours())){
        	if(request.getMaxHours ()>0) {
	        	LocalDateTime today = LocalDateTime.now();     
	        	dateE = java.sql.Timestamp.valueOf(today);
	            LocalDateTime minday = today.minusHours(request.getMaxHours());
	            date = java.sql.Timestamp.valueOf(minday);
        	}
        }
		String query = request.getQuery();
		if (request.getQuery() != null && this.isNumeric(request.getQuery())) {
			query = this.calculatedLenght(request.getQuery());
		}
		Date endPeriod = null;
		Date startPeriod = null;
		Date startBetween = null;
		Date endBetween = null;
		
		if (request.getPeriodDateField() != null && request.getPeriodDateField().equals("creation_date")) {
			if (request.getPeriodStartDate() != null && request.getPeriodEndDate() != null) {
				LocalDateTime local = LocalDateTime.ofInstant(request.getPeriodStartDate().toInstant(), ZoneId.systemDefault());
				startBetween = java.sql.Timestamp.valueOf(local);
				local = LocalDateTime.ofInstant(request.getPeriodEndDate().toInstant(), ZoneId.systemDefault());
				endBetween = java.sql.Timestamp.valueOf(local);;
			} else {
				startPeriod = request.getPeriodStartDate();
				endPeriod = request.getPeriodEndDate();
				if (startPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(startPeriod.toInstant(), ZoneId.systemDefault());
					startPeriod = java.sql.Timestamp.valueOf(local);
				}
				if (endPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(endPeriod.toInstant(), ZoneId.systemDefault());
					endPeriod = java.sql.Timestamp.valueOf(local);
				}
			}
		} else {
			if (request.getPeriodStartDate() != null && request.getPeriodEndDate() != null) {
				LocalDateTime local = LocalDateTime.ofInstant(request.getPeriodStartDate().toInstant(), ZoneId.systemDefault());
				startBetween = java.sql.Timestamp.valueOf(local);
				local = LocalDateTime.ofInstant(request.getPeriodEndDate().toInstant(), ZoneId.systemDefault());
				endBetween = java.sql.Timestamp.valueOf(local);;
			} else {
				startPeriod = request.getPeriodStartDate();
				endPeriod = request.getPeriodEndDate();
				if (startPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(startPeriod.toInstant(), ZoneId.systemDefault());
					startPeriod = java.sql.Timestamp.valueOf(local);
				}
				if (endPeriod != null) {
					LocalDateTime local = LocalDateTime.ofInstant(endPeriod.toInstant(), ZoneId.systemDefault());
					endPeriod = java.sql.Timestamp.valueOf(local);
				}
			}
		}
		
		boolean isEmergencyStateRequest = Objects.nonNull(request.getStatusEmergency()) ? true : false;
		
		String mainSql = "SELECT DISTINCT"
				+ "        e.id_emergency AS id, "
				+ "        e.folio_incidente AS folio,"
				+ "		   e.status AS statusEmergency,"
				+ "		   e.description,"
				+ "		   e.sense_detection AS senseDetection,"
				+ "		   e.is_manual AS isManual,"
				+ "		   e.event_date as eventDate,"
				+ "        e.creation_date AS createdAt, "
				+ "        e.update_date AS updatedAt,"
				+ "        ns.id_national_subtype AS nationalSubTypeId, "
				+ "        ns.name AS nationalSubTypeName, "
				+ "        ns.clave AS nationalSubTypeClave, "
				+ "        ns.is_subtype AS nationalSubTypeIsSubType, "
				+ "		   cnt.id_national_type AS nationalTypeId, "
				+ "        cnt.name AS nationalTypeName, "
				+ "        p.id_priority AS priorityId, "
				+ "        p.name as priorityName, "
				+ "        p.priority_desc AS priorityDescription, "
				+ "        p.text_color AS priorityTextColor, "
				+ "        p.back_color AS priorityBackColor, "
				+ "        ces.id_emergency_source AS sourceId, "
				+ "        ces.name AS sourceName, "
				+ "        ces.clave AS sourceClave, "
				+ "        ces.description AS sourceDescription, "
				+ "        ces.icon AS sourceIcon, "
				+ "        ces.show_alert_identifier AS showAlertIdentifier, "
				+ "        ces.play_sound AS playSound, "
				+ "        el.id_emergency_location AS emergencyLocationId, "
				+ "        el.latitude AS emergencyLocationLatitude, "
				+ "        el.longitude AS emergencyLocationLongitude, "
				+ "        el.address_alternative AS emergencyLocationAddressAlternative, "
				+ "        el.created_at AS emergencyLocationCreatedAt,"
				+ "        el.municipality_alternative AS emergencyLocationMunicipalityAlternative, "
				+ "        el.state_alternative AS emergencyLocationStateAlternative, "
				+ "        el.bwn_street1 AS bwnStreet1, "
				+ "        el.bwn_street2 AS bwnStreet2, "
				+ "		   u.id_user AS userId, "
				+ "		   u.alias AS userName, "
				+ "		   es.folio_secundary as secundaryFolio "
				+ "    FROM "
				+ "        {h-schema}emergency e "
				+ "	    LEFT OUTER JOIN {h-schema}emergency_corporation ec ON e.id_emergency = ec.id_emergency "
				+ "	    LEFT JOIN {h-schema}corporation_operative co ON ec.id_corporation_operative = co.id_corporation_operative "	
				+ "	    LEFT JOIN {h-schema}cat_corporation c ON co.id_corporation = c.id_corporation "
				+ "	    LEFT OUTER JOIN {h-schema}cat_priority p ON e.id_emergency_priority = p.id_priority "
				+ "	    LEFT OUTER JOIN {h-schema}cat_emergency_source ces ON e.id_emergency_source = ces.id_emergency_source "
				+ "	    LEFT OUTER JOIN {h-schema}emergency_location el ON e.id_emergency = el.id_emergency "
				+ "	    LEFT OUTER JOIN {h-schema}cat_national_subtype ns ON e.id_national_subtype = ns.id_national_subtype "
				+ "		LEFT OUTER JOIN {h-schema}cat_national_type cnt ON ns.id_national_type = cnt.id_national_type "
				+ "	    LEFT OUTER JOIN {h-schema}cat_state s ON el.id_state = s.id "
				+ "	    LEFT OUTER JOIN {h-schema}cat_settlement st ON el.id_settlement = st.id "
				+ "	    LEFT OUTER JOIN {h-schema}cat_municipality m ON el.id_municipality = m.id "
				+ "		LEFT OUTER JOIN {h-schema}emergency_secundary es ON e.id_emergency = es.id_emergency "
				+ "	    INNER JOIN {h-schema}cat_user u ON e.created_by = u.id_user "
				+ "     WHERE ".concat(isEmergencyStateRequest						
				? "        (p.id_priority IN (:priority)) "			
				+ "        AND (:statusEmergency IS NULL OR e.status = :statusEmergency) "
				+ "        AND (:createdBy IS NULL OR u.id_user = :createdBy) "
				+ "        AND (CAST(:startPeriod as timestamp) IS NULL OR e.creation_date > :startPeriod) "
				+ "        AND (CAST(:endPeriod as timestamp) IS NULL OR e.creation_date < :endPeriod) "
				+ "        AND (CAST(:startDate as timestamp) IS NULL OR e.creation_date BETWEEN :startDate AND :endDate) "
				+ "        AND (:query IS NULL "
				+ "            OR UPPER(e.folio_incidente) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(ns.name) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(s.state) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(st.settlement) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(m.municipality) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.street) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR (el.latitude||'') LIKE :query "
				+ "            OR (el.longitude||'') LIKE :query "				
				+ "            OR UPPER(el.country_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.state_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.municipality_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.settlement_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.address_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "        ) "
				: "        (p.id_priority IN (:priority)) "		
				+ "        AND (:statusCorp IS NULL OR ec.status = :statusCorp) "
				+ (request.getCorporationOperativeId() != null && !request.getCorporationOperativeId().isEmpty() ? " AND ec.id_corporation_operative IN (" + String.join(",", request.getCorporationOperativeId().stream().map(id -> String.valueOf(id)).collect(Collectors.toList())) + ")" : "")
				+ (request.getCorporationId() != null && !request.getCorporationId().isEmpty() ? " AND ec.id_corporation IN (" + String.join(",", request.getCorporationId().stream().map(id -> String.valueOf(id)).collect(Collectors.toList())) + ")" : "") 
				+          (request.getFollowUpStatusId() != null && !request.getFollowUpStatusId().isEmpty() ? " AND ec.id_followup_status IN (" + String.join(",", request.getFollowUpStatusId().stream().map(id -> String.valueOf(id)).collect(Collectors.toList())) + ")" : "")
				+ "        AND (cast(:ago as timestamp) IS NULL OR ec.creation_date between :ago AND :today) "
				+ "        AND (:createdBy IS NULL OR u.id_user = :createdBy) "
				+ "        AND (:isAssignedNull IS NULL OR ("
				+ "                SELECT"
				+ "                    CASE "
				+ "                        WHEN (COUNT(um.active) > 0) THEN true "
				+ "                        else false "
				+ "                    END "
				+ "                FROM"
				+ "                    {h-schema}unit_mission um "
				+ "                INNER JOIN"
				+ "                    {h-schema}unit_force_config ufc "
				+ "                        ON um.id_unit_force_config = ufc.id_unit_force_config "
				+ "                WHERE"
				+ "                    um.id_emergency = ec.id_emergency "
				+ "                    AND um.active = true"
				+ "            ) = :isAssigned"
				+ "        ) "
				+ "        AND (CAST(:startPeriod as timestamp) IS NULL OR e.creation_date > :startPeriod) "
				+ "        AND (CAST(:endPeriod as timestamp) IS NULL OR e.creation_date < :endPeriod) "
				+ "        AND (CAST(:startDate as timestamp) IS NULL OR e.creation_date BETWEEN :startDate AND :endDate) "
				+ "        AND (:query IS NULL "
				+ "            OR UPPER(e.folio_incidente) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(ns.name) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(s.state) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(st.settlement) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(m.municipality) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.street) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR (el.latitude||'') LIKE :query "
				+ "            OR (el.longitude||'') LIKE :query "				
				+ "            OR UPPER(el.country_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.state_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.municipality_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.settlement_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "            OR UPPER(el.address_alternative) LIKE UPPER(('%'||:query||'%')) "
				+ "        ) "
				+ "        AND ((:isNotAssociatedAsParent IS NULL OR :isNotAssociatedAsParent IS false) OR NOT EXISTS(SELECT 1 FROM {h-schema}emergency_association ea WHERE ea.id_emergency_primary = e.id_emergency )) "
				+ "        AND ((:isNotAssociatedAsChild  IS NULL OR :isNotAssociatedAsChild IS false) OR NOT EXISTS(SELECT 1 FROM {h-schema}emergency_association ea WHERE ea.id_emergency_second = e.id_emergency )) "
				+ "        AND (:nationalSubType IS NULL OR ns.id_national_subtype =:nationalSubType) " )
				+ "        ORDER BY e.creation_date " + (request.getOrder() == null ? "DESC" : request.getOrder());
		
		String sql = " WITH records AS (" + mainSql + "), "
				+ " total_records AS (SELECT COUNT(*) AS total FROM records), "
				+ " filtered_records AS (SELECT * FROM records OFFSET :page LIMIT :limit), "
				+ " corporation_info AS ( "
				+ " 					 SELECT "
				+ " 						co.id_corporation_operative AS corporationOperativeId, co.clave AS corporationOperativeClave, co.active AS corporationOperativeActive, co.name AS corporationOperativeName, "
				+ " 						cc.id_corporation AS corporationId, cc.corporation_name AS corporationName, cc.corporation_desc AS corporationDesc, cc.icon AS corporationIcon, cc.active AS corporationActive,"
				+ " 						cg.id_geofence AS geofenceId, cg.name AS geofenceName, cg.description AS geofenceDescription, cg.type AS geofenceType, cg.color AS geofenceColor, cg.opacity AS geofenceOpacity, cg.active as geofenceActive"
				+ "  					   FROM {h-schema}corporation_operative co INNER JOIN {h-schema}cat_corporation cc ON co.id_corporation = cc.id_corporation"
				+ "  				 INNER JOIN {h-schema}operative_groups_geofence ogg ON co.id_corporation_operative = ogg.id_corporation_operative "
				+ "  			     INNER JOIN {h-schema}cat_geofence cg ON ogg.id_geofence = cg.id_geofence " 
				+ "                    ) "
				+ " SELECT r.*, "
				+ "    (SELECT JSONB_AGG(ROW_TO_JSON(ecc)) FROM ("
				+ "        SELECT ec.created_by as emergencyCorporationCreatedBy, ec.assigned_unit_force_date as assignedUnitForceDate, ec.creation_date as emergencyCorporationCreatedAt, ec.status as emergencyCorporationStatus, ec.update_date as emergencyCorporationUpdatedAt, ec.updated_by as emergencyCorporationUpdatedBy,"
				+ "               ci.* "
				+ "   	     FROM {h-schema}emergency_corporation ec "
				+ "      INNER JOIN corporation_info ci on ec.id_corporation_operative = ci.corporationOperativeId "
				+ "      WHERE ec.id_emergency = r.id"
				+ "  ) ecc) as emergencyCorporations, "
				+ "    (SELECT JSONB_AGG(ROW_TO_JSON(e)) "
				+ "       FROM {h-schema}emergency_association ea "
				+ "      INNER JOIN (SELECT id_emergency AS id, folio_incidente AS folio FROM {h-schema}emergency) e ON ea.id_emergency_second = e.id WHERE ea.id_emergency_primary = r.id"
				+ "    ) AS associates, "
				+ "	  EXISTS(SELECT 1 FROM {h-schema}emergency_association ea WHERE ea.id_emergency_primary = r.id) AS isAsociation,"
				+ "   (SELECT total FROM total_records) AS total "
				+ "   FROM filtered_records r";

		SQLQuery sqlQuery = entityManager.createNativeQuery(sql).unwrap(SQLQuery.class)
				.addScalar("id", StringType.INSTANCE)
				.addScalar("folio", StringType.INSTANCE)
				.addScalar("statusEmergency", StringType.INSTANCE)
				.addScalar("description", StringType.INSTANCE)
				.addScalar("senseDetection", StringType.INSTANCE)
				.addScalar("isManual", BooleanType.INSTANCE)
				.addScalar("eventDate", TimestampType.INSTANCE)
				.addScalar("createdAt", TimestampType.INSTANCE)
				.addScalar("updatedAt", TimestampType.INSTANCE)
				.addScalar("nationalSubTypeId", LongType.INSTANCE)
				.addScalar("nationalSubTypeName", StringType.INSTANCE)
				.addScalar("nationalSubTypeClave", StringType.INSTANCE)
				.addScalar("nationalSubTypeIsSubType", BooleanType.INSTANCE)
				.addScalar("nationalTypeId", LongType.INSTANCE)
				.addScalar("nationalTypeName", StringType.INSTANCE)
				.addScalar("emergencyCorporations", StringType.INSTANCE)
				.addScalar("secundaryFolio", StringType.INSTANCE)
				.addScalar("priorityId", LongType.INSTANCE)
				.addScalar("priorityName", StringType.INSTANCE)
				.addScalar("priorityDescription", StringType.INSTANCE)
				.addScalar("priorityTextColor", StringType.INSTANCE)
				.addScalar("priorityBackColor", StringType.INSTANCE)
				.addScalar("sourceId", LongType.INSTANCE)
				.addScalar("sourceName", StringType.INSTANCE)
				.addScalar("sourceClave", StringType.INSTANCE)
				.addScalar("sourceDescription", StringType.INSTANCE)
				.addScalar("sourceIcon", StringType.INSTANCE)
				.addScalar("showAlertIdentifier", BooleanType.INSTANCE)
				.addScalar("playSound", BooleanType.INSTANCE)
				.addScalar("emergencyLocationId", LongType.INSTANCE)
				.addScalar("emergencyLocationLatitude", DoubleType.INSTANCE)
				.addScalar("emergencyLocationLongitude", DoubleType.INSTANCE)
				.addScalar("emergencyLocationAddressAlternative", StringType.INSTANCE)
				.addScalar("emergencyLocationCreatedAt", TimestampType.INSTANCE)
				.addScalar("emergencyLocationMunicipalityAlternative", StringType.INSTANCE)
				.addScalar("emergencyLocationStateAlternative", StringType.INSTANCE)
				.addScalar("bwnStreet1", StringType.INSTANCE)
				.addScalar("bwnStreet2", StringType.INSTANCE)
				.addScalar("associates", StringType.INSTANCE)
				.addScalar("isAsociation", BooleanType.INSTANCE)
				.addScalar("userId", StringType.INSTANCE)
				.addScalar("userName", StringType.INSTANCE)
				.addScalar("total", LongType.INSTANCE);

		if(isEmergencyStateRequest) {
			sqlQuery.setParameter("priority", request.getPriorityId())
				.setString("statusEmergency", request.getStatusEmergency().getValue())
				.setString("createdBy", request.getCreatedBy())
				.setParameter("startPeriod", startPeriod, TemporalType.TIMESTAMP)
				.setParameter("endPeriod", endPeriod, TemporalType.TIMESTAMP)
				.setParameter("startDate", startBetween, TemporalType.TIMESTAMP)
				.setParameter("endDate", endBetween, TemporalType.TIMESTAMP)
				.setString("query", query)
				.setParameter("limit", pageable.getPageSize())
				.setParameter("page", pageable.getPageNumber() > 0 ? (pageable.getPageNumber() * pageable.getPageSize()) : 0);

		} else {
			sqlQuery.setString("statusCorp", request.getStatus() != null ? request.getStatus().getValue() : null)
				.setParameter("priority", request.getPriorityId())
				.setParameter("ago", date, TemporalType.TIMESTAMP)
				.setParameter("today", dateE, TemporalType.TIMESTAMP)
				.setString("createdBy", request.getCreatedBy())
				.setString("isAssignedNull", request.getAssignedStatus() != null ? "NO_VACIO" : null)
				.setBoolean("isAssigned", request.getAssignedStatus() != null ? request.getAssignedStatus() : false)
				.setParameter("startPeriod", startPeriod, TemporalType.TIMESTAMP)
				.setParameter("endPeriod", endPeriod, TemporalType.TIMESTAMP)
				.setParameter("startDate", startBetween, TemporalType.TIMESTAMP)
				.setParameter("endDate", endBetween, TemporalType.TIMESTAMP)
				.setString("query", query)
				.setParameter("limit", pageable.getPageSize())
				.setParameter("page", pageable.getPageNumber() > 0 ? (pageable.getPageNumber() * pageable.getPageSize()) : 0)
				.setParameter("isNotAssociatedAsParent", request.getIsNotAssociatedAsParent())
				.setParameter("isNotAssociatedAsChild", request.getIsNotAssociatedAsChild())
				.setParameter("nationalSubType", request.getNationalSubTypeId(), LongType.INSTANCE);
		}

		List<IncidentCorporationCallResponse> list = sqlQuery.setResultTransformer(Transformers.aliasToBean(IncidentCorporationCallResponse.class)).getResultList();
		
		return new PageDataProjectionExtendedDTO<>(this.setResponseIncident(list.stream().map(this::toResponseMin).collect(Collectors.toList()), request.getStatus() == null ? null : request.getStatus().getValue()), list != null && !list.isEmpty() ? list.stream().findFirst().get().getTotal() : 0L);

	}
	
	public Pageable getPageable(CorporationIncidentSearchRequest request) {
		Pageable pageable = null;
		switch (request.getOrderByField()) {
		case "createdAt":
			if (request.getOrder().toString().equals("ASC")) {
				pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by(Sort.Direction.ASC, "createdAt"));
			} else {
				pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by(Sort.Direction.DESC, "createdAt"));
			}
			break;
		default:
			if (request.getOrder().toString().equals("ASC")) {
				pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by(Sort.Direction.ASC, "createdAt"));
			} else {
				pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by(Sort.Direction.DESC, "createdAt"));
			}
			break;
		}
		return pageable;
	}
	
	public boolean isNumeric (String s) {
		try {
			Double.parseDouble(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	public String calculatedLenght(String s) {
		String returnMax = s;
		if (s.indexOf(".") == -1) {
			return s;
		}
		String[] pointerPosition = s.split("\\.");
		int afterPoint = pointerPosition[1].length();
		if (afterPoint > 13) {
			returnMax = pointerPosition[0] + "." + pointerPosition[1].substring(0, 13);
		}
		return returnMax;
	}
	
	public List<IncidentResponseMin> setResponseIncident (List<IncidentResponseMin> incident, String status) {
		if (status == null) {
			return incident;
		}
		List<IncidentResponseMin> response = new ArrayList<>();
		for (IncidentResponseMin incidentResponseMin : incident) {
			List<IncidentCorporationSearchDetailResponseMin> corp = new ArrayList<>();
			for (IncidentCorporationSearchDetailResponseMin emergencyCorporation : incidentResponseMin.getEmergencyCorporations()) {
				if (emergencyCorporation.getStatus().equals(status)) {
					corp.add(emergencyCorporation);
				}
			}
			incidentResponseMin.setEmergencyCorporations(corp);
			response.add(incidentResponseMin);
		}
		return response;
	}
	
	public List<IncidentResponseMin> setResponse (Page<EmergencyCorporationEntity> emergencyCorporation) {
		List<IncidentResponseMin> incidentResponseMins = new ArrayList<>();
		for (EmergencyCorporationEntity emergencyCorporationEntity : emergencyCorporation) {
			if (exists(incidentResponseMins, emergencyCorporationEntity.getId().getIncident().getId())) {
				for (IncidentResponseMin incidentResponseMin : incidentResponseMins) {
					if (incidentResponseMin.getId().equals(emergencyCorporationEntity.getId().getIncident().getId())) {
						List<IncidentCorporationSearchDetailResponseMin> currentList = incidentResponseMin.getEmergencyCorporations();
						currentList = Stream.concat(currentList.stream(), this.setGroupOperative(emergencyCorporationEntity).stream()).collect(Collectors.toList());
						incidentResponseMin.setEmergencyCorporations(currentList);
						break;
					}
				}
			} else {
				IncidentResponseMin incidentResponseMin = new IncidentResponseMin(
						emergencyCorporationEntity.getId().getIncident().getId(),
						emergencyCorporationEntity.getId().getIncident().getFolio(),
						emergencyCorporationEntity.getId().getIncident().getStatus(),
						emergencyCorporationEntity.getId().getIncident().getDescription(),
						emergencyCorporationEntity.getId().getIncident().getSenseDetection(),
						emergencyCorporationEntity.getId().getIncident().getIsManual(),
						emergencyCorporationEntity.getId().getIncident().getEventDate(),
						emergencyCorporationEntity.getId().getIncident().getCreatedAt(),
						emergencyCorporationEntity.getId().getIncident().getUpdatedAt(),
						this.setNational(emergencyCorporationEntity.getId().getIncident().getNationalSubType()),
						this.setPriority(emergencyCorporationEntity.getId().getIncident().getPriority()),
						this.setSource(emergencyCorporationEntity.getId().getIncident().getSource()),
						this.setLocation(emergencyCorporationEntity.getId().getIncident().getLocation()),
						this.setUser(emergencyCorporationEntity.getId().getIncident().getCreatedByUser()),
						this.setIncidentAsociated(emergencyCorporationEntity.getId().getIncident().getAssociates()),
						this.setGroupOperative(emergencyCorporationEntity),//groupOperative
						this.setToken(emergencyCorporationEntity.getId().getIncident().getTokens()),
						emergencyCorporationEntity.getId().getIncident().getSecundaryIncident() != null ?	IncidentSecundaryMapper.toResponse(emergencyCorporationEntity.getId().getIncident().getSecundaryIncident()) : null,
					    !emergencyCorporationEntity.getId().getIncident().getAdditionalNationalSubTypes().isEmpty() ? IncidentAdditionalClassificationMapper.toIncidentAdditionalClassificationResponse(emergencyCorporationEntity.getId().getIncident().getAdditionalNationalSubTypes()) : null,
					    emergencyCorporationEntity.getId().getIncident().getIsAsociation()
				);
				incidentResponseMins.add(incidentResponseMin);
			}
		}
		
		return incidentResponseMins;
	}
	
	public List<IncidentCorporationSearchDetailResponseMin> setGroupOperative(EmergencyCorporationEntity opCorporationEntity) {
		List<IncidentCorporationSearchDetailResponseMin> corporationSearchDetailResponseMins = new ArrayList<>();
		IncidentCorporationSearchDetailResponseMin corporationSearchDetailResponseMin = new IncidentCorporationSearchDetailResponseMin(
				this.setID(opCorporationEntity.getId()),
				opCorporationEntity.getStatus(),
				opCorporationEntity.getMission(),
				opCorporationEntity.getCreationDate(),
				opCorporationEntity.getUpdateDate(),
				opCorporationEntity.getAssignedUnitForceDate(),
				Boolean.FALSE,
				opCorporationEntity.getCreatedBy(),
				opCorporationEntity.getUpdateBy(),
				this.setGeofence(opCorporationEntity.getGeofence())
		);
		corporationSearchDetailResponseMins.add(corporationSearchDetailResponseMin);
		return corporationSearchDetailResponseMins;
	}
	
	public IncidentCorporationSearchPKResponse setID(EmergencyCorporationEntityPK emergencyCorporationEntityPK) {
		CorporationOperativeResponse corporationOperativeResponse = new CorporationOperativeResponse(
				emergencyCorporationEntityPK.getCorporationOperative().getId(),
				emergencyCorporationEntityPK.getCorporationOperative().getName(),
				emergencyCorporationEntityPK.getCorporationOperative().getClave(),
				emergencyCorporationEntityPK.getCorporationOperative().getActive(),
				this.setCorporation(emergencyCorporationEntityPK.getCorporationOperative().getCorporation()));
		IncidentCorporationSearchPKResponse response = new IncidentCorporationSearchPKResponse(corporationOperativeResponse);
		return response;
	}
	
	public CorporationResponse setCorporation(CorporationEntity corporationEntity) {
		return new CorporationResponse(
				corporationEntity.getId(),
				corporationEntity.getName(),
				corporationEntity.getClave(),
				corporationEntity.getDescription(),
				corporationEntity.getIcon(),
				corporationEntity.getActive());
	}
	
	public GeofenceResponse setGeofence (GeofenceEntity geofenceEntity) {
		return new GeofenceResponse(
				geofenceEntity.getId(),
				geofenceEntity.getName(),
				geofenceEntity.getDescription(),
				geofenceEntity.getType(),
				geofenceEntity.getColor(),
				geofenceEntity.getOpacity(),
				geofenceEntity.getActive());
	}
	
	public NationalSubTypeResponseMin setNational(IncidentNationalSubtypeEntity incidentNationalSubtypeEntity) {
		NationalTypeResponseMin nat = new NationalTypeResponseMin(incidentNationalSubtypeEntity.getNationalType().getNationalTypeId(), incidentNationalSubtypeEntity.getNationalType().getName());
		NationalSubTypeResponseMin sub = new NationalSubTypeResponseMin(
				incidentNationalSubtypeEntity.getNationalSubTypeId(),
				incidentNationalSubtypeEntity.getName(),
				incidentNationalSubtypeEntity.getClave(),
				incidentNationalSubtypeEntity.isSubtype(),
				nat);
		return sub;
	}
	
	public PriorityResponse setPriority(Priority priority) {
		return new PriorityResponse(
				priority.getId(),
				priority.getName(),
				priority.getDescription(),
				priority.getTextColor(),
				priority.getBackColor());
	}
	
	public IncidentSourceResponse setSource(IncidentSource source) {
		return new IncidentSourceResponse(
				source.getId(),
				source.getName(),
				source.getClave(),
				source.getDescription(),
				source.getIcon(),
				source.getShowAlertIdentifier(),
				source.getPlaySound()
		);
	}
	
	public IncidentLocationResponseMin setLocation(IncidentLocationEntity location) {
		return new IncidentLocationResponseMin(
				location.getIdEmergencyLocation(),
				location.getLatitude(),
				location.getLongitude(),
				location.getAddressAlternative(),
				location.getCreatedAt(),
				Optional.ofNullable(location.getMunicipalityAlternative()).orElse(null),
				Optional.ofNullable(location.getStateAlternative()).orElse(null), null, null);
	}
	
	public AppUserResponse setUser(UserEntity user) {
		return new AppUserResponse(user.getIdUser(), user.getAlias());
	}
	
	public Set<IncidentAssociatedResponseMin> setIncidentAsociated(Set<IncidentAssociationEntity> inAssociation) {
		Set<IncidentAssociatedResponseMin> incidentAssociatedResponseMins = new HashSet<>();
		for (IncidentAssociationEntity incidentAssociationEntity : inAssociation) {
			incidentAssociatedResponseMins.add(new IncidentAssociatedResponseMin(
					incidentAssociationEntity.getId().getIdEmergencySecond().getId(),
					incidentAssociationEntity.getId().getIdEmergencySecond().getFolio()));
		}
		return incidentAssociatedResponseMins;
	}
	
	public IncidentTokenResponse setToken (List<IncidentTokenEntity> tokens) {
		return !tokens.isEmpty() ?	IncidentTokenMapper.toResponse(tokens.stream().filter(z -> z.isActive()).findFirst().get()) : null;
	}
	
	private static boolean exists(List<IncidentResponseMin> list, String idEmergency) {
        return list.stream()
            .filter(x -> x.getId().equals(idEmergency))
            .count() > 0;
    }
	
	@Override
    public PageDataProjectionDTO<IncidentSubSetResponse> searchByEmergencyId(IncidentSearchRequest request) {
		Page<IncidentEntity>result = this.repository.findAll(toSpecification(request),
	            PageRequest.of(request.getPage(), request.getSize()));
		return new PageDataProjectionDTO<>(result.get().map(this::toResponse).collect(Collectors.toList()),
	            result.getTotalElements());
	}
	
	private Specification<IncidentEntity> toSpecification(IncidentSearchRequest request) {
		
        List<Specification<IncidentEntity>> specifications = new ArrayList<>();

        Optional.ofNullable(request.getEmergencyId())
            .ifPresent(x -> specifications.add(IncidentSpecifications.EmergencyIdEq(x)));
        
        Specification<IncidentEntity> temp = (root, query, criteriaBuilder) -> null;
        for (Specification<IncidentEntity> specification : specifications) {
            temp = temp.and(specification);
        }
        return temp;
    }
	
	//DATA MIN LIST	
	private IncidentResponseMin toResponseMin (IncidentEntity entity) {				
		
		return Optional.ofNullable(entity).map(x -> new IncidentResponseMin(	
	        		x.getId(),
	        		x.getFolio(),
	        		x.getStatus(),
	        		x.getDescription(),
	        		x.getSenseDetection(),
	        		x.getIsManual(),
	        		x.getEventDate(),
	        		x.getCreatedAt(),
	        		x.getUpdatedAt(),
	        		this.toResponse(x.getNationalSubType()),
	                this.toResponse(x.getPriority()),
	                this.toResponse(x.getSource()),
	                this.toResponseMin(x.getLocation()),
	                this.toResponse(x.getCreatedByUser()),
	                this.toResponseMin(x.getAssociates()),
			        this.toResponseMin(x.getEmergencyCorporations()),
			        !entity.getTokens().isEmpty() ?	IncidentTokenMapper.toResponse(x.getTokens().stream().filter(z -> z.isActive()).findFirst().get()) : null,
		            x.getSecundaryIncident() != null ?	IncidentSecundaryMapper.toResponse(x.getSecundaryIncident()) : null,
		            !entity.getAdditionalNationalSubTypes().isEmpty() ? IncidentAdditionalClassificationMapper.toIncidentAdditionalClassificationResponse(entity.getAdditionalNationalSubTypes()) : null,
		            x.getIsAsociation()
	    )).orElse(null);
	}
	
	private IncidentLocationResponseMin toResponseMin(IncidentLocationEntity entity) {
		return new IncidentLocationResponseMin(
				entity.getIdEmergencyLocation(),
				entity.getLatitude(),
				entity.getLongitude(),
				entity.getAddressAlternative(),
				entity.getCreatedAt(),
				Optional.ofNullable(entity.getMunicipalityAlternative()).orElse(null),
				Optional.ofNullable(entity.getStateAlternative()).orElse(null),
				null,
				null
		);
	}
	
	private Set<IncidentAssociatedResponseMin> toResponseMin(Set<IncidentAssociationEntity> associates) {
		return associates.stream().map(this::toDTOMin).collect(Collectors.toSet());
	}

    private IncidentAssociatedResponseMin toDTOMin(IncidentAssociationEntity entity) {
    	return new IncidentAssociatedResponseMin(
    			entity.getId().getIdEmergencySecond().getId(),
    			entity.getId().getIdEmergencySecond().getFolio()
    	);
    }
    
    private List<IncidentCorporationSearchDetailResponseMin> toResponseMin(List<EmergencyCorporationEntity> entity) {
		return entity.stream().map(this::toDTODetailMin).collect(Collectors.toList());
    }
	
	private IncidentCorporationSearchDetailResponseMin toDTODetailMin(EmergencyCorporationEntity entity) {
        return new IncidentCorporationSearchDetailResponseMin(
        		this.toResponse(entity.getId()),
                entity.getStatus(),
                entity.getMission(),
                entity.getCreationDate(),
                entity.getUpdateDate(),
                entity.getAssignedUnitForceDate(),
                Boolean.FALSE,
                entity.getCreatedBy(),
                entity.getUpdateBy(),
                this.toResponse(entity.getGeofence())
        );
    }

	//DATA SHARED 
	private NationalSubTypeResponseMin toResponse(IncidentNationalSubtypeEntity entity) {
		return new NationalSubTypeResponseMin(
				entity.getNationalSubTypeId(),
				entity.getName(),
				entity.getClave(),
				entity.isSubtype(),
				this.toResponse(entity.getNationalType())
		);
	}
	private NationalSubTypeResponseMin toResponseWithSubSet(IncidentNationalSubtypeEntity entity, Boolean hasSubSet) {
		return new NationalSubTypeResponseMin(
				entity.getNationalSubTypeId(),
				entity.getName(),
				entity.getClave(),
				entity.isSubtype(),
				hasSubSet,
				this.toResponse(entity.getNationalType())
		);
	}
	
	private NationalTypeResponseMin toResponse(IncidentNationalTypeEntity entity) {
		return new NationalTypeResponseMin(
				entity.getNationalTypeId(), 
				entity.getName()
		);
	}
	
	private PriorityResponse toResponse(Priority entity) {
		 return Optional.ofNullable(entity).map(x -> new PriorityResponse(
				 x.getId(), 
				 x.getName(), 
				 x.getDescription(),
				 x.getTextColor(), 
				 x.getBackColor()
		 )).orElse(null);
	}
	
	private IncidentCorporationSearchPKResponse toResponse(EmergencyCorporationEntityPK entity) {
		return new IncidentCorporationSearchPKResponse(
				this.toResponse(entity.getCorporationOperative())
	    );
	}
		
	private CorporationOperativeResponse toResponse(CorporationOperativeEntity entity) {
		return new CorporationOperativeResponse(
				entity.getId(),
				entity.getName(),
				entity.getClave(),
				entity.getActive(),
				this.toResponse(entity.getCorporation())
		);
	}
	
	private CorporationResponse toResponse(CorporationEntity entity) {
		return new CorporationResponse(
				entity.getId(), 
				entity.getName(),
				entity.getDescription(),
				entity.getClave(),
				entity.getIcon(),
				entity.getActive()
		);
	}
	
	private AppUserResponse toResponse(UserEntity entity) {
		return new AppUserResponse(
				entity.getIdUser(), 
				entity.getAlias());
	}
	
	//DATA BY ID
	private IncidentSubSetResponse toResponse(IncidentEntity entity) {

		boolean hasSubSet = !subSetRepository.findAllByNationalSubType(entity.getNationalSubType().getNationalSubTypeId()).isEmpty();

		 return Optional.ofNullable(entity).map(x -> new IncidentSubSetResponse(
	        		entity.getId(),
	        		entity.getFolio(),
	        		entity.getStatus(),
	        		entity.getDescription(),
	        		entity.getSenseDetection(),
	        		entity.getIsManual(),
	        		entity.getEventDate(),
	        		entity.getCreatedAt(),
	        		entity.getUpdatedAt(),
	        		Optional.ofNullable(entity.getAttention()).map(IncidentAttendedEntity::getAttendDate).orElse(null),
	        		this.toResponse(x.getSource()),
	                this.toResponseWithSubSet(x.getNationalSubType(), hasSubSet),
	                this.toResponse(entity.getPriority()),
	                this.toResponse(entity.getLocation()),
	                this.toResponse(entity.getPhone(), entity.getCalls()),
	                Optional.ofNullable(entity.getStreetview()).map(this::toResponse).orElse(null),
	                this.toResponse(x.getCreatedByUser()),
	                Optional.ofNullable(entity.getAttention()).map(IncidentAttendedEntity::getAttendBy).map(this::toResponse).orElse(null),
	                this.toResponse(entity.getAssociates()),
			        this.toResponseDetail(entity.getEmergencyCorporations()),
			        !entity.getTokens().isEmpty() ? IncidentTokenMapper.toResponse(entity.getTokens().stream().filter(z -> z.isActive()).findFirst().get()) : null,
                    entity.getSecundaryIncident() != null ? IncidentSecundaryMapper.toResponse(entity.getSecundaryIncident()) : null,
                    toResponse(entity.getAdditionalNationalSubTypes()),
				 	Optional.ofNullable(entity.getSubSet()).map(this::toResponse).orElse(null),
				 	entity.getAttributes()
         )).orElse(null);
	}
	
	private IncidentSourceResponse toResponse(IncidentSource entity) {
		return new IncidentSourceResponse(
				entity.getId(),
				entity.getName(),
				entity.getClave(),
				entity.getDescription(),
				entity.getIcon(),
				entity.getShowAlertIdentifier(),
				entity.getPlaySound()
		);
	}
	
	private IncidentLocationResponse toResponse(IncidentLocationEntity entity) {
		return new IncidentLocationResponse(
				entity.getIdEmergencyLocation(), 
				entity.getHouseNumber(),
				entity.getStreet(),
				entity.getBwnStreet1(),
				entity.getBwnStreet2(),
				entity.getLatitude(),
				entity.getLongitude(),
				entity.getReference(),
				entity.getZipCode(),
				Optional.ofNullable(entity.getState()).map(LocationStateEntity::getId).orElse(null),
				Optional.ofNullable(entity.getSettlement()).map(LocationSettlementEntity::getId).orElse(null),
				Optional.ofNullable(entity.getMunicipality()).map(LocationMunicipalityEntity::getId).orElse(null),
				entity.getCreatedAt(),
				entity.getCountryAlternative(),
				entity.getStateAlternative(),
				entity.getMunicipalityAlternative(),
				entity.getSettlementAlternative(),
				entity.getAddressAlternative(),
				entity.getStateAlternativeId(),
				entity.getMunicipalityAlternativeId(),
				entity.getSettlementAlternativeId()
		);
	}
	
	private IncidentLocationStreetviewResponse toResponse(IncidentLocationStreetviewEntity entity) {
		return new IncidentLocationStreetviewResponse(
				entity.getLatitude(),
				entity.getLongitude(), 
				entity.getHeading(), 
				entity.getPitch(), 
				entity.getPanoId()
				);
	}
	
	private IncidentPhoneNumberResponse toResponse(PhoneEmergencyReporterEntity entity, List<CallEntity> callEntities) {
		return Optional.ofNullable(this.getFirstCall(callEntities)).map(this::toResponse)
                .orElse(this.toResponse(entity));
	}
	
	private IncidentPhoneNumberResponse toResponse(CallEntity entity) {
        return Optional.ofNullable(entity).map(x -> new IncidentPhoneNumberResponse(
        		x.getPhoneNumber()
         )).orElse(null);
    }
	
	private IncidentPhoneNumberResponse toResponse(PhoneEmergencyReporterEntity entity) {
		 return Optional.ofNullable(entity).map(x -> {
	            String phoneNumber = Optional.ofNullable(x.getPhone()).map(Object::toString).orElse("");
	            return new IncidentPhoneNumberResponse(
	            		phoneNumber
	            );
	        }).orElse(null);
	}

	private CallEntity getFirstCall(List<CallEntity> calls) {
        return Optional.ofNullable(calls).map(x -> !x.isEmpty() ? x : null).map(
        		x -> Collections.max(x, Comparator.comparingLong(y -> Optional.ofNullable(y.getReceivedAt()).map(Date::getTime).orElse(0L)))
        ).orElse(null);
    }
	
	private Set<IncidentAssociatedResponse> toResponse(Set<IncidentAssociationEntity> associates) {
		return associates.stream().map(this::toDTO).collect(Collectors.toSet());
	}

    private IncidentAssociatedResponse toDTO(IncidentAssociationEntity entity) {
    	return new IncidentAssociatedResponse(
    			entity.getId().getIdEmergencySecond().getId(),
    			entity.getId().getIdEmergencySecond().getFolio(),
    			toResponse(entity.getId().getIdEmergencySecond().getNationalSubType())
    	);
    }
    
    private List<IncidentCorporationSearchDetailResponse> toResponseDetail(List<EmergencyCorporationEntity> entity) {
		return entity.stream().map(this::toDTODetail).collect(Collectors.toList());
    }
	
	private IncidentCorporationSearchDetailResponse toDTODetail(EmergencyCorporationEntity entity) {
        return new IncidentCorporationSearchDetailResponse(
        		this.toResponse(entity.getId()),
                entity.getStatus(),
                entity.getObservationClose(),
                entity.getObservationCancel(),
                entity.getMission(),
                entity.getCreationDate(),
                entity.getUpdateDate(),
                entity.getClosedDate(),
                entity.getCancelDate(),
                entity.getTransferedDate(),
                entity.getAssignedUnitForceDate(),
                entity.getIsAssigned(),
                entity.getCreatedBy(),
                entity.getUpdateBy(),
                entity.getCancelBy(),
                entity.getClosedBy(),
                entity.getTransferedBy(),    
                Optional.ofNullable(entity.getGeofence()).map(this::toResponse).orElse(null),
                Optional.ofNullable(entity.getReasonClose()).map(this::toResponse).orElse(null),
                Optional.ofNullable(entity.getReasonCancel()).map(this::toResponse).orElse(null)
        );
    }
	
	private GeofenceResponse toResponse(GeofenceEntity entity) {
		return new GeofenceResponse(
				entity.getId(),
				entity.getName(),
				entity.getDescription(),
				entity.getType(),
				entity.getColor(),
				entity.getOpacity(),
				entity.getActive()
		);
	}
	
	private IncidentCloseResponse toResponse(EmergencyCloseEntity entity) {
		return new IncidentCloseResponse(
				entity.getId(),
	        	entity.getName()
		);
	}

	private IncidentCancellationResponse toResponse(EmergencyCancellationEntity entity) {
		return new IncidentCancellationResponse(
				entity.getId(),
	        	entity.getName()
		);
	}
	
	public static List<IncidentAdditionalClassificationResponse> toResponse(List<IncidentClassificationEntity> nationalSubTypes) {
		return Optional.ofNullable(nationalSubTypes).orElseGet(Collections::emptyList).stream()
				.map(x -> new IncidentAdditionalClassificationResponse(
						x.getNationalSubTypeId().getNationalSubTypeId(),
						x.getNationalSubTypeId().getName(),
						x.getNationalSubTypeId().getClave(),
						x.getNationalSubTypeId().getDescription(),
						toResponseTypeArray(x.getNationalSubTypeId().getNationalType()),
		                toResponse(x.getNationalSubTypeId() .getCategorySubtype()),
						x.getNationalSubTypeId().isSubtype()
				)).collect(Collectors.toList());
	}
	
	public static NationalTypeArrayResponse toResponseTypeArray(IncidentNationalTypeEntity emergencySubTypeEntity) {
	        return Optional.ofNullable(emergencySubTypeEntity).map(x -> new NationalTypeArrayResponse(
	                x.getNationalTypeId(),
	                x.getName(),
	                x.getClave(),
	                x.getDescription(),
	                toResponse(x.getNationalClassification())
	        )).orElse(null);
	}
	 
	public static ClassificationResponse toResponse (IncidentNationalClassificationEntity emergencyTypeEntity) {
		 return Optional.ofNullable(emergencyTypeEntity).map(x -> new ClassificationResponse(
				 x.getNationalClassificationId(),
				 x.getName(),
				 x.getClave(),
				 x.getDescription()
	     )).orElse(null);
	}
	
	public static NationalCategorySubtypeResponse toResponse(IncidentNationalCategorySubtypeEntity nationalCategorySubtype) {
		return Optional.ofNullable(nationalCategorySubtype).map(x -> new NationalCategorySubtypeResponse(
						x.getId(),
						x.getCategory().getId(),
						x.getSubtype().getNationalSubTypeId(),
						x.getCategory().getName(),
						x.getCategory().getClave()
		)).orElse(null);
	}

	public static List<IncidentCorporationAssociatesCallResponse> jsonToIncidentCorporationAssociatesCallResponse(String json) {
		List<IncidentCorporationAssociatesCallResponse> list = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		if(json != null && !json.isEmpty()) {
			try {
				list = mapper.readValue(json, new TypeReference<List<IncidentCorporationAssociatesCallResponse>>(){});
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}	
		}
		return list;
	}

	public static List<IncidentCorporationCorporationsCallResponse> jsonToIncidentCorporationCorporationsCallResponse(String json) {
		List<IncidentCorporationCorporationsCallResponse> list = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		if(json != null && !json.isEmpty()) {
			try {
				list = mapper.readValue(json, new TypeReference<List<IncidentCorporationCorporationsCallResponse>>(){});
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}	
		}
		return list;
	}
	
	public IncidentResponseMin toResponseMin(IncidentCorporationCallResponse incident) {
		List<IncidentCorporationAssociatesCallResponse> associates = jsonToIncidentCorporationAssociatesCallResponse(incident.getAssociates());
		List<IncidentCorporationCorporationsCallResponse> corporations = jsonToIncidentCorporationCorporationsCallResponse(incident.getEmergencyCorporations());
		return Optional.ofNullable(incident).map(i ->
		new IncidentResponseMin(
				i.getId(), 
				i.getFolio(), 
				i.getStatusEmergency(), 
				i.getDescription(), 
				i.getSenseDetection(), 
				i.isManual(), 
				i.getEventDate(),
				i.getCreatedAt(),
				i.getUpdatedAt(), 
				new NationalSubTypeResponseMin(i.getNationalSubTypeId(), i.getNationalSubTypeName(), i.getNationalSubTypeClave(), i.getNationalSubTypeIsSubType(), new NationalTypeResponseMin(i.getNationalTypeId(), i.getNationalTypeName())), 
				new PriorityResponse(i.getPriorityId(), i.getPriorityName(), i.getPriorityDescription(), i.getPriorityTextColor(), i.getPriorityBackColor()),
				new IncidentSourceResponse(i.getSourceId(), i.getSourceName(), i.getSourceClave(), i.getSourceDescription(), i.getSourceIcon(), i.getShowAlertIdentifier(), i.getPlaySound()),
				new IncidentLocationResponseMin(i.getEmergencyLocationId(), i.getEmergencyLocationLatitude(), i.getEmergencyLocationLongitude(), i.getEmergencyLocationAddressAlternative(), i.getEmergencyLocationCreatedAt(), Optional.ofNullable(i.getEmergencyLocationMunicipalityAlternative()).orElse(null), Optional.ofNullable(i.getEmergencyLocationStateAlternative()).orElse(null), i.getBwnStreet1(), i.getBwnStreet2()), 
				new AppUserResponse(i.getUserId(), i.getUserName()), 
				associates.stream().map(a -> new IncidentAssociatedResponseMin(i.getId(), a.getFolio())).collect(Collectors.toSet()), 
				corporations.stream().map(this::toResponseMin).collect(Collectors.toList()), 
				null,//token
				new IncidentSecundaryResponse(i.getSecundaryFolio()),//secundary
				new ArrayList<>(),//adionationalSubtype
				i.isAsociation())
		).orElse(null);
	}
	
	public IncidentCorporationSearchDetailResponseMin toResponseMin(IncidentCorporationCorporationsCallResponse incident) {
		return Optional.ofNullable(incident).map(i -> 
			new IncidentCorporationSearchDetailResponseMin(
					new IncidentCorporationSearchPKResponse(
							new CorporationOperativeResponse(
									i.getCorporationoperativeid(), 
									i.getCorporationoperativename(), 
									i.getCorporationoperativeclave(), 
									i.getCorporationactive(), 
									new CorporationResponse(
											i.getCorporationid(), 
											i.getCorporationname(), 
											i.getCorporationname(), 
											i.getCorporationdesc(), 
											i.getCorporationicon(), 
											i.getCorporationactive()
									)
							)
					),					
					i.getEmergencycorporationstatus(), 
					null, 
					i.getEmergencycorporationcreatedat(), 
					i.getEmergencycorporationupdatedat(), 
					i.getAssignedunitforcedate(),
					i.getIsAssignedUnitForce(),
					i.getEmergencycorporationcreatedby(),
					i.getEmergencycorporationupdatedby(),					
					new GeofenceResponse(
							i.getGeofenceid(), 
							i.getGeofencename(), 
							i.getGeofencedescription(), 
							i.getGeofencetype(), 
							i.getGeofencecolor(), 
							i.getGeofenceopacity(), 
							i.getGeofenceactive()
					)
		    )
	    ).orElse(null);
	}
	private SubSetResponse toResponse(SubSetEntity entity) {
		return Optional.ofNullable(entity).map(x -> new SubSetResponse(
				x.getId(),
				x.getName()
		)).orElse(null);
	}

}
