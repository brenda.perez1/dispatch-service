package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RejectSupportRequest {
    private Long operativeCorporationId;
    private String incidentId;
    private String userId;
    private Long profileId;
}
