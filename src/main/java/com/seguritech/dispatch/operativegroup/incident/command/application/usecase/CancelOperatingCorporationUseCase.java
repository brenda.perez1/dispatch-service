package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import java.util.List;

import javax.transaction.Transactional;

import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCloseRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CancelIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCloseException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationReasonExistenceEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class CancelOperatingCorporationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentOperatingCorporationRepository repository;
    private final IncidentOperatingCorporationReasonExistenceEnsurer reasonEnsurer;
    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final DomainEventPublisher publisher;
    private final EmergenciesClient client;

    public CancelOperatingCorporationUseCase(IncidentOperatingCorporationFinder finder,
                                             IncidentOperatingCorporationRepository repository,
                                             IncidentOperatingCorporationReasonExistenceEnsurer reasonEnsurer,
                                             ProfileCorporationConfigRepository profileCorporationConfigRepository, 
                                             DomainEventPublisher publisher,
                                             EmergenciesClient client) {
        this.finder = finder;
        this.repository = repository;
        this.reasonEnsurer = reasonEnsurer;
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.publisher = publisher;
        this.client = client;
    }
    
    @Transactional
    public void execute(String token, CancelIncidentCorporationRequest request) {
    	IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CorporationOperativeId corporationOperativeId = CorporationOperativeId.required(request.getOperatingCorporation());
        IncidentCorporationReasonId reasonId = IncidentCorporationReasonId.required(
                request.getReasonId()
        );
        IncidentCorporationReasonBounded reason = new IncidentCorporationReasonBounded(
                reasonId,
                IncidentCorporationObservation.optional(request.getObservation()),
                UserId.required(request.getCancelByUser()),
                new UserId(request.getCancelByUser()));

        ProfileId profileId = ProfileId.required(request.getProfileId());

        this.reasonEnsurer.ensure(reasonId);
        this.profileCorporationConfigEnsurer.ensure(profileId, corporationOperativeId);

        IncidentCorporationModel model = this.finder.findById(
                new CorporationOperativeId(request.getOperatingCorporation()),
                new IncidentId(request.getIncidentId())
        );
        model.cancel(reason, reasonId);
        this.repository.save(model);
	    List<IncidentCorporationModel> listIncidentsOpen = this.repository.findByIncidentAndStatus(incidentId, IncidentCorporationStatusEnum.OPEN.toString());
	    try {
	    	if (listIncidentsOpen.size() == 0) {
  			//se manda a cerrar el incidente principal
          	client.closeAnEmergency(token, incidentId.getValue(), new EmergencyCloseRequest("Prueba",0L));
	    	}
		} catch (Exception e) {
			throw new IncidentCloseException();
		}
        this.publisher.publish(model.pullEvents());
    }

}
