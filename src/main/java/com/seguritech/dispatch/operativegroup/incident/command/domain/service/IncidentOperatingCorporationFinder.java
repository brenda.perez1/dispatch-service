package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.FilterDTO;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;

@Service
public class IncidentOperatingCorporationFinder {

	private final IncidentOperatingCorporationRepository repository;
	private final ProfileCorporationConfigRepository profile;

	public IncidentOperatingCorporationFinder(IncidentOperatingCorporationRepository repository,
			ProfileCorporationConfigRepository profile) {
		this.repository = repository;
		this.profile = profile;
	}

	public IncidentCorporationModel findById(CorporationOperativeId corporationId, IncidentId incidentId) {
		return Optional.ofNullable(this.repository.findById(corporationId, incidentId))
				.orElseThrow(() -> new IncidentCorporationNotFoundException(corporationId.getValue(), incidentId.getValue()));
	}
	
	public List<IncidentCorporationModel> findById(CorporationId corporationId, IncidentId incidentId) {
		return Optional.ofNullable(this.repository.findById(corporationId, incidentId))
				.orElseThrow(() -> new IncidentCorporationNotFoundException(corporationId.getValue(), incidentId.getValue()));
	}
	
	public List<IncidentCorporationEntity> findByIdIncident(ProfileId profileId, IncidentId incidentId) {
		List<CorporationOperativeId>  corporationOperativeIds = profile.getCorporationOperativeIds(profileId);
		return this.repository.getByIncidentAndCorporationsOperative(corporationOperativeIds, incidentId);
	}

	public List<IncidentCorporationModel> find(IncidentId incidentId, List<CorporationOperativeId> corporationsList) {
		return corporationsList.stream().map(x -> this.findById(x, incidentId)).collect(Collectors.toList());
	}

	public List<IncidentCorporationEntity> findAll(ProfileId profileId, FilterDTO status) {
		List<CorporationOperativeId>  corporationOperativeIds = profile.getCorporationOperativeIds(profileId);
		List<IncidentCorporationEntity>  result =  repository.findAll(corporationOperativeIds, status);
		return result;

	}
	
	public IncidentCorporationModel findByIdIncidentAndCorporation(CorporationId corporationId, IncidentId incidentId) {
		return Optional.ofNullable(this.repository.findByIdIncidentAndCorporation(corporationId, incidentId))
				.orElseThrow(() -> new IncidentCorporationNotFoundException(corporationId.getValue(), incidentId.getValue()));
	}
	
	public Long countCorporationAssign (IncidentId incidentId) {
		return this.repository.countCorporationAssign(incidentId);
	}
	
}
