package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentPreconfigured {
    private String PhoneNumber;
    private  Long phoneNumberCountryId;
    private Long incidentTypeId;
    private Long stateId;
    private  Long townId;
    private Double latitude;
    private Double longitude;
}
