package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IncidentCorporationCorporationsCallResponse {

	private String emergencycorporationcreatedby;
	private Date assignedunitforcedate;
	private Boolean isAssignedUnitForce;
	private Date emergencycorporationcreatedat;
	private String emergencycorporationstatus;
	private Date emergencycorporationupdatedat;
	private String emergencycorporationupdatedby;
	private Long corporationoperativeid;
	private String corporationoperativeclave;
	private String corporationoperativeactive;
	private String corporationoperativename;
	private Long corporationid;
	private String corporationname;
	private String corporationdesc;
	private String corporationicon;
	private Boolean corporationactive;
	private Long geofenceid;
	private String geofencename;
	private String geofencedescription;
	private String geofencetype;
	private String geofencecolor;
	private String geofenceopacity;
	private Boolean geofenceactive;       

}
