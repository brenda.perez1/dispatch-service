package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;


import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.SupportRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.*;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentCreator;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentIdEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class SupportRequestUseCase {

    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final IncidentOperatingCorporationRepository incidentCorporationRepository;
    private final IncidentIdEnsurer incidentIdEnsurer;
    private final DomainEventPublisher publisher;

    public SupportRequestUseCase(IncidentRepository incidentRepository, ProfileCorporationConfigRepository profileCorporationConfigRepository, IncidentOperatingCorporationRepository incidentCorporationRepository, DomainEventPublisher publisher) {
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.incidentIdEnsurer = new IncidentIdEnsurer(incidentRepository);
        this.incidentCorporationRepository = incidentCorporationRepository;
        this.publisher = publisher;
    }

    public void execute(SupportRequest request) {
        CorporationOperativeId corporationId = new CorporationOperativeId(request.getOperatingCorporationId());
        ProfileId profileId = ProfileId.required(request.getProfileId());
        UserId userId = UserId.required(request.getUserId());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());

        this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);
        this.incidentIdEnsurer.ensurer(incidentId);

        IncidentCorporationModel model = IncidentCorporationModel.supportRequest(
                incidentId,
                corporationId,
                userId
        );

        this.incidentCorporationRepository.save(model);
        this.publisher.publish(model.pullEvents());
    }

}
