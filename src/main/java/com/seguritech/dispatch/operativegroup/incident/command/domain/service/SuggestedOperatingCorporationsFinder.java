package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested.SuggestedCorporationBoundedModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.SuggestedCorporationsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuggestedOperatingCorporationsFinder {

    private final SuggestedCorporationsRepository repository;

    public SuggestedOperatingCorporationsFinder(SuggestedCorporationsRepository repository) {
        this.repository = repository;
    }

    public List<SuggestedCorporationBoundedModel> find(IncidentTypeIdBounded id){
        return this.repository.findByIncidentTypeIdBounded(id);
    }
}
