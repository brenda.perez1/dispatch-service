package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncidentSourceResponse {

	private final Long id;
	private final String name;
	private final String clave;
	private final String description;
	private final String icon;
	private final Boolean showAlertIdentifier;
	private final Boolean playSound;

}
