package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.domain.model.corporation.entity.Corporation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested.SuggestedCorporationBoundedId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested.SuggestedCorporationBoundedModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.SuggestedCorporationsRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.CorporationCatEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.specification.IncidentCorporationSpecification;

@Repository
public class SuggestedCatCorporationsRepositoryImpl implements SuggestedCorporationsRepository {

    private final CorporationCatEntityRepository repository;

    public SuggestedCatCorporationsRepositoryImpl(CorporationCatEntityRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<SuggestedCorporationBoundedModel> findByIncidentTypeIdBounded(IncidentTypeIdBounded id) {
        return this.repository.findAll(IncidentCorporationSpecification.emergencyTypeId(id.getValue(), true)).stream()
            .map(this::toModel).collect(Collectors.toList());
    }

    private SuggestedCorporationBoundedModel toModel(Corporation corporationEntity) {
        return new SuggestedCorporationBoundedModel(new SuggestedCorporationBoundedId(corporationEntity.getId()));
    }

}

