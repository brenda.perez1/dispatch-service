package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationCodeRequiredException;
import lombok.Getter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
public class IncidentCorporationCode {

    private final String value;

    public IncidentCorporationCode(String value) {
        this.value = value;
    }

    public static IncidentCorporationCode required(String value) {
        return Optional.ofNullable(value).filter(x -> !x.equals("")).map(IncidentCorporationCode::new)
            .orElseThrow(IncidentCorporationCodeRequiredException::new);
    }

    public static List<IncidentCorporationCode> of(List<String> codes) {
        if (codes == null || codes.isEmpty()) {
            throw new IncidentCorporationCodeRequiredException();
        }
        return codes.stream().map(IncidentCorporationCode::required).collect(Collectors.toList());
    }
}
