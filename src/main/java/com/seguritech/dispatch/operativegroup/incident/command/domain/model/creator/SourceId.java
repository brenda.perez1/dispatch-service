package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.SourceRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class SourceId {
    private final Long value;

    private SourceId(Long value) {
        this.value = value;
    }

    public static SourceId required(Long id) {
        Optional.ofNullable(id).orElseThrow(SourceRequiredException::new);
        return new SourceId(id);
    }
}
