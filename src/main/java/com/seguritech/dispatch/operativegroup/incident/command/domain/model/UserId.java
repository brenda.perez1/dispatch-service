package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.UserRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class UserId {
    private final String value;
    private final static String DEFAULT_VALUE = "";

    public UserId(String value) {
        this.value = value;
    }

    public static UserId from(String value) {
        return Optional.ofNullable(value)
                .map(UserId::new)
                .orElse(empty());
    }
    
    public static UserId optional(String value) {
        return Optional.ofNullable(value)
                .map(UserId::new)
                .orElse(null);
    }

    public static UserId empty() {
        return new UserId(DEFAULT_VALUE);
    }

    public Boolean isEmpty() {
        return this.value == DEFAULT_VALUE;
    }

    public static UserId required(String id) {
        Optional.ofNullable(id)
                .orElseThrow(UserRequiredException::new);
        return new UserId(id);
    }

}
