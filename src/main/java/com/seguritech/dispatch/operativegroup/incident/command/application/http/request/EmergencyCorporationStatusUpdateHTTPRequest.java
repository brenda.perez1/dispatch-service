package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import java.util.List;

import lombok.Data;

@Data
public class EmergencyCorporationStatusUpdateHTTPRequest {
    private String status;
    private List<Long> operativeGroupIds;
}
