package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AutomaticCorporationAssignmentToClassifiedIncidentRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested.SuggestedCorporationBoundedModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedTypeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentsAssignationConfigurationChecker;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentTypeChecker;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.SuggestedOperatingCorporationsFinder;
import com.seguritech.platform.domain.DomainEventPublisher;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AutomaticOperatingCorporationAssignmentToClassifiedIncidentUseCase {

    private final IncidentsAssignationConfigurationChecker incidentsAssignationConfigurationChecker;
    private final SuggestedOperatingCorporationsFinder suggestedCorporationsFinder;
    private final IncidentTypeChecker incidentChecker;
    private final IncidentOperatingCorporationRepository repository;
    private final IncidentBoundedTypeRepository typeRepository;
    private final DomainEventPublisher publisher;

    public AutomaticOperatingCorporationAssignmentToClassifiedIncidentUseCase(
            IncidentsAssignationConfigurationChecker incidentsAssignationConfigurationChecker, SuggestedOperatingCorporationsFinder suggestedCorporationsFinder,
            IncidentTypeChecker incidentChecker, IncidentOperatingCorporationRepository repository, IncidentBoundedTypeRepository typeRepository,
            DomainEventPublisher publisher) {
        this.incidentsAssignationConfigurationChecker = incidentsAssignationConfigurationChecker;
        this.suggestedCorporationsFinder = suggestedCorporationsFinder;
        this.incidentChecker = incidentChecker;
        this.repository = repository;
        this.typeRepository = typeRepository;
        this.publisher = publisher;
    }

    public void execute(AutomaticCorporationAssignmentToClassifiedIncidentRequest request) {
        if (!incidentsAssignationConfigurationChecker.checkB() || !this.typeRepository
            .isAssignable(new IncidentTypeIdBounded(request.getIncidentTypeId()))) {
            return;
        }
        IncidentBounded incidentBounded = this.incidentChecker.check(IncidentId.required(request.getIncidentId()));
        List<SuggestedCorporationBoundedModel> suggestedCorporations = this.suggestedCorporationsFinder
            .find(incidentBounded.getType());

      //TODO set corporation id
        for (SuggestedCorporationBoundedModel suggestion : suggestedCorporations) {
            IncidentCorporationModel model = IncidentCorporationModel.assign(incidentBounded.getId(),
                new CorporationOperativeId(suggestion.getCorporationBoundedId().getValue()),
                null,
                new UserId(request.getCreateBy()));
            this.repository.save(model);
            this.publisher.publish(model.pullEvents());
        }

    }
}
