package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CreateManualIncidentResponse {
    private String incidentId;
    private String invoice;
}
