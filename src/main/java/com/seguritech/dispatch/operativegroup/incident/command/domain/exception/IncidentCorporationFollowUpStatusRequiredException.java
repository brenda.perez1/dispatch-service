package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationFollowUpStatusRequiredException extends BusinessException {

	public IncidentCorporationFollowUpStatusRequiredException() {
        super(MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_STATUS_REQUIRED_ID_EXCEPTION, 3015L);
    }

}
