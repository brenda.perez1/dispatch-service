package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.OperatingCorporationEntityRepository;

@Service
public class CorporationOperativeRepositoryImpl implements CorporationOperativeRepository {
	
	private final OperatingCorporationEntityRepository operatingCorporationEntityRepository;

	public CorporationOperativeRepositoryImpl (
			OperatingCorporationEntityRepository operatingCorporationEntityRepository
			) {
		this.operatingCorporationEntityRepository = operatingCorporationEntityRepository;
	}
	
	@Override
	public CorporationOperative findCorporationOperativeDefault(CorporationId corporation) {
		return Optional.ofNullable(this.operatingCorporationEntityRepository.findByCorporationAndIsDefaultIsTrueAndActiveIsTrue(new Corporation(corporation.getValue()))).orElse(null);
	}

	@Override
	public Boolean isCorporationOperativeDefault(Long corporationOperativeId) {
		return Optional.ofNullable(this.operatingCorporationEntityRepository.isDefault(corporationOperativeId)).orElse(false);
	}

}
