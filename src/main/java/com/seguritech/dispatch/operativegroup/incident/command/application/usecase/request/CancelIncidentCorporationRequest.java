package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CancelIncidentCorporationRequest {

    private final String incidentId;
    private final Long operatingCorporation;
    private final Long reasonId;
    private final String observation;
    private final String cancelByUser;
    private Long profileId;
}
