package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.Optional;

import com.domain.model.geofence.entity.Geofence;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.GeofenceIdRequierdException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class GeofenceId {
    private Long value;

    private GeofenceId(Long value) {
        this.value = value;
    }

    public static GeofenceId required(Long profileId) {
        Optional.ofNullable(profileId).orElseThrow(GeofenceIdRequierdException::new);
        return new GeofenceId(profileId);
    }
    
    public static GeofenceId optional(Geofence geofence) {
    	return Optional.ofNullable(geofence).map(Geofence::getId).map(GeofenceId::new).orElse(null);
    }

}
