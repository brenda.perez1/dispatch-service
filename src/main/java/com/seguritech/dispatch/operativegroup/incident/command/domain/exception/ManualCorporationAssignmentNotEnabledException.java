package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class ManualCorporationAssignmentNotEnabledException extends BusinessException {
    public ManualCorporationAssignmentNotEnabledException() {
        super(MessageResolver.MANUAL_CORPORATION_ASSIGNMENT_NOT_ENABLED_EXCEPTION, 3031L);
    }
}
