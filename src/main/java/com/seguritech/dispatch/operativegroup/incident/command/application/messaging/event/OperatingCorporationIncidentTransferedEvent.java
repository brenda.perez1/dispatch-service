package com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Getter
@NoArgsConstructor
public class OperatingCorporationIncidentTransferedEvent{
    private String emergencyId;
    private Long corporationOperativeId;
    private String transferedByUser;
    private Date transferedAt;
    private Long profileId;
}
