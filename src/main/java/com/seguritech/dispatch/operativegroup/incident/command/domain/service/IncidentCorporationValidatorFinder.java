package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationValidatorModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationValidatorRepository;

public class IncidentCorporationValidatorFinder {

    private final IncidentOperatingCorporationValidatorRepository incidentRepository;

    public IncidentCorporationValidatorFinder(IncidentOperatingCorporationValidatorRepository incidentRepository) {
        this.incidentRepository = incidentRepository;
    }

    public IncidentCorporationValidatorModel find(IncidentId incidentId) {
        return this.incidentRepository.find(incidentId);
    }


}
