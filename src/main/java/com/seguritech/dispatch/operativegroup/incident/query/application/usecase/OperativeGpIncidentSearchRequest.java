package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentOrderFieldEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentPeriodFilterEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryStatusEnum;
import com.seguritech.platform.application.http.Order;
import com.seguritech.platform.application.usecase.AbstractSearcherRequest;
import lombok.Getter;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
public class OperativeGpIncidentSearchRequest extends AbstractSearcherRequest {

    private final List<Long> operativeGroups;
    private final String folio;
    private final Long priorityId;
    private final Long emergencyTypeId;
    private final String location;
    private final String createdUserId;
    private final OpGpIncidentQueryStatusEnum status;
    private final Long phoneNumber;
    private final Long typeId;
    private final Long stateId;
    private final Long townId;
    private final Long localityId;
    private final Long sectorId;
    private final Long sourceId;
    private final String assignedUserId;
    private final String corporationCode;
    private final String corporationDescription;
    private final Long corporationId;

    public OperativeGpIncidentSearchRequest (Integer size, Integer page, List<Long> operativeGroups, String folio,
                                             Long priorityId,
                                             Long emergencyTypeId, String location,
                                             OpGpIncidentQueryStatusEnum status, Long phoneNumber,
                                             Long typeId, Long stateId, Long townId,
                                             Long localityId, Long sectorId, Long sourceId, String createdUserId,
                                             String assignedUserId,
                                             String corporationCode,
                                             String corporationDescription, Long corporationId,
                                             OpGpIncidentPeriodFilterEnum periodDateField,
                                             Date periodStartDate, Date periodEndDate, Order order,
                                             OpGpIncidentOrderFieldEnum orderByField, String query) {

        super(size,
                page,
                query,
                Optional
                        .ofNullable(periodDateField)
                        .map(Enum::name)
                        .orElse(null),
                periodStartDate,
                periodEndDate,
                order,
                Optional
                        .ofNullable(orderByField)
                        .map(Enum::name)
                        .orElse(null));
        this.operativeGroups = operativeGroups;
        this.folio = folio;
        this.priorityId = priorityId;
        this.emergencyTypeId = emergencyTypeId;
        this.createdUserId = createdUserId;
        this.location = location;
        this.status = status;
        this.phoneNumber = phoneNumber;
        this.typeId = typeId;
        this.stateId = stateId;
        this.townId = townId;
        this.localityId = localityId;
        this.sectorId = sectorId;
        this.sourceId = sourceId;
        this.assignedUserId = assignedUserId;
        this.corporationCode = corporationCode;
        this.corporationDescription = corporationDescription;
        this.corporationId = corporationId;

    }

}
