package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

import java.util.Collection;

@Data
public class EmergencyMultiCorporationReactivateApiRequest {

    private Long reactivationReasonId;
    private String observation;
    private Long profileId;
    private Collection<Long> operativeGroupIds;

}