package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationAcceptSupportHttpRequest {
    private Long profileId;
}
