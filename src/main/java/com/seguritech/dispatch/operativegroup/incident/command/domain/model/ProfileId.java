package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ProfileIdRequierdException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class ProfileId {
    private Long value;

    private ProfileId(Long value) {
        this.value = value;
    }

    public static ProfileId required(Long profileId) {
        Optional.ofNullable(profileId).orElseThrow(ProfileIdRequierdException::new);
        return new ProfileId(profileId);
    }

}
