package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class CorporationNotFoundException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public CorporationNotFoundException (Object... string) {
        super(MessageResolver.CORPORATION_NOT_FOUND_EXCEPTION, string, 3001L);
    }
}
