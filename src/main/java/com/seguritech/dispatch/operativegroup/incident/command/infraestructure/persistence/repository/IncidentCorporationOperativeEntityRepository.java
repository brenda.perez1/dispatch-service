package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;

@Repository
public interface IncidentCorporationOperativeEntityRepository extends JpaRepository<IncidentCorporationEntity, IncidentCorporationEntityPk>, 
JpaSpecificationExecutor<IncidentCorporationEntity> {

	@Query("SELECT e FROM IncidentEntity e INNER JOIN IncidentCorporationEntity ic on ic.id.incident = e.id WHERE"
			+ " ic.id.corporationOperative in (SELECT iee.id.corporationOperative FROM IncidentCorporationEntity iee WHERE iee.id.incident.id =:id) ")
	Page<IncidentEntity> getEmergencyCorporation(Pageable paging);
	
//	@Query(value = "SELECT ic.id.corporationOperative.id FROM IncidentCorporationEntity ic "
//			+ "INNER JOIN CorporationOperativeProfile cop ON cop.id.corporationOperative.id = ic.id.corporationOperative.id "
//			+ "WHERE cop.id.profile.id = :profileId ")
	@Query(value = "SELECT co.id FROM CorporationOperative co "
			+ "INNER JOIN CorporationOperativeProfile cop on cop.id.corporationOperative.id = co.id "
			+ "INNER JOIN IncidentCorporationEntity ec on ec.id.corporationOperative.id = cop.id.corporationOperative.id "
			+ "WHERE cop.id.profile.id = :profileId "
			+ "AND ec.id.incident.id = :emergencyId "
			+ "AND ec.status <> 'CLOSED'")
	List<Long> findAllOperativeGroupsWhitEmergency(@Param("profileId") Long profileId, @Param("emergencyId") String emergencyId);
}
