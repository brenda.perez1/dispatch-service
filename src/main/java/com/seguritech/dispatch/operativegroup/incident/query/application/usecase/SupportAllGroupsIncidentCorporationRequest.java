package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SupportAllGroupsIncidentCorporationRequest {

	private final String emergencyId;
	private final String userId;
	private final List<Long> operativeGroups;

}
