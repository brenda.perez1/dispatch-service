package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class OperativeGroupRequiredException extends BusinessException {

  public OperativeGroupRequiredException() {
        super(MessageResolver.OPERATIVE_GROUP_REQUERID_EXCEPTION, 3032L);
    }
}
