package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
public class OpGpIncidentSearchResponse {

    private String emergencyId;
    private String emergencyFolio;
    private String status;
    private String description;
    private IncidentSourceResponse source;
    private OpGpIncidentTypeResponse emergencyType;
    private OpGpIncidentPrioritySearchResponse priority;
    private Date createdAt;
    private Date dateAttention;
    private Date eventDate;
    private OpGpIncidentLocationSearchResponse location;
    private List<OpGpIncidentCorporationSearchResponse> corporations;
    private AppUserResponse assignedToUser;
    private Date updatedAt;
    private AppUserResponse updatedByUser;

}
