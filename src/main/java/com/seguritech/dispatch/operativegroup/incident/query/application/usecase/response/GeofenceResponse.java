package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GeofenceResponse {
	
	private Long id;
    private String name;
    private String description;
    private String type;
    private String color;
    private String opacity;
    private Boolean active;	
  
}
