package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporationoperative.entity.CorporationOperative;
//import com.domain.model.corporation.entity.CorporationOperative;
import com.domain.model.geofence.entity.Geofence;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.national.entity.IncidentNationalClassificationEntity;
import com.domain.model.national.entity.IncidentNationalSubtypeEntity;
import com.domain.model.national.entity.IncidentNationalTypeEntity;
import com.domain.model.priority.entity.Priority;
import com.domain.model.source.entity.IncidentSource;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentEntityRepository;
//import com.seguritech.dispatch.operativegroup.incident.query.application.entity.IncidentSourceEntity;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentClassificationTypeResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentCorporationSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentLocationSectorSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentPrioritySearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentSourceResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentSubClassificationTypeResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentSubTypeResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentTypeCustomResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentTypeResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryRepository;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.specification.IncidentOpGpSpecification;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.specification.IncidentSpecifications;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.dispatch.specification.DateRangeResolverSpecification;
import com.seguritech.dispatch.specification.SpecificationSortFieldResolver;


/*
 * TODO: Change with projection JPA and deprecated query with whole entities
 *  https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#projections.dtos
 * */

@Service
@Transactional(readOnly = true)
public class OpGpIncidentQueryQueryRepositoryJpaImpl implements OpGpIncidentQueryRepository {

    private final IncidentEntityRepository repository;
   // private final SpecificationSortFieldResolver sortFieldResolver;
    private final DateRangeResolverSpecification<IncidentEntity> dateRangeResolver;

    public OpGpIncidentQueryQueryRepositoryJpaImpl (IncidentEntityRepository repository) {
        this.repository =
                repository;
        //this.sortFieldResolver = new SpecificationSortFieldResolver(IncidentSpecifications.sortFields());
        this.dateRangeResolver = new DateRangeResolverSpecification<>(IncidentSpecifications.dateRage());
    }

    @Override
    public PageDataProjectionDTO<OpGpIncidentSearchResponse> search (OperativeGpIncidentSearchRequest request) {
        Page<IncidentEntity> result = this.repository
                .findAll(Specification
                                .where(toSpecifications(request)),
                        PageRequest.of(request.getPage(), request.getSize()/*, this.toSort(request)*/));
        return new PageDataProjectionDTO<>(result
                .get()
                .map(this::toResponse)
                .collect(Collectors.toList()),
                result.getTotalElements());
    }


    private Specification<IncidentEntity> toSpecifications (OperativeGpIncidentSearchRequest dto) {
        List<Specification<IncidentEntity>> specifications = new ArrayList<>();

        Optional
                .ofNullable(dto.getFolio())
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.FolioLike(x)));
        Optional
                .ofNullable(dto.getPriorityId())
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.priorityIdEq(x)));
        /*Optional
                .ofNullable(dto.getEmergencyTypeId())
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.TypeIdEq(x)));*/
//        Optional
//                .ofNullable(dto.getLocation())
//                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.inLocationLike(x)));
      /*  Optional
                .ofNullable(dto.getTypeId())
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.TypeIdEq(x)));*/
       /* Optional
                .ofNullable(dto.getSourceId())
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.SourceIdEq(x)));*/
//        Optional
//                .ofNullable(dto.getStateId())
//                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.stateIdEq(x)));
//        Optional
//                .ofNullable(dto.getTownId())
//                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.townIdEq(x)));
//        Optional
//                .ofNullable(dto.getLocalityId())
//                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.LocalityIdEq(x)));
//        Optional
//                .ofNullable(dto.getSectorId())
//                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.sectorIdEq(x)));

        //Period Filter
        Optional
                .ofNullable(dto.getPeriodDateField())
                .ifPresent(x -> specifications
                        .add(this.dateRangeResolver.resolve(x, dto.getPeriodStartDate(), dto.getPeriodEndDate())));

//        Optional
//                .ofNullable(dto.getQuery())
//                .ifPresent(x -> {
//                    ///Reset Query if Present
//                    specifications.clear();
//                    specifications.add(IncidentSpecifications.querySearch(x));
//                });

//        Optional
//                .ofNullable(dto.getOperativeGroups())
//                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.operativeCorporationIn(x)));
        Optional
                .ofNullable(dto.getStatus())
                .map(OpGpIncidentQueryStatusEnum::getValue)
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.operativeCorporationStatusEq(x)));

//        Optional
//                .ofNullable(dto.getAssignedUserId())
//                .ifPresent(x -> specifications.add(IncidentSpecifications.assignedUserIdEq(x)));
//        Optional
//                .ofNullable(dto.getCreatedUserId())
//                .ifPresent(x -> specifications.add(IncidentSpecifications.assignedUserIdEq(x)));

        //Specification behaviour is immutable
        Specification<IncidentEntity> temp = (root, query, criteriaBuilder) -> null;
        for (Specification<IncidentEntity> specification : specifications) {
            temp = temp.and(specification);
        }
        return temp;
    }


    private OpGpIncidentSearchResponse toResponse (IncidentEntity incidentEntity) {
//    	return null;
        return new OpGpIncidentSearchResponse(
                incidentEntity.getId(),
                incidentEntity.getFolio(),
                null,//TODOO:
                incidentEntity.getDescription(),
                null,//this.toResponse(incidentEntity.getSource()),
                this.toResponse(incidentEntity.getNationalSubType()),
                this.toResponse(incidentEntity.getPriority()),
                incidentEntity.getCreatedAt(),
                null,//incidentEntity.getDateAttention(),
                incidentEntity.getEventDate(),
                null,//this.toResponse(incidentEntity.getLocation()),
                null,//this.toResponse(incidentEntity.getCorporationEntities()),
                null,//this.toResponse(incidentEntity.getCreatedByUser()),
                incidentEntity.getUpdatedAt(),
                null//this.toResponse(incidentEntity.getUpdatedByUser())

        );
    }

    /*private Sort toSort (final OperativeGpIncidentSearchRequest request) {
        return Optional
                .ofNullable(request)
                .filter(x -> x.getOrderByField() != null)
                .filter(x -> x.getOrder() != null)
                .map(x -> this.sortFieldResolver.resolve(x.getOrderByField(), x
                        .getOrder()
                        .name()))
                .orElse(IncidentSpecifications.SortByCreatedAtDesc());

    }*/

    private IncidentSourceResponse toResponse (IncidentSource entity) {
        return Optional.ofNullable(entity).map(x -> new IncidentSourceResponse(
        		x.getId(), 
        		x.getName(),
        		x.getClave(),
        		x.getDescription(),
        		x.getIcon(),
        		x.getShowAlertIdentifier(),
        		x.getPlaySound()
        )).orElse(null);
    }

    private OpGpIncidentTypeCustomResponse toResponse (IncidentNationalTypeEntity entity) {
        Optional<IncidentNationalSubtypeEntity> subTypeEntity = null;
//        Optional
//                .ofNullable(entity.getNationalEmergency())
//                .map(IncidentNationalTypeEntity::getSubType);
        OpGpIncidentClassificationTypeResponse classificationTypeResponse = null;
//        subTypeEntity
//                .map(IncidentNationalSubtypeEntity::getType)
//                .map(this::toResponse)
//                .orElse(null);
        OpGpIncidentSubClassificationTypeResponse incidentSubClassificationTypeResponse = null;
//        subTypeEntity
//                .map(this::toResponse)
//                .orElse(null);
        
        
        
        return new OpGpIncidentTypeCustomResponse(
               null,
                null,
                classificationTypeResponse,
                incidentSubClassificationTypeResponse);
    }


//    private OpGpIncidentTypeResponse toResponse (IncidentNationalTypeEntity emergencyEntity) {
//        return Optional.ofNullable(emergencyEntity)
//                .map(x -> new OpGpIncidentTypeResponse(emergencyEntity.getNationalTypeId(), emergencyEntity
//                        .getDescription()))
//                .orElse(null);
//    }




    private OpGpIncidentSubTypeResponse toResponseSubType (IncidentNationalSubtypeEntity entity) {
        return Optional.ofNullable(entity)
                .map(x -> new OpGpIncidentSubTypeResponse(entity.getNationalSubTypeId(), entity.getDescription()))
                .orElse(null);
    }


    public OpGpIncidentTypeResponse toResponse (IncidentNationalSubtypeEntity
                                                                         emergencySubTypeEntity) {
        return Optional
                .ofNullable(emergencySubTypeEntity)
                .map(x -> new OpGpIncidentTypeResponse(x.getNationalSubTypeId(),
                        x.getDescription()))
                .orElse(null);
    }

    private OpGpIncidentClassificationTypeResponse toResponse (IncidentNationalClassificationEntity
                                                                       emergencyTypeEntity) {
//        return Optional
//                .ofNullable(emergencyTypeEntity)
//                .map(x -> new OpGpIncidentClassificationTypeResponse(x.getNationalEmergencyTypeId(), x.getDescription
//                        ()))
//                .orElse(null);
    	return null;
    }

    private OpGpIncidentPrioritySearchResponse toResponse (Priority incidentPriorityEntity) {
        return Optional
                .ofNullable(incidentPriorityEntity)
                .map(x -> new OpGpIncidentPrioritySearchResponse(x.getId(), x.getDescription(), x.getBackColor()))
                .orElse(null);
    }

//    private OpGpIncidentLocationSearchResponse toResponse (IncidentLocationEntity x) {
//        return Optional
//                .ofNullable(x)
//                .map(y -> new OpGpIncidentLocationSearchResponse(
//                        Optional
//                                .ofNullable(y.getState())
//                                .map(this::toResponse)
//                                .orElse(null),
//                        Optional
//                                .ofNullable(y.getMunicipality())
//                                .map(this::toResponse)
//                                .orElse(null),
//                        Optional
//                                .ofNullable(y.getSettlement())
//                                .map(this::toResponse)
//                                .orElse(null),
//                        y.getHouseNumber(),
//                        y.getStreet(),
//                        y.getBwnStreet1(),
//                        y.getBwnStreet2(),
//                        y.getReference(),
//                        y.getLatitude(),
//                        y.getLongitude(),
//                        this.toResponse(y.getGeofence())))
//                .orElse(null);
//    }

//    private OpGpIncidentLocationStateSearchResponse toResponse (LocationStateEntity entity) {
//        return new OpGpIncidentLocationStateSearchResponse(entity.getId(), entity.getState());
//    }
//
//    private OpGpIncidentLocationMunicipalitySearchResponse toResponse (LocationMunicipalityEntity entity) {
//        return new OpGpIncidentLocationMunicipalitySearchResponse(entity.getId(), entity.getMunicipality());
//    }
//
//    private OpGpIncidentLocationLocalitySearchResponse toResponse (LocationSettlementEntity entity) {
//        return new OpGpIncidentLocationLocalitySearchResponse(entity.getId(), entity.getSettlement(),
//                entity.getPostalCode());
//    }

    private OpGpIncidentLocationSectorSearchResponse toResponse (Geofence entity) {
        return new OpGpIncidentLocationSectorSearchResponse(entity.getId(), entity.getName());
    }

    private List<OpGpIncidentCorporationSearchResponse> toResponse (List<IncidentCorporationEntity> corporationEntities) {
        return Optional
                .ofNullable(corporationEntities)
                .map(x -> x
                        .stream()
                        .map(this::toResponse)
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    private OpGpIncidentCorporationSearchResponse toResponse (IncidentCorporationEntity entity) {
        return Optional
                .ofNullable(entity)
                .flatMap(x -> Optional
                        .ofNullable(x.getId())
                        .map(y -> this.toResponseDetails(y.getCorporationOperative(), x.getCreationDate(), x.getStatus())))
                .orElse(null);
    }

    private OpGpIncidentCorporationSearchResponse toResponseDetails (CorporationOperative entity, Date assigned,
                                                                     String status) {
        return new OpGpIncidentCorporationSearchResponse(entity.getId(), Optional
                .ofNullable(entity.getCorporation())
                .map(Corporation::getId)
                .orElse(null), entity.getName(), assigned, status
        );
    }

//    private AppUserResponse toResponse (AppUserEntity appUserEntity) {
//        return Optional
//                .ofNullable(appUserEntity)
//                .map(x -> new AppUserResponse(x.getIdUser(), x.getAlias()))
//                .orElse(null);
//    }

}
