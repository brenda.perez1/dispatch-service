package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UncoupleAllIncidentCorporationRequest {
    
    private final String emergencyId;
    private final String userId;

}
