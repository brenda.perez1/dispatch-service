package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.specification;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.domain.model.corporationoperative.entity.CorporationOperative_;
//import com.domain.model.corporation.entity.CorporationOperative_;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incident.entity.IncidentEntity_;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk_;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity_;

public class IncidentOpGpSpecification extends IncidentSpecifications {

    public static Specification<IncidentCorporationEntity> incidentIdEq (String incidentId) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root
                .get(IncidentCorporationEntity_.ID)
                .get(IncidentCorporationEntityPk_.INCIDENT)
                .get(IncidentEntity_.ID), incidentId
        );

    }

    public static Specification<IncidentCorporationEntity> statusEq (String status) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root
                .get(IncidentCorporationEntity_.STATUS), status);
    }

    public static Specification<IncidentEntity> operativeCorporationIn (List<Long> ids) {

        return (root, query, criteriaBuilder) -> {
            query.distinct(true);
            return criteriaBuilder
                    .in(root
                            .join(IncidentEntity_.ID)//TODO CORPORATION_ENTITIES
                            .get(IncidentCorporationEntity_.ID)
                            .get(IncidentCorporationEntityPk_.CORPORATION_OPERATIVE)
                            .get(CorporationOperative_.ID))
                    .value(ids);
        };

    }

    public static Specification<IncidentEntity> operativeCorporationStatusEq (String status) {
        return (root, query, criteriaBuilder) -> {
            query.distinct(true);
            return criteriaBuilder.equal(root
                    .join(IncidentEntity_.ID)//TODO CORPORATION_ENTITIES
                    .get(IncidentEntity_.ID)//TODO IncidentEntity_.CORPORATION_ENTITIES
                    .get(IncidentCorporationEntity_.STATUS), status);
        };
    }

}
