package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.CorporationNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationRepository;
import org.springframework.stereotype.Service;

@Service
public class OperatingCorporationEnsurer {

    private final CorporationRepository repository;

    public OperatingCorporationEnsurer(CorporationRepository repository) {
        this.repository = repository;
    }

    public void ensure(CorporationOperativeId id) {
        if (!this.repository.exist(id)) {
            throw new CorporationNotFoundException(id.getValue());
        }
    }
}
