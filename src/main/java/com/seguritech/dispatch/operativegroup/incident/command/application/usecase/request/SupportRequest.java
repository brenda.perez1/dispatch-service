package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SupportRequest {
    private String userId;
    private Long operatingCorporationId;
    private Long profileId;
    private String incidentId;
}
