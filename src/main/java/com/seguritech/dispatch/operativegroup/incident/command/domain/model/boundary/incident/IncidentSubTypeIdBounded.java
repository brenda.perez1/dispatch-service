package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentSubTypeIdBounded {

  private Long value;
}
