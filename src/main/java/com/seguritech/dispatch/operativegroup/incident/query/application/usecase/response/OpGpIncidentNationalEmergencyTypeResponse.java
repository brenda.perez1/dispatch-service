package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpGpIncidentNationalEmergencyTypeResponse {

    private final Long nationalEmergencyTypeId;
    private final String description;

}
