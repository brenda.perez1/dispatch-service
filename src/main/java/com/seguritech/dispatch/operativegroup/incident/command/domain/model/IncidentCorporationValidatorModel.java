package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import java.util.Date;
import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.event.RootIncidentCancelEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.event.RootIncidentClosureEvent;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.platform.domain.RootAggregate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentCorporationValidatorModel extends RootAggregate {
    private final IncidentId incidentId;
    private final List<CorporationId> corporations;

    public void validateCanBeClosed(UserId userId, IncidentId incidentId) {
        if (!this.corporations.isEmpty())
            return;

        this.record(new RootIncidentClosureEvent(
                incidentId.getValue(),
                new Date(),
                userId.getValue()
        ));
    }

    public void validateCanBeCancel(UserId userId, IncidentId incidentId, ReasonCancelId reasonCancelId, ObservationCancel observationCancel) {
        if (!this.corporations.isEmpty())
            return;

        this.record(new RootIncidentCancelEvent(
                incidentId.getValue(),
                reasonCancelId.getValue(),
                observationCancel.getValue(),
                new Date(),
                userId.getValue()
        ));
    }
}
