package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentResponse {
	
	private final String id;
	private final String folio;
	private final String status;
    private final String description;
	private final String senseDetection;
	private final Boolean isManual;	
	private final Date eventDate;
	private final Date createdAt;
	private final Date updatedAt;
	private final LocalDateTime attendDate;
    private final IncidentSourceResponse source;
	private final NationalSubTypeResponseMin nationalSubType;
	private final PriorityResponse priority;
	private final IncidentLocationResponse location;
	private IncidentPhoneNumberResponse phoneNumber;
	private final IncidentLocationStreetviewResponse streetview;
	private final AppUserResponse createdByUser;
	private final AppUserResponse attendBy;
	private final Set<IncidentAssociatedResponse> associates;
	private final List<IncidentCorporationSearchDetailResponse> emergencyCorporations;
	private IncidentTokenResponse token;
	private IncidentSecundaryResponse secundary;
	private List<IncidentAdditionalClassificationResponse> additionalNationalSubTypes;
	
}
