package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationCloseApiRequest {

    private Long closeReasonId;
    private String observation;
    private Long profileId;
}
