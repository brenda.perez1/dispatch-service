package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class PhoneNumberCountryId {
    private final Long value;

    private PhoneNumberCountryId(Long value) {
        this.value = value;
    }

    public static PhoneNumberCountryId optional(Long id) {
        return new PhoneNumberCountryId(id);
    }
}
