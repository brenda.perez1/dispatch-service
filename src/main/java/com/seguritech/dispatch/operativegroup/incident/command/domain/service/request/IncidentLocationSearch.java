package com.seguritech.dispatch.operativegroup.incident.command.domain.service.request;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Latitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Longitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.StateId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.TownId;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentLocationSearch {
    private final StateId stateId;
    private final TownId townId;
    private final Latitude latitude;
    private final Longitude longitude;
}