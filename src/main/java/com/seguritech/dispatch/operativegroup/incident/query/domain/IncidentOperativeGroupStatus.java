package com.seguritech.dispatch.operativegroup.incident.query.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum IncidentOperativeGroupStatus {

    OPEN("OPEN"),
    CLOSED("CLOSED"),
    TRANSFERRED("TRANSFERRED"),
    IN_PROCESS("IN_PROCESS"),
    POSITIVE_CLOSE("POSITIVE_CLOSE"),
    NEGATIVE_CLOSE("NEGATIVE_CLOSE");
    private final String value;

}
