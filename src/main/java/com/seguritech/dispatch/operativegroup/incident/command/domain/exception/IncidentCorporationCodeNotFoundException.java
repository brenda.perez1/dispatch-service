package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationCodeNotFoundException extends BusinessException {

    public IncidentCorporationCodeNotFoundException(Object... code) {
        super(MessageResolver.INCIDENT_CORPORATION_CODE_NOT_FOUND_EXCEPTION,code, 3006L);
    }
}
