package com.seguritech.dispatch.operativegroup.incident.command.application.messaging;

import com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event.OperatingCorporationIncidentTransferedEvent;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.AssignIncidentToOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignIncidentCorporationRequest;
import com.seguritech.platform.application.messagging.DomainEventListener;
import com.seguritech.platform.application.messagging.DomainSourceEventListener;
import com.seguritech.platform.infrastructure.messaging.DomainEventEnvelope;
import lombok.extern.slf4j.Slf4j;

@DomainSourceEventListener(domain = "incidents-service")
@Slf4j
public class OperatingCorporationIncidentTransferedListener {

    private final AssignIncidentToOperatingCorporationUseCase assignUseCase;

    public OperatingCorporationIncidentTransferedListener(AssignIncidentToOperatingCorporationUseCase assignUseCase) {
        this.assignUseCase = assignUseCase;
    }

    @DomainEventListener(name = "operating-corporation-incident-transfered", version = "v1")
    public void onIncidentCreatedEvent(DomainEventEnvelope<OperatingCorporationIncidentTransferedEvent> evt) {
        OperatingCorporationIncidentTransferedEvent event = evt.getPayload();

        this.assignUseCase.execute(new AssignIncidentCorporationRequest(
                event.getEmergencyId(),
                event.getCorporationOperativeId(),
                null,
                event.getTransferedByUser(),
                event.getProfileId()
        ),null);

    }
}
