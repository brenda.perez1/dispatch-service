package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentCorporationClosedEvent implements DomainEvent {

	private String incidentId;
    private String closedByUser;
    private Long corporationId;

}