package com.seguritech.dispatch.operativegroup.incident.query.domain;

public enum OpGpIncidentQueryStatusEnum {

    OPEN("OPEN"),
    DISPATCHING("DISPATCHING"),
    CLOSED("CLOSED"),
    IN_PROCESS("IN_PROCESS"),
    POSITIVE_CLOSE("POSITIVE_CLOSE"),
    NEGATIVE_CLOSE("NEGATIVE_CLOSE");

    private String value;

    OpGpIncidentQueryStatusEnum (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
