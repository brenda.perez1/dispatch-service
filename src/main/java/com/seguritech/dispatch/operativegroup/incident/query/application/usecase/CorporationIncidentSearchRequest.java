package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AbstractSearcherRequest;
import com.seguritech.dispatch.operativegroup.incident.query.domain.AssignedStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOrderFieldEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentQueryStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentPeriodFilterEnum;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryStatusEnum;
import com.seguritech.platform.application.http.Order;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CorporationIncidentSearchRequest extends AbstractSearcherRequest {

    private final OpGpIncidentQueryStatusEnum status;
    private List<Long> priorityId;
    private final Long subCenter;
    private List<Long> corporationId;
    private Long profileId;
    private final IncidentQueryStatusEnum statusEmergency;
    private final Integer maxHours;
    private String createdBy;
    private final Boolean assignedStatus;
    private List<Long> followUpStatusId;
    private List<Long> corporationOperativeId;
    private final Boolean isNotAssociatedAsParent;
	private final Boolean isNotAssociatedAsChild;
	private final Long nationalSubTypeId;
    
    public CorporationIncidentSearchRequest(Integer size, Integer page, String query, OpGpIncidentPeriodFilterEnum periodDateField, Date periodStartDate,
    		Date periodEndDate, Order order, IncidentOrderFieldEnum orderByField, Integer maxHours, OpGpIncidentQueryStatusEnum status, List<Long> priorityId, 
    		IncidentQueryStatusEnum statusEmergency, Long subCenter, List<Long> followUpStatusId, List<Long> corporationId, String createdBy,
    		AssignedStatusEnum assignedStatus, List<Long> corporationOperativeId, Boolean isNotAssociatedAsParent, Boolean isNotAssociatedAsChild, Long nationalSubTypeId) {
		super(size,
	          page,
	          query,
	          Optional.ofNullable(periodDateField).map(Enum::name).orElse(null),
	          periodStartDate,
	          periodEndDate,
	          order,
	          Optional.ofNullable(orderByField).map(Enum::name).orElse(null)
	    );
		this.status = status;
		this.priorityId = priorityId;
		this.statusEmergency = statusEmergency;
		this.subCenter = subCenter;
		this.corporationId = corporationId;
		this.maxHours = maxHours;
		this.createdBy = createdBy;
		this.assignedStatus = Optional.ofNullable(assignedStatus).map(AssignedStatusEnum::getValue).orElse(null);
		this.followUpStatusId = followUpStatusId;
		this.corporationOperativeId = corporationOperativeId;
		this.isNotAssociatedAsParent = isNotAssociatedAsParent;
		this.isNotAssociatedAsChild = isNotAssociatedAsChild;
		this.nationalSubTypeId = nationalSubTypeId;
	}

}
