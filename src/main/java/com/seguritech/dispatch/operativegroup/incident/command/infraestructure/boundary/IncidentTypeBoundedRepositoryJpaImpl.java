package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.domain.model.national.entity.IncidentNationalSubtypeEntity;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedTypeRepository;

@Service
public class IncidentTypeBoundedRepositoryJpaImpl implements IncidentBoundedTypeRepository {


    private final Long unidentified;
    private final NationalSubTypeEntityRepository repository;
    private final IncidentCorporationTypeEntityQueryRepository corporationTypeRepository;

    public IncidentTypeBoundedRepositoryJpaImpl(@Value("${cad-lite.incident.config.type.unidentified}") Long unidentified,
    		NationalSubTypeEntityRepository repository,
                                                IncidentCorporationTypeEntityQueryRepository corporationTypeRepository) {
        this.unidentified = unidentified;
        this.repository = repository;
        this.corporationTypeRepository = corporationTypeRepository;
    }

    @Override
    public IncidentTypeIdBounded findUnidentified() {
        return this.repository.findById(unidentified).map(IncidentNationalSubtypeEntity::getNationalSubTypeId)
            .map(IncidentTypeIdBounded::new).orElse(new IncidentTypeIdBounded(0L));
    }

    @Override
    public Boolean isAssignable(IncidentTypeIdBounded type) {
        Long corporations = this.corporationTypeRepository.countByIncidentType(type.getValue(), true);
        return corporations > 0;
    }
}
