package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NationalSubTypeResponseMin {

	private Long nationalSubTypeId;
	private String name;
	private String clave;
	private  Boolean isSubtype;
	private  Boolean hasSubSet;
	private NationalTypeResponseMin nationalType;

	public NationalSubTypeResponseMin(Long nationalSubTypeId, String name, String clave, Boolean isSubtype, NationalTypeResponseMin nationalType) {
		this.nationalSubTypeId = nationalSubTypeId;
		this.name = name;
		this.clave = clave;
		this.isSubtype = isSubtype;
		this.nationalType = nationalType;
	}

}