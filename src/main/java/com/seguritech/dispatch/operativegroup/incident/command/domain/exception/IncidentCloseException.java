package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCloseException extends BusinessException {

	public IncidentCloseException() {
		super(MessageResolver.INCIDENT_CLOSE_EXCEPTION, 3082L);
	}
	
	

}
