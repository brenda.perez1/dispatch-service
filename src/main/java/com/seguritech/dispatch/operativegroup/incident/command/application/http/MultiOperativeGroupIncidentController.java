package com.seguritech.dispatch.operativegroup.incident.command.application.http;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.*;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.CancelOperatingMultiCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.ClosureOperatingMultiCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.ReactivateOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.UpdateEmergencyStatusUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.*;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.Date;

import static com.seguritech.dispatch.config.SwaggerConfig.AUTHORIZATION_HEADER;

@RestController
public class MultiOperativeGroupIncidentController extends MultiOperativeGroupIncidentBaseController {

    private static final String AUTHORIZATION_HEADER = "AUTHORIZATION";
    private static final String AUTHORIZATION_BEARER = "Bearer ";
    private final ClosureOperatingMultiCorporationUseCase closureOperatingMultiCorporationUseCase;
    private final CancelOperatingMultiCorporationUseCase cancelOperatingMultiCorporationUseCase;
    private final ReactivateOperatingCorporationUseCase reactivateOperatingCorporationUseCase;
    private final UpdateEmergencyStatusUseCase updateEmergencyStatusUseCase;
    
    public MultiOperativeGroupIncidentController(final ClosureOperatingMultiCorporationUseCase closureOperatingMultiCorporationUseCase,
                                                 final CancelOperatingMultiCorporationUseCase cancelOperatingMultiCorporationUseCase,
                                                 final ReactivateOperatingCorporationUseCase reactivateOperatingCorporationUseCase,
                                                 UpdateEmergencyStatusUseCase updateEmergencyStatusUseCase) {
        this.closureOperatingMultiCorporationUseCase = closureOperatingMultiCorporationUseCase;
        this.cancelOperatingMultiCorporationUseCase = cancelOperatingMultiCorporationUseCase;
        this.reactivateOperatingCorporationUseCase = reactivateOperatingCorporationUseCase;
		this.updateEmergencyStatusUseCase = updateEmergencyStatusUseCase;
    }

    @ApiOperation(value = "Close incident at the corporations")
    @PatchMapping("/{emergency-id}/close")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void closeIncidentCorporation(
            @PathVariable("emergency-id") String incidentId,
            @RequestBody @Valid EmergencyMultiCorporationCloseApiRequest request,
            UsernamePasswordAuthenticationToken user,
            HttpServletRequest http) {

    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
    	Long profileId = Long.parseLong(principal.getActiveProfile().orElse("-1"));
        this.closureOperatingMultiCorporationUseCase.execute(
                AUTHORIZATION_BEARER.concat(http.getHeader(AUTHORIZATION_HEADER)),
                new ClosureIncidentMultiCorporationRequest(
            incidentId,
            request.getOperativeGroupIds(),
            principal.getId(),
            profileId,
            request.getCloseReasonId(),
            request.getObservation()));
    }
    
    
    
    @PatchMapping(value = {"/{emergency-id}/status"} )
	@ApiOperation(value = "Update status - Incident by corps")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateStatus(@PathVariable("emergency-id") String emergencyId,
			@RequestBody @Valid EmergencyCorporationStatusUpdateHTTPRequest requestHttp,
			UsernamePasswordAuthenticationToken user, HttpServletRequest http) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		Long profileId = Long.parseLong(principal.getActiveProfile().orElse("-1"));

		this.updateEmergencyStatusUseCase.execute(AUTHORIZATION_BEARER.concat(http.getHeader(AUTHORIZATION_HEADER)),
				new UpdateIncidentStatusRequest(
						emergencyId, 
						requestHttp.getStatus().toString(),
						requestHttp.getOperativeGroupIds(),
			            principal.getId(),
			            profileId
				));
	} 
    
    

    @ApiOperation(value = "Cancel incident at the corporations")
    @PatchMapping("/{emergency-id}/cancel")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void cancelIncidentCorporation(
            @PathVariable("emergency-id") String incidentId,
            @RequestBody @Valid EmergencyMultiCorporationCancelApiRequest request,
            UsernamePasswordAuthenticationToken user,
            HttpServletRequest http) {

    	AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
    	Long profileId = Long.parseLong(principal.getActiveProfile().orElse("-1"));
        this.cancelOperatingMultiCorporationUseCase.execute(
            AUTHORIZATION_BEARER.concat(http.getHeader(AUTHORIZATION_HEADER)),
            new CancelIncidentMultiCorporationRequest(
                incidentId,
                request.getOperativeGroupIds(),
                principal.getId(),
                profileId,
                request.getCancelReasonId(),
                request.getObservation()));
    }

    @ApiOperation(value = "Reopen corporations for an emergency",
            notes = "Reopen corporation associated to emergency , if emergency is closed, emergency will reopen")
    @PatchMapping("/{emergency-id}/reopen")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void reactivateCorporation(@PathVariable("emergency-id") String incidentId,
                                      @RequestBody EmergencyMultiCorporationReactivateApiRequest request,
                                      UsernamePasswordAuthenticationToken user) {

        AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
        this.reactivateOperatingCorporationUseCase.executeMultipleCorporation(new ReactivateMultiCorporationRequest(
                incidentId,
                request.getOperativeGroupIds(),
                request.getObservation(),
                request.getReactivationReasonId(),
                principal.getId(),
                new Date(),
                request.getProfileId()));
    }

}