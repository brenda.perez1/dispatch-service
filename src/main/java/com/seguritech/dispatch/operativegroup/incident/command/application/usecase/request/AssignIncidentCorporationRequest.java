package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public class AssignIncidentCorporationRequest {
    private final String incidentId;
    private final Long corporationId;
    private final Boolean secondaryInvoice; 
    private final String assignedBy;
    private final Long profileId;
}
