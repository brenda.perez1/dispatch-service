package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentIdRequiredException extends BusinessException {

    public IncidentIdRequiredException() {
        super(MessageResolver.INCIDENT_ID_REQUERID_EXCEPTION, 3020L);
    }
}
