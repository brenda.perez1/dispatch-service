package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import com.domain.model.corporation.followup.reason.entity.CorporationFollowUpReasonEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationFollowUpReasonRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.CorporationFollowUpReasonEntityRepository;
import com.seguritech.platform.Service;

@Service
public class CorporationFollowUpReasonJpaImpl implements CorporationFollowUpReasonRepository {

	private final CorporationFollowUpReasonEntityRepository repository;

	public CorporationFollowUpReasonJpaImpl(CorporationFollowUpReasonEntityRepository repository) {
		this.repository = repository;
	}

	@Override
	public IncidentCorporationFollowUpReasonId findById(IncidentCorporationFollowUpReasonId reasonId) {
		return this.repository.findById(reasonId.getValue()).map(x -> x.getActive() ? x : null).map(this::toModel)
				.orElse(null);
	}

	private IncidentCorporationFollowUpReasonId toModel(CorporationFollowUpReasonEntity entity) {
		return IncidentCorporationFollowUpReasonId.required(entity.getId());
	}

}
