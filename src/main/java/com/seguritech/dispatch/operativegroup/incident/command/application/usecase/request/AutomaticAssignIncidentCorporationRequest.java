package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AutomaticAssignIncidentCorporationRequest {

    private final String incidentId;
    private final String assignedBy;

}
