package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncidentLocationResponse {
	
	private Long idEmergencyLocation;
	private String houseNumber;
	private String street;
	private String bwnStreet1;
	private String bwnStreet2;
    private Double latitude;
	private Double longitude;
	private String reference;
	private String zipCode;
	private Long state;
	private Long settlement;
	private Long municipality;
	private Date createdAt;
	private String countryAlternative;
	private String stateAlternative;
	private String municipalityAlternative;
	private String settlementAlternative;
	private String addressAlternative;
	private Long stateAlternativeId;
	private Long municipalityAlternativeId;
	private Long settlementAlternativeId;
	
}
