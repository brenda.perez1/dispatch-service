package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.Getter;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
public class ReactivateCorporationsRequest {

    private final String incidentId;
    private final Long reactivationReasonId;
    private final String observation;
    private final List<Long> corporationId;
    private final String reactivatedBy;
    private final Date reactivationDate;

    public ReactivateCorporationsRequest(String incidentId, Long reactivationReasonId, String observation,
                                         List<Long> corporationId, String reactivatedBy, Date reactivationDate) {
        this.incidentId = incidentId;
        this.reactivationReasonId = reactivationReasonId;
        this.observation = observation;
        this.corporationId = Optional.ofNullable(corporationId).orElse(Collections.emptyList());
        this.reactivatedBy = reactivatedBy;
        this.reactivationDate = reactivationDate;
    }
}
