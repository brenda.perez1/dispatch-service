package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentWithForceUnitAsiggnedException extends BusinessException {
    public IncidentWithForceUnitAsiggnedException(Object... incidentId) {
        super(MessageResolver.INCIDENT_WITH_FORCE_UNIT_ASSIGNED_EXCEPTION, incidentId, 3029L);
    }
}
