package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;

public interface CorporationOperativeRepository {
	
	CorporationOperative findCorporationOperativeDefault(CorporationId corporation);
	
	Boolean isCorporationOperativeDefault(Long corporationOperativeId);

}
