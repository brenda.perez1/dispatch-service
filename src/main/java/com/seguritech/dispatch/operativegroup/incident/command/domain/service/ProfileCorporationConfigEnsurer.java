package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.CorporationNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;

public class ProfileCorporationConfigEnsurer {

    private final ProfileCorporationConfigRepository profileCorporationConfigRepository;

    public ProfileCorporationConfigEnsurer(ProfileCorporationConfigRepository profileCorporationConfigRepository) {
        this.profileCorporationConfigRepository = profileCorporationConfigRepository;
    }

    public void ensure(ProfileId profileId, CorporationOperativeId corporationOperativeId) {
        if (!this.profileCorporationConfigRepository.exist(profileId, corporationOperativeId)) {
            throw new CorporationNotFoundException("Operative Group [" + corporationOperativeId.getValue() + "] not configured in profile [" + profileId.getValue() + "]");
        }
    }

}
