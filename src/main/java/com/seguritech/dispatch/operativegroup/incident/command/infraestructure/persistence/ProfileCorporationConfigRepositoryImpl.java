package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.corporationoperativeprofile.entity.CorporationOperativeProfile;
import com.domain.model.corporationoperativeprofile.entity.CorporationOperativeProfilePK;
import com.domain.model.profile.Profile;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.ProfileCorporationConfigEntityRepository;

@Service
public class ProfileCorporationConfigRepositoryImpl implements ProfileCorporationConfigRepository {

    private final ProfileCorporationConfigEntityRepository profileCorporationConfigEntityRepository;

    public ProfileCorporationConfigRepositoryImpl(ProfileCorporationConfigEntityRepository profileCorporationConfigEntityRepository) {
        this.profileCorporationConfigEntityRepository = profileCorporationConfigEntityRepository;
    }

    @Override
    public boolean exist(ProfileId profileId, CorporationOperativeId corporationOperativeId) {
        return this.profileCorporationConfigEntityRepository.findById(
                new CorporationOperativeProfilePK(
                		new Profile(profileId.getValue()),
                		new CorporationOperative(corporationOperativeId.getValue())
                )
        ).isPresent();
    }
    

	@Override
	public List<CorporationOperativeId> getCorporationOperativeIds(ProfileId profileId) {
				return profileCorporationConfigEntityRepository.findByProfile(profileId.getValue(), Boolean.TRUE)
				.stream()
				.map(CorporationOperativeProfile::getId).map(CorporationOperativeProfilePK::getCorporationOperative)
				.map(CorporationOperative::getId)
				.map(CorporationOperativeId::new)
				.collect(Collectors.toList());
	}
	
	@SuppressWarnings("removal")
	@Override
	public List<Long> getCorporationOperativeId(ProfileId profileId) {
				return profileCorporationConfigEntityRepository.findByProfile(profileId.getValue(), Boolean.TRUE)
				.stream()
				.map(CorporationOperativeProfile::getId).map(CorporationOperativeProfilePK::getCorporationOperative)
				.map(CorporationOperative::getId)
				.map(Long::new)
				.collect(Collectors.toList());
	}

}
