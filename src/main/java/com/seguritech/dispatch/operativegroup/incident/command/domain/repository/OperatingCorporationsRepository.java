package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import java.util.List;

import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntity;

public interface OperatingCorporationsRepository {

	 List<OperativeGroupGeofencesEntity> find(Long corporationId);

	 List<OperativeGroupGeofencesEntity> findDefault(Long corporationId);
	 
	 OperativeGroupGeofencesEntity findDefaultGeofence(Long groupOperativeId);
	 
}
