package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class SourceRequiredException extends BusinessException {
    public SourceRequiredException() {
        super(MessageResolver.SOURCE_REQUERID_EXCEPTION, 3034L);
    }
}
