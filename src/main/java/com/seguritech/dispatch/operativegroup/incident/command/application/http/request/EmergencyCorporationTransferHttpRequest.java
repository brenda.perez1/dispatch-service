package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationTransferHttpRequest {
    private Long sourceCorporationId;
    private Long profileId;
}
