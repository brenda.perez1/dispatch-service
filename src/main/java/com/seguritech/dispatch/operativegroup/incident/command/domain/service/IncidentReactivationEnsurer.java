package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentStatusBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.platform.Service;

@Service
public class IncidentReactivationEnsurer {

    private final IncidentTypeBoundedFinder incidentBoundedFinder;
    private final IncidentRepository incidentRepository;
    private final IncidentStatusBounded checkStatus = IncidentStatusBounded.closed();

    public IncidentReactivationEnsurer(IncidentTypeBoundedFinder incidentBoundedFinder, IncidentRepository incidentRepository) {
        this.incidentBoundedFinder = incidentBoundedFinder;
        this.incidentRepository = incidentRepository;
    }

    public void check(IncidentId incidentId, IncidentCorporationReasonBounded reactivationReasonBounded) {
        IncidentBounded incident = this.incidentBoundedFinder.find(incidentId);
        if (incident.getStatus().equals(checkStatus)) {
            this.incidentRepository.reactivate(incidentId, reactivationReasonBounded);
        }
    }
}
