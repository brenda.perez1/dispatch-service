package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentToOperativeGroupsResponse {
	
	private final IncidentToOperativeGroupsPkResponse id;
	private final String status;
	private final String observationClose;
	private final String observationCancel;
	private final String createdBy;
	private final String updateBy;
	private final String closedBy;
	private final String cancelBy;
	private final String transferedBy;
	private final Integer mission;
	private final Date creationDate;
	private final Date updateDate;
	private final Date closedDate;
	private final Date cancelDate;
	private final Date transferedDate;
	private final Date assignedUnitForceDate;
	private final IncidentCloseResponse reasonClose;
	private final IncidentCancellationResponse reasonCancel;
	private final IncidentLocationResponse location;
	
}
