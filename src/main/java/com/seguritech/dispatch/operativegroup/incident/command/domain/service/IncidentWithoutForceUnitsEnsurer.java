package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentWithForceUnitAsiggnedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;

public class IncidentWithoutForceUnitsEnsurer {
    private final IncidentOperatingCorporationRepository repository;

    public IncidentWithoutForceUnitsEnsurer(IncidentOperatingCorporationRepository repository) {
        this.repository = repository;
    }

    public void ensurer(CorporationOperativeId corporationId, IncidentId incidentId) {
        if (!this.repository.searchUnitForce(corporationId, incidentId).isEmpty())
            throw new IncidentWithForceUnitAsiggnedException(incidentId.getValue());
    }
}
