package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.google.common.collect.Lists;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ReactivateCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ReactivateMultiCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.*;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentReactivationEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

import java.util.Collection;

@Service
public class ReactivateOperatingCorporationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentReactivationEnsurer incidentRepository;
    private final IncidentOperatingCorporationRepository incidentCorporationRepository;
    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final DomainEventPublisher publisher;


    public ReactivateOperatingCorporationUseCase(IncidentOperatingCorporationFinder finder, IncidentReactivationEnsurer incidentRepository,
                                                 IncidentOperatingCorporationRepository incidentCorporationRepository,
                                                 ProfileCorporationConfigRepository profileCorporationConfigRepository, DomainEventPublisher publisher) {
        this.finder = finder;
        this.incidentRepository = incidentRepository;
        this.incidentCorporationRepository = incidentCorporationRepository;
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.publisher = publisher;
    }

    public void execute(ReactivateCorporationRequest request) {

        UserId reactivatedBy = new UserId(request.getReactivatedBy());

        IncidentCorporationReasonId reasonId = IncidentCorporationReasonId.required(
                request.getReactivationReasonId()
        );

        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getCorporationId());
        ProfileId profileId = ProfileId.required(request.getProfileId());

        this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);

        IncidentCorporationReasonBounded reactivationReasonBounded = new IncidentCorporationReasonBounded(
                reasonId,
                IncidentCorporationObservation.optional(request.getObservation()),
                reactivatedBy,
                UserId.empty());

        IncidentCorporationModel corporationModel = this.finder.findById(corporationId, incidentId);

        corporationModel.reactivate(reactivatedBy);

        //Reactivate  Incident
        this.incidentRepository.check(incidentId, reactivationReasonBounded);

        this.incidentCorporationRepository.save(corporationModel);

        this.publisher.publish(corporationModel.pullEvents());

    }

    public void executeMultipleCorporation(ReactivateMultiCorporationRequest request) {

        UserId reactivatedBy = new UserId(request.getReactivatedBy());

        IncidentCorporationReasonId reasonId = IncidentCorporationReasonId.required(
                request.getReactivationReasonId());

        IncidentId incidentId = IncidentId.required(request.getIncidentId());

        Collection<IncidentCorporationModel> corporationModels = Lists.newArrayList();
        request.getCorporationIds().forEach(corporation -> {
            CorporationOperativeId corporationId = CorporationOperativeId.required(corporation);
            IncidentCorporationModel corporationModel = this.finder.findById(corporationId, incidentId);
            if(!corporationModel.getStatus().getValue().getValue().equals(IncidentCorporationStatusEnum.OPEN.getValue())) {
                corporationModel.reactivate(reactivatedBy);
                corporationModels.add(corporationModel);
                this.publisher.publish(corporationModel.pullEvents());
            }
        });

        IncidentCorporationReasonBounded reactivationReasonBounded = new IncidentCorporationReasonBounded(
                reasonId,
                IncidentCorporationObservation.optional(request.getObservation()),
                reactivatedBy,
                UserId.empty());

        this.incidentRepository.check(incidentId, reactivationReasonBounded);
        this.incidentCorporationRepository.saveAll(corporationModels);

    }

}