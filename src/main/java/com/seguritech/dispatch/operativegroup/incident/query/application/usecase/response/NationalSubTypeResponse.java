package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NationalSubTypeResponse {
	
	private final Long nationalSubTypeId;
	private final String name;
	private final String description;
	private final String clave;
	private final Boolean isSubType;
	private NationalTypeResponse nationalType;

}
