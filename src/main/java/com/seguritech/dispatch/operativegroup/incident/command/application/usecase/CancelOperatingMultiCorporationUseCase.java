package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCloseRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CancelIncidentMultiCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCloseException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.*;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.CorporationOperativeRepositoryImpl;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationReasonExistenceEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CancelOperatingMultiCorporationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentOperatingCorporationRepository repository;
    private final IncidentOperatingCorporationReasonExistenceEnsurer reasonEnsurer;
    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final DomainEventPublisher publisher;
    private final EmergenciesClient client;
    private final CorporationOperativeRepository corporationOperativeRepository;

    public CancelOperatingMultiCorporationUseCase(final IncidentOperatingCorporationFinder finder,
                                                  final IncidentOperatingCorporationRepository repository,
                                                  final IncidentOperatingCorporationReasonExistenceEnsurer reasonEnsurer,
                                                  final ProfileCorporationConfigRepository profileCorporationConfigRepository,
                                                  final DomainEventPublisher publisher,
                                                  final EmergenciesClient client,
                                                  final CorporationOperativeRepository corporationOperativeRepository) {
        this.finder = finder;
        this.repository = repository;
        this.reasonEnsurer = reasonEnsurer;
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.publisher = publisher;
        this.client = client;
        this.corporationOperativeRepository = corporationOperativeRepository;
    }
    
    @Transactional
    public void execute(final String token, final CancelIncidentMultiCorporationRequest request) {

        Collection<CorporationOperativeId> corporationOperativeIds =
            request.getOperatingCorporations().stream().map(CorporationOperativeId::required).collect(Collectors.toList());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        IncidentCorporationReasonId reasonId = IncidentCorporationReasonId.required(request.getReasonId());
        ProfileId profileId = ProfileId.required(request.getProfileId());
        IncidentCorporationReasonBounded reason = new IncidentCorporationReasonBounded(
            reasonId,
            IncidentCorporationObservation.optional(request.getObservation()),
            UserId.required(request.getCancelByUser()),
            new UserId(request.getCancelByUser()));

        this.reasonEnsurer.ensure(reasonId);
        
        corporationOperativeIds.forEach(corporationOperativeId
            -> this.check(profileId, corporationOperativeId));

        Collection<IncidentCorporationModel> models = request.getOperatingCorporations().stream().map(operatingCorporation
            -> this.finder.findById(new CorporationOperativeId(operatingCorporation),
                new IncidentId(request.getIncidentId()))).collect(Collectors.toList());

        models.forEach(model -> model.cancel(reason, reasonId));
        this.repository.saveAll(models);

        List<IncidentCorporationModel> listIncidentsOpen =
            this.repository.findByIncidentAndStatus(incidentId, IncidentCorporationStatusEnum.OPEN.toString());
        
        models.forEach(model -> this.publisher.publish(model.pullEvents()));
        
        try {
            if (listIncidentsOpen.size() == 0) /** Close parent incident **/
                client.closeAnEmergency(token, incidentId.getValue(), new EmergencyCloseRequest("Closed by System",0L));
        } catch (Exception e) {
            throw new IncidentCloseException();
        }
    }
    
    public void check(ProfileId profileId, CorporationOperativeId corporationOperativeId) {
    	if (!this.corporationOperativeRepository.isCorporationOperativeDefault(corporationOperativeId.getValue())) {
    		this.profileCorporationConfigEnsurer.ensure(profileId, corporationOperativeId);
		}
    }

}