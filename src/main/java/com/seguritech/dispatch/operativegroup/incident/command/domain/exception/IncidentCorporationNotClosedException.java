package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationNotClosedException extends BusinessException {

    public IncidentCorporationNotClosedException(Object... value) {
        super(MessageResolver.INCIDENT_CORPORATION_NOT_CLOSED_EXCEPTION, value, 3009L);
    }
}
