package com.seguritech.dispatch.operativegroup.incident.query.application.entity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.domain.model.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "cat_emergency_source")
@AllArgsConstructor
//@AttributeOverride(name = "id", column = @Column(name = "id_emergency_source"))
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class IncidentSourceEntity extends AbstractAuditEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_emergency_source")
	private Long id;

	private String name;
	private String clave;
	private String description;
	private String icon;

	public IncidentSourceEntity(Long id) {
		this.id = id;
	}
	
}
