package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OpGpIncidentPrioritySearchResponse {

    private Long priorityId;
    private String priority;
    private String color;

}
