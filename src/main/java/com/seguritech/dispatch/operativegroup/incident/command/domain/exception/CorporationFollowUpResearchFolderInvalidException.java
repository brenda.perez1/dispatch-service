package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class CorporationFollowUpResearchFolderInvalidException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public CorporationFollowUpResearchFolderInvalidException(Object... maxLength) {
        super(MessageResolver.CORPORATION_FOLLOWUP_RESEARCH_FOLDER_INVALID_EXCEPTION, maxLength, 3013L);
    }

}
