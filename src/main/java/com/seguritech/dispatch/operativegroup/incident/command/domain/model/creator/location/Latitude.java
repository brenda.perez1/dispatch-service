package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class Latitude {

    private Double value;

    public static Latitude optional(Double value) {
        return new Latitude(value);
    }
}
