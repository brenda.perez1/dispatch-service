package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

public enum IncidentCorporationStatusEnum {

    OPEN("OPEN"),
    CLOSED("CLOSED"),
    TRANSFERED("TRANSFERED"),
    ON_HOLD("ON_HOLD"),
    REJECTED("REJECTED"),
    CANCELED("CANCELED");

    private final String value;

    IncidentCorporationStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.value.toUpperCase();
    }

}
