package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class HostIpRequiredException extends BusinessException {
    public HostIpRequiredException() {
        super(MessageResolver.HOST_IP_REQUERID_EXCEPTION, 3004L);
    }
}
