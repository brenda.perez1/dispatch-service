package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.geofence.entity.Geofence;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.GeofenceId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentIdEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.OperatingCorporationEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.GeofenceOperativeGroupRepository;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class SupportGroupsIncidentCorporationUseCase {

	private final IncidentOperatingCorporationRepository incidentCorporationRepository;
	private final IncidentIdEnsurer incidentIdEnsurer;
	private final OperatingCorporationEnsurer operatingCorporationEnsurer;
	private final CorporationRepository corporationRepository;
	private final GeofenceOperativeGroupRepository geofenceOperativeGroupRepository;
	private final IncidentRepository incidentRepository;
	private final DomainEventPublisher publisher;
	
	public SupportGroupsIncidentCorporationUseCase(
			IncidentOperatingCorporationRepository incidentCorporationRepository,
			IncidentRepository incidentRepository,
			CorporationRepository corporationRepository,
			GeofenceOperativeGroupRepository geofenceOperativeGroupRepository,
			DomainEventPublisher publisher
			) {
		this.incidentCorporationRepository = incidentCorporationRepository;
		this.incidentIdEnsurer = new IncidentIdEnsurer(incidentRepository);
		this.operatingCorporationEnsurer = new OperatingCorporationEnsurer(corporationRepository);
		this.corporationRepository = corporationRepository;
		this.geofenceOperativeGroupRepository = geofenceOperativeGroupRepository;
		this.incidentRepository = incidentRepository;
		this.publisher = publisher;
	}

	public void execute(SupportGroupsIncidentCorporationRequest request) {
		IncidentId incidentId = IncidentId.required(request.getEmergencyId());
		incidentIdEnsurer.ensurer(incidentId);
		
		IncidentBounded incident = this.incidentRepository.find(incidentId);
		
		IncidentLocationBounded incidentLocationBounded = incidentRepository.findLocation(incidentId);
		List<IncidentCorporationModel> incidentCorporations = incidentCorporationRepository.findByIncident(incidentId);
		List<IncidentCorporationModel> newIncidentCorporations = new ArrayList<>(); 

		List<Long> currentOperativeGroups = incidentCorporations.stream().map(i -> i.getCorporationOperativeId().getValue()).collect(Collectors.toList());
		Optional.ofNullable(request.getOperativeGroups()).ifPresent(operativeGroups -> {
			operativeGroups.forEach(og -> {
				if(Boolean.FALSE.equals(currentOperativeGroups.contains(og))) {
					CorporationOperativeId newCorporationOperativeId = CorporationOperativeId.required(og);
					operatingCorporationEnsurer.ensure(newCorporationOperativeId);
					CorporationOperative newCorporationOperative = corporationRepository.findById(newCorporationOperativeId);
					
					IncidentCorporationModel newIncidentCorporationModel = IncidentCorporationModel.supportGroup(incidentId, newCorporationOperativeId, CorporationId.required(newCorporationOperative.getCorporation().getId()), UserId.from(request.getUserId()), incident.getFolio(), incident.getSourceName(), incident.getSubTypeName());
					
					List<Geofence> geofences = geofenceOperativeGroupRepository.getByOperativeGroupId(Arrays.asList(og));
					newIncidentCorporationModel.setGeofence(GeofenceId.required(geofences.stream().findFirst().get().getId()));
					newIncidentCorporationModel.setLocation(IncidentLocationId.from(incidentLocationBounded.getCoordinates().getLocationId()));
					newIncidentCorporations.add(newIncidentCorporationModel);
				}
			});
		});

		incidentCorporationRepository.saveAll(newIncidentCorporations);
		
		newIncidentCorporations.forEach(ic -> this.publisher.publish(ic.pullEvents()));
		
	}

}
