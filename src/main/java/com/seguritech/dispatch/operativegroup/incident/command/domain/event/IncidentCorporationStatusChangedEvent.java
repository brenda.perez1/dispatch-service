package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import java.util.Date;

import com.seguritech.platform.domain.DomainEvent;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentCorporationStatusChangedEvent implements DomainEvent  {

	 private final String incidentId;
	 private final Long corporationId;
	 private final String statusId;
	 private String updatedBy;
	 private final Date updateDate;

	
}