package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.domain.model.corporation.followup.status.entity.CorporationFollowUpStatusEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationFollowUpReasonRequiredException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCorporationFollowUpStatusId {

	private final Long value;

	public static IncidentCorporationFollowUpStatusId required(Long value) {
		return Optional.ofNullable(value).map(IncidentCorporationFollowUpStatusId::new)
				.orElseThrow(IncidentCorporationFollowUpReasonRequiredException::new);
	}

	public static IncidentCorporationFollowUpStatusId optional(CorporationFollowUpStatusEntity value) {
		return Optional.ofNullable(value).map(CorporationFollowUpStatusEntity::getId)
				.map(IncidentCorporationFollowUpStatusId::new).orElse(null);
	}

}
