package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationFollowUpStatusNotFoundException extends BusinessException {

    public IncidentCorporationFollowUpStatusNotFoundException() {
        super(MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_STATUS_NOT_FOUND_EXCEPTION, 3015L);
    }

}
