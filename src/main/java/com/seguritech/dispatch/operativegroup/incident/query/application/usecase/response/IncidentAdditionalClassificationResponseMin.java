package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.Data;

@Data
public class IncidentAdditionalClassificationResponseMin {
	
	private final String idEmergency;
	private final Long idNationalSubTypeId;

}
