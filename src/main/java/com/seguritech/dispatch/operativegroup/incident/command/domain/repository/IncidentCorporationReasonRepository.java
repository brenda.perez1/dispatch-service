package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;

public interface IncidentCorporationReasonRepository {

    IncidentCorporationReasonId findById(IncidentCorporationReasonId reasonId);

}
