package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationCodeRequiredException extends BusinessException {

    public IncidentCorporationCodeRequiredException() {
        super(MessageResolver.INCIDENT_CORPORATION_CODE_REQUIRED_EXCEPTION, 3007L);
    }
}
