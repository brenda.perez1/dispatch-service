package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import org.springframework.stereotype.Service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;

@Service
public class IncidentIdEnsurer {
    private final IncidentRepository repository;

    public IncidentIdEnsurer(IncidentRepository repository) {
        this.repository = repository;
    }

    public void ensurer(IncidentId incidentId) {
        if (!this.repository.exist(incidentId))
            throw new IncidentNotFoundException(incidentId.getValue());
    }

}
