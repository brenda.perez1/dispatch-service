package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationStatusRequiredException extends BusinessException {

    public IncidentCorporationStatusRequiredException() {
        super(MessageResolver.INCIDENT_CORPORATION_STATUS_REQUERID_EXCEPTION, 3018L);
    }
}
