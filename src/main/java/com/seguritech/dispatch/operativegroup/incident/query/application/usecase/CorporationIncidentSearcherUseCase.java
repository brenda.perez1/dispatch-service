package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentResponseMin;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentSubSetResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.PageDataProjectionExtendedDTO;
import com.seguritech.dispatch.operativegroup.incident.query.domain.CorporationIncidentQueryRepository;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.repository.PriorityCatalogRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.platform.Service;

@Service
public class CorporationIncidentSearcherUseCase {
	
	private final CorporationIncidentQueryRepository repository;
	private final ProfileCorporationConfigRepository profile;
	private final PriorityCatalogRepository catalogRepository;
	
	public CorporationIncidentSearcherUseCase(CorporationIncidentQueryRepository repository, ProfileCorporationConfigRepository profile,
			PriorityCatalogRepository catalogRepository) {
		this.repository = repository;
		this.profile = profile;
		this.catalogRepository = catalogRepository;
	}
	
	public PageDataProjectionExtendedDTO<IncidentResponseMin> execute (CorporationIncidentSearchRequest request) {		
		if (request.getCorporationId() == null || request.getCorporationId().isEmpty()) {
			request.setCorporationOperativeId(getOperativeGroups(request));
		}
		if (request.getPriorityId() == null || request.getPriorityId().isEmpty()) {
			request.setPriorityId(getPriorityIds());
		}
		return this.repository.search(request);
    }
	
	public PageDataProjectionExtendedDTO<IncidentResponseMin> executeAdvance(CorporationIncidentSearchRequest request) {		
		if (request.getCorporationOperativeId() == null || request.getCorporationOperativeId().isEmpty()) {
			request.setCorporationOperativeId(getOperativeGroups(request));
		}
		if (request.getPriorityId() == null || request.getPriorityId().isEmpty()) {
			request.setPriorityId(getPriorityIds());
		}
		return this.repository.searchAdvance(request);
    }

	public  PageDataProjectionDTO<IncidentSubSetResponse> execute(IncidentSearchRequest request) {
		return this.repository.searchByEmergencyId(request);
	}
	
	 public List<Long> getOperativeGroups(CorporationIncidentSearchRequest request){
		 return profile.getCorporationOperativeId(
		      		ProfileId.required(request.getProfileId())
		 );
	}
	 
	public List<Long> getPriorityIds() {
		return this.catalogRepository.findIds();
	}

}
