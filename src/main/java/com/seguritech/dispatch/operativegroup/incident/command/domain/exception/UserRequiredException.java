package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class UserRequiredException extends BusinessException {

    public UserRequiredException() {
        super(MessageResolver.USER_REQUERID_EXCEPTION, 3037L);
    }

}
