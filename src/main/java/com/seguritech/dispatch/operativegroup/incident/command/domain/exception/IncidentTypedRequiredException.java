package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentTypedRequiredException extends
    BusinessException {

  public IncidentTypedRequiredException() {
    super(MessageResolver.INCIDENT_TYPE_REQUERID_EXCEPTION, 3026L);
  }
}
