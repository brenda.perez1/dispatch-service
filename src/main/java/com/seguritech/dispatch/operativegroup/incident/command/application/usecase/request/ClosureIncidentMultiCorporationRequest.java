package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

@Getter
@AllArgsConstructor
public class ClosureIncidentMultiCorporationRequest {

    private String incidentId;
    private Collection<Long> operativeGroupIds;
    private String userId;
    private Long profileId;
    private Long closeReasonId;
    private String observation;
}
