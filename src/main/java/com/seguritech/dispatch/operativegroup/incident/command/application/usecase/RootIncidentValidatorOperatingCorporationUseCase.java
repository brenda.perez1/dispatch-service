package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.RootIncidentCancelValidatorRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.RootIncidentClosureValidatorRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationValidatorModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ObservationCancel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ReasonCancelId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationValidatorRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentCorporationValidatorFinder;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class RootIncidentValidatorOperatingCorporationUseCase {
    private final DomainEventPublisher publisher;
    private final IncidentCorporationValidatorFinder incidentCorporationValidatorFinder;

    public RootIncidentValidatorOperatingCorporationUseCase(DomainEventPublisher publisher, IncidentOperatingCorporationValidatorRepository incidentRepository) {
        this.publisher = publisher;
        this.incidentCorporationValidatorFinder = new IncidentCorporationValidatorFinder(incidentRepository);
    }

    public IncidentCorporationValidatorModel execute(RootIncidentClosureValidatorRequest request) {
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        UserId userId = UserId.from(request.getUserId());
        //TODO: MEjorar implementación de reglas de negocio
        IncidentCorporationValidatorModel model = this.incidentCorporationValidatorFinder.find(incidentId);
        model.validateCanBeClosed(userId, incidentId);

        this.publisher.publish(model.pullEvents());
        return model;
    }

    public IncidentCorporationValidatorModel execute(RootIncidentCancelValidatorRequest request) {
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        UserId userId = UserId.from(request.getUserId());
        ReasonCancelId reasonCancelId = ReasonCancelId.optional(request.getReasonCancelId());
        ObservationCancel observationCancel = ObservationCancel.optional(request.getObservationCancel());
        //TODO: MEjorar implementación de reglas de negocio
        IncidentCorporationValidatorModel model = this.incidentCorporationValidatorFinder.find(incidentId);
        model.validateCanBeCancel(userId, incidentId, reasonCancelId, observationCancel);

        this.publisher.publish(model.pullEvents());
        return model;
    }

}
