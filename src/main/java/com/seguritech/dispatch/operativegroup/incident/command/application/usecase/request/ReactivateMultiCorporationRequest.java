package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;
import java.util.Date;

@Getter
@AllArgsConstructor
public class ReactivateMultiCorporationRequest {

    private final String incidentId;
    private final Collection<Long> corporationIds;
    private final String observation;
    private final Long reactivationReasonId;
    private final String reactivatedBy;
    private final Date reactivationDate;
    private Long profileId;

}