package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOpGpRepository;
import com.seguritech.dispatch.operativegroup.incident.query.domain.exception.IncidentOperativeGroupNotFoundException;
import com.seguritech.platform.Service;

@Service
public class IncidentOperativeGroupsSearchUseCase {


    private final IncidentOpGpRepository repository;

    public IncidentOperativeGroupsSearchUseCase (IncidentOpGpRepository repository) {this.repository = repository;}


    public List<IncidentOperativeGroupsResponse> search (IncidentOperativeGroupSearchRequest request) {
        List<IncidentOperativeGroupsResponse> response = this.repository.search(request);
        if (response.isEmpty()) {
            throw new IncidentOperativeGroupNotFoundException();
        }
        return response;
    }

}
