package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncidentLocationStreetviewResponse {

	private Double latitude;
	private Double longitude;
	private Double heading;
	private Double pitch;
	private String panoId;

}
