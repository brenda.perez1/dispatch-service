package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporation.entity.Corporation_;
import com.domain.model.corporationnational.subtype.entity.CorporationNationalSubType_;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity_;
import com.domain.model.corporationnational.subtype.entity.CorporationNationalSubType;
import com.domain.model.national.entity.IncidentNationalTypeEntity_;
import com.domain.model.specification.CatEntitySpecification;

public class IncidentCorporationSpecification extends CatEntitySpecification {

    /**
     * Filter corporation  matching
     **/

    public static Specification<Corporation> corporationMatch(String corporation) {
        return (root, query, criteriaBuilder) -> criteriaBuilder
            .like(criteriaBuilder.upper(root.get(Corporation_.DESCRIPTION)),
                "%" + corporation.toUpperCase() + "%");
    }

    /**
     * Filter corporation code  matching
     **/

    public static Specification<Corporation> corporationCodeMatch(String corporationCode) {
        return (root, query, criteriaBuilder) -> criteriaBuilder
            .like(criteriaBuilder.upper(root.get(Corporation_.NAME)),
                "%" + corporationCode.toUpperCase() + "%");
    }

    /**
     * Filter by emergency type custom
     **/

    public static Specification<Corporation> emergencyTypeId(Long emergencyTypeId, boolean active) {

        return (root, query, criteriaBuilder) -> {
            Join<Corporation, CorporationNationalSubType> rootJoin = root.join(Corporation_.DESCRIPTION);
            Predicate predicate = criteriaBuilder
                .equal(rootJoin.get(CorporationNationalSubType_.ID)
                    .get(IncidentNationalTypeEntity_.NATIONAL_TYPE_ID), emergencyTypeId);

            Predicate predicateActive = criteriaBuilder.equal(rootJoin.get(CorporationNationalSubType_.ACTIVE), active);
            return criteriaBuilder.and(predicate, predicateActive);
        };
    }


    /**
     * Filter by emergency type custom
     **/

    public static Specification<CorporationNationalSubType> active(boolean active) {

        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(CorporationNationalSubType_.ACTIVE), active);
    }

	public static Sort SortByEventDateAtDesc() {
		return Sort.by(Sort.Direction.DESC, IncidentCorporationEntity_.CREATION_DATE);
	}

	public static Sort SortByCreatedAtDesc() {
		return Sort.by(Sort.Direction.ASC, IncidentCorporationEntity_.CREATION_DATE);
	}
	
}
