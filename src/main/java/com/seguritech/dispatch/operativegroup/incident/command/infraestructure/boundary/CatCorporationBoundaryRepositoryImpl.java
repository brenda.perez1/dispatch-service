package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporation.entity.Corporation_;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationCode;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.CorporationCatEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.OperatingCorporationEntityRepository;

@Service
public class CatCorporationBoundaryRepositoryImpl implements CorporationRepository {

    private final CorporationCatEntityRepository repository;
    private final OperatingCorporationEntityRepository corporationOperativeRepository;


    public CatCorporationBoundaryRepositoryImpl(CorporationCatEntityRepository repository, OperatingCorporationEntityRepository corporationOperativeRepository) {
        this.repository = repository;
        this.corporationOperativeRepository = corporationOperativeRepository;
    }

    @Override
    public Boolean exist(CorporationOperativeId id) {
        return this.corporationOperativeRepository.existsById(id.getValue());
    }

    @Override
    public CorporationOperativeId find(IncidentCorporationCode code) {
        return this.repository.findOne((Specification<Corporation>) (root, query, criteriaBuilder) -> criteriaBuilder
                .equal(criteriaBuilder.lower(root.get(Corporation_.NAME)), code.getValue().toLowerCase()))
                .map(Corporation::getId).map(CorporationOperativeId::required).orElse(null);
    }

	@Override
	public CorporationOperative findById(CorporationOperativeId id) {
		return corporationOperativeRepository.getById(id.getValue());
	}
	
}
