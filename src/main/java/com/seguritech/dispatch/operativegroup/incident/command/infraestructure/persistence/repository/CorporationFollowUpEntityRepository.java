package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.domain.model.corporation.followup.entity.CorporationFollowUpEntity;

@Repository
public interface CorporationFollowUpEntityRepository
		extends PagingAndSortingRepository<CorporationFollowUpEntity, String> {

	Page<CorporationFollowUpEntity> findAllByIncidentId(String incident, Pageable pageable);

}
