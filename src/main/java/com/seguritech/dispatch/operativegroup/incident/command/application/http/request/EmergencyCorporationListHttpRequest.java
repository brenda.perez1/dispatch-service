package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import java.util.List;

import lombok.Data;

@Data
public class EmergencyCorporationListHttpRequest {

    private List<Long> corporationsId;
    private Boolean secondaryInvoice;

}
