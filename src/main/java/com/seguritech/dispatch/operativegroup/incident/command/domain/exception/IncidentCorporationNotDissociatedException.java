package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationNotDissociatedException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public IncidentCorporationNotDissociatedException() {
        super(MessageResolver.INCIDENT_CORPORATION_NOT_DISSOCIATED_EXCEPTION, 3010L);
    }
	
}
