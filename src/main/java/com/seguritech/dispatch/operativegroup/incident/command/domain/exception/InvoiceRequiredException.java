package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class InvoiceRequiredException extends BusinessException {
    public InvoiceRequiredException() {
        super(MessageResolver.INVOICE_REQUERID_EXCEPTION, 3030L);
    }
}