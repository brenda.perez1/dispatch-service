package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

import java.util.List;

@Data
public class EmergencyCorporationsReactivateApiRequest {

    private Long reactivationReasonId;
    private String observation;
    private List<Long> corporationsId;

}
