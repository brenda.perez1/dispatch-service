package com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Getter
@NoArgsConstructor
public class IncidentOperatingCorporationCancelEvent implements Serializable {
    private String emergencyId;
    private Long corporationId;
    private Long cancelReasonId;
    private String observation;
    private String cancelByUser;
    private Date cancelAt;
}
