package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import java.util.Optional;

import javax.transaction.Transactional;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CorporationFollowUpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationFollowUpModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.CorporationFollowUpObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.CorporationFollowUpResearchFolder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationFollowUpRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentCorporationFollowUpReasonExistenceEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentCorporationFollowUpStatusExistenceEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentIdEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.OperatingCorporationEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class OperatingCorporationFollowUpUseCase {

	private final DomainEventPublisher publisher;
	private final IncidentIdEnsurer incidentEnsurer;
	private final CorporationFollowUpRepository repository;
	private final IncidentOperatingCorporationFinder finder;
	private final OperatingCorporationEnsurer operatingCorporationEnsurer;
	private final IncidentOperatingCorporationRepository corporationRepository;
	private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
	private final IncidentCorporationFollowUpReasonExistenceEnsurer followUpReasonEnsurer;
	private final IncidentCorporationFollowUpStatusExistenceEnsurer followUpstatusEnsurer;

	public OperatingCorporationFollowUpUseCase(DomainEventPublisher publisher, IncidentIdEnsurer incidentEnsurer,
			CorporationFollowUpRepository repository, IncidentOperatingCorporationFinder finder,
			OperatingCorporationEnsurer operatingCorporationEnsurer,
			IncidentOperatingCorporationRepository corporationRepository,
			ProfileCorporationConfigRepository profileCorporationConfigRepository,
			IncidentCorporationFollowUpReasonExistenceEnsurer followUpReasonEnsurer,
			IncidentCorporationFollowUpStatusExistenceEnsurer followUpstatusEnsurer) {
		this.finder = finder;
		this.publisher = publisher;
		this.repository = repository;
		this.incidentEnsurer = incidentEnsurer;
		this.corporationRepository = corporationRepository;
		this.followUpReasonEnsurer = followUpReasonEnsurer;
		this.followUpstatusEnsurer = followUpstatusEnsurer;
		this.operatingCorporationEnsurer = operatingCorporationEnsurer;
		this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
	}

	@Transactional
	public void execute(CorporationFollowUpRequest request) {
		UserId userId = UserId.required(request.getUserId());
		ProfileId profileId = ProfileId.required(request.getProfileId());
		IncidentId incidentId = IncidentId.required(request.getIncidentId());
		CorporationOperativeId corporationOperativeId = CorporationOperativeId.required(request.getCorporationId());
		IncidentCorporationFollowUpReasonId reasonId = IncidentCorporationFollowUpReasonId.required(request.getFollowUpReason());
		IncidentCorporationFollowUpStatusId statusId = IncidentCorporationFollowUpStatusId.required(request.getFollowUpStatus());
		CorporationFollowUpObservation observation = CorporationFollowUpObservation.optional(request.getObservation());
		CorporationFollowUpResearchFolder researchFolder = CorporationFollowUpResearchFolder.optional(request.getResearchFolder());

		this.incidentEnsurer.ensurer(incidentId);
		this.followUpReasonEnsurer.ensure(reasonId);
		this.followUpstatusEnsurer.ensure(statusId);
		this.operatingCorporationEnsurer.ensure(corporationOperativeId);
		this.profileCorporationConfigEnsurer.ensure(profileId, corporationOperativeId);
		
		IncidentCorporationModel corporationModel = this.finder.findById(corporationOperativeId, incidentId);
		final IncidentCorporationFollowUpStatusId oldStatusId = 
				Optional.ofNullable(corporationModel.getFollowUpId()).orElse(null);
		
		CorporationFollowUpModel model = CorporationFollowUpModel.save(incidentId, corporationOperativeId, reasonId,
				statusId, oldStatusId, observation, researchFolder, userId);
		
		if (corporationModel != null)
			corporationModel.updateFollowUp(statusId, userId);

		this.repository.save(model);
		this.corporationRepository.save(corporationModel);
		this.publisher.publish(model.pullEvents());
	}

}
