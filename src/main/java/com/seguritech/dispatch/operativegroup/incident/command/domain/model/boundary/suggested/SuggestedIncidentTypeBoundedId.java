package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.suggested;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class SuggestedIncidentTypeBoundedId {
    private final Long value;
}
