package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.HostIpRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class HostIp {
    private final String value;

    private HostIp(String value) {
        this.value = value;
    }

    public static HostIp required(String ip) {
        Optional.ofNullable(ip).orElseThrow(HostIpRequiredException::new);
        return new HostIp(ip);
    }

}
