package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

@Getter
@AllArgsConstructor
public class CancelIncidentMultiCorporationRequest {

    private final String incidentId;
    private final Collection<Long> operatingCorporations;
    private final String cancelByUser;
    private final Long profileId;
    private final Long reasonId;
    private final String observation;

}