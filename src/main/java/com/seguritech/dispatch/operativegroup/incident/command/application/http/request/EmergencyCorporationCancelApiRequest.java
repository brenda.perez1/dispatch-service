package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationCancelApiRequest {

    private Long cancelReasonId;
    private String observation;
    private Long profileId;
}
