package com.seguritech.dispatch.operativegroup.incident.query.domain.exception;

import com.seguritech.dispatch.exception.NotFoundException;

public class IncidentOperativeGroupNotFoundException extends NotFoundException {

	private static final long serialVersionUID = 1L;

	public IncidentOperativeGroupNotFoundException () {
        super("Operative groups not found", 0L);
    }

}
