  
package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentTokenResponse {
    private final String token;
    private final Boolean active;

}