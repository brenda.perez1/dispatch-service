package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import java.util.Date;

import com.seguritech.platform.application.http.Order;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class FilterDTO {

    private final String status;
    private final Long priority;
    private Integer size;
    private Integer page;
    private final String query;
    private String periodDateField;
    private Date periodStartDate;
    private Date periodEndDate;
    private Order order;
    private String orderByField;

    public FilterDTO(Integer size, Integer page, String reason, String query, Long priority) {
        this.status = reason;
        this.priority = priority;
        this.size = size;
        this.page = page;
        this.query = query; 
    }

    public boolean isPageable () {
        return this.page != null && this.size != null;
    }
    
    public int getStart(Integer count, Integer limit) {
    	Double totalPages = 0.0;
    	if(count > 0) {
			Double ceil = ((double)count  / limit);
			totalPages = Math.ceil(ceil);
		}
		if(totalPages <= 0) {
			this.page = 1;
		}else if (page > totalPages)
			this.page = totalPages.intValue();
		
		int start =  ((limit * this.page ) -limit) ;
    	return start;
    }
}
