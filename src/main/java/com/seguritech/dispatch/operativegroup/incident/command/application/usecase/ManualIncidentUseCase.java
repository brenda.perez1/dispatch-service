package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;


import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CreateManualIncidentRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.CreateManualIncidentResponse;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.IncidentCreatorResponse;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.*;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentCreator;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentCreatorRequest;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

import java.util.Optional;

@Service
public class ManualIncidentUseCase {

    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final IncidentPreconfigured incidentPreconfigured;
    private final IncidentOperatingCorporationRepository incidentCorporationRepository;
    private final IncidentCreator incidentCreator;
    private final DomainEventPublisher publisher;

    public ManualIncidentUseCase(ProfileCorporationConfigRepository profileCorporationConfigRepository, IncidentPreconfigured incidentPreconfigured, IncidentOperatingCorporationRepository incidentCorporationRepository, IncidentBoundedRepository incidentBoundedRepository, DomainEventPublisher publisher) {
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.incidentPreconfigured = incidentPreconfigured;
        this.incidentCorporationRepository = incidentCorporationRepository;
        this.incidentCreator = new IncidentCreator(incidentBoundedRepository);
        this.publisher = publisher;
    }

    public CreateManualIncidentResponse execute(CreateManualIncidentRequest request) {
        CorporationOperativeId corporationId = new CorporationOperativeId(request.getOperatingCorporationId());
        ProfileId profileId = ProfileId.required(request.getProfileId());
        UserId userId = UserId.required(request.getUserId());

        this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);

        IncidentCreatorResponse incidentCreatorResponse = this.createIncidentOperator(request);
        IncidentId incidentId = incidentCreatorResponse.getIncidentId();

        //TODO set corporation id
        IncidentCorporationModel model = IncidentCorporationModel.assign(
                incidentId,
                corporationId,
                null,
                userId
        );

        this.incidentCorporationRepository.save(model);
        this.publisher.publish(model.pullEvents());
        return new CreateManualIncidentResponse(incidentId.getValue(), incidentCreatorResponse.getInvoice().getValue());
    }

    private IncidentCreatorResponse createIncidentOperator(CreateManualIncidentRequest request) {
        String phoneNumber = resolvePhoneNumber(request.getPhoneNumber());
        Long phoneNumberCountryId = this.resolvePhoneNumberCountryId(request.getPhoneNumberCountryId());

        return this.incidentCreator.generate(IncidentCreatorRequest.build(
                request.getSourceId(),
                phoneNumber,
                phoneNumberCountryId,
                request.getEventDate(),
                request.getCreatedFrom(),
                request.getDescription(),
                request.getUserId(),
                this.incidentPreconfigured.getIncidentTypeId(),
                this.incidentPreconfigured.getStateId(),
                this.incidentPreconfigured.getTownId(),
                this.incidentPreconfigured.getLatitude(),
                this.incidentPreconfigured.getLongitude()
        ));
    }

    private String resolvePhoneNumber(String phoneNumber) {
        return Optional.ofNullable(phoneNumber).orElse(this.incidentPreconfigured.getPhoneNumber());
    }

    private Long resolvePhoneNumberCountryId(Long phoneNumberCountryId) {
        return Optional.ofNullable(phoneNumberCountryId).orElse(this.incidentPreconfigured.getPhoneNumberCountryId());
    }

}
