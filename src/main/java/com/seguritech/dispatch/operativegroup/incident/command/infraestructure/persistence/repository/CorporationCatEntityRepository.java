package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.domain.model.corporation.entity.Corporation;

public interface CorporationCatEntityRepository extends JpaRepository<Corporation, Long>,
    JpaSpecificationExecutor<Corporation> {}
