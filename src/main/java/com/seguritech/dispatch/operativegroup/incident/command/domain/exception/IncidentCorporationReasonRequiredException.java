package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationReasonRequiredException extends BusinessException {

    public IncidentCorporationReasonRequiredException() {
        super(MessageResolver.INCIDENT_CORPORATION_REASON_REQUERID_EXCEPTION, 3015L);
    }

}
