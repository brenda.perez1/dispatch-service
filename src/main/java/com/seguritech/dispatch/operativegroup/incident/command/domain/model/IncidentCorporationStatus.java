package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationStatusInvalidException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationStatusRequiredException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class IncidentCorporationStatus {

    private final IncidentCorporationStatusEnum value;

    public IncidentCorporationStatus(IncidentCorporationStatusEnum value) {
        this.value = value;
    }

    public static IncidentCorporationStatus initial() {
        return new IncidentCorporationStatus(IncidentCorporationStatusEnum.OPEN);
    }
    public static IncidentCorporationStatus onHold() {
        return new IncidentCorporationStatus(IncidentCorporationStatusEnum.ON_HOLD);
    }

    public static IncidentCorporationStatus of(String value) {
        return Optional.ofNullable(value).map(IncidentCorporationStatus::insure).map(IncidentCorporationStatus::new)
            .orElseThrow(IncidentCorporationStatusRequiredException::new);
    }

    public IncidentCorporationStatus(String value) {
        this.value = ensureValid(value);
      }
    
    private IncidentCorporationStatusEnum ensureValid(String value) {
        if (value == null || value.trim().isEmpty()) {
          throw new IncidentCorporationStatusRequiredException();
        }
        for (IncidentCorporationStatusEnum status : IncidentCorporationStatusEnum.values()) {
          if (value.toUpperCase().equals(status.toString())) {
            return status;
          }
        }
        throw new IncidentCorporationStatusInvalidException();
      }
    
    private static IncidentCorporationStatusEnum insure(String value) {
        try {
            return IncidentCorporationStatusEnum.valueOf(value.toUpperCase());
        } catch (Exception e) {
            throw new IncidentCorporationStatusInvalidException(value);
        }
    }


}
