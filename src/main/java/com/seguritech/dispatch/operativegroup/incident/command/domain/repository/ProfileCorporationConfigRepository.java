package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import java.util.List;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;

public interface ProfileCorporationConfigRepository {
	
    boolean exist(ProfileId profileId, CorporationOperativeId corporationOperativeId);
    
    List<CorporationOperativeId> getCorporationOperativeIds(ProfileId profileId);
    
    List<Long> getCorporationOperativeId(ProfileId profileId);
    
}
