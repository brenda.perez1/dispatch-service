package com.seguritech.dispatch.operativegroup.incident.command.application.http;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.auth.AppUserPrincipal;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCorporationListHttpRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.AssignIncidentToListOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.AssignIncidentToOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.UnassignIncidentToOperatingCorporationUseCase;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignIncidentCorporationListRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AssignIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.UnassignIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ProfileIdRequierdException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.rest.CadLiteAPIResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = { "CORPORATION EMERGENCIES" })
@RequestMapping("/dispatch")
public class CorporationIncidentController {

	private final AssignIncidentToOperatingCorporationUseCase assignIncidentToOperatingCorporationUseCase;
	private final UnassignIncidentToOperatingCorporationUseCase unassignIncidentToOperatingCorporationUseCase;
	private final AssignIncidentToListOperatingCorporationUseCase assignIncidentToListOperatingCorporationUseCase;
	private static final String AUTHORIZATION_HEADER = "AUTHORIZATION";
	private static final String AUTHORIZATION_BEARER = "Bearer ";

	public CorporationIncidentController(AssignIncidentToOperatingCorporationUseCase assignIncidentToOperatingCorporationUseCase,
										 UnassignIncidentToOperatingCorporationUseCase unassignIncidentToOperatingCorporationUseCase,
										 AssignIncidentToListOperatingCorporationUseCase assignIncidentToListOperatingCorporationUseCase) {

		this.assignIncidentToOperatingCorporationUseCase = assignIncidentToOperatingCorporationUseCase;
		this.unassignIncidentToOperatingCorporationUseCase = unassignIncidentToOperatingCorporationUseCase;
		this.assignIncidentToListOperatingCorporationUseCase = assignIncidentToListOperatingCorporationUseCase;

	}

	@ApiOperation(value = "Add corporations for emergency care", nickname = "assignCorporation", notes = "Associate corporation to the Incident")
	@PostMapping("{emergency-id}/emergency")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<CadLiteAPIResponse<List<IncidentCorporationModel>>> assign(
			@PathVariable("emergency-id") String incidentId, @RequestBody EmergencyCorporationHttpRequest request,
			UsernamePasswordAuthenticationToken user, HttpServletRequest httpRequest) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		return CadLiteAPIResponse.build(this.assignIncidentToOperatingCorporationUseCase.execute(
				new AssignIncidentCorporationRequest(
						incidentId, 
						request.getCorporationId(),
						request.getSecondaryInvoice(), 
						principal.getId(), 
						Long.parseLong(activeProfile)),
				AUTHORIZATION_BEARER.concat(httpRequest.getHeader(AUTHORIZATION_HEADER))
		));
	}
	
	@ApiOperation(value = "Add corporations in list for emergency care", nickname = "assignCorporation", notes = "Associate corporation to the Incident")
	@PostMapping("{emergency-id}/emergency/list")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<CadLiteAPIResponse<List<IncidentCorporationModel>>> assignList(
			@PathVariable("emergency-id") String incidentId, @RequestBody EmergencyCorporationListHttpRequest request,
			UsernamePasswordAuthenticationToken user, HttpServletRequest httpRequest) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		return CadLiteAPIResponse.build(this.assignIncidentToListOperatingCorporationUseCase.execute(
				new AssignIncidentCorporationListRequest(
						incidentId, 
						request.getCorporationsId(),
						request.getSecondaryInvoice(), 
						principal.getId(), 
						Long.parseLong(activeProfile)),
				AUTHORIZATION_BEARER.concat(httpRequest.getHeader(AUTHORIZATION_HEADER))
				));
	}

	@ApiOperation(value = "Remove corporation for emergency care", nickname = "unassignCorporation", notes = "Disassociate corporation to the Incident")
	@PostMapping("{emergency-id}/emergency/unassign")
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<CadLiteAPIResponse<String>> unassign(
			@PathVariable("emergency-id") String incidentId, @RequestBody EmergencyCorporationHttpRequest request,
			UsernamePasswordAuthenticationToken user) {

		AppUserPrincipal principal = ((AppUserPrincipal) user.getPrincipal());
		String activeProfile = principal.getActiveProfile().orElseThrow(ProfileIdRequierdException::new);
		return CadLiteAPIResponse
				.build(this.unassignIncidentToOperatingCorporationUseCase.execute(new UnassignIncidentCorporationRequest(
						incidentId, request.getCorporationId(), principal.getId(), Long.parseLong(activeProfile))));
	}

}