package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import com.seguritech.platform.application.http.Order;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Date;

@Getter
@EqualsAndHashCode
public abstract class AbstractSearcherRequest {

    protected final Integer size;
    protected Integer page;
    protected final String query;
    protected String periodDateField;
    protected Date periodStartDate;
    protected Date periodEndDate;
    protected Order order;
    protected String orderByField;

    public AbstractSearcherRequest (Integer size, Integer page, String query) {
        this.size = size;
        this.page = page;
        this.query = query; // El campo query tiene mayor prioridad sobre el resto de campos.
    }

    public AbstractSearcherRequest (Integer size, Integer page, String query, String periodDateField,
                                    Date periodStartDate, Date periodEndDate, Order order, String orderByField) {
        this.size = size;
        this.page = page;
        this.query = query; // El campo query tiene mayor prioridad sobre el resto de campos.
        this.periodDateField = periodDateField;
        this.periodStartDate = periodStartDate;
        this.periodEndDate = periodEndDate;
        this.order = order != null ? order : Order.ASC;
        this.orderByField = orderByField;
    }


    public boolean isPageable () {
        return this.page != null && this.size != null;
    }
    
    public int getStart(Integer count, Integer limit) {
    	Double totalPages = 0.0;
    	if(count > 0) {
			Double ceil = ((double)count  / limit);
			totalPages = Math.ceil(ceil);
		}
		if(totalPages <= 0) {
			this.page = 1;
		}else if (page > totalPages)
			this.page = totalPages.intValue();
		
		int start =  ((limit * this.page ) -limit) ;
    	return start;
    }

}
