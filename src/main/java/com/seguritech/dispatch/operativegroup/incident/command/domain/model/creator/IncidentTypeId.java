package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class IncidentTypeId {
    private Long value;

    public static IncidentTypeId optional(Long id) {
        return new IncidentTypeId(id);
    }

}
