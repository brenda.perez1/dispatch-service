package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import lombok.Getter;

import java.util.Optional;

@Getter
public class ObservationCancel {
    private static final String DEFAULT_VALUE = "";
    private String value;

    private ObservationCancel(String value) {
        this.value = value;
    }

    public static ObservationCancel optional(String value) {
        return new ObservationCancel(Optional.ofNullable(value).orElse(DEFAULT_VALUE));
    }

}
