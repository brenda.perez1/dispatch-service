package com.seguritech.dispatch.operativegroup.incident.command.domain.model;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.CorporationRequiredException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationsRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@EqualsAndHashCode
public class CorporationOperativeId {

    private final Long value;

    public CorporationOperativeId(Long value) {
        this.value = Optional.ofNullable(value).orElseThrow(CorporationRequiredException::new);
    }

    public static CorporationOperativeId required(Long value) {
        return new CorporationOperativeId(value);
    }

    public static List<CorporationOperativeId> requiredOf(List<Long> values) {
        if (values == null || values.isEmpty()) {
            throw new IncidentCorporationsRequiredException();
        }
        return values.stream().map(CorporationOperativeId::required).collect(Collectors.toList());
    }
}
