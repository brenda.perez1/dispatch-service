package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.List;

@Getter
@AllArgsConstructor
public class IncidentOperatingCorporationAssignedCorpEvent implements DomainEvent {

  private final String incidentId;
  private final Long corporationId;
  private final String createdByUser;
  private final Date createdAt;
  private final String folio;
  private final String sourceName;
  private final Long sourceId;
  private final Boolean isManual;
  private final String subTypeName;
  private final String status;
  private final Boolean secondaryInvoice; 
  private final String updateByUser;
  private final Date updatedAt;
  private final List<Long> listCorporationOperativeId;
  private final Boolean playSound;
}
