package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncidentCorporationSearchDetailResponseMin {
	
	private final IncidentCorporationSearchPKResponse id;
	private final String status;     
    private final Integer mission;    
	private final Date creationDate;
	private final Date updateDate;
    private final Date assignedUnitForceDate;
    private final Boolean isAssignedUnitForce;
    private final String createdBy; 
	private final String updateBy;
	private final GeofenceResponse geofence;

}
