package com.seguritech.dispatch.operativegroup.incident.query.domain;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.CorporationIncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentResponseMin;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentSubSetResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.PageDataProjectionExtendedDTO;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;

public interface CorporationIncidentQueryRepository {
	   
	 PageDataProjectionExtendedDTO<IncidentResponseMin> search(CorporationIncidentSearchRequest request);

	 PageDataProjectionDTO<IncidentSubSetResponse> searchByEmergencyId(IncidentSearchRequest request);
	 
	 PageDataProjectionExtendedDTO<IncidentResponseMin> searchAdvance(CorporationIncidentSearchRequest request);

}
