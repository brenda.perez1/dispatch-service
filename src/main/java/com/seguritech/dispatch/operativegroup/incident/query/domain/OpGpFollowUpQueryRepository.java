package com.seguritech.dispatch.operativegroup.incident.query.domain;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.OperativeGpFollowUpSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpFollowUpSearchResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;

public interface OpGpFollowUpQueryRepository {

	PageDataProjectionDTO<OpGpFollowUpSearchResponse> search(OperativeGpFollowUpSearchRequest request);

}
