package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentOperativeGroupsResponse {

    private final Long operativeGroupId;
    private final String name;

}
