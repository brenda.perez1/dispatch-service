package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationReasonCloseRequiredException extends BusinessException {

	public IncidentCorporationReasonCloseRequiredException() {
		super(MessageResolver.INCIDENT_CORPORATION_REASON_CLOSE_REQUERID_EXCEPTION, 3081L);
	}

}
