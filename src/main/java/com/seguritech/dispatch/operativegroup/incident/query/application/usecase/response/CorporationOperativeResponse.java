package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CorporationOperativeResponse {

	 private final Long id;
	 private final String name;
	 private final String clave;
	 private final Boolean active;
	 private final CorporationResponse corporation;
	 
}
