package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentLocationBounded {
    private final IncidentId id;
    private final IncidentCoordinatesBounded coordinates;
}
