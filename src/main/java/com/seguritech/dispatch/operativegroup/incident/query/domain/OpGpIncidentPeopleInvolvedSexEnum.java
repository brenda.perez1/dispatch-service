package com.seguritech.dispatch.operativegroup.incident.query.domain;

public enum OpGpIncidentPeopleInvolvedSexEnum {

    MALE("MALE"),
    FEMALE("FEMALE");

    private final String value;

    OpGpIncidentPeopleInvolvedSexEnum (String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
