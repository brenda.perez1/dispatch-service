package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PriorityResponse {
	
	private Long id;
	private String name;
	private String description;
	private String textColor;
	private String backColor;

}
