package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AssignSupportGroupsIncidentCorporationRequest {

	private final List<Long> operativeGroups;
	
	public AssignSupportGroupsIncidentCorporationRequest() {
		this.operativeGroups = null;
	}

}
