package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

@Data
public class EmergencyCorporationRejectSupportHttpRequest {
    private Long profileId;
}
