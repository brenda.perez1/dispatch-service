package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domain.model.corporation.followup.status.entity.CorporationFollowUpStatusEntity;
import com.seguritech.platform.Service;

@Service
public interface CorporationFollowUpStatusEntityRepository extends JpaRepository<CorporationFollowUpStatusEntity, Long> {

}
