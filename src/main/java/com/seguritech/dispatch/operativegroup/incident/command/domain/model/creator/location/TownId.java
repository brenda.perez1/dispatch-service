package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class TownId {

    private Long value;

    public static TownId optional(Long id) {
        return new TownId(id);
    }
}
