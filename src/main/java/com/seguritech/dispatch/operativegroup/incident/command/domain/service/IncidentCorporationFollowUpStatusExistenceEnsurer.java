package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationFollowUpStatusNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationFollowUpStatusRepository;
import com.seguritech.platform.Service;

import java.util.Optional;

@Service
public class IncidentCorporationFollowUpStatusExistenceEnsurer {

    private final CorporationFollowUpStatusRepository repository;

    public IncidentCorporationFollowUpStatusExistenceEnsurer(CorporationFollowUpStatusRepository repository) {
        this.repository = repository;
    }

    public void ensure(IncidentCorporationFollowUpStatusId reasonId) {
        Optional.ofNullable(this.repository.findById(reasonId))
                .orElseThrow(IncidentCorporationFollowUpStatusNotFoundException::new);
    }

}
