package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpGpIncidentLocationSectorSearchResponse {

    private final Long sectorId;
    private final String clave;

}
