package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationObservationInvalidException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public IncidentCorporationObservationInvalidException(Object... maxLength) {
        super(MessageResolver.INCIDENT_CORPORATION_OBSERVATION_INVALID_EXCEPTION, maxLength, 3013L);
    }

}
