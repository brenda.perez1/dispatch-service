package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.corporation.entity.Corporation_;
import com.domain.model.corporation.followup.status.entity.CorporationFollowUpStatusEntity;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.geofence.entity.Geofence;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incident.entity.IncidentEntity_;
import com.domain.model.incident.entity.IncidentLocationEntity;
import com.domain.model.incidentcancellation.entity.IncidentCancellationEntity;
import com.domain.model.incidentclose.entity.IncidentCloseEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;
import com.domain.model.unit.entity.UnitForceConfigEntity;
import com.domain.model.unit.entity.UnitForceConfigEntity_;
import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk;
import com.domain.model.unit.entity.UnitMissionEntityPk_;
import com.domain.model.unit.entity.UnitMissionEntity_;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.FilterDTO;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.GeofenceId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatus;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.unit.UnitForceList;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentOperatingCorporationEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentOperatingCorporationUnitMissionEntityRepository;

@Service 
public class IncidentOperatingCorporationRepositoryImpl implements IncidentOperatingCorporationRepository, CorporationConfigRepository {

    private static final String CREATION_DATE = "creationDate";
	private final Boolean automaticAssignmentEnable;
    private final IncidentOperatingCorporationEntityRepository repository;
    private final IncidentOperatingCorporationUnitMissionEntityRepository unitMissionEntityRepository;
    
    public IncidentOperatingCorporationRepositoryImpl(
            @Value("${cad-lite.incident.config.corporation.automatic:false}") Boolean automaticAssignmentEnable,
            IncidentOperatingCorporationEntityRepository repository,
            IncidentOperatingCorporationUnitMissionEntityRepository unitMissionEntityRepository) {
        this.automaticAssignmentEnable = automaticAssignmentEnable;
        this.repository = repository;
        this.unitMissionEntityRepository = unitMissionEntityRepository;
    }

    @Override
    public IncidentCorporationModel save(IncidentCorporationModel model) {
        this.repository.save(toEntity(model));
        return model;
    }

	@Override
	public Collection<IncidentCorporationModel> saveAll(Collection<IncidentCorporationModel> models) {
		Collection<IncidentCorporationEntity> entities = models.stream().map(this::toEntity).collect(Collectors.toList());
		this.repository.saveAll(entities);
		return models;
	}

    @Override
    public void save(List<IncidentCorporationModel> model) {
        System.out.println("IN SAVE ALL " + model.size());
        model.forEach(x -> System.out.println("STATUS " + x.getStatus().getValue().getValue()));
        model.forEach(this::save);
    }

    @Override
    public void remove(IncidentCorporationModel model) {
        this.repository
                .delete(new IncidentCorporationEntity(new IncidentCorporationEntityPk(new IncidentEntity(model.getIncidentId()
                        .getValue()), new CorporationOperative(model.getCorporationOperativeId().getValue()))));
    }

    public IncidentCorporationModel findById(CorporationOperativeId corporationId, IncidentId incidentId) {
        return this.repository.findById(new IncidentCorporationEntityPk(
                new IncidentEntity(incidentId.getValue()),
                new CorporationOperative(corporationId.getValue())))
                .map(entity -> {
                	IncidentCorporationModel incidentCorporationModel =  this.toModel(entity, corporationId, incidentId);
                	incidentCorporationModel.setGeofence(Optional.ofNullable(entity.getGeofence()).map(x -> GeofenceId.required(x.getId())).orElse(null));
                	incidentCorporationModel.setLocation(IncidentLocationId.required(entity.getLocation().getIdEmergencyLocation()));

                	return incidentCorporationModel;
                })
                .orElse(null);
    }
    
    public List<IncidentCorporationModel> findById(CorporationId corporationId, IncidentId incidentId) {
        return Optional.ofNullable(this.repository.findByCorporationIdAndIncidentId(new Corporation(corporationId.getValue()), 
        		new IncidentEntity(incidentId.getValue())).stream().map(x -> setIncidentCorporationModel(x, corporationId, incidentId)).collect(Collectors.toList())).orElse(null);
    }
    
    public IncidentCorporationModel setIncidentCorporationModel (IncidentCorporationEntity entity, CorporationId corporationId, IncidentId incidentId) {
    	IncidentCorporationModel incidentCorporationModel =  this.toModel(entity, corporationId, incidentId);
		incidentCorporationModel.setGeofence(GeofenceId.optional(entity.getGeofence()));
    	incidentCorporationModel.setLocation(IncidentLocationId.required(entity.getLocation().getIdEmergencyLocation()));
    	return incidentCorporationModel;
    }
    
    @SuppressWarnings("removal")
	public IncidentCorporationEntity getByIncident(List<CorporationOperativeId> corporationOperativeIds, IncidentId incidentId) {
    	List<Long> corporationOperativeLongs = corporationOperativeIds.stream()
    			.map(CorporationOperativeId::getValue).map(Long::new)
    			.collect(Collectors.toList());
    	return this.repository.getIncidentByIdAndCorporation(corporationOperativeLongs, incidentId.getValue());
    }

	@Override
    public List<IncidentCorporationModel> findByIncident(IncidentId id) {
        return this.repository.findByIdIncident(new IncidentEntity(id.getValue()))
                .map(x -> x.stream()
                        .map(this::toModelCorporation).collect(Collectors.toList())).orElse(Collections.emptyList());
    }

    private IncidentCorporationModel toModelCorporation(IncidentCorporationEntity entity) {
        return new IncidentCorporationModel(
                IncidentId.required(entity.getId().getIncident().getId()),
                new CorporationOperativeId(entity.getId().getCorporationOperative().getId()),
                CorporationId.required(entity.getCorporation().getId()),
                UnitForceList.required(Arrays.asList()),
                IncidentCorporationStatus.of(entity.getStatus()),
                IncidentCorporationReasonId.optional(entity.getReasonCancel()),
                IncidentCorporationObservation.optional(entity.getObservationCancel()),
                UserId.required(entity.getCreatedBy()),
                entity.getCreationDate(),
                UserId.optional(entity.getCancelBy()),
                entity.getCancelDate(),
                UserId.optional(entity.getClosedBy()),
                entity.getClosedDate(),
                UserId.optional(entity.getTransferedBy()),
                entity.getTransferedDate(),
                IncidentCorporationReasonCloseId.optional(entity.getReasonClose()),
                IncidentCorporationObservation.optional(entity.getObservationClose()),
                IncidentCorporationFollowUpStatusId.optional(entity.getFollowUpStatus()),
                UserId.optional(entity.getUpdateBy()),
                entity.getUpdateDate()
        		);
    }

    public IncidentCorporationEntity toEntity(IncidentCorporationModel model) {
        return new IncidentCorporationEntity(
        		new IncidentCorporationEntityPk(
        				new IncidentEntity(model.getIncidentId().getValue()),
        				new CorporationOperative(model.getCorporationOperativeId().getValue())
        		),
                model.getStatus().getValue().getValue(),
                model.getCreatedByUser() == null || model.getCreatedByUser().isEmpty() ? null : model.getCreatedByUser().getValue(),
                model.getCreatedAt(),
                model.getObservation().getValue(),
                model.getObservationClose().getValue(),
                model.getCancelByUser() == null || model.getCancelByUser().getValue() == null || model.getCancelByUser().getValue().isEmpty() ? null : model.getCancelByUser().getValue(),
                model.getCancelAt(),
                model.getReasonId() == null || model.getReasonId().getValue().toString().isEmpty()  ? null : new IncidentCancellationEntity(model.getReasonId().getValue()),
                model.getReasonCloseId() == null ? null : new IncidentCloseEntity(model.getReasonCloseId().getValue()),
                new Corporation(model.getCorporationId().getValue()),
                /**
                 * Se tiene que ajustar al campo updateBy cuando se corriga desde el modelo
                 * IncidentCorporationModel *****
                 * 
                 */
                model.getCreatedByUser() == null || model.getCreatedByUser().getValue() == null || model.getCreatedByUser().getValue().isEmpty() ? null : model.getCreatedByUser().getValue(),//updateBy
                model.getUpdateAt(),//updateDate
                model.getClosedByUser() == null  || model.getClosedByUser().getValue() == null || model.getClosedByUser().getValue().isEmpty() ? null : model.getClosedByUser().getValue(),//ClsedBy
                model.getClosedAt(),//ClosedDate
                model.getTraferedByUser() == null || model.getTraferedByUser().getValue() == null || model.getTraferedByUser().getValue().isEmpty() ? null : model.getTraferedByUser().getValue(),
                model.getTraferedAt(),
                model.getGeofence() == null ? null : new Geofence(model.getGeofence().getValue()),
                new IncidentLocationEntity(model.getLocation().getValue()),
                model.getAssignedUnitForceDate(),
                model.getFollowUpId() == null ? null : new CorporationFollowUpStatusEntity(model.getFollowUpId().getValue()),
                null
        );
    }

    public IncidentCorporationModel toModel(IncidentCorporationEntity entity, CorporationOperativeId corporationId, IncidentId incidentId) {
        return new IncidentCorporationModel(
                IncidentId.required(entity.getId().getIncident().getId()),
                new CorporationOperativeId(entity.getId().getCorporationOperative().getId()),
                CorporationId.required(entity.getCorporation().getId()),
                UnitForceList.required(this.searchUnitForce(corporationId, incidentId)),
                IncidentCorporationStatus.of(entity.getStatus()),
                entity.getReasonCancel() == null ? null :IncidentCorporationReasonId.required(entity.getReasonCancel().getId()),
                IncidentCorporationObservation.optional(entity.getObservationCancel()),
                new UserId(entity.getCreatedBy()),
                entity.getCreationDate(),
                new UserId(entity.getCancelBy()),
                entity.getCancelDate(),
                new UserId(entity.getClosedBy()),
                entity.getClosedDate(),
                new UserId(entity.getTransferedBy()),
                entity.getTransferedDate(),
                entity.getReasonClose() == null ? null : IncidentCorporationReasonCloseId.required(entity.getReasonClose().getId()),
                IncidentCorporationObservation.optional(entity.getObservationClose()),
                entity.getFollowUpStatus() == null ? null: IncidentCorporationFollowUpStatusId.required(entity.getFollowUpStatus().getId()),
                new UserId(entity.getUpdateBy()),
                entity.getUpdateDate()
        );
    }
    
    public IncidentCorporationModel toModel(IncidentCorporationEntity entity, CorporationId corporationId, IncidentId incidentId) {
        return new IncidentCorporationModel(
                IncidentId.required(entity.getId().getIncident().getId()),
                new CorporationOperativeId(entity.getId().getCorporationOperative().getId()),
                CorporationId.required(entity.getCorporation().getId()),
                UnitForceList.required(this.searchUnitForce(corporationId, incidentId)),
                IncidentCorporationStatus.of(entity.getStatus()),
                entity.getReasonCancel() == null ? null :IncidentCorporationReasonId.required(entity.getReasonCancel().getId()),
                IncidentCorporationObservation.optional(entity.getObservationCancel()),
                new UserId(entity.getCreatedBy()),
                entity.getCreationDate(),
                new UserId(entity.getCancelBy()),
                entity.getCancelDate(),
                new UserId(entity.getClosedBy()),
                entity.getClosedDate(),
                new UserId(entity.getTransferedBy()),
                entity.getTransferedDate(),
                entity.getReasonClose() == null ? null : IncidentCorporationReasonCloseId.required(entity.getReasonClose().getId()),
                IncidentCorporationObservation.optional(entity.getObservationClose()),
                entity.getFollowUpStatus() == null ? null: IncidentCorporationFollowUpStatusId.required(entity.getFollowUpStatus().getId()),
                new UserId(entity.getUpdateBy()),
                entity.getUpdateDate()
        );
    }

    @Override
    public List<String> searchUnitForce(CorporationOperativeId corporationId, IncidentId incidentId) {
        return this.unitMissionEntityRepository.findAll(
                Specification.where((Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                        .equal(root.get(UnitMissionEntity_.ID)
                                .get(UnitMissionEntityPk_.EMERGENCY_ID)
                                .get(IncidentEntity_.ID), incidentId.getValue()))
                        .and((Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                                .equal(root.get(UnitMissionEntity_.ID)
                                        .get(UnitMissionEntityPk_.UNIT_FORCE_CONFIG)
                                        .get(UnitForceConfigEntity_.CORPORATION_ENTITY)
                                        .get(Corporation_.ID), corporationId.getValue()))
                        .and((Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                                .isTrue(root.get(UnitMissionEntity_.ACTIVE))))
                .stream().map(UnitMissionEntity::getId)
                .map(UnitMissionEntityPk::getUnitForceConfig)
                .map(UnitForceConfigEntity::getIdUnitForceConfig)
                .collect(Collectors.toList());
    }
    
    @Override
    public List<String> searchUnitForce(CorporationId corporationId, IncidentId incidentId) {
        return this.unitMissionEntityRepository.findAll(
                Specification.where((Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                        .equal(root.get(UnitMissionEntity_.ID)
                                .get(UnitMissionEntityPk_.EMERGENCY_ID)
                                .get(IncidentEntity_.ID), incidentId.getValue()))
                        .and((Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                                .equal(root.get(UnitMissionEntity_.ID)
                                        .get(UnitMissionEntityPk_.UNIT_FORCE_CONFIG)
                                        .get(UnitForceConfigEntity_.CORPORATION_ENTITY)
                                        .get(Corporation_.ID), corporationId.getValue()))
                        .and((Specification<UnitMissionEntity>) (root, query, criteriaBuilder) -> criteriaBuilder
                                .isTrue(root.get(UnitMissionEntity_.ACTIVE))))
                .stream().map(UnitMissionEntity::getId)
                .map(UnitMissionEntityPk::getUnitForceConfig)
                .map(UnitForceConfigEntity::getIdUnitForceConfig)
                .collect(Collectors.toList());
    }
    
    @Override
    public Boolean automaticAssignmentEnable() {
        return automaticAssignmentEnable;
    }

	@SuppressWarnings("removal")
	@Override
	public List<IncidentCorporationEntity> findAll(List<CorporationOperativeId> corporationOperativeIds, FilterDTO filter) { //
	
		List<Long> corporationOperativeLongs = corporationOperativeIds.stream()
				.map(CorporationOperativeId::getValue).map(Long::new)
				.collect(Collectors.toList());		
		
		String status = IncidentCorporationStatus.of(filter.getStatus()).getValue().getValue();
		Integer limit = filter.getSize() != null ? filter.getSize() : 100;
		Long count = 0L;
		Long priority = filter.getPriority();
		String query = filter.getQuery() != null ? filter.getQuery().toUpperCase(): filter.getQuery() ;
		Date startDate = filter.getPeriodStartDate();
		Date endDate = filter.getPeriodEndDate();
		String orderBy = Objects.isNull(filter.getOrderByField()) || filter.getOrderByField().isEmpty() ? CREATION_DATE : filter.getOrderByField() ;
		Direction order = !Objects.isNull(filter.getOrder()) ? Sort.Direction.valueOf(filter.getOrder().name()) : Sort.Direction.ASC;
		
		if(query != null && startDate != null && endDate != null) {
			count = repository.countByQueryAndDateInitAndDateEnd(query,startDate,endDate,status);
		}else if(query != null && startDate != null) {
			count = repository.countByQueryAndDateInit(query,startDate,status);
		}else if(query != null && endDate != null) {
			count = repository.countByQueryAndDateEnd(query,endDate,status);
		}else if(startDate != null && endDate != null) {
			count = repository.countByDateInitAndDateEnd
					(startDate, endDate, status);
		}else if(filter.getPeriodStartDate() != null ) {
			count = repository.countByDateInit
					(filter.getPeriodStartDate());
		}else if(filter.getPeriodEndDate() != null) {
			count = repository.countByDateEnd
					(filter.getPeriodEndDate());
		}else if(filter.getStatus() != null) {
			count = repository.countByStatus(status);
		}else {
			count = repository.onlyCount();
		}
//		limit = limit > 100 ? 100 : limit; //valdiar null
		Integer start = filter.getStart(count.intValue(), limit); //validar null
		
		Pageable pageable = PageRequest.of(start, limit, order, orderBy);

		if( startDate != null && endDate != null) {
			return this.repository.findIncidentCorporationByQueryAndStatusAndDateInitAndDateEndAndPriority(
					query,
					corporationOperativeLongs,
					startDate,
					endDate,
					status,
					priority,
					pageable);
		} else if(startDate != null) {
			return this.repository.findIncidentCorporationByQueryAndStatusAndOrderByCreatedAtAscAndPriority(
					query,
					corporationOperativeLongs,
					startDate,
					status,
					priority,
					pageable);
		} else if(endDate != null) {
			return this.repository.findIncidentCorporationByQueryAndStatusAndDateEndAndPriority(
					query,
					corporationOperativeLongs,
					endDate,
					status,
					priority,
					pageable);
		}else if(filter.getStatus() != null) {
			return this.repository.findIncidentCorporationByQueryAndStatusAndPriority(
					query,
					corporationOperativeLongs,
					status,
					priority,
					pageable);
		}
		else {

		}
		return new ArrayList<>();		
	}

	@Override
	public List<IncidentCorporationModel> findByIncidentAndStatus(IncidentId id, String  status) {
		return this.repository.findByIdIncidentAndStatus(new IncidentEntity(id.getValue()), status)
				.map(x -> x.stream()
						.map(this::toModelCorporation).collect(Collectors.toList())).orElse(Collections.emptyList());
	}
	
	@Override
	public List<IncidentCorporationModel> findByIncidentAndStatusAndCorporation(IncidentId id, String  status, CorporationId corporationId) {
		return this.repository.findByIdIncidentAndStatusAndCorporation(new IncidentEntity(id.getValue()), status, new Corporation(corporationId.getValue()))
                .map(x -> x.stream()
                        .map(this::toModelCorporation).collect(Collectors.toList())).orElse(Collections.emptyList());
	}

	@SuppressWarnings("removal")
	@Override
	public List<IncidentCorporationEntity> getByIncidentAndCorporationsOperative(
			List<CorporationOperativeId> corporationOperativeIds, IncidentId incidentId) {
		List<Long> corporationOperativeLongs = corporationOperativeIds.stream()
    			.map(CorporationOperativeId::getValue).map(Long::new)
    			.collect(Collectors.toList());
    	return this.repository.getIncidentByIdAndCorporationOperative(corporationOperativeLongs, incidentId.getValue());
	}
	
	@Override
	public void updateCorporationOperativeByIncidentIdAndCorporationOperativeId(IncidentId incidentId, CorporationOperativeId currentCorporationOperativeId, CorporationOperativeId newCorporationOperativeId) {
		this.repository.updateCorporationOperativeByIncidentIdAndCorporationOperativeId(incidentId.getValue(), currentCorporationOperativeId.getValue(), newCorporationOperativeId.getValue());
	}
	
	@Override
	public IncidentCorporationModel findByIdIncidentAndCorporation(CorporationId corporationId, IncidentId incidentId) {
		IncidentCorporationEntity entity = this.repository.findTop1ByIdIncidentIdAndCorporationId(incidentId.getValue(),corporationId.getValue());				   
		return this.toModel(entity, corporationId, incidentId);
	}

	@Override
	public Long countByIncidentAndStatus(IncidentId id, String status) {
		return this.repository.countByIdIncidentIdAndStatus(id.getValue(),status);
	}

	@Override
	public Long countCorporationAssign(IncidentId id) {
		return this.repository.countByIdIncidentAndStatusIgnoreCase(new IncidentEntity(id.getValue()), IncidentCorporationStatusEnum.OPEN.getValue());
	}

	@Override
	public IncidentCorporationModel findByIdAndStatus(CorporationOperativeId corporationId, IncidentId incidentId,
			IncidentCorporationStatusEnum status) {
		 return this.repository.findByIdAndStatus(new IncidentCorporationEntityPk(
	                new IncidentEntity(incidentId.getValue()),
	                new CorporationOperative(corporationId.getValue())),
				 	status.getValue())
	                .map(entity -> {
	                	IncidentCorporationModel incidentCorporationModel =  this.toModel(entity, corporationId, incidentId);
	                	incidentCorporationModel.setGeofence(Optional.ofNullable(entity.getGeofence()).map(x -> GeofenceId.required(x.getId())).orElse(null));
	                	incidentCorporationModel.setLocation(IncidentLocationId.required(entity.getLocation().getIdEmergencyLocation()));

	                	return incidentCorporationModel;
	                })
	                .orElse(null);
	}
	
	

}
