package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.exception.NotFoundException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentNotFoundException extends BusinessException {
    
	public IncidentNotFoundException(Object... message) {
        super(MessageResolver.INCIDENT_NOT_FOUND_EXCEPTION, message, 3036L);
    }
	
}
