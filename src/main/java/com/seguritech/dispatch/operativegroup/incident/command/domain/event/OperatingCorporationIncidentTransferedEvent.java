package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class OperatingCorporationIncidentTransferedEvent implements DomainEvent {
	
    private String emergencyId;
    private Long oldcorporationOperativeId;
    private Long newcorporationOperativeId;
    private String transferedByUser;
    private Date transferedAt;

}
