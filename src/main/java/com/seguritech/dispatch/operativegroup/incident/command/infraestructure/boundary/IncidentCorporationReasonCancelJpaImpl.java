package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.boundary;

import com.domain.model.incidentcancellation.entity.IncidentCancellationEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentCorporationReasonRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentOperatingCorporationReasonEntityRepository;
import com.seguritech.platform.Service;

@Service
public class IncidentCorporationReasonCancelJpaImpl implements IncidentCorporationReasonRepository {

    private final IncidentOperatingCorporationReasonEntityRepository reasonEntityRepository;

    public IncidentCorporationReasonCancelJpaImpl(IncidentOperatingCorporationReasonEntityRepository reasonEntityRepository) {
        this.reasonEntityRepository = reasonEntityRepository;
    }

    @Override
    public IncidentCorporationReasonId findById(IncidentCorporationReasonId reasonId) {
        return this.reasonEntityRepository.findById(reasonId.getValue())
                .map(x -> x.getActive() ? x : null)
                .map(this::toModel)
                .orElse(null);
    }

    private IncidentCorporationReasonId toModel(IncidentCancellationEntity incidentCancellationEntity) {
        return IncidentCorporationReasonId.required(incidentCancellationEntity.getId());
    }

}
