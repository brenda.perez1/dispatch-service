package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AssignIncidentCorporationListRequest {

	private final String incidentId;
	private final List<Long> corporationId;
	private final Boolean secondaryInvoice; 
	private final String assignedBy;
	private final Long profileId;

}
