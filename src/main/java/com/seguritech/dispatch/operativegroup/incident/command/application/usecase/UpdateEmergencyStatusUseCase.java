package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import org.springframework.transaction.annotation.Transactional;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.UpdateIncidentStatusRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatus;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class UpdateEmergencyStatusUseCase {

	private final DomainEventPublisher publisher;
	private final IncidentOperatingCorporationRepository repository;
	private final IncidentOperatingCorporationFinder finder;
	private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
		
	public UpdateEmergencyStatusUseCase(DomainEventPublisher publisher,
			IncidentOperatingCorporationRepository repository, IncidentOperatingCorporationFinder finder,
			 ProfileCorporationConfigRepository profileCorporationConfigRepository) {
		super();
		this.publisher = publisher;
		this.repository = repository;
		this.finder = finder;
		this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
	}
	
	@Transactional
	public void execute(final String token, UpdateIncidentStatusRequest request) {

		IncidentId incidentId = IncidentId.required(request.getIncidentId());
		UserId userId = UserId.required(request.getUserId());
		ProfileId profileId = ProfileId.required(request.getProfileId());

		IncidentCorporationStatus status = new IncidentCorporationStatus(request.getStatus());

		request.getCorporationIds().forEach(id -> {
			CorporationOperativeId corporationId = CorporationOperativeId.required(id);
			this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);

			IncidentCorporationModel model = this.finder.findById(corporationId, incidentId);
			model.changeStatus(status, userId.getValue());
			this.repository.save(model);
			this.publisher.publish(model.pullEvents());

		});

	}
}


   
   