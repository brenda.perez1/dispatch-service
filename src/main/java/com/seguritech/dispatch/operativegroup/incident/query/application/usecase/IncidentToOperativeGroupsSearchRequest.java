package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.AbstractSearcherRequest;

import lombok.Getter;

@Getter
public class IncidentToOperativeGroupsSearchRequest extends AbstractSearcherRequest {

	private final String emergencyId;
    private final Long corporationId;
    
	public IncidentToOperativeGroupsSearchRequest(Integer size, Integer page, String emergencyId, Long corporationId) {
		super(size, page, null);
		 this.emergencyId = emergencyId;
	     this.corporationId = corporationId;
	}

}
