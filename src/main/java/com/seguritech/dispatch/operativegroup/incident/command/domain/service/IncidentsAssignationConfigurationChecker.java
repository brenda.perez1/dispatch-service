package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.ManualCorporationAssignmentNotEnabledException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationConfigRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentsAssignationConfigurationChecker {

    private final CorporationConfigRepository corporationConfigRepository;

    public IncidentsAssignationConfigurationChecker(CorporationConfigRepository corporationConfigRepository) {
        this.corporationConfigRepository = corporationConfigRepository;
    }

    public Boolean checkB() {
        return this.corporationConfigRepository.automaticAssignmentEnable();
    }

    public void check() {
        if (this.corporationConfigRepository.automaticAssignmentEnable())
            throw new ManualCorporationAssignmentNotEnabledException();
    }
}
