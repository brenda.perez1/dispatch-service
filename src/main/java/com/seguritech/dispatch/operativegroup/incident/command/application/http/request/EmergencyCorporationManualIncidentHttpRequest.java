package com.seguritech.dispatch.operativegroup.incident.command.application.http.request;

import lombok.Data;

import java.util.Date;

@Data
public class EmergencyCorporationManualIncidentHttpRequest {
    private Long sourceId;
    private String phoneNumber;
    private Long phoneNumberCountryId;
    private String description;
    private Date eventDate;
    private Long profileId;
}
