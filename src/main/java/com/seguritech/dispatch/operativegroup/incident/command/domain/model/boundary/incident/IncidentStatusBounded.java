package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.domain.model.incident.dto.IncidentStatus.IncidentStatusEnum;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class IncidentStatusBounded {

    private final IncidentStatusEnum value;

    private IncidentStatusBounded(IncidentStatusEnum value) {
        this.value = value;
    }

    public static IncidentStatusBounded closed() {
        return new IncidentStatusBounded(IncidentStatusEnum.CLOSED);
    }

    private static IncidentStatusBounded empty() {
        return new IncidentStatusBounded(null);
    }

    public boolean isEmpty() {
        return this.value == null;
    }

    public static IncidentStatusBounded of(String value) {
        try {
            return new IncidentStatusBounded(IncidentStatusEnum.valueOf(value));
        } catch (Exception e) {
            return IncidentStatusBounded.empty();
        }
    }


}
