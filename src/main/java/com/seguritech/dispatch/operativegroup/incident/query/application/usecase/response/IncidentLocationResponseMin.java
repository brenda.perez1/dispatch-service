package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncidentLocationResponseMin {
	
	private Long idEmergencyLocation;
    private Double latitude;
	private Double longitude;
	private String addressAlternative;
	private Date createdAt;
	private String municipalityAlternative;
	private String stateAlternative;
	private String bwnStreet1;
	private String bwnStreet2;

}
