package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.domain.model.incident.dto.IncidentStatus.IncidentStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.UnassignIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationNotAssignedException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationUnassingModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.unitforce.command.domain.exception.ExistUnitMissionException;
import com.seguritech.dispatch.unitforce.command.domain.service.UnitMissionFinder;
import com.seguritech.platform.domain.DomainEventPublisher;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UnassignIncidentToOperatingCorporationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentOperatingCorporationRepository repository;
    private final IncidentRepository incidentRepository;
    private final UnitMissionFinder unitMissionFinder;
    private final DomainEventPublisher publisher;

    @Value("${unassigned.incident.close.id}")
    private Long unassignedIncidentCloseId;

    public UnassignIncidentToOperatingCorporationUseCase(IncidentOperatingCorporationFinder finder,
                                                         IncidentOperatingCorporationRepository repository,
                                                         IncidentRepository incidentRepository,
                                                         UnitMissionFinder unitMissionFinder,
                                                         DomainEventPublisher publisher) {
        this.finder = finder;
        this.repository = repository;
        this.incidentRepository = incidentRepository;
        this.unitMissionFinder = unitMissionFinder;
        this.publisher = publisher; 
    }

    public String execute(UnassignIncidentCorporationRequest request) {

        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        //CorporationOperativeId corporationId = CorporationOperativeId.required(request.getCorporationId());
        CorporationId corporationId = CorporationId.required(request.getCorporationId());
        UserId userId = UserId.required(request.getUserId());

        // closedId = 0 (Cerrado Automatico)
        IncidentCorporationReasonCloseId closeId = IncidentCorporationReasonCloseId.required(unassignedIncidentCloseId);

        IncidentCorporationReasonCloseBounded reason = new IncidentCorporationReasonCloseBounded(
                closeId,
                IncidentCorporationObservation.empty(),
                UserId.required(request.getUserId()),
                new UserId(request.getUserId()));

        List<IncidentCorporationModel> listModel = this.finder.findById(corporationId, incidentId);
        if (listModel == null || listModel.isEmpty()) {
			throw new IncidentCorporationNotAssignedException(incidentId.getValue());
		}
        
        Long countMission = this.unitMissionFinder.countUnitMission(incidentId, corporationId);

        if (countMission != null && countMission > 0) {
			throw new ExistUnitMissionException();
		}
        IncidentCorporationUnassingModel model = IncidentCorporationUnassingModel.unassign(listModel, incidentId, corporationId, closeId, reason, userId);
        this.repository.saveAll(listModel);
        Long countCorporationAssign = this.finder.countCorporationAssign(incidentId);
        if (countCorporationAssign != null && countCorporationAssign == 0) {
			this.incidentRepository.updateStatus(incidentId, IncidentStatusEnum.OPEN);
			model.openIncident(incidentId.getValue());
		}
        this.publisher.publish(model.pullEvents());

        return request.getCorporationId() + " unassigned";

    }
    
}