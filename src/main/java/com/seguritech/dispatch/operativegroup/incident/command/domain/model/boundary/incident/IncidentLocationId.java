package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import java.util.Optional;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentRequiredException;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class IncidentLocationId {
	private final Long value;

    public IncidentLocationId(Long value) {
        this.value = value;
    }

    public static IncidentLocationId required(Long incidentId) {
        return Optional.ofNullable(incidentId).map(IncidentLocationId::new).orElseThrow(IncidentRequiredException::new);
    }

    public static IncidentLocationId from(Long incidentId) {
        return new IncidentLocationId(incidentId);
    }
}
