package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpGpIncidentTypeCustomResponse {

    private final OpGpIncidentTypeResponse type;
    private final OpGpIncidentSubTypeResponse subType;
    private final OpGpIncidentClassificationTypeResponse classificationType;
    private final OpGpIncidentSubClassificationTypeResponse subClassificationType;

}
