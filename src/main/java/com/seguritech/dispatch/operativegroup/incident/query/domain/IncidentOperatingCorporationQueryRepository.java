package com.seguritech.dispatch.operativegroup.incident.query.domain;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentToOperativeGroupsSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.IncidentToOperativeGroupsResponse;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;

public interface IncidentOperatingCorporationQueryRepository {
	
	PageDataProjectionDTO<IncidentToOperativeGroupsResponse> search (IncidentToOperativeGroupsSearchRequest request);

}
