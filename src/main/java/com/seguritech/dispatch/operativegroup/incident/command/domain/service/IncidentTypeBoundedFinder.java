package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentLocationNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentNotFoundException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;

@Service
public class IncidentTypeBoundedFinder {

	private final IncidentRepository repository;

	public IncidentTypeBoundedFinder(IncidentRepository repository) {
		this.repository = repository;
	}

	public IncidentBounded find(IncidentId id) {
		System.out.println("com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentTypeBoundedFinder=========================");
		return Optional.ofNullable(this.repository.find(id))
				.orElseThrow(() -> new IncidentNotFoundException(id.getValue()));
	}

//	public IncidentModel findIncidentModel(IncidentId id) {
//		return this.repository.findById(id.getValue()).map(IncidentEntityMapper::toIncidentModel)
//				.orElseThrow(() -> new IncidentNotFoundException("Incident [" + id.getIncidentId() + "] not found"));
//	}

	public IncidentLocationBounded findLocation(IncidentId id) {
		return Optional.ofNullable(this.repository.findLocation(id))
				.orElseThrow(() -> new IncidentLocationNotFoundException(id.getValue()));
	}
}
