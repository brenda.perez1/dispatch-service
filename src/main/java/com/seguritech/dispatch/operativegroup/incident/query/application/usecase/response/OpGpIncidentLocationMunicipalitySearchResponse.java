package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpGpIncidentLocationMunicipalitySearchResponse {

    private final Long municipalityId;
    private final String municipality;

}
