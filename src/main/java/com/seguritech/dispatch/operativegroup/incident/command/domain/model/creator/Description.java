package com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class Description {
    private static final String DEFAULT_VALUE = "";
    private final String value;

    private Description(String value) {
        this.value = value;
    }

    public static Description optional(String value) {
        return new Description(Optional.ofNullable(value).orElse(DEFAULT_VALUE));
    }
}
