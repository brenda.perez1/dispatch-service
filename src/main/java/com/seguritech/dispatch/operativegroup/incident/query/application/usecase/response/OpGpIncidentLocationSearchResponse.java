package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OpGpIncidentLocationSearchResponse {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private OpGpIncidentLocationStateSearchResponse state;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private OpGpIncidentLocationMunicipalitySearchResponse municipality;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private OpGpIncidentLocationLocalitySearchResponse settlement;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String houseNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String street;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String btnStreet1;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String btnStreet2;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String reference;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double latitude;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double longitude;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private OpGpIncidentLocationSectorSearchResponse sector;

}
