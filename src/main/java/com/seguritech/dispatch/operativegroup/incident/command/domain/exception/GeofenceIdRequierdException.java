package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class GeofenceIdRequierdException extends BusinessException {
    public GeofenceIdRequierdException() {
        super(MessageResolver.GEOFENCE_ID_REQUERID_EXCEPTION, 3003L);
    }
}
