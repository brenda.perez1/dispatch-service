package com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Getter
@NoArgsConstructor
public class OperatingCorporationIncidentClosedEvent implements Serializable {
    private String emergencyId;
    private Long corporationId;
    private String status;
    private String closedByUser;
    private Date closedAt;

}
