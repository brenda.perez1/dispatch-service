package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentTypeNotAssignableException extends BusinessException {

    public IncidentTypeNotAssignableException() {
        super(MessageResolver.INCIDENT_TYPE_NOT_ASSIGNABLE_EXCEPTION, 3027L);
    }
}