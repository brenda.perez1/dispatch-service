package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.domain.model.incidentcancellation.entity.IncidentCancellationEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationReasonRequiredException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCorporationReasonId {

    private final Long value;
    private final static Long DEFAULT_VALUE = 0L;

    public static IncidentCorporationReasonId required(Long value) {
        return Optional.ofNullable(value).map(IncidentCorporationReasonId::new)
                .orElseThrow(IncidentCorporationReasonRequiredException::new);
    }

    public static IncidentCorporationReasonId empty() {
        return new IncidentCorporationReasonId(DEFAULT_VALUE);
    }

    public boolean isEmpty() {
        return this.value == DEFAULT_VALUE;
    }

    public static IncidentCorporationReasonId defaultId() {
        return new IncidentCorporationReasonId(DEFAULT_VALUE);
    }
    
    public static IncidentCorporationReasonId optional(IncidentCancellationEntity value) {
    	return Optional.ofNullable(value).map(IncidentCancellationEntity::getId).map(IncidentCorporationReasonId::new).orElse(null);
    }

}
