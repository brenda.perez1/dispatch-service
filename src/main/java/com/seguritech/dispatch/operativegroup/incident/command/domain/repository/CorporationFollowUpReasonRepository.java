package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpReasonId;

public interface CorporationFollowUpReasonRepository {

	IncidentCorporationFollowUpReasonId findById(IncidentCorporationFollowUpReasonId reasonId);

}
