package com.seguritech.dispatch.operativegroup.incident.command.application.http;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = {"OPERATIVE GROUP EMERGENCIES"})
@RequestMapping("/dispatch/operative-groups/emergencies")
public class MultiOperativeGroupIncidentBaseController {
}

