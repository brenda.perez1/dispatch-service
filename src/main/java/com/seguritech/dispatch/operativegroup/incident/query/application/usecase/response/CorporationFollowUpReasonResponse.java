package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CorporationFollowUpReasonResponse {

	private final Long id;
	private final String name;
	private final String description;

}
