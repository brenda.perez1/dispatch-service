package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.domain.model.geofenceoperativegroup.entity.OperativeGroupGeofencesEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.OperatingCorporationsRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.OperatingCorporationsEntityRepository;

@Repository
public class OperatingCorporationsRepositoryImpl implements OperatingCorporationsRepository {

    private final OperatingCorporationsEntityRepository corporationsEntityRepository;

    public OperatingCorporationsRepositoryImpl(OperatingCorporationsEntityRepository corporationsEntityRepository) {
        this.corporationsEntityRepository = corporationsEntityRepository;
    }

    @Override
    public  List<OperativeGroupGeofencesEntity> find(Long corporationId) {
    	Boolean active = Boolean.TRUE;    	
    	List<OperativeGroupGeofencesEntity> operativeGroupGeofencesEntity = corporationsEntityRepository.findOperativeGroupGeofences(active, corporationId).stream().collect(Collectors.toList());
    	return operativeGroupGeofencesEntity;
    }
    
    @Override
    public  List<OperativeGroupGeofencesEntity> findDefault(Long corporationId) {
    	Boolean active = Boolean.TRUE;
    	List<OperativeGroupGeofencesEntity> findOperativeGroupGeofenceByDefault = corporationsEntityRepository.findOperativeGroupGeofenceByDefault(active, corporationId).stream().collect(Collectors.toList());
    	return findOperativeGroupGeofenceByDefault;
    }

	@Override
	public OperativeGroupGeofencesEntity findDefaultGeofence(Long groupOperativeId) {
		Boolean active = Boolean.TRUE;
		OperativeGroupGeofencesEntity geofenceDefault = this.corporationsEntityRepository.findDefaultOperativeGroupGeofences(active, groupOperativeId);
		return geofenceDefault;
	}

}
