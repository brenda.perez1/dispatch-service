package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.domain.model.corporation.entity.Corporation_;
import com.domain.model.corporationoperative.entity.CorporationOperative_;
import com.domain.model.incident.entity.IncidentEntity_;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk_;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity_;

public class IncidentOperativeCorporationDetails {
	
	 public static Specification<IncidentCorporationEntity> incidentIdEq(String incidentId) {
	        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(IncidentCorporationEntity_.ID)
	            .get(IncidentCorporationEntityPk_.INCIDENT)
	            .get(IncidentEntity_.ID), incidentId);
	 }
	 
	 public static Specification<IncidentCorporationEntity> corporationIdEq(Long corporationId) {
	        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(IncidentCorporationEntity_.ID)
	            .get(IncidentCorporationEntityPk_.CORPORATION_OPERATIVE)
	            .get(CorporationOperative_.CORPORATION)
	            .get(Corporation_.ID), corporationId);
	 }
	 
	//Order  Desc creationDate
	public static Sort SortByCreatedAtDesc() {
		return Sort.by(Sort.Direction.DESC, IncidentCorporationEntity_.CREATION_DATE);
	}

}
