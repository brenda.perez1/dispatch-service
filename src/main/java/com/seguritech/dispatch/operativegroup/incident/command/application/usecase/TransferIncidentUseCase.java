package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.TransferIncidentRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.OperatingCorporationsRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

@Service
public class TransferIncidentUseCase {

	private final OperatingCorporationsRepository operatingCorporationsRepository;	
    private final IncidentOperatingCorporationRepository repository;
    private final IncidentOperatingCorporationFinder finder;
    private final DomainEventPublisher publisher;

    public TransferIncidentUseCase(IncidentOperatingCorporationRepository repository, 
    							   ProfileCorporationConfigRepository profileCorporationConfigRepository, 
    							   OperatingCorporationsRepository operatingCorporationsRepository,
    							   IncidentRepository incidentRepository,
    							   IncidentOperatingCorporationFinder finder,
    							   DomainEventPublisher publisher) {
        this.repository = repository;
        this.operatingCorporationsRepository = operatingCorporationsRepository;
        this.finder = finder;
        this.publisher = publisher;
    }

    public void execute(TransferIncidentRequest request) {

    	IncidentId incidentId = IncidentId.required(request.getEmergencyId());
        CorporationOperativeId currentCorporationOperativeId = CorporationOperativeId.required(request.getCurrentCorporationOperativeId());
        CorporationOperativeId newCorporationOperativeId = CorporationOperativeId.required(request.getNewCorporationOperativeId());
        UserId transferedBy = UserId.from(request.getTransferedBy());
        
        IncidentCorporationModel model = this.finder.findById(
                new CorporationOperativeId(request.getCurrentCorporationOperativeId()),
                new IncidentId(request.getEmergencyId())
        );
        
        model.transfer(
        		incidentId,
        		currentCorporationOperativeId,
        		newCorporationOperativeId,
        		transferedBy
        );
        
        repository.updateCorporationOperativeByIncidentIdAndCorporationOperativeId(incidentId, currentCorporationOperativeId, newCorporationOperativeId);
        
        this.publisher.publish(model.pullEvents());
    }
    
}
