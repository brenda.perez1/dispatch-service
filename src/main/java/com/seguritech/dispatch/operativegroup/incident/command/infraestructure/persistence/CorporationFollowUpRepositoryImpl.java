package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.domain.model.corporation.followup.entity.CorporationFollowUpEntity;
import com.domain.model.corporation.followup.reason.entity.CorporationFollowUpReasonEntity;
import com.domain.model.corporation.followup.status.entity.CorporationFollowUpStatusEntity;
import com.domain.model.corporationoperative.entity.CorporationOperative;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.user.entity.AppUserEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationFollowUpId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationFollowUpModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.CorporationFollowUpObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.CorporationFollowUpResearchFolder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpReasonId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationFollowUpStatusId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationFollowUpRepository;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.CorporationFollowUpEntityRepository;

@Service
public class CorporationFollowUpRepositoryImpl implements CorporationFollowUpRepository {

	private final CorporationFollowUpEntityRepository repository;

	public CorporationFollowUpRepositoryImpl(CorporationFollowUpEntityRepository repository) {
		this.repository = repository;
	}

	@Override
	public void save(CorporationFollowUpModel model) {
		this.repository.save(toEntity(model));
	}

	public CorporationFollowUpEntity toEntity(CorporationFollowUpModel model) {
		return new CorporationFollowUpEntity(
			model.getId().getValue(),
			new IncidentEntity(model.getIncidentId().getValue()),
			new CorporationOperative(model.getCorporationOperativeId().getValue()),
			new CorporationFollowUpReasonEntity(model.getIncidentCorporationFollowUpReasonId().getValue()),
			new CorporationFollowUpStatusEntity(model.getIncidentCorporationFollowUpStatusId().getValue()),
			Optional.ofNullable(model.getCorporationFollowUpObservation()).map(CorporationFollowUpObservation::getValue).orElse(null),
			Optional.ofNullable(model.getCorporationFollowUpResearchFolder()).map(CorporationFollowUpResearchFolder::getValue).orElse(null),
			new AppUserEntity(model.getCreatedByUser().getValue()),
			model.getCreatedAt(),
			new AppUserEntity(model.getUpdatedByUser().getValue()),
			model.getUpdatedAt()
		);
	}

    public CorporationFollowUpModel toModel(CorporationFollowUpEntity entity) {
        return new CorporationFollowUpModel(
        	CorporationFollowUpId.required(entity.getId()),
        	Optional.ofNullable(entity.getIncident()).map(IncidentEntity::getId).map(IncidentId::required).orElse(null),
        	Optional.ofNullable(entity.getCorporationOperative()).map(CorporationOperative::getId).map(CorporationOperativeId::required).orElse(null),
        	Optional.ofNullable(entity.getFollowUpReason()).map(CorporationFollowUpReasonEntity::getId).map(IncidentCorporationFollowUpReasonId::required).orElse(null),
        	Optional.ofNullable(entity.getFollowUpStatus()).map(CorporationFollowUpStatusEntity::getId).map(IncidentCorporationFollowUpStatusId::required).orElse(null),
        	Optional.ofNullable(entity.getObservation()).map(CorporationFollowUpObservation::optional).orElse(null),
        	Optional.ofNullable(entity.getResearchFolder()).map(CorporationFollowUpResearchFolder::optional).orElse(null),
        	UserId.required(entity.getCreatedBy().getId()),
        	entity.getCreationDate(),
        	UserId.required(entity.getUpdateBy().getId()),
        	entity.getUpdateDate()
        );
    }

}
