package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClosingRecommendationIncidentCorporationRequest {

    private String incidentId;
    private Long corporationId;
    private String userId;

}
