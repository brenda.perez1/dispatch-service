package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationObservationInvalidException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCorporationObservation {

    private final String value;
    private final static Long MAX_LENGTH = 7000L;
    private static final String DEFAULT_VALUE = "";

    public static IncidentCorporationObservation optional(String value) {
        return Optional.ofNullable(value)
                .map(IncidentCorporationObservation::ensure)
                .map(IncidentCorporationObservation::new)
                .orElse(IncidentCorporationObservation.empty());
    }

    public static IncidentCorporationObservation empty() {
        return new IncidentCorporationObservation(DEFAULT_VALUE);
    }

    private static String ensure(String value) {
        if (value.length() > MAX_LENGTH) {
            throw new IncidentCorporationObservationInvalidException(MAX_LENGTH);
        }
        return value;
    }

    public boolean isEmpty() {
        return this.value == DEFAULT_VALUE;
    }

}
