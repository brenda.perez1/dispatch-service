package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ClassificationResponse {

	private final Long id;
	private final String name;
	private final String clave;
	private final String description;

}
