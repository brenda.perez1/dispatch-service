package com.seguritech.dispatch.operativegroup.incident.query.application.api;

import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupsResponse;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupsSearchUseCase;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOperativeGroupStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@Api(tags = {"EMERGENCIES OPERATIVE GROUP"})
@RequestMapping("/emergencies/{emergency-id}/operative-groups")
@RestController
public class IncidentOperativeGroupQueryController {


    private final IncidentOperativeGroupsSearchUseCase searchUseCase;
    private final IncidentOperatingCorporationFinder incidentOperatingCorporationFinder;

    public IncidentOperativeGroupQueryController (IncidentOperativeGroupsSearchUseCase searchUseCase,
    		IncidentOperatingCorporationFinder incidentOperatingCorporationFinder
    		) {
    			this.searchUseCase = searchUseCase;
    			this.incidentOperatingCorporationFinder = incidentOperatingCorporationFinder;
    		}


    @GetMapping
    @ApiOperation(value = "Find all operative groups form emergency")
    @ApiIgnore
    public List<IncidentOperativeGroupsResponse> findAll (@PathVariable("emergency-id") String incidentId,
                                                          @RequestParam(value = "status", required = false) IncidentOperativeGroupStatus status) {
        return this.searchUseCase.search(new IncidentOperativeGroupSearchRequest(incidentId, status));
    }
   

}
