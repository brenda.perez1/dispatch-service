package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AutomaticCorporationAssignmentToClassifiedIncidentRequest {
    private final String incidentId;
    private final Long incidentTypeId;
    private final String createBy;
}
