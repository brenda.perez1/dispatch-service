package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentFolioRequiredException;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
public class IncidentFolio {

    private final String value;

    public IncidentFolio(String value) {
        this.value = value;
    }

    public static IncidentFolio required(String incidentId) {
        return Optional.ofNullable(incidentId).map(IncidentFolio::new).orElseThrow(IncidentFolioRequiredException::new);
    }

    public static IncidentFolio from(String incidentId) {
        return new IncidentFolio(incidentId);
    }

}
