package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationAssociatedException  extends BusinessException {

    public IncidentCorporationAssociatedException (String  message) {
        super(MessageResolver.INCIDENT_CORPORATION_ASSOCIATED_EXCEPTION, 3005L);
    }
}
