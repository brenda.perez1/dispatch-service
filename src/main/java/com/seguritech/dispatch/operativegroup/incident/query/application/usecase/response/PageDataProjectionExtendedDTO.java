package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageDataProjectionExtendedDTO<T> {
	
	private List<T> items;
    private Long total;

}
