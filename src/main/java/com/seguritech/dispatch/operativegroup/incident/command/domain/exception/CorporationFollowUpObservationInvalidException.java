package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class CorporationFollowUpObservationInvalidException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public CorporationFollowUpObservationInvalidException(Object... maxLength) {
        super(MessageResolver.CORPORATION_FOLLOWUP_OBSERVATION_INVALID_EXCEPTION, maxLength, 3013L);
    }

}
