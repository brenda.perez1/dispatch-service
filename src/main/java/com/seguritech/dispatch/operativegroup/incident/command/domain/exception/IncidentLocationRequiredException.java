package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentLocationRequiredException extends BusinessException {

    public IncidentLocationRequiredException() {
        super(MessageResolver.INCIDENT_LOCATION_REQUERID_EXCEPTION, 3022L);
    }
}
