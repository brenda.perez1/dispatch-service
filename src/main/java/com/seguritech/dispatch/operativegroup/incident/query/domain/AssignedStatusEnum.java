package com.seguritech.dispatch.operativegroup.incident.query.domain;

public enum AssignedStatusEnum {
	
	assigned(true),
	unassigned(false);
	
	private Boolean value;
	
	private AssignedStatusEnum (Boolean value) {
		this.value = value;
	}
	
	public Boolean getValue() {
		return value;
	}
}
