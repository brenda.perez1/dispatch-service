package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.domain.model.corporation.entity.Corporation;
import com.domain.model.incident.entity.IncidentEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;

public interface IncidentOperatingCorporationEntityRepository extends PagingAndSortingRepository<IncidentCorporationEntity, IncidentCorporationEntityPk> {
	
	Optional<List<IncidentCorporationEntity>> findByIdIncidentAndStatus(IncidentEntity incidentId,String status);
   
	Optional<List<IncidentCorporationEntity>> findByIdIncidentAndStatusAndCorporation(IncidentEntity incidentId,String status,Corporation corporationId);
   
	Optional<List<IncidentCorporationEntity>> findByIdIncident(IncidentEntity incidentId);
    
	@Query(" SELECT u, u.id, u.id.incident, u.id.incident.priority, u.id.incident.nationalSubType,  u.id.incident.nationalSubType.nationalType, u.id.incident.createdAt,"
    		+ " u.id.corporationOperative, u.id.corporationOperative.corporation, u.geofence, u.location FROM IncidentCorporationEntity u WHERE u.id.corporationOperative.id"
    		+ " IN :corporation AND u.id.incident.id IN :incidentId ORDER BY DATE(u.id.incident.createdAt) ASC"
    )
    IncidentCorporationEntity getIncidentByIdAndCorporation(@Param("corporation") Collection<Long> corporation, @Param ("incidentId") String incidentId);

	@Query(" SELECT u, u.id, u.id.incident, u.id.incident.priority, u.id.incident.nationalSubType,  u.id.incident.nationalSubType.nationalType, u.id.incident.createdAt,"
    		+ " u.id.corporationOperative, u.id.corporationOperative.corporation, u.geofence, u.location FROM IncidentCorporationEntity u WHERE u.id.corporationOperative.id"
    		+ " IN :corporation AND u.id.incident.id IN :incidentId ORDER BY DATE(u.id.incident.createdAt) ASC"
    )
    List<IncidentCorporationEntity> getIncidentByIdAndCorporationOperative(@Param("corporation") Collection<Long> corporation, @Param ("incidentId") String incidentId);

	@Query(" SELECT u, u.id, u.id.incident, u.id.incident.priority, u.id.incident.nationalSubType,  u.id.incident.nationalSubType.nationalType, u.id.incident.createdAt,"
    		+ " u.id.corporationOperative, u.id.corporationOperative.corporation, u.geofence, u.location FROM IncidentCorporationEntity u WHERE u.id.corporationOperative.id"
    		+ " IN :corporations AND (:priority is null OR u.id.incident.priority.id = :priority) AND u.status=:status AND (:word is null OR (UPPER(u.geofence.name) LIKE %:word%))"
    )
    List<IncidentCorporationEntity> findIncidentCorporationByQueryAndStatusAndPriority(
    		@Param("word")String word,
    		@Param("corporations") List<Long> corporations,
    		@Param("status") String status,
    		@Param("priority")Long priority,Pageable pageable
    );
    
	 @Query(" SELECT u, u.id, u.id.incident, u.id.incident.priority, u.id.incident.nationalSubType, u.id.incident.nationalSubType.nationalType, u.id.incident.createdAt,"
	    		+ " u.id.corporationOperative, u.id.corporationOperative.corporation, u.geofence, u.location FROM IncidentCorporationEntity u WHERE u.id.corporationOperative.id"
	    		+ " IN :corporations AND u.creationDate <= :endDate AND (:priority is null OR u.id.incident.priority.id = :priority) AND u.status=:status AND (:word is null"
	    		+ " OR (UPPER(u.geofence.name) LIKE %:word%))"
	)	    
    List<IncidentCorporationEntity> findIncidentCorporationByQueryAndStatusAndDateEndAndPriority(
    		@Param("word")String word,
    		@Param("corporations") List<Long> corporations,
    		@Param("endDate") Date endDate,
    		@Param("status") String status,
    		@Param("priority")Long priority,
    		Pageable pageable
    );
    
	 @Query(" SELECT u, u.id,  u.id.incident, u.id.incident.priority, u.id.incident.nationalSubType,  u.id.incident.nationalSubType.nationalType, u.id.incident.createdAt,"
	    		+ " u.id.corporationOperative, u.id.corporationOperative.corporation, u.geofence, u.location FROM IncidentCorporationEntity u WHERE u.id.corporationOperative.id"
	    		+ " IN :corporations AND u.creationDate >= :startDate AND (:priority is null OR u.id.incident.priority.id = :priority) AND u.status=:status AND (:word is null"
	    		+ " OR (UPPER(u.geofence.name) LIKE %:word%))"
	)
    List<IncidentCorporationEntity> findIncidentCorporationByQueryAndStatusAndOrderByCreatedAtAscAndPriority(
    		@Param("word")String word,
    		@Param("corporations") List<Long> corporations,
    		@Param("startDate") Date startDate,
    		@Param("status") String status,
    		@Param("priority")Long priority,
    		Pageable pageable
    );
 
	 @Query(" SELECT u, u.id, u.id.incident, u.id.incident.priority, u.id.incident.nationalSubType,  u.id.incident.nationalSubType.nationalType, u.id.incident.createdAt,"
	    		+ " u.id.corporationOperative, u.id.corporationOperative.corporation, u.geofence, u.location FROM IncidentCorporationEntity u WHERE u.id.corporationOperative.id"
	    		+ " IN :corporations AND u.creationDate BETWEEN :startDate AND :endDate AND (:priority is null or u.id.incident.priority.id = :priority) AND u.status=:status"
	    		+ " AND (:word is null OR (UPPER(u.geofence.name) LIKE %:word%))"
	)
    List<IncidentCorporationEntity> findIncidentCorporationByQueryAndStatusAndDateInitAndDateEndAndPriority(
    		@Param("word")String word,
    		@Param("corporations") List<Long> corporations,
    		@Param("startDate") Date startDate,
    		@Param("endDate") Date endDate,
    		@Param("status") String status,
			@Param("priority")Long priority, Pageable pageable);
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u")
    Long onlyCount();
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.status=:status")
    Long countByStatus(@Param("status") String status);
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.creationDate >= :creationDate")
    Long countByDateInit(@Param("creationDate") Date creationDate);
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.creationDate <= :endDate ")
    Long countByDateEnd(@Param("endDate") Date endDate);
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.creationDate BETWEEN :creationDate AND :endDate and u.status=:status")
    Long countByDateInitAndDateEnd(@Param("creationDate") Date creationDate,@Param("endDate") Date endDate, @Param("status") String status);
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.creationDate BETWEEN :creationDate AND :endDate and u.status=:status AND (UPPER(u.geofence.name) LIKE %:word%)")
    Long countByQueryAndDateInitAndDateEnd(@Param("word")String word,@Param("creationDate") Date creationDate,@Param("endDate") Date endDate, @Param("status") String status); //ORDER BY (u.creationDate) ASC
    
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.creationDate >= :creationDate AND u.status=:status AND (UPPER(u.geofence.name) LIKE %:word%)")
    Long countByQueryAndDateInit(@Param("word")String word,@Param("creationDate") Date creationDate, @Param("status") String status);
	
    @Query("SELECT COUNT(u) FROM IncidentCorporationEntity u WHERE u.creationDate <= :endDate AND u.status=:status AND (UPPER(u.geofence.name) LIKE %:word%)")
    Long countByQueryAndDateEnd(@Param("word")String word,@Param("endDate") Date endDate, @Param("status") String status);
    
    @Transactional
    @Modifying
    @Query("   UPDATE IncidentCorporationEntity i "
    		+ "   SET i.id.corporationOperative.id = :newCorporationOperativeId "
    		+ " WHERE i.id.incident.id = :incidentId "
    		+ "   AND i.id.corporationOperative.id = :currentCorporationOperativeId ")
    void updateCorporationOperativeByIncidentIdAndCorporationOperativeId(@Param("incidentId") String incidentId, @Param("currentCorporationOperativeId") Long currentCorporationOperativeId,  @Param("newCorporationOperativeId") Long newCorporationOperativeId);
	
    @Query("SELECT i FROM IncidentCorporationEntity i WHERE i.corporation =:corporation AND i.id.incident =:incidentEntity AND UPPER(i.status) = 'OPEN' ")
    List<IncidentCorporationEntity> findByCorporationIdAndIncidentId(Corporation corporation, IncidentEntity incidentEntity);
    
    IncidentCorporationEntity findTop1ByIdIncidentIdAndCorporationId(String idIncident, Long idCorporation);
    
    Long countByIdIncidentIdAndStatus(String idIncident,String status);
    
    Long countByIdIncidentAndStatusIgnoreCase(IncidentEntity incidentEntity, String status);
    
    Optional<IncidentCorporationEntity> findByIdAndStatus(IncidentCorporationEntityPk id, String status);

}
