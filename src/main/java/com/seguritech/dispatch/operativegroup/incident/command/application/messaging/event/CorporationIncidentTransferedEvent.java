package com.seguritech.dispatch.operativegroup.incident.command.application.messaging.event;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class CorporationIncidentTransferedEvent {
    private String emergencyId;
    private Long corporationOperativeId;
    private String transferedByUser;
    private Date transferedAt;
}
