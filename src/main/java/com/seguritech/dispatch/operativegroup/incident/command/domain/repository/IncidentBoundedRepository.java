package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.IncidentCreatorResponse;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentCreatorRequest;

public interface IncidentBoundedRepository {
    IncidentCreatorResponse create(IncidentCreatorRequest request);
}
