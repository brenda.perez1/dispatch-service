package com.seguritech.dispatch.operativegroup.incident.query.application.usecase;

import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response.OpGpIncidentSearchResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.OpGpIncidentQueryRepository;
import com.seguritech.dispatch.rest.PageDataProjectionDTO;
import com.seguritech.platform.Service;

@Service
public class OperativeGpIncidentSearcherUseCase {

    private final OpGpIncidentQueryRepository repository;

    public OperativeGpIncidentSearcherUseCase (OpGpIncidentQueryRepository repository) {
        this.repository = repository;
    }

    public PageDataProjectionDTO<OpGpIncidentSearchResponse> execute (OperativeGpIncidentSearchRequest request) {
        return this.repository.search(request);
    }

}
