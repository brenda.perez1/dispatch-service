package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OpGpIncidentLocationLocalitySearchResponse {

    private final Long settlementId;
    private final String settlement;
    private final Long zipCode;

}
