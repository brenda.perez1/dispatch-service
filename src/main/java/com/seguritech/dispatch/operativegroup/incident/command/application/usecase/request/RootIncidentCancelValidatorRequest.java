package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RootIncidentCancelValidatorRequest {
    private final String incidentId;
    private final String userId;
    private final Long reasonCancelId;
    private final String observationCancel;
}
