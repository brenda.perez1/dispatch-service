package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationsRequiredException extends BusinessException {

    public IncidentCorporationsRequiredException() {
        super(MessageResolver.INCIDENT_CORPORATIONS_REQUERID_EXCEPTION, 3016L);
    }
}
