package com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import com.domain.model.corporationoperative.entity.CorporationOperative;
//import com.domain.model.corporation.entity.CorporationOperative;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntity;
import com.domain.model.incidentcorporation.entity.IncidentCorporationEntityPk;
import com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository.IncidentCorporationOperativeEntityRepository;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupSearchRequest;
import com.seguritech.dispatch.operativegroup.incident.query.application.usecase.IncidentOperativeGroupsResponse;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOpGpRepository;
import com.seguritech.dispatch.operativegroup.incident.query.domain.IncidentOperativeGroupStatus;
import com.seguritech.dispatch.operativegroup.incident.query.infraestructure.persistence.jpa.specification.IncidentOpGpSpecification;
import com.seguritech.platform.Service;

@Service
public class IncidentOpGpRepositoryImpl implements IncidentOpGpRepository {

    private final IncidentCorporationOperativeEntityRepository repository;

    public IncidentOpGpRepositoryImpl (IncidentCorporationOperativeEntityRepository repository) {
        this.repository =
                repository;
    }


    @Override
    public List<IncidentOperativeGroupsResponse> search (IncidentOperativeGroupSearchRequest request) {
        return this.repository
                .findAll(this.toSpecification(request))
                .stream()
                .map(IncidentCorporationEntity::getId)
                .map(IncidentCorporationEntityPk::getCorporationOperative)
                .map(this::toResponse)
                .collect(Collectors.toList());
    }

    private Specification<IncidentCorporationEntity> toSpecification (IncidentOperativeGroupSearchRequest request) {

        List<Specification<IncidentCorporationEntity>> specifications = new ArrayList<>();
        Optional
                .ofNullable(request.getIncidentId())
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.incidentIdEq((x))));

        Optional
                .ofNullable(request.getStatus())
                .map(IncidentOperativeGroupStatus::getValue)
                .ifPresent(x -> specifications.add(IncidentOpGpSpecification.statusEq(x)));

        Specification<IncidentCorporationEntity> temp = (root, query, criteriaBuilder) -> null;
        for (Specification<IncidentCorporationEntity> specification : specifications) {
            temp = temp.and(specification);
        }
        return temp;
    }

    private IncidentOperativeGroupsResponse toResponse (CorporationOperative corporation) {
        return new IncidentOperativeGroupsResponse(corporation.getId(), corporation.getName());
    }

}
