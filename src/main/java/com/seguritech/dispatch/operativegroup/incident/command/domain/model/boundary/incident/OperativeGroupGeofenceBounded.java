package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class OperativeGroupGeofenceBounded {
    private final Long operativeGruopId;
    private final Long geofenceId;
}
