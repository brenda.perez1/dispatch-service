package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class IncidentCorporationUncoupleAllEvent implements DomainEvent {

    private final String incidentId;
    private final Long corporationId;
    private final String userId;

}
