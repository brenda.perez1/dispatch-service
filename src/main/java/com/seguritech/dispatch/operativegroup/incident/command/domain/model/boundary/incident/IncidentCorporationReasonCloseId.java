package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import java.util.Optional;

import com.domain.model.incidentclose.entity.IncidentCloseEntity;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationReasonCloseRequiredException;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCorporationReasonCloseId {
	
	private final Long value;
    private final static Long DEFAULT_VALUE = 0L;
    
    public static IncidentCorporationReasonCloseId required(Long value) {
        return Optional.ofNullable(value).map(IncidentCorporationReasonCloseId::new)
                .orElseThrow(IncidentCorporationReasonCloseRequiredException::new);
    }

    public static IncidentCorporationReasonCloseId empty() {
        return new IncidentCorporationReasonCloseId(DEFAULT_VALUE);
    }

    public boolean isEmpty() {
        return this.value == DEFAULT_VALUE;
    }

    public static IncidentCorporationReasonCloseId defaultId() {
        return new IncidentCorporationReasonCloseId(DEFAULT_VALUE);
    }
    
    public static IncidentCorporationReasonCloseId optional(IncidentCloseEntity value) {
    	return Optional.ofNullable(value).map(IncidentCloseEntity::getId).map(IncidentCorporationReasonCloseId::new).orElse(null);
    }

}
