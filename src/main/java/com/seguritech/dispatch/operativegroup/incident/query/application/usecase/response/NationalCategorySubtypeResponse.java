package com.seguritech.dispatch.operativegroup.incident.query.application.usecase.response;

import lombok.Data;

@Data
public class NationalCategorySubtypeResponse {
	
	private final Long id;
    private final Long idNationalCategory;
    private final Long idNationalSubtype;
    private final String name;
    private final String clave;

}
