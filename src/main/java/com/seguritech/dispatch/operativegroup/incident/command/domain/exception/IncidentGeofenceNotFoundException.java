package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentGeofenceNotFoundException extends
    BusinessException {

	private static final long serialVersionUID = 1L;

	public IncidentGeofenceNotFoundException() {
		super(MessageResolver.INCIDENT_GEOFENCE_NOT_FOUND_EXCEPTION, 3038L);
	}

}
