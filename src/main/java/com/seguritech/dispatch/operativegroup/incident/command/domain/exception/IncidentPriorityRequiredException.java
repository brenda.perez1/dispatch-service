package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentPriorityRequiredException extends
    BusinessException {

  public IncidentPriorityRequiredException() {
    super(MessageResolver.INCIDENT_PRIORITY_REQUERID_EXCEPTION, 3025L);
  }
}
