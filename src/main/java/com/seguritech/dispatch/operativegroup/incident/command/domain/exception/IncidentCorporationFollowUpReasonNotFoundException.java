package com.seguritech.dispatch.operativegroup.incident.command.domain.exception;

import com.seguritech.dispatch.exception.BusinessException;
import com.seguritech.dispatch.share.MessageResolver;

public class IncidentCorporationFollowUpReasonNotFoundException extends BusinessException {

    public IncidentCorporationFollowUpReasonNotFoundException() {
        super(MessageResolver.INCIDENT_CORPORATION_FOLLOWUP_REASON_NOT_FOUND_EXCEPTION, 3015L);
    }

}
