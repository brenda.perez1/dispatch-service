package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class IncidentOperatingSupportGroupEvent implements DomainEvent {

	private final String incidentId;
	private final Long operatingCorporationId;
	private final String createdByUser;
	private final Date createdAt;
	private final String folio;
	private final String sourceName;
    private final String subTypeName;

}
