package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.CorporationFollowUpObservationInvalidException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class CorporationFollowUpObservation {

    private final String value;
    private final static Long MAX_LENGTH = 100L;
    private static final String DEFAULT_VALUE = "";

    public static CorporationFollowUpObservation optional(String value) {
        return Optional.ofNullable(value)
                .map(CorporationFollowUpObservation::ensure)
                .map(CorporationFollowUpObservation::new)
                .orElse(CorporationFollowUpObservation.empty());
    }

    public static CorporationFollowUpObservation empty() {
        return new CorporationFollowUpObservation(DEFAULT_VALUE);
    }

    private static String ensure(String value) {
        if (value.length() > MAX_LENGTH) {
            throw new CorporationFollowUpObservationInvalidException(MAX_LENGTH);
        }
        return value;
    }

    public boolean isEmpty() {
        return this.value == DEFAULT_VALUE;
    }

}
