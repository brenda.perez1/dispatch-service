package com.seguritech.dispatch.operativegroup.incident.command.infraestructure.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.domain.model.unit.entity.UnitMissionEntity;
import com.domain.model.unit.entity.UnitMissionEntityPk;

public interface IncidentOperatingCorporationUnitMissionEntityRepository extends JpaRepository<UnitMissionEntity, UnitMissionEntityPk>, JpaSpecificationExecutor<UnitMissionEntity> {

}
