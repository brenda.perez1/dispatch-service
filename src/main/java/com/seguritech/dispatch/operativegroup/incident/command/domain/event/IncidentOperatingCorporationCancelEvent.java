package com.seguritech.dispatch.operativegroup.incident.command.domain.event;

import com.seguritech.platform.domain.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class IncidentOperatingCorporationCancelEvent implements DomainEvent {

    private final String emergencyId;
    private final Long corporationId;
    private final Long cancelReasonId;
    private final String observation;
    private final String cancelByUser;
    private final Date cancelAt;
    private Long corporationOperativeId;
}
