package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationFollowUpReasonRequiredException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Optional;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCorporationFollowUpReasonId {

	private final Long value;

	public static IncidentCorporationFollowUpReasonId required(Long value) {
		return Optional.ofNullable(value).map(IncidentCorporationFollowUpReasonId::new)
				.orElseThrow(IncidentCorporationFollowUpReasonRequiredException::new);
	}

}
