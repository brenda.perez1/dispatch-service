package com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@AllArgsConstructor
public class IncidentCoordinatesBounded {

    private final Double latitude;
    private final Double longitude;
    private final Long locationId;
}
