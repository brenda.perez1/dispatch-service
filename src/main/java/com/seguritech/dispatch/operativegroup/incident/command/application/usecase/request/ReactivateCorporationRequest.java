package com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class ReactivateCorporationRequest {

    private final String incidentId;
    private final Long corporationId;
    private final String observation;
    private final Long reactivationReasonId;
    private final String reactivatedBy;
    private final Date reactivationDate;
    private Long profileId;

}
