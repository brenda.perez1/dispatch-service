package com.seguritech.dispatch.operativegroup.incident.command.domain.service;

import org.springframework.stereotype.Service;

import com.domain.model.incident.dto.IncidentStatus.IncidentStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentLocationRequiredException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentNotOpenException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentPriorityRequiredException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentLocationBounded;
@Service
public class IncidentTypeChecker {

	private final IncidentTypeBoundedFinder finder;
	private final IncidentBoundedTypeEnsurer typeChecker;

	public IncidentTypeChecker(IncidentTypeBoundedFinder finder, IncidentBoundedTypeEnsurer typeChecker) {
		this.finder = finder;
		this.typeChecker = typeChecker;		
	}

	public IncidentBounded check(IncidentId id) {
		IncidentBounded incidentBounded = this.finder.find(id);
		this.typeChecker.check(incidentBounded.getType());

		if (incidentBounded.getStatus().getValue().equals(IncidentStatusEnum.CLOSED)) {
			throw new IncidentNotOpenException();
		}

		if (incidentBounded.getPriority().isEmpty()) {
			throw new IncidentPriorityRequiredException();
		}

		return incidentBounded;
	}

	public IncidentLocationBounded getLocation(IncidentId id) {
		IncidentLocationBounded incidentLocationBounded = this.finder.findLocation(id);
		
		if(incidentLocationBounded.getCoordinates().getLatitude() == null ||
				incidentLocationBounded.getCoordinates().getLongitude() == null  ) {
			throw new IncidentLocationRequiredException();
		}
		
		return incidentLocationBounded;
	}

} 
