package com.seguritech.dispatch.operativegroup.incident.command.domain.repository;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentTypeIdBounded;

public interface IncidentBoundedTypeRepository {

    IncidentTypeIdBounded findUnidentified();

    Boolean isAssignable(IncidentTypeIdBounded type);
}
