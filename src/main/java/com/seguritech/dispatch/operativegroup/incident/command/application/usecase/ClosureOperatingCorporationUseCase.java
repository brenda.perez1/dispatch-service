package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import java.util.List;

import javax.transaction.Transactional;

import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.application.http.request.EmergencyCloseRequest;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosureIncidentCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCloseException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatusEnum;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseBounded;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonCloseId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.Service;
import com.seguritech.platform.domain.DomainEventPublisher;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ClosureOperatingCorporationUseCase {

    private final IncidentOperatingCorporationFinder finder;
    private final IncidentOperatingCorporationRepository repository;
    private final ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    private final DomainEventPublisher publisher;
    private final EmergenciesClient client;

    public ClosureOperatingCorporationUseCase(IncidentOperatingCorporationRepository repository,
                                              ProfileCorporationConfigRepository profileCorporationConfigRepository, DomainEventPublisher publisher,
                                              EmergenciesClient client) {
        this.finder = new IncidentOperatingCorporationFinder(repository,null);
        this.repository = repository;
        this.profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        this.publisher = publisher;
        this.client = client;
    }

    @Transactional
    public void execute(String token, ClosureIncidentCorporationRequest request) {
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getCorporationId());
        UserId userId = UserId.required(request.getUserId());
        ProfileId profileId = ProfileId.required(request.getProfileId());
        
        IncidentCorporationReasonCloseId closeId = IncidentCorporationReasonCloseId.required(request.getCloseReasonId());
        
        IncidentCorporationReasonCloseBounded reason = new IncidentCorporationReasonCloseBounded(
        		closeId, 
        		IncidentCorporationObservation.optional(request.getObservation()), 
        		UserId.required(request.getUserId()), 
        		new UserId(request.getUserId()));

        this.profileCorporationConfigEnsurer.ensure(profileId, corporationId);

        IncidentCorporationModel model = this.finder.findById(corporationId, incidentId);
        model.close(closeId, reason, userId);
        this.repository.save(model);
        
        List<IncidentCorporationModel> listIncidentsOpen = this.repository.findByIncidentAndStatus(incidentId, IncidentCorporationStatusEnum.OPEN.toString());
        try {
        	if (listIncidentsOpen.size() == 0) {
    			//se manda a cerrar el incidente principal
        		log.info("Cerrando incidente principal: "+incidentId.getValue());
            	client.closeAnEmergency(token, incidentId.getValue(), new EmergencyCloseRequest("Cierre automatico",0L));
    		}
		} catch (Exception e) {
			throw new IncidentCloseException();
		}
        
        this.publisher.publish(model.pullEvents());
    }

}
