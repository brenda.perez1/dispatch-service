package com.seguritech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import com.seguritech.license.check.EnableLicense;
import com.seguritech.platform.Service;


@SpringBootApplication
@PropertySources({
//    @PropertySource("classpath:/security/security-${spring.profiles.active:default}.properties"),
//    @PropertySource("classpath:/database/database-${spring.profiles.active:default}.properties"),
    @PropertySource("classpath:application-${spring.profiles.active:default}.properties"),
    @PropertySource("classpath:/git.properties")
})
@EnableEurekaClient
@ComponentScan(includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = Service.class))
@EntityScan({"com.domain.model", "com.seguritech.dispatch"}) 
//@ComponentScan({"com.domain.model","com.seguritech.*"})
@EnableFeignClients
@EnableLicense
public class ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);
    }

}
