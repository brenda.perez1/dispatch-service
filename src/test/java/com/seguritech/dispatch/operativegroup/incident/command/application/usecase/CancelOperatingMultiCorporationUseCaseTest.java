package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.google.common.collect.Lists;
import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.CancelIncidentMultiCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatus;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.unit.UnitForceList;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationReasonExistenceEnsurer;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.domain.DomainEventPublisher;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.when;

class CancelOperatingMultiCorporationUseCaseTest {

    @Mock
    private IncidentOperatingCorporationFinder finder;
    @Mock
    private IncidentOperatingCorporationReasonExistenceEnsurer ensurer;
    @Mock
    private ProfileCorporationConfigRepository profileCorporationConfigRepository;
    @Mock
    private IncidentOperatingCorporationRepository repository;
    @Mock
    private DomainEventPublisher publisher;
    @Mock
    private EmergenciesClient client;
    @Mock
    private CorporationOperativeRepository corporationOperativeRepository;
    @Mock
    private ProfileCorporationConfigEnsurer profileCorporationConfigEnsurer;
    @Mock
    private IncidentCorporationStatus status;
    @Mock
    private UnitForceList unitForceList;
    private CancelIncidentMultiCorporationRequest request;
    private IncidentCorporationModel model;
    private CancelOperatingMultiCorporationUseCase useCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        request = buildFakeCancelIncidentMultiCorporationRequest();
        model = buildFakeIncidentCorporationModel();
        profileCorporationConfigEnsurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        useCase = new CancelOperatingMultiCorporationUseCase(
            finder, repository, ensurer, profileCorporationConfigRepository, publisher, client, corporationOperativeRepository);
    }

    @AfterEach
    void tearDown() {
        finder = null;
        ensurer = null;
        unitForceList = null;
        status = null;
        profileCorporationConfigRepository = null;
        repository = null;
        publisher = null;
        client = null;
        request = null;
        model = null;
        useCase = null;
    }

    @Test
    @DisplayName("execute: When cancel a valid IncidentOperatingCorporation model, should return the same model")
    void execute_WhenCancelAValidIncidentOperatingCorporationModel_ShouldReturnTheSameModel() {

        /*****| Arrange |*****/
        Collection<IncidentCorporationModel> expectedModels, models;
        expectedModels = models =  Lists.newArrayList(model);
        when(profileCorporationConfigRepository.exist(ProfileId.required(1L), CorporationOperativeId.required(1L))).thenReturn(true);
        when(finder.findById(CorporationOperativeId.required(1L), IncidentId.required("incident-id"))).thenReturn(model);
        when(unitForceList.isEmpty()).thenReturn(true);
        when(repository.saveAll(models)).thenReturn(models);

        /*****| Act |*****/
        useCase.execute(null, request);
        Collection<IncidentCorporationModel> modelsSaved = repository.saveAll(models);

        /*****| Assert |*****/
        assertSame(expectedModels, modelsSaved);

    }

    /*******************************************************************************************************************
     *                                                    Utilities                                                    *
     ******************************************************************************************************************/

    private IncidentCorporationModel buildFakeIncidentCorporationModel() {

        return new IncidentCorporationModel(new IncidentId("incident-id"),
                CorporationOperativeId.required(1L), null, unitForceList, IncidentCorporationStatus.initial(), null,
                null, null, null, null, null, null,
                null, null, null, null, null, null, null, null);
    }
    private CancelIncidentMultiCorporationRequest buildFakeCancelIncidentMultiCorporationRequest() {

        return new CancelIncidentMultiCorporationRequest(
        "incident-id", Lists.newArrayList(1L), "cancel-user", 1L, 1L, "observation");
    }

}