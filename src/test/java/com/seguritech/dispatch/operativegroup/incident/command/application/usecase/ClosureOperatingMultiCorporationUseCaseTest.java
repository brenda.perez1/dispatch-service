package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;

import com.google.common.collect.Lists;
import com.seguritech.dispatch.client.EmergenciesClient;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.ClosureIncidentMultiCorporationRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.exception.IncidentCorporationUnitNotCancelException;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.unit.UnitForceList;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.CorporationOperativeRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentOperatingCorporationFinder;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.ProfileCorporationConfigEnsurer;
import com.seguritech.platform.domain.DomainEventPublisher;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ClosureOperatingMultiCorporationUseCaseTest {

    private ClosureOperatingMultiCorporationUseCase useCase;
    private ClosureIncidentMultiCorporationRequest request;

    private ProfileCorporationConfigEnsurer ensurer;
    private IncidentCorporationModel model;
    @Mock
    private ProfileCorporationConfigRepository profileCorporationConfigRepository;
    @Mock
    private IncidentOperatingCorporationRepository repository;
    @Mock
    private IncidentOperatingCorporationFinder finder;
    @Mock
    private UnitForceList unitForceList;
    @Mock
    private DomainEventPublisher publisher;
    @Mock
    private EmergenciesClient client;
    @Mock
    private CorporationOperativeRepository corporationOperativeRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        model = buildFakeIncidentCorporationModel();
        request = buildClosureIncidentMultiCorporationRequest();
        ensurer = new ProfileCorporationConfigEnsurer(profileCorporationConfigRepository);
        useCase = new ClosureOperatingMultiCorporationUseCase(repository, profileCorporationConfigRepository, publisher, client, corporationOperativeRepository);
    }

    @AfterEach
    void tearDown() {
        client = null;
        finder = null;
        ensurer = null;
        publisher = null;
        model = null;
        request = null;
        repository = null;
        profileCorporationConfigRepository = null;
        useCase = null;
    }

    @Test
    @DisplayName("execute: When close a valid IncidentOperatingCorporation model, should return the same model")
    void execute_WhenCloseAValidIncidentOperatingCorporationModel_ShouldReturnTheSameModel() {

        /*****| Arrange |*****/
        ProfileId profileId = ProfileId.required(request.getProfileId());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getOperativeGroupIds().stream().findFirst().get());
        when(model.getUnitForceList().isEmpty()).thenReturn(true);
        when(profileCorporationConfigRepository.exist(profileId, corporationId)).thenReturn(true);
        when(repository.findById(corporationId, incidentId)).thenReturn(model);
        when(repository.save(model)).thenReturn(model);
        when(finder.findById(corporationId, IncidentId.required(request.getIncidentId()))).thenReturn(model);
        IncidentCorporationModel incidentCorporationExpected = model;

        /*****| Act |*****/
        useCase.execute(null, request);
        IncidentCorporationModel incidentCorporationSaved = repository.save(model);

        /*****| Assert |*****/
        assertSame(incidentCorporationExpected, incidentCorporationSaved);
    }

    @Test
    @DisplayName("execute: When close a IncidentOperatingCorporation with unit force list empty , should throw a IncidentCorporationUnitNotCancelException exception")
    void execute_WhenCloseAIncidentOperatingCorporationWithUnitForceListEmpty_ShouldThrowAIncidentCorporationUnitNotCancelExceptionException() {

        /*****| Arrange |*****/
        ProfileId profileId = ProfileId.required(request.getProfileId());
        IncidentId incidentId = IncidentId.required(request.getIncidentId());
        CorporationOperativeId corporationId = CorporationOperativeId.required(request.getOperativeGroupIds().stream().findFirst().get());
        when(profileCorporationConfigRepository.exist(profileId, corporationId)).thenReturn(true);
        when(repository.findById(corporationId, incidentId)).thenReturn(model);
        when(finder.findById(corporationId, IncidentId.required(request.getIncidentId()))).thenReturn(model);

        /*****| Assert |*****/
        assertThrows(IncidentCorporationUnitNotCancelException.class,

        /*****| Act |*****/
        () -> useCase.execute(null, request));

    }

    /*******************************************************************************************************************
     *                                                    Utilities                                                    *
     ******************************************************************************************************************/

    private IncidentCorporationModel buildFakeIncidentCorporationModel() {

        return new IncidentCorporationModel(new IncidentId("incident-id"),
                CorporationOperativeId.required(1L), null, unitForceList, null, null,
        null, null, null, null, null, null,
        null, null, null, null, null, null, null, null);
    }

    private ClosureIncidentMultiCorporationRequest buildClosureIncidentMultiCorporationRequest() {

        return new ClosureIncidentMultiCorporationRequest(
        "incident-id", Lists.newArrayList(1L), "user-id", 1L, 1L, "observation");
    }

}