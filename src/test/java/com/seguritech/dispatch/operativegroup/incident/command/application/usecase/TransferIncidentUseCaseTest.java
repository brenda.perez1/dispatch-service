//package com.seguritech.dispatch.operativegroup.incident.command.application.usecase;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import java.util.Date;
//
//import org.junit.jupiter.api.Assertions;
//
//import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.request.TransferIncidentRequest;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.CorporationOperativeId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationModel;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.IncidentCorporationStatus;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.ProfileId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationObservation;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentCorporationReasonId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.unit.UnitForceList;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
//import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.ProfileCorporationConfigRepository;
//import com.seguritech.platform.DomainEventPublisherStub;
//
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
//
//public class TransferIncidentUseCaseTest {
//
//    private ProfileCorporationConfigRepository profileCorporationConfigRepository = mock(ProfileCorporationConfigRepository.class);
//    private IncidentOperatingCorporationRepository repository = mock(IncidentOperatingCorporationRepository.class);
//    private IncidentCorporationModel model;
//
//    @Given("Existe un grupo operativo id {long} en el sector seleccionado a transferir por el Perfil Id {long}")
//    public void existe_un_grupo_operativo_id_en_el_sector_seleccionado_a_transferir_por_el_Perfil_Id(Long groupOperativeId, Long profileId) {
//        when(profileCorporationConfigRepository.exist(ProfileId.required(profileId), CorporationOperativeId.required(groupOperativeId))).thenReturn(true);
//    }
//
//    @Given("Existe un incidente con Id {string} asignado a la corporacion Id {long}")
//    public void existe_un_incidente_con_Id_asignado_a_la_corporacion_Id(String incident, Long corporationId) {
//        CorporationOperativeId sourceCorporationId = CorporationOperativeId.required(corporationId);
//        IncidentId incidentId = IncidentId.required(incident);
//
//        when(this.repository.findById(sourceCorporationId, incidentId))
//                .thenReturn(new IncidentCorporationModel(
//                        incidentId,
//                        sourceCorporationId,
//                        UnitForceList.optional(null),
//                        IncidentCorporationStatus.initial(),
//                        IncidentCorporationReasonId.defaultId(),
//                        IncidentCorporationObservation.empty(),
//                        UserId.empty(),
//                        new Date(),
//                        UserId.empty(),
//                        new Date(),
//                        UserId.empty(),
//                        new Date(),
//                        UserId.empty(),
//                        new Date()
//                ));
//    }
//
//
//    @When("El despachador transfiere el incidente con Id {string} de la corporacion Id {long} a la corporación id {long} del sector seleccionado")
//    public void el_despachador_transfiere_el_incidente_con_Id_de_la_corporacion_Id_a_la_corporación_id_del_sector_seleccionado(String incidentId, Long sourceCorporationId, Long corporationId) {
//        doAnswer(invocation -> {
//            this.model = invocation.getArgument(0, IncidentCorporationModel.class);
//            return null;
//        }).when(repository).save(any(IncidentCorporationModel.class));
//
//        TransferIncidentUseCase useCase = new TransferIncidentUseCase(
//                this.repository,
//                this.profileCorporationConfigRepository,
//                new DomainEventPublisherStub()
//        );
//
//        useCase.execute(new TransferIncidentRequest(
//                incidentId,
//                corporationId,
//                sourceCorporationId,
//                "MOCK_USER_ID",
//                1L// profile Id
//        ));
//    }
//
//
//    @Then("el incidente con Id {string} de la corporacion Id {long} deberá estar cerrado, y los campos de control de la transferencia del incidente correctamente registrados")
//    public void el_incidente_con_Id_de_la_corporacion_Id_deberá_estar_cerrado_y_los_campos_de_control_de_la_transferencia_del_incidente_correctamente_registrados(String incidentId, Long corporationId) {
//        Assertions.assertNotNull(this.model);
//    }
//
//}