package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.CorporationNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatCorporationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CorporationEnsurerTest {


    @Test
    public void shouldFindCorporation() {
        CatCorporationRepository corporationRepository = mock(CatCorporationRepository.class);
        when(corporationRepository.exist(CorporationOperativeId.required(1L))).thenReturn(true);

        CorporationEnsurer ensurer = new CorporationEnsurer(corporationRepository);
        Assertions.assertDoesNotThrow(() -> ensurer.ensurer(CorporationOperativeId.required(1L)));
    }

    @Test
    public void shouldNotFindCorporation() {
        CatCorporationRepository corporationRepository = mock(CatCorporationRepository.class);
        when(corporationRepository.exist(CorporationOperativeId.required(1L))).thenReturn(false);

        CorporationEnsurer ensurer = new CorporationEnsurer(corporationRepository);
        Assertions.assertThrows(CorporationNotFoundException.class, () -> ensurer.ensurer(CorporationOperativeId.required(1L)));
    }
}