package com.seguritech.dispatch.unitforce.command.domain.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;

public class UnitMissionFinderTest {

    @Test
    public void mustFindStartedUnitMission() {
        List<StageId> nextStage = new ArrayList<StageId>() {{
            add(StageId.required(
                    1L, "", 1L, 1L
            ));
            add(StageId.required(
                    2L, "", 2L, 2L
            ));
            add(StageId.required(
                    3L, "", 3L, 3L
            ));
            add(StageId.required(
                    4L, "", 4L, 4L
            ));
        }};

        UnitMissionRepository repository = mock(UnitMissionRepository.class);
        when(repository.find(UnitForceConfigId.required("MOCK_INCIDENT_ID"), IncidentId.required("MOCK_INCIDENT_ID"))
        ).thenReturn(

                new UnitMissionModel(
                        UnitMissionId.required("MOCK_MISSION_ID"),
                        SectorId.required(1L),
                        CorporationId.required(1L),
                        CorporationOperativeId.required(1L),
                        nextStage,
                        StageId.required(
                        		1L, "", 1L, 1L
                        ),
                        UnitForceConfigId.required("MOCK_UNIT_FORCE_ID"),
                        IncidentId.required("MOCK_INCIDENT_ID"),
                        UserId.required("MOCK_USER_ID"),
                        UserId.required("MOCK_USER_ID"),
                        new Date(),
                        new Date(),
                        new Date(),
                        new Date(),
                        ReasonCancelId.required(0L),
                        Observations.optional(null),
                        Persons.optional(null),
                        true,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                )
        );
        UnitMissionFinder unitMissionFinder = new UnitMissionFinder(repository);
        UnitMissionModel model = unitMissionFinder.find(UnitForceConfigId.required("MOCK_INCIDENT_ID"), IncidentId.required("MOCK_INCIDENT_ID"));

        Assertions.assertNotNull(model);
    }

    @Test
    public void mustNotFindStartedUnitMission() {
        UnitMissionRepository repository = mock(UnitMissionRepository.class);
        when(repository.find(UnitForceConfigId.required("MOCK_INCIDENT_ID"), IncidentId.required("MOCK_INCIDENT_ID"))).thenReturn(null);

        UnitMissionFinder unitMissionFinder = new UnitMissionFinder(repository);
        Assertions.assertThrows(UnitForceConfigNotFoundException.class, () -> unitMissionFinder.find(UnitForceConfigId.required("MOCK_INCIDENT_ID"), IncidentId.required("MOCK_INCIDENT_ID")));

    }

}