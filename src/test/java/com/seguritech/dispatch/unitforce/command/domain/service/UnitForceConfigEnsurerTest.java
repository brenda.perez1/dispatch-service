package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitForceConfigNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UnitForceConfigEnsurerTest {

    @Test
    public void shouldFindTheUnitForcePreconfigured(){
        UnitForceConfigRepository repository = mock(UnitForceConfigRepository.class);
        when(repository.ensurer(UnitForceConfigId.required("MOCK_ID"))).thenReturn(true);

        UnitForceConfigEnsurer ensurer = new UnitForceConfigEnsurer(repository);
        Assertions.assertDoesNotThrow(()-> ensurer.ensurer(UnitForceConfigId.required("MOCK_ID")));
    }

    @Test
    public void shouldNotFindTheUnitForcePreconfigured(){
        UnitForceConfigRepository repository = mock(UnitForceConfigRepository.class);
        when(repository.ensurer(UnitForceConfigId.required("MOCK_ID"))).thenReturn(false);

        UnitForceConfigEnsurer ensurer = new UnitForceConfigEnsurer(repository);
        Assertions.assertThrows(UnitForceConfigNotFoundException.class,()-> ensurer.ensurer(UnitForceConfigId.required("MOCK_ID")));
    }

}