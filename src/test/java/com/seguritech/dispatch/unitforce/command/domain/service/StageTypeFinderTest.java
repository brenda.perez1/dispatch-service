package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageTypeNotSupportException;
import com.seguritech.dispatch.unitforce.command.domain.model.StageType;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StageTypeFinderTest {

    @Test
    public void mustFindStageType(){
        CatStageRepository repository = mock(CatStageRepository.class);
        when(repository.findByStageId(CatStageId.required(1L))).thenReturn(1L);

        StageTypeFinder stageTypeFinder = new StageTypeFinder(repository);
        StageType stageType = stageTypeFinder.find(CatStageId.required(1L));

        Assertions.assertEquals(StageType.INITIAL, stageType);
    }
    @Test
    public void mustNotFindStageType(){
        CatStageRepository repository = mock(CatStageRepository.class);
        when(repository.findByStageId(CatStageId.required(1L))).thenReturn(null);

        StageTypeFinder stageTypeFinder = new StageTypeFinder(repository);

        Assertions.assertThrows(StageTypeNotSupportException.class,()->stageTypeFinder.find(CatStageId.required(1L)));
    }

}