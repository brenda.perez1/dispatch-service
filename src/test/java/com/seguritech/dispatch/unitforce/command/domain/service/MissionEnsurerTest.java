package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.MissionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatMissionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MissionEnsurerTest {

    @Test
    public void shouldFindMission() {
        CatMissionRepository repository = mock(CatMissionRepository.class);
        when(repository.exist(MissionId.required(1L))).thenReturn(true);

        MissionEnsurer ensurer = new MissionEnsurer(repository);
        Assertions.assertDoesNotThrow(() -> ensurer.ensurer(MissionId.required(1L)));
    }

    @Test
    public void shouldNotFindMission() {
        CatMissionRepository repository = mock(CatMissionRepository.class);
        when(repository.exist(MissionId.required(1L))).thenReturn(false);

        MissionEnsurer ensurer = new MissionEnsurer(repository);
        Assertions.assertThrows(MissionNotFoundException.class, () -> ensurer.ensurer(MissionId.required(1L)));
    }

}