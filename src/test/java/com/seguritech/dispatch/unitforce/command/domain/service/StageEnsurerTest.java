package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StagePreconfiguredNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StageEnsurerTest {

    @Test
    public void shouldFindTheStagePreconfigured() {
        CatStageRepository repository = mock(CatStageRepository.class);
        when(repository.exist(CatStageId.required(1L))).thenReturn(true);
        StageEnsurer ensurer = new StageEnsurer(repository);

        Assertions.assertDoesNotThrow(() -> ensurer.ensurer(CatStageId.required(1L)));
    }

    @Test
    public void shouldNotFindTheStagePreconfigured() {
        CatStageRepository repository = mock(CatStageRepository.class);
        when(repository.exist(CatStageId.required(1L))).thenReturn(false);
        StageEnsurer ensurer = new StageEnsurer(repository);

        Assertions.assertThrows(StagePreconfiguredNotFoundException.class, () -> ensurer.ensurer(CatStageId.required(1L)));
    }

}