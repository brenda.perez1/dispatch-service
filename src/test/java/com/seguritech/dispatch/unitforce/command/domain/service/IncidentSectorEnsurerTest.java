package com.seguritech.dispatch.unitforce.command.domain.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentSectorNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.repository.DispatchIncidentSectorRepository;

public class IncidentSectorEnsurerTest {

    @Test
    public void itShouldFindSectorOfIncident() {
        DispatchIncidentSectorRepository incidentSectorRepository = mock(DispatchIncidentSectorRepository.class);
        when(incidentSectorRepository.exist(IncidentId.required("MOCK_INCIDENT"), SectorId.required(1L))).thenReturn(true);

        IncidentSectorEnsurer incidentSectorEnsurer = new IncidentSectorEnsurer(incidentSectorRepository);
        Assertions.assertDoesNotThrow(() -> incidentSectorEnsurer.ensurer(IncidentId.required("MOCK_INCIDENT"), SectorId.required(1L)));
    }

    @Test
    public void itShouldNotFindSectorOfIncident() {
        DispatchIncidentSectorRepository incidentSectorRepository = mock(DispatchIncidentSectorRepository.class);
        when(incidentSectorRepository.exist(IncidentId.required("MOCK_INCIDENT"), SectorId.required(1L))).thenReturn(false);

        IncidentSectorEnsurer incidentSectorEnsurer = new IncidentSectorEnsurer(incidentSectorRepository);
        Assertions.assertThrows(IncidentSectorNotFoundException.class, () -> incidentSectorEnsurer.ensurer(IncidentId.required("MOCK_INCIDENT"), SectorId.required(1L)));
    }

}