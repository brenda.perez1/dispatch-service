package com.seguritech.dispatch.unitforce.command.domain.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.seguritech.dispatch.unitforce.command.domain.exception.MissionIdNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;

public class CorporationMissionEnsurerTest {

    @Test
    public void theMissionShouldExist() {
        CorporationMissionRepository repository = mock(CorporationMissionRepository.class);
        when(repository.exist(MissionId.required(1L), CorporationId.required(1L))).thenReturn(true);
        CorporationMissionEnsurer ensurer = new CorporationMissionEnsurer(repository);
        ensurer.ensurer(MissionId.required(1L), CorporationId.required(1L));
    }

    @Test
    public void theMissionShouldNotExist() {
        CorporationMissionRepository repository = mock(CorporationMissionRepository.class);
        when(repository.exist(MissionId.required(1L), CorporationId.required(1L))).thenReturn(false);
        CorporationMissionEnsurer ensurer = new CorporationMissionEnsurer(repository);
        Assertions.assertThrows(MissionIdNotFoundException.class, () -> ensurer.ensurer(MissionId.required(1L), CorporationId.required(1L)));
    }

}