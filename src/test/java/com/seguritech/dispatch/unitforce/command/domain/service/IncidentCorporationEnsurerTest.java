package com.seguritech.dispatch.unitforce.command.domain.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentCorporationException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationOfIncidentRepository;

public class IncidentCorporationEnsurerTest {

    @Test
    public void itShouldFindTheCorporationOfTheIncident() {
        CorporationOfIncidentRepository repository = mock(CorporationOfIncidentRepository.class);
        when(repository.exist(IncidentId.required("MOCK_INCIDENT_ID"), CorporationOperativeId.required(1L))).thenReturn(true);

        IncidentCorporationEnsurer incidentCorporationEnsurer = new IncidentCorporationEnsurer(repository);
        Assertions.assertDoesNotThrow(() -> incidentCorporationEnsurer.ensurer(IncidentId.required("MOCK_INCIDENT_ID"), CorporationOperativeId.required(1L)));
    }

    @Test
    public void itShouldNotFindTheCorporationOfTheIncident() {
        CorporationOfIncidentRepository repository = mock(CorporationOfIncidentRepository.class);
        when(repository.exist(IncidentId.required("MOCK_INCIDENT_ID"), CorporationOperativeId.required(1L))).thenReturn(false);

        IncidentCorporationEnsurer incidentCorporationEnsurer = new IncidentCorporationEnsurer(repository);
        Assertions.assertThrows(IncidentCorporationException.class, () -> incidentCorporationEnsurer.ensurer(IncidentId.required("MOCK_INCIDENT_ID"), CorporationOperativeId.required(1L)));
    }


}