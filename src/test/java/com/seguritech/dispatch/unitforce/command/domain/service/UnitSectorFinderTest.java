package com.seguritech.dispatch.unitforce.command.domain.service;


import com.seguritech.dispatch.unitforce.command.domain.exception.UnitSectorNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitSectorRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UnitSectorFinderTest {

    @Test
    public void shouldFindTheDriveSector() {
        UnitSectorRepository repository = mock(UnitSectorRepository.class);
        when(repository.find(UnitForceConfigId.required("MOCK_UNIT_FORCE"))).thenReturn(SectorId.required(1L));

        UnitSectorFinder finder = new UnitSectorFinder(repository);
        Assertions.assertEquals(finder.find(UnitForceConfigId.required("MOCK_UNIT_FORCE")), SectorId.required(1L));
    }

    @Test
    public void shouldNotFindTheDriveSector() {
        UnitSectorRepository repository = mock(UnitSectorRepository.class);
        when(repository.find(UnitForceConfigId.required("MOCK_UNIT_FORCE"))).thenReturn(null);

        UnitSectorFinder finder = new UnitSectorFinder(repository);

        Assertions.assertThrows(UnitSectorNotFoundException.class, () -> finder.find(UnitForceConfigId.required("MOCK_UNIT_FORCE")));
    }

}