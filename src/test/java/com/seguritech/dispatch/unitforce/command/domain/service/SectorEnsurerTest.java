package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.SectorNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CatSectorRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SectorEnsurerTest {

    @Test
    public void shouldFindSector() {
        CatSectorRepository catSectorRepository = mock(CatSectorRepository.class);
        when(catSectorRepository.exist(SectorId.required(1L))).thenReturn(true);

        SectorEnsurer sectorEnsurer = new SectorEnsurer(catSectorRepository);
        Assertions.assertDoesNotThrow(() -> sectorEnsurer.ensurer(SectorId.required(1L)));
    }

    @Test
    public void shouldNotFindSector() {
        CatSectorRepository catSectorRepository = mock(CatSectorRepository.class);
        when(catSectorRepository.exist(SectorId.required(1L))).thenReturn(false);

        SectorEnsurer sectorEnsurer = new SectorEnsurer(catSectorRepository);
        Assertions.assertThrows(SectorNotFoundException.class, () -> sectorEnsurer.ensurer(SectorId.required(1L)));
    }
}