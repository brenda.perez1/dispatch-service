package com.seguritech.dispatch.unitforce.command.domain.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageNotCofiguredException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;

public class StagePreconfiguredFinderTest {

    @Test
    public void shouldFindStagePreconfigured() {
        StageId stageIdMock = StageId.required(
                1L, "", 1L, 1L
        );

        CorporationMissionRepository repository = mock(CorporationMissionRepository.class);
        when(repository.findStageId(CorporationId.required(1L), MissionId.required(1L))).thenReturn(
                stageIdMock
        );
        StagePreconfiguredFinder finder = new StagePreconfiguredFinder(repository);
        StageId stageId = finder.find(CorporationId.required(1L), MissionId.required(1L));

        Assertions.assertEquals(stageId, stageIdMock);
    }

    @Test
    public void shouldNotFindStagePreconfigured() {
        CorporationMissionRepository repository = mock(CorporationMissionRepository.class);
        when(repository.findStageId(CorporationId.required(1L), MissionId.required(1L))).thenReturn(
                null
        );

        StagePreconfiguredFinder finder = new StagePreconfiguredFinder(repository);
        Assertions.assertThrows(StageNotCofiguredException.class, () -> finder.find(CorporationId.required(1L), MissionId.required(1L)));
    }
}