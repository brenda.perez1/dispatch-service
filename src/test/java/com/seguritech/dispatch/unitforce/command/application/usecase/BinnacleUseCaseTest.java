//package com.seguritech.dispatch.unitforce.command.application.usecase;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import java.util.Date;
//
//import org.junit.jupiter.api.Assertions;
//
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
//import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangeLogRequest;
//import com.seguritech.dispatch.unitforce.command.domain.model.BinnacleModel;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
//import com.seguritech.dispatch.unitforce.command.domain.repository.BinnacleRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CatCorporationRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CatMissionRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CatSectorRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.StageTransitionRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
//import com.seguritech.platform.DomainEventPublisherStub;
//
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
//
//public class BinnacleUseCaseTest {
//
//    private UnitMissionRepository unitMissionRepository;
//    private CatStageRepository catStageRepository;
//    private UnitForceConfigRepository unitForceConfigRepository;
//    private StageIncidentRepository stageIncidentRepository;
//    private ReasonCancelRepository reasonCancelRepository;
//    private CatSectorRepository catSectorRepository;
//    private CatCorporationRepository catCorporationRepository;
//    private CatMissionRepository catMissionRepository;
//    private StageTransitionRepository stageTransitionRepository;
//    public BinnacleRepository repository = mock(BinnacleRepository.class);
//    private BinnacleModel model;
//
//    @Given("Existe una Unidad Misión ID {string}")
//    public void existe_una_Unidad_Misión_ID(String string) {
//        UnitMissionRepository repository = mock(UnitMissionRepository.class);
//        when(repository.exist(UnitMissionId.required(string))).thenReturn(true);
//        this.unitMissionRepository = repository;
//    }
//
//    @Given("Existe una etapa Id {long}")
//    public void existe_una_etapa_Id(Long int1) {
//        CatStageRepository repository = mock(CatStageRepository.class);
//        when(repository.exist(CatStageId.required(int1))).thenReturn(true);
//
//        this.catStageRepository = repository;
//    }
//
//    @Given("Existe una unidad Id {string} preconfigurada")
//    public void existe_una_unidad_Id_preconfigurada(String string) {
//        UnitForceConfigRepository repository = mock(UnitForceConfigRepository.class);
//        when(repository.ensurer(UnitForceConfigId.required(string))).thenReturn(true);
//
//        this.unitForceConfigRepository = repository;
//    }
//
//    @Given("Existe un incidente Id {string}")
//    public void existe_un_incidente_Id(String string) {
//        StageIncidentRepository repository = mock(StageIncidentRepository.class);
//        when(repository.exist(IncidentId.required(string))).thenReturn(true);
//
//        this.stageIncidentRepository = repository;
//    }
//
//    @Given("Existe una razon de cancelación ID {long}")
//    public void existe_una_razon_de_cancelación_ID(Long int1) {
//        ReasonCancelRepository repository = mock(ReasonCancelRepository.class);
//        when(repository.exist(ReasonCancelId.required(int1))).thenReturn(true);
//
//        this.reasonCancelRepository = repository;
//    }
//
//    @Given("Existe un sector con ID {long}")
//    public void existe_un_sector_con_ID(Long int1) {
//        CatSectorRepository catSectorRepository = mock(CatSectorRepository.class);
//        when(catSectorRepository.exist(SectorId.required(int1))).thenReturn(true);
//
//        this.catSectorRepository = catSectorRepository;
//    }
//
//    @Given("Existe una corporacion con ID {long}")
//    public void existe_una_corporacion_con_ID(Long int1) {
//        CatCorporationRepository corporationRepository = mock(CatCorporationRepository.class);
//        when(corporationRepository.exist(CorporationOperativeId.required(int1))).thenReturn(true);
//
//        this.catCorporationRepository = corporationRepository;
//    }
//
//    @Given("Existe una mision ID {long}")
//    public void existe_una_mision_ID(Long int1) {
//        CatMissionRepository repository = mock(CatMissionRepository.class);
//        when(repository.exist(MissionId.required(int1))).thenReturn(true);
//
//        this.catMissionRepository = repository;
//    }
//
//    @Given("Existe una etapa transitoria ID {long}")
//    public void existe_una_etapa_transitoria_ID(Long int1) {
//        StageTransitionRepository repository = mock(StageTransitionRepository.class);
//        when(repository.exist(StageTransitionId.required(int1))).thenReturn(true);
//
//        this.stageTransitionRepository = repository;
//    }
//
//    @When("Se cambia la etapa de una misión, se guarda en la bitácora de cambios la Unidad Misión ID {string} , la mision ID {long} , etapa Id {long} preconfigurada , unidad Id {string} preconfigurada , incidente Id {string} , razón de cancelación ID {long}, un sector id {long}, una corporacion id {long} y una etapa transitoria id {long}")
//    public void se_cambia_la_etapa_de_una_misión_se_guarda_en_la_bitácora_de_cambios_la_Unidad_Misión_ID_la_mision_ID_etapa_Id_preconfigurada_unidad_Id_preconfigurada_incidente_Id_razón_de_cancelación_ID_un_sector_id_una_corporacion_id_y_una_etapa_transitoria_id
//            (
//                    String unitMissionId, Long missionId, Long stageId, String unitPreconfiguredId, String incidentId, Long reasonCancelId, Long sectorId, Long corporationId, Long transitionalStage) {
//        doAnswer(invocationOnMock ->
//                this.model = invocationOnMock.getArgument(0, BinnacleModel.class)
//        ).when(this.repository).save(any(BinnacleModel.class));
//
//        BinnacleUseCase useCase = new BinnacleUseCase(
//                this.repository,
//                new DomainEventPublisherStub(),
//                this.unitMissionRepository,
//                this.catStageRepository,
//                this.unitForceConfigRepository,
//                this.stageIncidentRepository,
//                this.reasonCancelRepository,
//                this.catSectorRepository,
//                this.catCorporationRepository,
//                this.catMissionRepository,
//                this.stageTransitionRepository
//        );
//
//        useCase.create(new StageChangeLogRequest(
//                sectorId,
//                corporationId,
//                unitMissionId,
//                missionId,
//                stageId,
//                unitPreconfiguredId,
//                incidentId,
//                "MOCK_USER",
//                "MOCK_USER",
//                new Date(),
//                new Date(),
//                new Date(),
//                new Date(),
//                reasonCancelId,
//                null,
//                null,
//                true,
//                transitionalStage
//        ));
//
//    }
//
//    @Then("La bitácora no deberá estar nula")
//    public void la_bitácora_no_deberá_estar_nula() {
//        Assertions.assertNotNull(this.model);
//    }
//
//}