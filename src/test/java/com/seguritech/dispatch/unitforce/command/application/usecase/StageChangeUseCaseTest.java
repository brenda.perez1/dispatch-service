//package com.seguritech.dispatch.unitforce.command.application.usecase;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import java.util.ArrayList;
//import java.util.Date;
//
//import org.junit.jupiter.api.Assertions;
//
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
//import com.seguritech.dispatch.unitforce.command.application.usecase.request.StageChangerequest;
//import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CatStageId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Observations;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.Persons;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UserId;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CatStageRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.StageRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
//import com.seguritech.platform.DomainEventPublisherStub;
//
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
//
//public class StageChangeUseCaseTest {
//
//    private UnitMissionRepository unitMissionRepository;
//    private ReasonCancelRepository reasonCancelEnsurer;
//    private UnitForceConfigRepository forceConfigRepository;
//    private StageIncidentRepository incidentRepository;
//    private StageRepository nextStageRepository;
//    private CatStageRepository catStageRepository;
//    private UnitMissionModel misionModel;
//    /*private UnitMissionRepository repository = mock(UnitMissionRepository.class);*/
//
//
//    @Given("Existe una Unidad mision ID {string} asignada al incidente {string}")
//    public void existe_una_Unidad_mision_ID_asignada_al_incidente(String unitMissionId, String incidentId) {
//        UnitMissionRepository unitMissionRepository = mock(UnitMissionRepository.class);
//        when(unitMissionRepository.find(UnitForceConfigId.required(unitMissionId), IncidentId.required(incidentId))).thenReturn(
//                new UnitMissionModel(
//                        UnitMissionId.required(unitMissionId),
//                        SectorId.required(1L),
//                        CorporationOperativeId.required(1L),
//                        new ArrayList<StageId>() {{
//                            add(StageId.required(
//                                    2L,
//                                    1L,
//                                    3L
//                            ));
//                        }}
//                        ,
//                        StageId.required(
//                                1L,
//                                1L,
//                                1L
//                        ),
//                        UnitForceConfigId.required("MOCK_UNIT_FORCE_ID"),
//                        IncidentId.required("MOCK_INCIDENT_ID"),
//                        UserId.required("MOCK_USER_ID"),
//                        UserId.required("MOCK_USER_ID"),
//                        new Date(),
//                        new Date(),
//                        new Date(),
//                        new Date(),
//                        ReasonCancelId.required(0L),
//                        Observations.optional("MOCK_TEST"),
//                        Persons.optional(0L),
//                        true
//                )
//        );
//        this.unitMissionRepository = unitMissionRepository;
//    }
//
//    @Given("Existe una razon de cancelación Id {long}")
//    public void existe_una_razon_de_cancelación_Id(Long int1) {
//        ReasonCancelRepository reasonCancelEnsurer = mock(ReasonCancelRepository.class);
//        when(reasonCancelEnsurer.exist(ReasonCancelId.required(int1))).thenReturn(true);
//
//        this.reasonCancelEnsurer = reasonCancelEnsurer;
//    }
//
//    @Given("Existe una unidad de fuerza con Id {string}")
//    public void existe_una_unidad_de_fuerza_con_Id(String string) {
//        UnitForceConfigRepository forceConfigRepository = mock(UnitForceConfigRepository.class);
//        when(forceConfigRepository.ensurer(UnitForceConfigId.required(string))).thenReturn(true);
//
//        this.forceConfigRepository = forceConfigRepository;
//    }
//
//    @Given("Existe un incidente ID {string}")
//    public void existe_un_incidente_ID(String string) {
//        StageIncidentRepository incidentRepository = mock(StageIncidentRepository.class);
//        when(incidentRepository.exist(IncidentId.required(string))).thenReturn(true);
//
//        this.incidentRepository = incidentRepository;
//    }
//
//    @Given("Existe una etapa con un ID compuesto stage Id {long} , una mision Id {long} y una etapa transitoria Id {long} Traslado")
//    public void existe_una_etapa_con_un_ID_compuesto_stage_Id_una_mision_Id_y_una_etapa_transitoria_Id_Traslado(Long stageId, Long missionId, Long transitionId) {
//        StageRepository nextStageRepository = mock(StageRepository.class);
//        when(nextStageRepository.find(MissionId.required(missionId), CatStageId.required(stageId))).thenReturn(
//                StageId.required(stageId, missionId, transitionId)
//        );
//        this.nextStageRepository = nextStageRepository;
//    }
//
//    @Given("Existe una etapa ID {long}")
//    public void existe_una_etapa_ID(Long int1) {
//        CatStageRepository catStageRepository = mock(CatStageRepository.class);
//        when(catStageRepository.exist(CatStageId.required(int1))).thenReturn(true);
//
//        this.catStageRepository = catStageRepository;
//    }
//
//    @When("A la unidad de fuerza Id {string} con el incidente ID {string}, se cambia a la siguiente etapa stage Id {long} , por el usuario Id {string}")
//    public void a_la_unidad_de_fuerza_Id_con_el_incidente_ID_se_cambia_a_la_siguiente_etapa_stage_Id_por_el_usuario_Id(
//            String unitForceId, String incidentId, Long stageId, String userId
//    ) {
//        doAnswer(invocationOnMock ->
//                this.misionModel = invocationOnMock.getArgument(0, UnitMissionModel.class)
//        ).when(this.unitMissionRepository).save(any(UnitMissionModel.class));
//
//        StageChangeUseCase stageChangeUseCase = new StageChangeUseCase(
//                this.unitMissionRepository
//                , new DomainEventPublisherStub()
//                , this.reasonCancelEnsurer
//                , this.forceConfigRepository
//                , this.incidentRepository
//                , this.nextStageRepository
//                , this.catStageRepository
//        );
//
//        stageChangeUseCase.execute(
//                new StageChangerequest(
//                        unitForceId,
//                        incidentId,
//                        stageId,
//                        userId,
//                        null,
//                        null,
//                        null
//                )
//        );
//    }
//
//    @Then("La misión deberá tener asignada la etapa preconfigurada stage Id {long} , una mision Id {long} y una etapa transitoria Id {long} Traslado")
//    public void la_misión_deberá_tener_asignada_la_etapa_preconfigurada_stage_Id_una_mision_Id_y_una_etapa_transitoria_Id_Traslado(Long stageId, Long missionId, Long transitionId) {
//        Assertions.assertEquals(StageId.required(stageId, missionId, transitionId), this.misionModel.getCurrentStageId());
//    }
//}