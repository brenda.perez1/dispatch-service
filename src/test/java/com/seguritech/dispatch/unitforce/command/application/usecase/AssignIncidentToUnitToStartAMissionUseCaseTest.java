//package com.seguritech.dispatch.unitforce.command.application.usecase;
//
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doAnswer;
//import static org.mockito.Mockito.mock;
//import static org.mockito.Mockito.when;
//
//import org.junit.jupiter.api.Assertions;
//
//import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
//import com.seguritech.dispatch.unitforce.command.application.usecase.request.AssignIncidentToUnitMissionRequest;
//import com.seguritech.dispatch.unitforce.command.domain.model.UnitMissionModel;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.MissionId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.SectorId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageId;
//import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationMissionRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.CorporationOfIncidentRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.DispatchIncidentSectorRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitCorporationRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitForceConfigRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
//import com.seguritech.dispatch.unitforce.command.domain.repository.UnitSectorRepository;
//import com.seguritech.platform.DomainEventPublisherStub;
//
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
//
//public class AssignIncidentToUnitToStartAMissionUseCaseTest {
//
//    private UnitForceConfigRepository unitForceConfigRepository;
//    private CorporationMissionRepository corporationMissionRepository;
//    private StageIncidentRepository incidentRepository;
//    private UnitSectorRepository unitSectorRepository;
//    private UnitCorporationRepository unitCorporationRepository;
//    private CorporationOfIncidentRepository corporationOfIncidentRepository;
//    private DispatchIncidentSectorRepository incidentSectorRepository;
//    private UnitMissionModel missionModel;
//    private UnitMissionRepository repository = mock(UnitMissionRepository.class);
//    private AssignIncidentToUnitToStartAMissionUseCase useCase;
//
//    @Given("Existe una unidad de fuerza preconfigurada {string}")
//    public void existe_una_unidad_de_fuerza_preconfigurada(String string) {
//        UnitForceConfigRepository unitForceConfigRepository = mock(UnitForceConfigRepository.class);
//        when(unitForceConfigRepository.ensurer(UnitForceConfigId.required(string))).thenReturn(true);
//        when(unitForceConfigRepository.available(UnitForceConfigId.required(string))).thenReturn(true);
//
//        this.unitForceConfigRepository = unitForceConfigRepository;
//    }
//
//    @Given("Existe una mision Id {long} , asignada a la corporacion id {long}")
//    public void existe_una_mision_Id_asignada_a_la_corporacion_id(Long int1, Long int2) {
//        CorporationMissionRepository repository = mock(CorporationMissionRepository.class);
//        when(repository.exist(MissionId.required(int1), CorporationOperativeId.required(int2))).thenReturn(true);
//
//        this.corporationMissionRepository = repository;
//    }
//
//    @Given("Existe un incidente  Id {string}")
//    public void existe_un_incidente_Id(String string) {
//        StageIncidentRepository incidentRepository = mock(StageIncidentRepository.class);
//        when(incidentRepository.exist(IncidentId.required(string))).thenReturn(true);
//
//        this.incidentRepository = incidentRepository;
//    }
//
//    @Given("Existe el sector {long} Id  de la unidad preconfigurada {string}")
//    public void existe_el_sector_Id_de_la_unidad_preconfigurada(Long int1, String string) {
//        UnitSectorRepository unitSectorRepository = mock(UnitSectorRepository.class);
//        when(unitSectorRepository.find(UnitForceConfigId.required(string))).thenReturn(SectorId.required(int1));
//
//        this.unitSectorRepository = unitSectorRepository;
//    }
//
//    @Given("Existe la corporacion Id {long} de la unidad  preconfigurada {string}")
//    public void existe_la_corporacion_Id_de_la_unidad_preconfigurada(Long int1, String string) {
//        UnitCorporationRepository unitCorporationRepository = mock(UnitCorporationRepository.class);
//        when(unitCorporationRepository.find(UnitForceConfigId.required(string))).thenReturn(CorporationOperativeId.required(int1));
//
//        this.unitCorporationRepository = unitCorporationRepository;
//    }
//
//    @Given("Existe un incidente Id {string} asignado a la corporacion Id {long}")
//    public void existe_un_incidente_Id_asignado_a_la_corporacion_Id(String string, Long int1) {
//        CorporationOfIncidentRepository corporationOfIncidentRepository = mock(CorporationOfIncidentRepository.class);
//        when(corporationOfIncidentRepository.exist(IncidentId.required(string), CorporationOperativeId.required(int1))).thenReturn(true);
//
//        this.corporationOfIncidentRepository = corporationOfIncidentRepository;
//    }
//
//    @Given("Existe un incidente Id {string} asignado al sector Id {long}")
//    public void existe_un_incidente_Id_asignado_al_sector_Id(String string, Long int1) {
//        DispatchIncidentSectorRepository incidentSectorRepository = mock(DispatchIncidentSectorRepository.class);
//        when(incidentSectorRepository.exist(IncidentId.required(string), SectorId.required(int1))).thenReturn(true);
//
//        this.incidentSectorRepository = incidentSectorRepository;
//    }
//
//    @Given("Existe una mision Id {long} preconfigurada a la corporacion id {long}")
//    public void existe_una_mision_Id_preconfigurada_a_la_corporacion_id(Long int1, Long int2) {
//        when(this.corporationMissionRepository.find(CorporationOperativeId.required(int2))).thenReturn(MissionId.required(int1));
//    }
//
//    @Given("Existe una etapa inicial preconfigurada con un id compuesto por etapa id {long} ASIGNADA, mision id {long} ATENCION ENFERMOS, tipo etapa id {long} INICIAL , configurada a la mision id {long} de la corporacion id {long}")
//    public void existe_una_etapa_inicial_preconfigurada_con_un_id_compuesto_por_etapa_id_ASIGNADA_mision_id_ATENCION_ENFERMOS_tipo_etapa_id_INICIAL_configurada_a_la_mision_id_de_la_corporacion_id(Long stageId, Long missionIdPk, Long typeStageId, Long missionId, Long corporationId) {
//        when(this.corporationMissionRepository.findStageId(CorporationOperativeId.required(corporationId), MissionId.required(missionId)))
//                .thenReturn(StageId.required(
//                        stageId,
//                        missionIdPk,
//                        typeStageId
//                ));
//    }
//
//    @When("A la unidad de fuerza Id {string} se le asigna el incidente Id {string} , una mision Id {long} por el usuario {string}")
//    public void a_la_unidad_de_fuerza_Id_se_le_asigna_el_incidente_Id_una_mision_Id_por_el_usuario(String string, String string2, Long int1, String string3) {
//        doAnswer(invocationOnMock ->
//                this.missionModel = invocationOnMock.getArgument(0, UnitMissionModel.class))
//                .when(repository).save(any(UnitMissionModel.class));
//
//        this.useCase = new AssignIncidentToUnitToStartAMissionUseCase(
//                this.unitForceConfigRepository,
//                this.corporationMissionRepository,
//                this.incidentRepository,
//                this.unitSectorRepository,
//                this.unitCorporationRepository,
//                this.corporationOfIncidentRepository,
//                this.incidentSectorRepository,
//                this.repository,
//                new DomainEventPublisherStub());
//
//        this.useCase.execute(new AssignIncidentToUnitMissionRequest(string, int1, string2, string3));
//    }
//
//    @Then("La unidad misión no debera ser nula")
//    public void la_unidad_misión_no_debera_ser_nula() {
//        Assertions.assertNotNull(this.missionModel);
//    }
//
//}