package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitCorporationNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.CorporationOperativeId;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitForceConfigId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitCorporationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UnitCorporationFinderTest {

    @Test
    public void shouldFindTheUnitaryCorporation() {
        UnitCorporationRepository repository = mock(UnitCorporationRepository.class);
        when(repository.findCorporationOperativeId(UnitForceConfigId.required("MOCK_UNIT_FORCE"))).thenReturn(CorporationOperativeId.required(1L));

        UnitCorporationFinder unitCorporationFinder = new UnitCorporationFinder(repository);
        Assertions.assertEquals(unitCorporationFinder.findCorporationOperativeId(UnitForceConfigId.required("MOCK_UNIT_FORCE")), CorporationOperativeId.required(1L));
    }

    @Test
    public void shouldNotFindTheUnitaryCorporation() {
        UnitCorporationRepository repository = mock(UnitCorporationRepository.class);
        when(repository.findCorporationOperativeId(UnitForceConfigId.required("MOCK_UNIT_FORCE"))).thenReturn(null);

        UnitCorporationFinder unitCorporationFinder = new UnitCorporationFinder(repository);
        Assertions.assertThrows(UnitCorporationNotFoundException.class, () -> unitCorporationFinder.findCorporationOperativeId(UnitForceConfigId.required("MOCK_UNIT_FORCE")));
    }

}