package com.seguritech.dispatch.unitforce.command.domain.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.unitforce.command.domain.exception.IncidentNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageIncidentRepository;

class IncidentEnsurerTest {


    @Test
    public void theIncidentShouldNotExist() {
        StageIncidentRepository repository = mock(StageIncidentRepository.class);
        when(repository.exist(IncidentId.required("MOCK_ID"))).thenReturn(true);
        IncidentEnsurer ensurer = new IncidentEnsurer(repository);
        ensurer.ensurer(IncidentId.required("MOCK_ID"));
    }

    @Test
    public void theIncidentShouldExist() {
        StageIncidentRepository repository = mock(StageIncidentRepository.class);
        when(repository.exist(IncidentId.required("MOCK_ID"))).thenReturn(false);
        IncidentEnsurer ensurer = new IncidentEnsurer(repository);
        Assertions.assertThrows(IncidentNotFoundException.class, () -> ensurer.ensurer(IncidentId.required("MOCK_ID")));

    }

}