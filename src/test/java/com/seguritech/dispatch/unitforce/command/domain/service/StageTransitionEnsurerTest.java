package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.StageTrasitionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.StageTransitionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.StageTransitionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StageTransitionEnsurerTest {

    @Test
    public void shouldFindStageTransition() {
        StageTransitionRepository repository = mock(StageTransitionRepository.class);
        when(repository.exist(StageTransitionId.required(1L))).thenReturn(true);

        StageTransitionEnsurer ensurer = new StageTransitionEnsurer(repository);
        Assertions.assertDoesNotThrow(() -> ensurer.ensurer(StageTransitionId.required(1L)));
    }

    @Test
    public void shouldNotFindStageTransition() {
        StageTransitionRepository repository = mock(StageTransitionRepository.class);
        when(repository.exist(StageTransitionId.required(1L))).thenReturn(false);

        StageTransitionEnsurer ensurer = new StageTransitionEnsurer(repository);
        Assertions.assertThrows(StageTrasitionNotFoundException.class, () -> ensurer.ensurer(StageTransitionId.required(1L)));
    }
}