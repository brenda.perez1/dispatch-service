package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.ReasonCancelNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.ReasonCancelId;
import com.seguritech.dispatch.unitforce.command.domain.repository.ReasonCancelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ReasonCancelEnsurerTest {

    @Test
    public void mustFindReasonCancelMission() {
        ReasonCancelRepository repository = mock(ReasonCancelRepository.class);
        when(repository.exist(ReasonCancelId.required(1L))).thenReturn(true);
        ReasonCancelEnsurer reasonCancelEnsurer = new ReasonCancelEnsurer(repository);

        Assertions.assertDoesNotThrow(() -> reasonCancelEnsurer.ensurer(ReasonCancelId.required(1L)));
    }

    @Test
    public void mustNotFindReasonCancelMission() {
        ReasonCancelRepository repository = mock(ReasonCancelRepository.class);
        when(repository.exist(ReasonCancelId.required(1L))).thenReturn(false);
        ReasonCancelEnsurer reasonCancelEnsurer = new ReasonCancelEnsurer(repository);

        Assertions.assertThrows(ReasonCancelNotFoundException.class, () -> reasonCancelEnsurer.ensurer(ReasonCancelId.required(1L)));
    }

}