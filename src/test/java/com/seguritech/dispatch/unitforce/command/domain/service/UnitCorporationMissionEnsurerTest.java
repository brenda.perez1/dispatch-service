package com.seguritech.dispatch.unitforce.command.domain.service;

import com.seguritech.dispatch.unitforce.command.domain.exception.UnitMissionNotFoundException;
import com.seguritech.dispatch.unitforce.command.domain.model.attribute.UnitMissionId;
import com.seguritech.dispatch.unitforce.command.domain.repository.UnitMissionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UnitCorporationMissionEnsurerTest {

    @Test
    public void shouldFindTheUnitMission() {
        UnitMissionRepository repository = mock(UnitMissionRepository.class);
        when(repository.exist(UnitMissionId.required("MOCK_UNIT_MISSION"))).thenReturn(true);

        UnitMissionEnsurer ensurer = new UnitMissionEnsurer(repository);
        Assertions.assertDoesNotThrow(() -> ensurer.ensurer(UnitMissionId.required("MOCK_UNIT_MISSION")));
    }

    @Test
    public void shouldNotFindTheUnitMission() {
        UnitMissionRepository repository = mock(UnitMissionRepository.class);
        when(repository.exist(UnitMissionId.required("MOCK_UNIT_MISSION"))).thenReturn(false);

        UnitMissionEnsurer ensurer = new UnitMissionEnsurer(repository);
        Assertions.assertThrows(UnitMissionNotFoundException.class, () -> ensurer.ensurer(UnitMissionId.required("MOCK_UNIT_MISSION")));
    }

}