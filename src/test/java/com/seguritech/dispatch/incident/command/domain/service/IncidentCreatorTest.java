package com.seguritech.dispatch.incident.command.domain.service;

import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.IncidentCreatorResponse;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.*;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Latitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Longitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.StateId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.TownId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.IncidentCreator;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentCreatorRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentLocationSearch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IncidentCreatorTest {

    @Test
    public void shouldCreateAnIncident() {

        IncidentCreatorRequest request = new IncidentCreatorRequest(
                SourceId.required(1L),
                PhoneNumber.optional("1234567890"),
                PhoneNumberCountryId.optional(1L),
                new Date(),
                IncidentTypeId.optional(1L),
                new IncidentLocationSearch(
                        StateId.optional(1L),
                        TownId.optional(1L),
                        Latitude.optional(12345.0),
                        Longitude.optional(-12345.0)
                ),
                HostIp.required("127.0.0.1"),
                Description.optional(""),
                UserId.required("MOCK_USER"),
                true
        );

        IncidentBoundedRepository incidentRepository = mock(IncidentBoundedRepository.class);
        when(incidentRepository.create(
                request
        )).thenReturn(new IncidentCreatorResponse(
                IncidentId.required("MOCK_ID"),
                Invoice.required("12345")));

        IncidentCreator incidentCreator = new IncidentCreator(incidentRepository);
        IncidentCreatorResponse response = incidentCreator.generate(request);
        Assertions.assertEquals(response.getIncidentId(), IncidentId.required("MOCK_ID"));
    }

}