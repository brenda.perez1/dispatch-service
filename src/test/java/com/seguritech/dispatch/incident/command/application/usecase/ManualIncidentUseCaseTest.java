package com.seguritech.dispatch.incident.command.application.usecase;
//TODO: Descomentar una vez se refactoricen las pruebas unitarias
/*
import com.seguritech.corporation.incident.command.domain.repository.IncidentCorporationRepository;
import com.seguritech.dispatch.manual.incidents.command.domain.model.incident.IncidentModel;
import com.seguritech.dispatch.operativegroup.incident.command.application.usecase.response.IncidentCreatorResponse;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.UserId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.boundary.incident.IncidentId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.*;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Latitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.Longitude;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.StateId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.model.creator.location.TownId;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentBoundedRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.repository.IncidentOperatingCorporationRepository;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentCreatorRequest;
import com.seguritech.dispatch.operativegroup.incident.command.domain.service.request.IncidentLocationSearch;
import com.seguritech.platform.DomainEventPublisherStub;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.junit.jupiter.api.Assertions;

import static org.mockito.Mockito.*;

public class ManualIncidentUseCaseTest {

    private IncidentCreatorRequest request = new IncidentCreatorRequest(
            SourceId.required(1L),
            PhoneNumber.optional("1234567890"),
            PhoneNumberCountryId.optional(1L),
            null,
            IncidentTypeId.optional(1L),
            new IncidentLocationSearch(
                    StateId.optional(1L),
                    TownId.optional(1L),
                    Latitude.optional(20312.0),
                    Longitude.optional(-23120.0)
            ),
            HostIp.required("127.0.0.1"),
            Description.optional(""),
            UserId.required("MOCK_USER_ID"),
            true
    );
    private IncidentBoundedRepository incidentRepository;
    private IncidentModel incidentModel;
    private IncidentCorporationRepository repository = mock(IncidentCorporationRepository.class);
    private IncidentOperatingCorporationRepository  corporationRepository;

    @Dado("Se genera un incidente Id {string} en el contexto del operador telefónico, con un source Id {long}, un host ip {string} por el usuario Id {string}")
    public void se_genera_un_incidente_Id_en_el_contexto_del_operador_telefónico_con_un_source_Id_un_host_ip_por_el_usuario_Id(String incidentId, Long sourceId, String hostIp, String userId) {
        IncidentBoundedRepository incidentRepository = mock(IncidentBoundedRepository.class);
        when(incidentRepository.create(
                this.request
        )).thenReturn(new IncidentCreatorResponse(
                IncidentId.required(incidentId),
                Invoice.required("12345")
        ));
        this.incidentRepository = incidentRepository;
    }

    @Dado("Existe una corporacion  Id {long}")
    public void existe_una_corporacion_Id(Long int1) {

    }

    @Dado("Debe de existir una corporaciion Id {long}")
    public void debe_de_existir_una_corporaciion_Id(Long int1) {
        IncidentOperatingCorporationRepository repository = mock(IncidentOperatingCorporationRepository.class);
        when(repository.exist(CorporationId.required(int1))).thenReturn(true);
        this.corporationRepository = repository;
    }


    @Cuando("Se crea el incidente id {string} en la corporacion Id {long}, con un source Id {long}, un host ip {string} por el usuario Id {string}")
    public void se_crea_el_incidente_id_en_la_corporacion_Id_con_un_source_Id_un_host_ip_por_el_usuario_Id(String incidentId, Long corporationId, Long sourceId, String hostIp, String userId) {
        doAnswer(invocationOnMock ->
                this.incidentModel = invocationOnMock.getArgument(0, IncidentModel.class)
        ).when(this.repository).save(any());

        ManualIncidentUseCase useCase = new ManualIncidentUseCase(
                this.incidentRepository,
                this.repository,
                new DomainEventPublisherStub(),
                new IncidentPreconfigured(
                        "1234567890",
                        1L,
                        1L,
                        1L,
                        1L,
                        20312.0,
                        -23120.0
                ), this.corporationRepository);
        useCase.execute(
                new CreateManualIncidentRequest(
                        this.request.getSourceId().getValue(),
                        this.request.getPhoneNumber().getValue(),
                        this.request.getPhoneNumberCountryId().getValue(),
                        this.request.getDescription().getValue(),
                        this.request.getUserId().getValue(),
                        null,
                        this.request.getCreatedFrom().getValue(),
                        corporationId
                )
        );

    }

    @Entonces("El incidente id {string} debera estar asignado a la corporacion id {long}")
    public void el_incidente_id_debera_estar_asignado_a_la_corporacion_id(String string, Long int1) {
        Assertions.assertEquals(this.incidentModel.getStatus(), IncidentCorporationStatus.required(IncidentCorporationStatusEnum.OPEN.toString()));
    }


}*/
