# Proyect Dispatch

The SDCW system is an application that allows emergency response centers to manage each of the stages of incident response and, in turn, coordinate the operation, reaction and response among the different institutions, organizations, authorities and jurisdictions involved in emergency, incident and disaster response.

SDCW has the ability to manage emergency response through the Web system where, in addition to acting as system administrator, users, catalogs and system configurations can be managed, thus achieving a better management of the system, among other integrations.
 
The purpose of the dispatch microservice, is:
- Assign corporations
- Assign and update the association between emergency and force units and stages of a mission.
- Close, reopen and cancel emergencies with or without corporations.


## Prerequisites

For the development of a project in Spring Boot, the following requirements must be met:

- Java 8
- Maven 3.6.X
- Spring Boot 2.6.6
- Docker Desktop (X.X.X) y docker-engine (X.X)
- Docker-compose (1.X.X)
- Git
- Spring Tools Suite o Intellij


## Frameworks and Programming Languages

- Spring Boot 2.x.x
- Hibernate
- MapStruct


## Libraries

- Java 8
- Lombok


## Download <dispatch-service>

Download the project from the git repo, which you will find in the following link: https://docs.google.com/document/d/1IhGOp5ZVivf5nuJx8096N9fcppgSfQ0vPJPz7TaekKA/edit


## Installing and compile <dispatch-service>

1. Compile the ms dispatch-service and this: mvn clean install


## Profiles an arguments

The microservice has the following profiles:

1. Develop: dev
2. Tests: test-cad-lite
3. Production:  staging-cad-lite-v2

The dev profile is not prepared to work in the Spring security environment, so it will not try to connect to Eureka.


## Working with RabbitMQ in the QA environment

1. Raise the microservice with qa profile
2. Enter next link: http://34.148.17.207:15672/#/users
3. Access to either of the two leagues are:
            • User: seguritech
            • Password: s3gur1t3ch
			
Once you have accessed, you will be able to view the sending and receiving of messages between the microservices.


## Install and compile

The microservice contains different profiles. If you want to work in the qa environment, you can inject the value "qa" into the maven profiles property:

java -Dspring.profiles.active=qa -jar application.jar / -Dspring.profiles.active=qa


## Deployment (Start the application service)

To deploy the <dispatch-service> application service, follow these steps:

Windows, Linux and macOS: 

1. In STS, right click on the microservice, select "Run As" in the menu and then the "Maven Build..." option.
2. Once the microservice is compiled, we click on "Run As..." again and then select "Spring Boot App"


## Arguments

N/A


## Additional configurations

The ms mission-data y classification-data "clean install" must be compiled, since it is a dependency with the ms the dispatch.


## Using <dispatch-service>

git clone https://git.seguritech.org:92/cad/cad-lite/micro-services/dispatch-service.git
git branch test-cad-lite


## Contributing to <dispatch-service>

To contribute to the SDCW project, just follow the steps below:

1. Create a branch: git checkout -b <branch_name>.
2. Make your changes and commit: git commit -m <commit_message>.
3. Create the Pull Request for the changes: git push origin <project_name>/<location>.
4. Switch to develop branch and pull changes: 'git checkout test-cad-lite', 'git pull'
5. Go back to the created branch and rebase against test-cad-lite: 'git rebase test-cad-lite'
6. Resolve conflicts if they exist
7. Push: 'git push'. In case you have trouble pushing the change, run 'git push --force'; can sometimes throw an index missmatch error, but doing
   the rebase, we make sure to carry all the changes from the 'test-cad-lite' trunk branch.
8. Create the merge request so that it is authorized by the technical leader or a member of the team.


## Contributors

Thanks to the following people who have contributed to this project:

- **Brenda Lizeth Pérez González** &lt;brenda.perez@seguritech.com&gt;
- **Emilio Cesar Saldivar Acevedo** &lt;emilio.saldivar@seguritech.com&gt;


## Contact

- **Brenda Lizeth Pérez González** &lt;brenda.perez@seguritech.com&gt;
- **Emilio Cesar Saldivar Acevedo** &lt;emilio.saldivar@seguritech.com&gt;


## License

This project is licensed under the (SEGURITECH) License - see the [LICENSE.md](LICENSE.md) file for details.
